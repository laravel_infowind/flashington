@extends('layouts.admin')

@section('title',tr('email_templates'))

@section('content-header',tr('email_templates'))

@section('breadcrumb')

	<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
	<li class="active">{{tr('email_templates')}}</li>

@endsection

@section('content')
	@include('notification.notify')
	<div class="row redeems-sec">
		<div class="col-lg-12">
			<div class="box box-info">
				<div class="box-header label-primary">
					<b style="font-size: 18px;">{{tr('email_templates')}}</b>
				</div>
				<div class="box-body"> 
					<table id = "example1" class="table table-bordered table-striped">
						<thead>
							<tr>								
								<th>{{tr('template')}}</th>
								<th>{{tr('subject')}}</th>								
							</tr>
						</thead>
						<tbody>
							@foreach($templates as $i=>$value)
							<tr>								
								<td>{{getTemplateName($value->template_type)}}
									<ul class="table-row-actions">										 
										<li role="presentation">
											<a class = "menuitem"  tabindex= "-1" href="{{route('admin.edit.template',['id'=>$value->id])}}">{{tr('edit')}}</a>
										</li>

										<li role="presentation">
											<a class="menuitem" tabindex="-1" href="{{route('admin.view.template', ['id'=>$value->id])}}">{{tr('view')}}</a>
										</li>
									</ul>
								</td>
								<td>{{$value->subject}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>


@endsection

