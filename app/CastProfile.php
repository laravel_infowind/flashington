<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CastProfile extends Model
{
    public $table = "cast_profiles";

    protected $fillable = ['id', 'cast_id', 'cast_crew_id', 'name', 'biography', 'birthday', 'deathday', 'gender','known_for_department', 'imdb_id', 'place_of_birth', 'profile_image', 'also_known_as', 'popularity', 'homepage', 'cast_credits', 'crew_credits'];

    public function cast_crew()
    {
        return $this->belongsTo('App\CastCrew' , 'cast_crew_id' ,'id');
    }

}
