@extends('layouts.admin')

@section('title', tr('spam_videos'))

@section('content-header', tr('spam_videos'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-flag"></i> {{tr('spam_videos')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
        <div class="col-lg-12">
         	
         	<div class="box">
            
	            <div class="box-body">

	            	@if(count($model) > 0)

		              	<table id="datatable-withoutpagination" class="table table-bordered table-striped">

							<thead>
							    <tr>
							     
								  <th>{{tr('title')}}</th>
							      <th>{{tr('category')}}</th>
							      <th>{{tr('sub_category')}}</th>
							      {{-- <th>{{tr('genre')}}</th> --}}
							      <th>{{tr('user_count')}}</th>
							      <th>{{tr('status')}}</th>
							    
							    </tr>
							</thead>

							<tbody>
								
								@foreach($model as $i => $video)
								    <tr>								      	
										<td><a href="{{ url('admin/view/video?id='.$video->adminVideo->id) }}">{{$video->adminVideo->title}}</a>
								      		<ul class="table-row-actions">
						                  	{{-- @if($video->adminVideo->is_approved)
						                		<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.video.decline',$video->video_id)}}">{{tr('decline')}}</a></li>
						                	@else
						                  		<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.video.approve',$video->video_id)}}">{{tr('approve')}}</a></li>
						                  	@endif --}}

						                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.spam-videos.user-reports' , $video->video_id)}}">{{tr('user_reports')}}</a></li>
							                </ul>	
								      	</td>
								      	<td>{{$video->adminVideo->category->name}}</td>
								      	<td>{{$video->adminVideo->subCategory->name}}</td>
								      	{{-- <td>{{($video->adminVideo->genreName) ? $video->adminVideo->genreName->name : '-'}}</td> --}}
								      	<td>{{count($video->adminVideo->userFlags)}}</td>
								      	<td>
								      		@if($video->adminVideo->is_approved)
								      			<span class="label label-success">{{tr('approved')}}</span>
								       		@else
								       			<span class="label label-warning">{{tr('pending')}}</span>
								       		@endif
								      	</td>
								      
								    </tr>
								@endforeach
							</tbody>
						
						</table>

						<div align="right" id="paglink"><?php echo $model->links(); ?></div>

					@else
						<h3 class="no-result">{{tr('no_result_found')}}</h3>
					@endif
	            
	            </div>

          	</div>
          	
        </div>
    </div>

@endsection
