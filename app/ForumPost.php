<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class ForumPost extends Model
{
    public $table = "forum_posts";

	public function topic()
    {
        return $this->belongsTo('App\ForumTopic', 'forum_topic_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function parent()
    {
        return $this->belongsTo(ForumPost::class, 'forum_post_id');
    }
    
    public function childs()
    {
        return $this->hasMany(ForumPost::class, 'forum_post_id');
    }
}
