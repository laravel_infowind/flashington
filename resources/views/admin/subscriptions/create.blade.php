@extends('layouts.admin')
@section('title', tr('add_subscription'))
@section('content-header', tr('add_subscription'))
@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.subscriptions.index')}}"><i class="fa fa-key"></i> {{tr('subscriptions')}}</a></li>
    <li class="active">{{tr('add_subscription')}}</li> -->
@endsection
@section('content')
@include('notification.notify')

<div class="main-content">

    <form class="form-horizontal" action="{{route('admin.subscriptions.save')}}" method="POST" enctype="multipart/form-data" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_subscription')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label for="title">*{{tr('title')}}</label>
                                        <input type="text" required name="title" class="form-control" id="title" value="{{old('title')}}" placeholder="{{tr('title')}}" pattern = "[a-zA-Z,0-9\s\-\.]{2,100}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">*{{tr('description')}}</label>
                                        <textarea class="form-control" name="description" required placeholder="{{tr('description')}}">{{old('description')}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="plan">*{{tr('no_of_months')}}</label>
                                        <input type="number" min="1" max="12" pattern="[0-9][0-2]{2}"  required name="plan" class="form-control" id="plan" value="{{old('plan')}}" title="{{tr('please_enter_plan_month')}}" placeholder="{{tr('no_of_months')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount">*{{tr('no_of_account')}}</label>
                                        <input type="text" required name="no_of_account" class="form-control" id="manage_account_count" placeholder="{{tr('manage_account_count')}}" pattern="[0-9]{1,}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="amount">*{{tr('amount')}}</label>
                                        <input type="number" required name="amount" class="form-control" id="amount" placeholder="{{tr('amount')}}" step="any">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-checkbox">
                                            <input type="checkbox" name="popular_status" class="">
                                            <label for="mark_popular">
                                            {{tr('mark_popular')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($model) !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Publish</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Publish</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>

@endsection