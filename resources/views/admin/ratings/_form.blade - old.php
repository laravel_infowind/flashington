<div class="row">

    <div class="col-md-10">

        <div class="box box-primary">

            <div class="box-header label-primary">
                <b style="font-size:18px;">@yield('title')</b>
                <a href="{{route('admin.ratings.index')}}" class="btn btn-default pull-right">{{tr('view_rating')}}</a>
            </div>

            <form class="form-horizontal" action="{{route('admin.ratings.save')}}" method="POST" enctype="multipart/form-data" role="form">

                <div class="box-body">

                    <input type="hidden" name="id" value="{{$model->id}}">

                    <div class="form-group row">

                        <label for="review" class="col-sm-2 control-label">*Subject</label>

                        <br>

                        <div class="col-sm-10">

                        <input required id="subject" name="subject" class="form-control" placeholder="Write your Subject or headline" value="{{$model->subject}}"/>

                        </div>
                        
                    </div>


                    <div class="form-group row">
                        <label for="rating" class="col-sm-2 control-label">*{{tr('rating')}}</label>
                        <div class="col-sm-10"> 

                            
                            <div class="starRating">
                                <input id="rating5" type="radio" name="rating" value="5"
                                    @if($model->rating == 5) checked @endif>
                                <label for="rating5">5</label>
                                <input id="rating4" type="radio" name="rating" value="4"
                                    @if($model->rating == 4) checked @endif>
                                <label for="rating4">4</label>
                                <input id="rating3" type="radio" name="rating" value="3"
                                    @if($model->rating == 3) checked @endif>
                                <label for="rating3">3</label>
                                <input id="rating2" type="radio" name="rating" value="2"
                                    @if($model->rating == 2) checked @endif>
                                <label for="rating2">2</label>
                                <input id="rating1" type="radio" name="rating" value="1"
                                    @if($model->rating == 1) checked @endif>
                                <label for="rating1">1</label>
                            </div>
                            
                        </div>
                    </div>

                    <div class="form-group row">

                        <label for="review" class="col-sm-2 control-label">*{{tr('review')}}</label>

                        <br>

                        <div class="col-sm-10">

                        <textarea required id="ckeditor" name="review" class="form-control" placeholder="Write your review here">{{$model->comment}}</textarea>

                        </div>
                        
                    </div>

                    <div class="form-group row offset-md-2">
                        <label for="spoilers" class="col-sm-12 text-left">*Does this review contain spoilers?
                        </label>
                        <div class="col-sm-12"> 

                            <input type="radio" value="1" @if($model->spoilers=='YES') checked @endif name="spoilers"/> Yes
                            <input type="radio" value="0" @if($model->spoilers=='NO') checked @endif name="spoilers"/> No
                            
                        </div>
                    </div>

                </div>

                <div class="box-footer">
                <a href="{{ url('admin/ratings/index') }}" class="btn btn-danger">{{tr('cancel')}}</a>
                    @if(Setting::get('admin_delete_control'))
                        <a href="#" class="btn btn-success pull-right" disabled>{{tr('submit')}}</a>
                    @else
                        <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
                    @endif
                </div>
            </form>
        
        </div>

    </div>

</div>

