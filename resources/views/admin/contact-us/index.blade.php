@extends('layouts.admin')

@section('title', tr('view_videos'))

@section('content-header')

Contacts

@endsection

@section('breadcrumb')
<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
<li class="active"><i class="fa fa-video-camera"></i> Contacts</li>
@endsection

@section('content')
@include('notification.notify')

<div class="row">
	<div class="col-lg-12">

		<div class="box box-primary">

			<div class="box-header label-primary">
				<b style="font-size:18px;">
					Contacts
				</b>
			</div>


			<div class="box-body">

				<div class="table-responsive" style="padding: 35px 0px">

					@if(count($contacts) > 0)

					<div class="table table-responsive">

						<table id="datatable-withoutpagination" class="table table-bordered table-striped ">
							<div class="search-table-top">
								<div class="row">
									<div class="col-sm-6">
										<div class="dataTables_length">
											<label>Show
												<form method="get" action="{{ url('admin/contacts') }}"
													class="videos-change-pagination">
													<input type="hidden" name="column" value="{{ $column }}">
													<input type="hidden" name="order_by"
														value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
													<input type="hidden" name="search" value="{{ $contact_name }}">


													<select name="per_page" aria-controls="datatable"
														class="form-control input-sm" onchange="this.form.submit()">
														<option
															<?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
															value="10">10</option>
														<option
															<?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
															value="25">25</option>
														<option
															<?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
															value="50">50</option>
														<option
															<?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
															value="100">100</option>
													</select>
												</form>
												entries</label>
										</div>
									</div>



									<div class="col-sm-6">
										<form action="{{url('admin/contacts')}}" class="form">
											<div class="input-group">
												<input type="text" placeholder="Search by Name/Subject"
													class="form-control input-video-search" name="search"
													value="{{ $contact_name }}">
												<div class="input-group-prepend">
													<button type="submit" class="btn btn-success">Search</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<thead>
								<tr>
									<th>Name</th>
									<th>{{tr('email')}}</th>
									<th>
									  <a href="{{ url('admin/contacts?column=subject&order_by='.$asc_or_desc) }}">Subject<i class="fa fa-sort<?php echo $column == 'subject' ? '-' . $up_or_down : ''; ?>"></i></a>
									</th>									
								</tr>
							</thead>
							<tbody>
								@foreach($contacts as $i => $contact)
								<tr>
									<td><a href="{{ url('/admin/contacts/'.$contact->id) }}">{{$contact->name}}</a>
										<ul class="table-row-actions">
											
											<li role="presentation">
												@if(Setting::get('admin_delete_control'))
												<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('view')}}</a>
												@else
												<a role="menuitem" tabindex="-1" href="{{ url('/admin/contacts/'.$contact->id) }}">{{tr('view')}}</a>
												@endif
											</li> 
											<li role="presentation">
												@if(Setting::get('admin_delete_control'))
												<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
												@else
												<a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('contact_delete_confirmation' , $contact->name)}}&quot;);" href="{{route('admin.delete.contact' , array('id' => $contact->id))}}">{{tr('delete')}}</a>
												@endif
											</li>
										</ul>											
									</td>
									<td>{{$contact->email}}</td>
									<td>{{$contact->subject}}</td>									 
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination-video-list">

							<div class="row">
								<div class="col-sm-5">
									<div class="dataTables_info" id="datatable-checkbox_info" role="status"
										aria-live="polite">Showing
										{{ ($contacts->currentpage()-1)*$contacts->perpage()+1 }} to
										{{ ($contacts->currentpage()*$contacts->perpage() < $contacts->total() ) ? $contacts->currentpage()*$contacts->perpage() : $contacts->total() }}
										of {{ $contacts->total() }} entries
									</div>
								</div>
								<div class="col-sm-7 text-right">
									<div>{{ $contacts->links() }}</div>
								</div>
							</div>

						</div>
					</div>
					@else
					<h3 class="no-result">No Contacts Found</h3>
					@endif
				</div>
			</div>
		</div>
	</div>

</div>

@endsection