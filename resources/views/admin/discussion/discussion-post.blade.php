@extends('layouts.admin')

@section('title', tr('chat_list'))

@section('content-header')

 <span style="color:#1d880c !important">{{$video}} </span> - {{tr('chat_list') }}
@endsection
@section('breadcrumb')

<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

<li class="active"><i class="fa fa-star"></i> {{tr('chat_list')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">

            <div class="box-header label-primary">

                <b style="font-size:18px;">{{tr('chat_list')}}</b>
            </div>

            <div class="box-body">

                <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                    <div class="search-table-top">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>Show
                                        <form method="get" action="{{ url('admin/discussion/subjects') }}"
                                            class="videos-change-pagination">
                                            <input type="hidden" name="column" value="{{ $column }}">
                                            <input type="hidden" name="video_id" value="">
                                            <input type="hidden" name="order_by"
                                                value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="{{ $rated_by_user_search }}">


                                            <select name="per_page" aria-controls="datatable"
                                                class="form-control input-sm" onchange="this.form.submit()">
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
                                                    value="10">10</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
                                                    value="25">25</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
                                                    value="50">50</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
                                                    value="100">100</option>
                                            </select>
                                        </form>
                                        entries</label>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <form action="{{ url('admin/discussion/subject/posts') }}" class="form">
                                    <div class="input-group">
                                        <input type="hidden" name="forum_topic_id" value="{{$_GET['forum_topic_id']}}">
                                        <input type="text" placeholder="Search by Reply Message"
                                            class="form-control input-video-search" name="search"
                                            value="{{session('rated_by_user_search')}}">
                                            <input type="hidden" name="video_id" value="">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <thead>
                        <tr>
                           <th>
                            Subject
                            </th>
                            <th>
                               Subject Message
                            </th>
                            <th>
                               Replied By
                            </th>
                            <th>Reply
                            </th>
                            
                        </tr>
                    </thead>

                    <tbody class="comment-table response-links">
                        @foreach ($model as $value)
                        <tr>    
                            <td>
                             {{ $value->subject }}
                            </td>
                            <td>{{ strip_tags($value->message) }}
                            </td>
                            <td>
                            {{ $value->name }}
                            </td>
                            <td>
                             {{strip_tags($value->reply)}}
                             <ul class="table-row-actions">
                             <li role="presentation">
                                @if(Setting::get('admin_delete_control'))
                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                                @else
                                    <a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('you_want_post')}}&quot;);" href="{{url('admin/discussion/post/delete/'. $value->id)}}">{{tr('delete')}}</a>
                                @endif
                                </li>    
                             </ul
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="pagination-video-list">

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">
                                Showing {{ ($model->currentpage()-1)*$model->perpage()+1 }} to
                                {{ ($model->currentpage()*$model->perpage() < $model->total() ) ? $model->currentpage()*$model->perpage() : $model->total() }}
                                of {{ $model->total() }} entries
                            </div>
                        </div>
                        <div class="col-sm-7 text-right">
                            <div>{{ $model->links() }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection