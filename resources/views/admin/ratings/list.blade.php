@extends('layouts.admin')

@section('title', $video_title)

@section('content-header')
<h1>Reviews on <a href="{{url('admin/videos/edit'.'/'.$video_id)}}">{{ $video_title }}</a></h1>
@endsection

@section('breadcrumb')

<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

<li class="active"><i class="fa fa-star"></i> {{tr('view_rating')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">

            <div class="box-header label-primary">

                <b style="font-size:18px;">Reviews on {{ $video_title }}</b>
                <a href="{{url('admin/ratings/index')}}" class="btn btn-default pull-right">{{ tr('view_rating') }}</a>
            </div>

            <div class="box-body">

                <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                    <div class="search-table-top">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>Show
                                        <form method="get" action="{{ url('admin/ratings/list').'/'.$video_id }}"
                                            class="videos-change-pagination">
                                            <input type="hidden" name="column" value="{{ $column }}">
                                            <input type="hidden" name="order_by"
                                                value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="{{ $rated_by_user_search }}">


                                            <select name="per_page" aria-controls="datatable"
                                                class="form-control input-sm" onchange="this.form.submit()">
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
                                                    value="10">10</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
                                                    value="25">25</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
                                                    value="50">50</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
                                                    value="100">100</option>
                                            </select>
                                        </form>
                                        entries</label>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <form action="{{url('admin/ratings/list').'/'.$video_id}}" class="form">
                                    <div class="input-group">
                                        <input type="text" placeholder="Subject or Review"
                                            class="form-control input-video-search" name="search"
                                            value="{{session('rated_by_user_search')}}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <thead>
                        <tr>
                            <th>
                                <a
                                    href="{{ url('admin/ratings/list'.'/'.$video_id.'?column=user_id&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">Author
                                    <i
                                        class="fa fa-sort<?php echo $column == 'user_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th>Subject</th>
                            <th>
                                <a
                                href="{{ url('admin/ratings/list'.'/'.$video_id.'?column=rating&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">{{tr('rating')}}
                                <i class="fa fa-sort<?php echo $column == 'user_id' ? '-' . $up_or_down : ''; ?>"></i></a></th>
                            <th>{{tr('review')}}</th>
                            
                            <th><a
                                href="{{ url('admin/ratings/list'.'/'.$video_id.'?column=spoilers&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">{{tr('spoiler')}}
                                <i class="fa fa-sort<?php echo $column == 'spoilers' ? '-' . $up_or_down : ''; ?>"></i></a></th>
                            <th>Submitted on</th>
                        </tr>
                    </thead>

                    <tbody class="comment-table">
                        @foreach ($model as $value)
                        <tr class="comment-tr-{{ $value->id }}">    
                            <td>{{ $value->user->name }}</td>
                            
                            <td>
                                {{ $value->subject }}
                                <div class="row-actions">
                                <span class="status-{{ $value->id }}">
                                        @if($value->status == 0)
                                        <a class="menuitem comment-status" tabindex="-1" href="javascript:void(0)" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('ratings_approve_confirmation')}}&quot)">{{tr('approve')}} </a>
                                        @else
                                        <a class="menuitem comment-status" href="javascript:void(0)" tabindex="-1" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('decline_ratings')}}&quot)">{{tr('decline')}}</a>
                                        @endif
                                    </span>
                                    |
                                    <span>
                                        @if(Setting::get('admin_delete_control'))
                                        <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                        @else
                                        <a role="menuitem" tabindex="-1" href="{{route('admin.ratings.edit' , array('id' => $value->id))}}">{{tr('edit')}}</a>
                                        @endif
                                    </span>
                                    |
                                    <span class="trash">
                                        @if(Setting::get('admin_delete_control'))
                                        <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                                        @else
                                        <a class="comment-del" role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('rating_delete_confirmation')}}&quot;);" href="javascript:void(0)" url="{{route('admin.ratings.delete' , array('id' => $value->id))}}">{{tr('delete')}}</a>
                                        @endif
                                    </span>
                                </div>
                            </td>
                            
                            <td>{{ $value->rating }}/10</td>
                            <td>
                                @php
                                    $review = (strlen($value->comment) > 50) ? substr($value->comment, 0, 50).'...' : $value->comment;
                                @endphp
                                {!! ($review) !!}
                                
                                </td>
                                
                                <td>
                                    @if($value->spoilers == 'YES')
                                        <span class="label label-danger">{{tr('yes')}}</span>
                                    @else
                                        <span class="label label-success">{{tr('no')}}</span>
                                    @endif
                                </td>
                            <td>{{ $value->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="pagination-video-list">

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">
                                Showing {{ ($model->currentpage()-1)*$model->perpage()+1 }} to
                                {{ ($model->currentpage()*$model->perpage() < $model->total() ) ? $model->currentpage()*$model->perpage() : $model->total() }}
                                of {{ $model->total() }} entries
                            </div>
                        </div>
                        <div class="col-sm-7 text-right">
                            <div>{{ $model->links() }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
        // delete comment
        $(document).on('click', '.comment-del', function(){
            var url = $(this).attr('url');
            $.ajax({
                type:'GET',
                url:url,
                success: function(data){
                    if (data.status) {
                        $('.comment-tr-' + data.data).remove();
                        $('.comment-count-'+ data.video_id).text(data.comment_count);
                    } else {
                        alert('Something wrong with request');
                    }
                },
                error: function(){
                    alert('Something wrong with request');
                }
            });
        });

        // change status comment
        $(document).on('click', '.comment-status', function(){
            var url = $(this).attr('url');
            $.ajax({
                type:'GET',
                url:url,
                success: function(data){
                    if (data.status) {
                        $('.status-' + data.data).html(data.html);
                    } else {
                        alert('Something wrong with request');
                    }
                },
                error: function(){
                    alert('Something wrong with request');
                }
            });
        });
    });
    </script>
    @endsection