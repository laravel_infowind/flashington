@extends('layouts.admin')

@section('title', tr('discussion_category'))

@section('content-header', tr('discussion_category'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-suitcase"></i> {{tr('discussion_category')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
        <div class="col-lg-12">
          	<div class="box box-primary">
	          	<div class="box-header label-primary">
	                <b style="font-size:18px;">{{tr('discussion_category')}}</b>
	                <a href="{{route('admin.add.discussion.category')}}" class="btn btn-default pull-right">{{tr('add_category')}}</a>
	            </div>
	            
	            <div class="box-body">

	            	@if(count($categories) > 0)

		              	<table id="datatable-withoutpagination" class="table table-bordered table-striped">

							<thead>
							    <tr>
							      {{-- <th>{{tr('id')}}</th> --}}
							      <th>{{tr('name')}}</th>
                                  <th>{{tr('description')}}</th>
							      <th>{{tr('status')}}</th>							      
							    </tr>
							</thead>

							<tbody>
								@foreach($categories as $i => $category)

								    <tr>
								      	{{-- <td>{{showEntries($_GET, $i+1)}}</td> --}}
								      	<td>{{$category->name}}

								      		<ul class="table-row-actions"> 
	            								
						                  	<li role="presentation">
                                                @if(Setting::get('admin_delete_control'))
                                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                                @else
                                                    <a role="menuitem" tabindex="-1" href="{{route('admin.edit.discussion.category' , array('id' => $category->id))}}">{{tr('edit')}}</a>
                                                @endif
                                            </li>
                                            
						                  	<li role="presentation">

							                  	@if(Setting::get('admin_delete_control'))

								                  	<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>

								                @else

						                  			<a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_delete_confirmation' , $category->name)}}&quot;);" href="{{route('admin.delete.discussion.category' , array('category_id' => $category->id))}}">{{tr('delete')}}</a>
						                  		@endif
						                  	</li>
											

						                  	@if($category->status)
						                  		<li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_decline_confirmation' , $category->name)}}&quot;);" href="{{route('admin.category.discussion.approve' , array('id' => $category->id , 'status' =>0))}}">{{tr('decline')}}</a></li>
						                  	@else
						                  		<li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_approve_confirmation' , $category->name)}}&quot;);" href="{{route('admin.category.discussion.approve' , array('id' => $category->id , 'status' => 1))}}">{{tr('approve')}}</a></li>
						                  	@endif
						                  
									              
	            							</ul>
								      	</td>
                                        <td>
                                        {{strip_tags($category->description)}}
		                            	</td>
								    
								      <td>
								      		@if($category->status)
								      			<span class="label label-success">{{tr('approved')}}</span>
								       		@else
								       			<span class="label label-warning">{{tr('pending')}}</span>
								       		@endif
								       </td>
								       
								    </tr>
								@endforeach
							</tbody>
						
						</table>
						<div align="right" id="paglink"><?php echo $categories->links(); ?></div>

					@else
						<h3 class="no-result">{{tr('no_result_found')}}</h3>
					@endif
	            
	            </div>

          	</div>
        </div>
    </div>

@endsection
