@extends('layouts.admin')

@section('title', tr('edit_subadmin'))

@section('content-header', tr('edit_subadmin'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.moderators')}}"><i class="fa fa-users"></i> {{tr('subadmins')}}</a></li>
    <li class="active">{{tr('edit_subadmin')}}</li>
@endsection

@section('content')

@include('notification.notify')





<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.subadmin')}}" method="POST" enctype="multipart/form-data" role="form">
        <div class="row">
            
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('edit_subadmin')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <input type="hidden" name="id" value="{{$subadmin->id}}">
                                    <div class="form-group">
                                        <label for="username">*{{tr('username')}}</label>
                                        <input type="text" required pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}"  name="name" value="{{$subadmin->name}}" class="form-control" id="username" placeholder="{{tr('name')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">*{{tr('email')}}</label>
                                        <input type="email" maxlength="255"  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$" required class="form-control" value="{{$subadmin->email}}" id="email" name="email" placeholder="{{tr('email')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">{{tr('mobile')}}</label>
                                        <input type="text" name="mobile" value="{{$subadmin->mobile}}" class="form-control" id="mobile" minlength="4"  maxlength="16" pattern="[0-9]{4,16}" placeholder="{{tr('mobile')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{tr('description')}}</label>
                                        <textarea class="form-control"  placeholder="{{tr('description')}}" name="description" id="ckeditor">{{$subadmin->description}}</textarea>
                                    </div>
                                </div>

                                <input type="hidden" name="timezone" value="" id="userTimezone">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($subadmin) !!}

                            @if($subadmin->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($subadmin->updated_at)). ' at '. date('H:i', strtotime($subadmin->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Update</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Upload Image</label>
                    </div>
                    <div class="card-body">
                        <div class="image-upload">
                            <div class="image-upload-item mb-20">
                                <input type="file"  accept="image/png, image/jpeg" name="picture" id="picture" placeholder="Default image" style="display:none" onchange="loadFile(this,'picture_img')">
                                
                               
                                <img src="{{$subadmin->picture ? $subadmin->picture : asset('images/default.png')}}" onclick="$('#picture').click();return false;" id="picture_img" style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">

                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>


@endsection

@section('scripts')
<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });

function loadFile(event, id) {
    // alert(event.files[0]);
    var reader = new FileReader();

    reader.onload = function() {
        var output = document.getElementById(id);
        // alert(output);
        output.src = reader.result;
        //$("#imagePreview").css("background-image", "url("+this.result+")");
    };
    reader.readAsDataURL(event.files[0]);
}
</script>

@endsection