<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Setting;
use Exception;
use App\Helpers\Helper;

class UserLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
  public function authenticated(Request $request, $user)
{
  
     $vue_site_base_url = Setting::get('vue_site_base_url');
     $video_id = $request->video_id;
     $video_trailerid = $request->video_trailerid;
     $video_trailer = $request->video_trailer;
  
     if(!$user->is_verified && Auth::check()) {
        auth()->logout();
        Session()->flush();
        redirect('/')->with('error' , tr('verify_email'));
    }

    if (Auth::check())
    {
       
        if($video_id != null)
        {
            $query = http_build_query([
            'id' => Auth::user()->id,
            'card_id' => $video_id ,
        ]);
            // Auth::logout();
         // return redirect('http://localhost:8080/#?'.$query);
          
          return redirect($vue_site_base_url.'#/?'.$query); 
        }
         

        elseif($video_id == null && $video_trailerid == null)
        {
            $query = http_build_query([
            'id' => Auth::user()->id,
        ]);
            // Auth::logout();
         // return redirect('http://localhost:8080/#?'.$query);
          return redirect($vue_site_base_url.'/#/?'.$query); 
        }
          elseif($video_trailerid != null)
        {
            $query = http_build_query([
            'id' => Auth::user()->id,
            'card_id' => $video_trailerid ,
            'name' => $video_trailer,
        ]);
            // Auth::logout();
         // return redirect('http://localhost:8080/#?'.$query);
          return redirect($vue_site_base_url.'/#/?'.$query); 
        }      
    }
 

   
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
