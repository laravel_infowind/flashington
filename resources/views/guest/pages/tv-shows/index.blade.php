@extends('guest.layout.app')

<?php 
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
        @section('title', Setting::get('site_name').' | '. 'TV Shows')
        @section('description', Setting::get('meta_description'))
        @section('keywords', Setting::get('meta_keywords'))
        @section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')

<section class="home-pg-movie ptb80">
    <div class="container container-main">
        <div class="row cat_top_head">
            @if(!empty($category))
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="cat_top_left_img">
                            @if (!empty($category->picture))
                                <img class="channel_detailimg" src="{{ $category->picture }}" alt="{{ Setting::get('site_name'). ' | ' .$category->name}}">
                            @else
                                <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$category->name}}">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-6">
                        <div class="cat_top_right_cont">
                            <h1 class="channel_name_heading">{{ $category->name }}</h1>
                            <p class="cat_top_details">{!! $category->description !!}</p>
                        </div>
                    </div>
                
                @endif
        </div>
        <div class="row movie_heading">
            <div class="col col-md-6 col-sm-12">
                <h3 class="mmt_title title">TV Shows</h3>
            </div>
        </div>

        @foreach($sub_categories as $sub_cat)
        <div class="row sub-category">
            <div class="col col-md-9 col-sm-6">
                <h3 class="mmt_subtitle title">{{ $sub_cat->name }}</h3>
            </div>
            <div class="col col-md-3 col-sm-6 align-self-center text-right">
                <a href="{{ url("search?sub-category=".$sub_cat->name) }}" class="btn btn-icon btn-main btn-effect">View
                    All
                    <i class="ti-angle-double-right"></i>
                </a>
            </div>
        </div>
        <div class="related-movies row">
            <?php $i=1; ?>
            @foreach($videos as $data)
            @if($sub_cat->id == $data->sub_category_id)
            <?php if ($i++ > 6) break; 
                //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
                $movie_description = App\Helpers\Helper::truncate($data['details'], 200, '...<a href="'.url('movies/'.$data['unique_id']).'">More</a>', true, true);
                //$movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
            ?>
            <div class="col-lg-2 col-md-4 col-sm-4 col-4 related-movies-column">
                <a href="{{ url('movies/'.$data['unique_id'])  }}">
                <div class="movie-box-1">
                    <div class="poster">
                        <!-- <p>{{ $data['name'] }}</p> -->
                        <div class="movie_item">
                            <img class="cover_img" src="{{ $data['default_image'] }}"
                                alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button"
                                data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                data-title="" data-content="" data-container="body" data-html="true" data-original-title="" title="" />

                            <div class="movie_item_hover">
                                <div class='popover-movie-title'>{{ $data['title'] }}</div>
                                <div class='movie-info-row'>
                                    <div class="movie-info-item">
                                        <span class='movie-year'>{{ $data['release_date'] }}</span>
                                        <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                    </div>
                                    <div class="movie-info-item">
                                        <span class='movie-stars'>
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            {{ $data['ratings'] }}/<span class='rating_out_of'>10</span>
                                        </span>
                                        <span class='movie-vote'>
                                            <span class='vote_total'>{{ (!empty($data->vote_rating_total)) ? $data->vote_rating_total : 0 }} votes</span>
                                        </span>
                                    </div>
                                </div>
                                <div class='movie-description'>
                                    {!! $movie_description !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </a>
            </div>
            @endif
            @endforeach
        </div>
        @endforeach
    </div>
</section>

<style type="text/css">
    .owl-prev span,
    .owl-next span {
        color: #FFF;
    }

    .owl-prev,
    .owl-next {
        position: absolute;
        top: 0;
        height: 100%;
    }

    .owl-prev {
        left: 7px;
    }

    .owl-next {
        right: 7px;
    }
</style>

@endsection