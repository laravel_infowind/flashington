@extends('layouts.admin')

@section('title', tr('view_videos'))

@section('content-header')

{{tr('view_videos')}}

@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-video-camera"></i> {{tr('view_videos')}}</li>
@endsection



@section('content')

    @include('notification.notify')

    @if($category || $sub_category || $genre || isset($moderator_details))

	    <div class="row">

	    	<div class="col-lg-12">

	    		@if($category)
	    			<p class="text-uppercase">{{tr('category')}} - {{$category ? $category->name : "-" }}</p>
	    		@endif

	    		@if($sub_category)
	    			<p class="text-uppercase">{{tr('sub_category')}} - {{$sub_category ? $sub_category->name : "-" }}</p>
	    		@endif

	    		@if($genre) 
	    			<p class="text-uppercase">{{tr('genre')}} - {{$genre ? $genre->name : "-"}}</p>
	    		@endif

	    		@if(isset($moderator_details))
	    			@if($moderator_details)<p class="text-uppercase">{{tr('moderator')}} - {{$moderator_details->name}}</p>@endif
	    		@endif

	    	</div>

	    </div>

    @endif



	<div class="row">

        <div class="col-lg-12">

          <div class="box box-primary">

          	<div class="box-header label-primary">

                <b style="font-size:18px;">{{tr('view_videos')}}</b>

                <a href="{{route('admin.videos.create')}}" class="btn btn-default pull-right">{{tr('add_video')}}</a>

                 <!-- EXPORT OPTION START -->

                @if(count($videos) > 0 )
                @endif

                <!-- EXPORT OPTION END -->
            </div>

            <div class="box-body">

            	<div class=" table-responsive"> 
            		<div class="search-table-top"> 
	            		<div class="row">
		                    <div class="col-sm-6">
		                        <div class="dataTables_length">
		                            <label>Show
		                            <form method="get" action="{{ url('admin/videos') }}<?= (isset($_GET['moderator_id']) && ($_GET['moderator_id'] != '')) ? '?moderator_id='.$_GET['moderator_id'] : ''; ?>" class="videos-change-pagination">
		                                <input type="hidden" name="column" value="{{ $column }}">
	                                    <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'desc')) ? 'desc' : 'asc'; ?>">
	                                    <input type="hidden" name="search" value="{{ $title_search }}">
	                                    <input type="hidden" name="moderator_id" value="<?= (isset($_GET['moderator_id']) && ($_GET['moderator_id'] != '')) ? $_GET['moderator_id'] : ''; ?>">
	                                    
		                                
		                                <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
		                                   <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
		                                   <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
		                                   <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
		                                   <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
		                                </select>
		                            </form>
		                            entries</label>
		                        </div>
		                    </div>
		                

		                
		                    <div class="col-sm-6">
			            		<form action="{{route('admin.videos')}}" class="form">
									<div class="input-group">
										<input type="text" placeholder="Search by Title" class="form-control input-video-search" name="search" value="{{session('search')}}">
										<div class="input-group-prepend">
										    <button type="submit" class="btn btn-success">search</button>
										  </div>
									</div>
								</form>
							</div>
						</div>
					</div>
            		@if(count($videos) > 0)

	              	<table id="datatable-withoutpagination" class="table table-bordered table-striped">

							<thead>
						    <tr>
						    	<!-- <th style="vertical-align: 0px;">{{tr('id')}}</th> -->
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=title&order_by='.$asc_or_desc.'&per_page='.$per_page.'&category_name='.$category.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('title')}} <i class="fa fa-sort<?php echo $column == 'title' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<!-- <th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=admin_amount&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('revenue')}} <i class="fa fa-sort<?php echo $column == 'admin_amount' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th> -->
								<th style="vertical-align: 0px;"><a href="{{ url('admin/videos?column=amount&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('ppv')}} <i class="fa fa-sort<?php echo $column == 'amount' ? '-' . $up_or_down : ''; ?>"></i></a></th>
								
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=category_name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('category')}} <i class="fa fa-sort<?php echo $column == 'category_name' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<th style="vertical-align: 0px;">{{tr('sub_category')}}</th>
						    	<!-- <th style="vertical-align: 0px;">{{tr('genre_name')}}</th> -->
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=watch_count&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('viewers_cnt')}} <i class="fa fa-sort<?php echo $column == 'watch_count' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=is_banner&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">Banner <i class="fa fa-sort<?php echo $column == 'is_banner' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<!-- <th style="vertical-align: 0px;">{{tr('position')}}</th> -->
						    	@if(Setting::get('theme') == 'default')
						    	<th style="vertical-align: 0px;">{{tr('slider_video')}}</th>
						    	@endif
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=uploaded_by&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('uploaded_by')}} <i class="fa fa-sort<?php echo $column == 'uploaded_by' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=publish_time&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">Publish Date <i class="fa fa-sort<?php echo $column == 'publish_time' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
						    	<th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/videos?column=is_approved&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$title_search.'&moderator_id='.$moderator_id) }}">{{tr('status')}} <i class="fa fa-sort<?php echo $column == 'is_approved' ? '-' . $up_or_down : ''; ?>"></i></a> 
						    	</th>
						    	{{-- <th style="vertical-align: 0px;">{{tr('action')}}</th> --}}
						    </tr>
						</thead>

						<tbody>
							@foreach($videos as $i => $video)

							    <tr>
							      	<!-- <td>{{showEntries($_GET, $i+1)}}</td> -->
 

									<td><a href="{{route('admin.view.video' , array('id' => $video->video_id))}}">{{ $video->title }}</a>
									
										<ul class="table-row-actions">
												@if ($video->compress_status >= OVERALL_COMPRESS_COMPLETED)
												  <li role="presentation">
													@if(Setting::get('admin_delete_control'))
														<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
													@else
														<a role="menuitem" tabindex="-1" href="{{route('admin.videos.edit' , array('id' => $video->video_id))}}">{{tr('edit')}}</a>
													@endif
												</li>
												@endif
												  {{-- <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{route('admin.view.video' , array('id' => $video->video_id))}}">{{tr('view')}}</a></li>
												  <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{route('admin.video.seo' , array('id' => $video->video_id))}}">SEO</a></li> --}}

												   {{-- <li role="presentation"><a role="menuitem" href="{{route('admin.gif_generator' , array('video_id' => $video->video_id))}}">{{tr('generate_gif_image')}}</a></li> --}}

											  @if ($video->genre_id > 0 && $video->is_approved && $video->status)

												  <li role="presentation">
													<a role="menuitem" tabindex="-1" role="menuitem" tabindex="-1" data-toggle="modal" data-target="#video_{{$video->video_id}}">{{tr('change_position')}}</a>
												</li>

												@endif

												  @if ($video->compress_status >= OVERALL_COMPRESS_COMPLETED)

													  @if($video->is_approved && $video->status)

													  {{-- <li role="presentation">

														  <a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#banner_{{$video->video_id}}">

															  {{tr('banner_videos')}}

															  @if($video->is_banner == BANNER_VIDEO)

															  <span class="text-green"><i class="fa fa-check-circle"></i></span>

															  @endif

														  </a>

													  </li> --}}

													  @endif

												  @endif
												  @if ($video->compress_status >= OVERALL_COMPRESS_COMPLETED)

													  @if($video->is_approved && $video->status)

													  {{-- <li role="presentation">

														  <a role="menuitem" href="{{ url('admin/change_slider/'.$video->video_id)}}" onclick="return confirm('Do you want to change it as home slider')">

															  Home Slider

															  @if($video->is_home_slider == 1)

															  <span class="text-green"><i class="fa fa-check-circle"></i></span>

															  @endif

														  </a>

													  </li> --}}

													  @endif

												  @endif

												  

												  @if($video->is_approved == VIDEO_APPROVED)

													<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.video.decline',$video->video_id)}}">{{tr('decline')}}</a></li>
												@else

													@if ($video->compress_status < OVERALL_COMPRESS_COMPLETED)
														<li role="presentation">
															<a href="{{route(
															'admin.compress.status', ['id'=>$video->video_id])}}" role="menuitem" tabindex="-1">
																{{tr('do_compression_in_background')}}
															</a>
														</li>
													@else 
														  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.video.approve',$video->video_id)}}">{{tr('approve')}}</a></li>
													  @endif
												  @endif

												  @if ($video->compress_status >= OVERALL_COMPRESS_COMPLETED)

													  <li role="presentation" class="presentation_delete">
														  @if(Setting::get('admin_delete_control'))

															   <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>

														  @else
															  <a role="menuitem" tabindex="-1" onclick="return confirm('Are you sure want to delete video? Remaining video positions will Rearrange')" href="{{route('admin.delete.video' , array('id' => $video->video_id))}}">{{tr('delete')}}</a>
														  @endif
													  
													  </li>
												  @endif

												  @if($video->status == 0)
													  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.video.publish-video',$video->video_id)}}">{{tr('publish')}}</a></li>
												  @endif
										</ul>
									</td>

									  <!-- <td>{{Setting::get('currency')}} {{$video->admin_amount ? $video->admin_amount : "0.00"}}</td> -->
									  
									@if(Setting::get('is_payper_view'))
							      	<td class="text-center">
										<a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#ppv_{{$video->video_id}}">  
										@if($video->amount > 0)
							      			<span class="label label-success btn">{{tr('yes')}}</span>
							      		@else
							      			<span class="label label-danger btn">{{tr('no')}}</span>
										@endif
										</a>
							      	</td>
							      	@endif

							      	<td>{{$video->category_name}}</td>
							      	<td>{{ category($video->sub_category_id) }}</td>
							      	<!-- <td>{{$video->genre_name ? $video->genre_name : '-'}}</td> -->

							      	<td>{{number_format_short($video->watch_count)}}</td>

							      	<td class="text-center">
									  <div class="banner_status_{{ $video->video_id }}">  
										@if($video->is_banner == BANNER_VIDEO)
							      			<span onclick="change_banner_status({{$video->video_id}}, 1)" class="label label-success btn">{{tr('yes')}}</span>
							      		@else
							      			<span onclick="change_banner_status({{$video->video_id}}, 0)" class="label label-danger btn">{{tr('no')}}</span>
										@endif
										</div>
							      	</td>

							      	<!-- <td>

							      		@if ($video->genre_id > 0)
							      			
								      		@if($video->position > 0)

									      	<span class="label label-success">{{$video->position}}</span>

									      	@else

									      	<span class="label label-danger">{{$video->position}}</span>

									      	@endif

									    @else

									    	<span class="label label-warning">{{tr('not_genre')}}</span>

									    @endif

							      	</td> -->

							      	

							      	
							      	@if(Setting::get('theme') == 'default')
							      	<td>
							      		@if($video->is_home_slider == 0 && $video->is_approved && $video->status)
							      			<a href="{{route('admin.slider.video' , $video->video_id)}}"><span class="label label-danger">{{tr('set_slider')}}</span></a>
							      		@elseif($video->is_home_slider)
							      			<span class="label label-success">{{tr('slider')}}</span>
							      		@else
							      			-
							      		@endif
							      	</td>

							      	@endif

							      	<td>

							      		{{-- @if(is_numeric($video->uploaded_by))

							      			<a href="{{route('admin.moderator.view',$video->uploaded_by)}}">{{$video->moderator ? $video->moderator->name : ''}}</a>


							      		@else 
										
							      			{{$video->uploaded_by}}

										@endif --}}
											@if(is_numeric($video->uploaded_by))
												{{($video->uploaded_by == 1 || $video->uploaded_by == 'admin') ? 'Admin': App\Helpers\Helper::getUsername($video->uploaded_by) }}
											@else
												{{ ucfirst($video->uploaded_by) }}
											@endif
							      	</td>
							      	<td>{{ date('Y-m-d', strtotime($video->publish_time)) }}</td>
							      	<td>
							      		@if ($video->compress_status < OVERALL_COMPRESS_COMPLETED)
							      			<span class="label label-danger">{{tr('compress')}}</span>
							      		@else
								      		@if($video->is_approved)
								      			<span class="label label-success">{{tr('approved')}}</span>
								       		@else
								       			<span class="label label-warning">{{tr('pending')}}</span>
								       		@endif
								       	@endif
							      	</td>
								     
								</tr>

								<!-- PPV Modal Popup-->

								<div id="ppv_{{$video->video_id}}" class="modal fade" role="dialog">

									<div class="modal-dialog">

										<form action="{{route('admin.save.video-payment', $video->video_id)}}" method="POST">
										  <!-- Modal content-->
											 <div class="modal-content">

												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												  
												  <h4 class="modal-title text-uppercase">

													  <b>{{tr('pay_per_view')}}</b>

													  @if($video->amount > 0)

															<span class="text-green"><i class="fa fa-check-circle"></i></span>

														@endif

												  </h4>
												</div>

												 <div class="modal-body">

												  <div class="row">

													  <input type="hidden" name="ppv_created_by" id="ppv_created_by" value="{{Auth::guard('admin')->user()->name}}">

													  <div class="col-lg-12">
														  <label class="text-uppercase">{{tr('video')}}</label>
													  </div>

													  <div class="col-lg-12">

														  <p>{{$video->title}}</p>

													  </div>

													  <div class="col-lg-12">
														  <label class="text-uppercase">{{tr('type_of_user')}} *</label>
													  </div>

													  <div class="col-lg-12">

															<div class="input-group">

															  <input type="radio" name="type_of_user" value="{{NORMAL_USER}}" {{($video->type_of_user == 0 || $video->type_of_user == '') ? 'checked' : (($video->type_of_user == NORMAL_USER) ? 'checked' : '')}}>&nbsp;<label class="text-normal">{{tr('normal_user')}}</label>&nbsp;
															  
															  <input type="radio" name="type_of_user" value="{{PAID_USER}}" {{($video->type_of_user == PAID_USER) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('paid_user')}}</label>&nbsp;
															  
															  <input type="radio" name="type_of_user" value="{{BOTH_USERS}}" {{($video->type_of_user == BOTH_USERS) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('both_user')}}</label>
															</div>
															
															<!-- /input-group -->
													  </div>
												  </div>
												  <br>
												  <div class="row">
													  <div class="col-lg-12">

														  <label class="text-uppercase">{{tr('type_of_subscription')}} *</label>

													  </div>
													  <div class="col-lg-12">

														<div class="input-group">
															  <input type="radio" name="type_of_subscription" value="{{ONE_TIME_PAYMENT}}" {{($video->type_of_subscription == 0 || $video->type_of_subscription == '') ? 'checked' : (($video->type_of_subscription == ONE_TIME_PAYMENT) ? 'checked' : '')}}>&nbsp;<label class="text-normal">{{tr('one_time_payment')}}</label>&nbsp;
															  <input type="radio" name="type_of_subscription" value="{{RECURRING_PAYMENT}}" {{($video->type_of_subscription == RECURRING_PAYMENT) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('recurring_payment')}}</label>
														</div>
														<!-- /input-group -->
													  </div>
												  
												  </div>

												  <br>
												  <div class="row">
													  <div class="col-lg-12">
														  <label class="text-uppercase">{{tr('amount')}} *</label>
													  </div>
													  <div class="col-lg-12">
														  <input type="number" required value="{{$video->amount}}" name="amount" class="form-control" id="amount" placeholder="{{tr('amount')}}" step="any">
													  </div>
												  
												  </div>

											  </div>
												
												<div class="modal-footer">
													<div class="pull-left">

														@if($video->amount > 0)

															 <a class="btn btn-danger" href="{{route('admin.remove_pay_per_view', $video->video_id)}}" onclick="return confirm(&quot;{{tr('remove_ppv_confirmation')}}&quot;);">

																 {{tr('remove_pay_per_view')}}

															 </a>
														 @endif
													 </div>

												  <div class="pull-right">
													  <button type="button" class="btn btn-default" data-dismiss="modal">{{tr('close')}}</button>

													  <button type="submit" class="btn btn-primary" onclick="return confirm(&quot;{{tr('set_ppv_confirmation')}}&quot;);">{{tr('submit')}}</button>
												  </div>
												  
												  <div class="clearfix"></div>
												
												</div>
										  
										  </div>
									  </form>
								</div>

							  </div>


								@if ($video->compress_status >= OVERALL_COMPRESS_COMPLETED && $video->is_approved && $video->status)

								<!-- Modal -->
								<div id="banner_{{$video->video_id}}" class="modal fade" role="dialog">
								  	
								  	<div class="modal-dialog">

										<form action="{{route('admin.banner.set', ['admin_video_id'=>$video->video_id])}}" method="POST" enctype="multipart/form-data">

										    <!-- Modal content-->
										   	<div class="modal-content">
										      	
										      	<div class="modal-header">
										        	<button type="button" class="close" data-dismiss="modal">&times;</button>
										        	
										        	<h4 class="modal-title">{{tr('set_banner_image')}}</h4>
										      	</div>

										      	<div class="modal-body">

											        <div class="row">

											    		<div class="col-lg-12">
											    			<p class="text-blue text-uppercase">
											    				{{-- {{ tr('banner_video_notes') }} --}}
											    			</p>
											    		</div>

											    		<div class="col-lg-12">

											    			<p>{{$video->title}}</p>

											    		</div>
											        	
											        	<div class="col-lg-12">
											        		<label>{{tr('banner_image_website')}} *</label>

											        		<p class="help-block">{{tr('image_validate')}} {{tr('rectangle_image')}}</p>
											        	</div>

											            <div class="col-lg-12">

											      <div class="input-group">

								<input type="file" id="banner_image_file_{{$video->video_id}}" accept="image/png,image/jpeg" name="banner_image" placeholder="{{tr('banner_image')}}" style="display:none" onchange="loadFile(this,'banner_image_{{$video->video_id}}')" />

											                 <div>
				<img src="{{($video->is_banner) ? $video->banner_image : asset('images/320x150.png')}}" style="width:300px;height:150px;" 
					onclick="$('#banner_image_file_{{$video->video_id}}').click();return false;" id="banner_image_{{$video->video_id}}" style="cursor: pointer;" />
											            </div>
											                    
											 </div>
											              <!-- /input-group -->
											            </div>
											        
											        </div>

										        	<br>
										      	</div>
										      	
										      	<div class="modal-footer">

											      	@if($video->is_banner == BANNER_VIDEO)

											      	<div class="pull-left">

											      		<?php $remove_banner_image_notes = tr('remove_banner_image_notes');?>

											          	<a onclick="return confirm('{{$remove_banner_image_notes}}')" role="menuitem" tabindex="-1" href="{{route('admin.banner.remove',['admin_video_id'=>$video->video_id])}}" class="btn btn-danger">{{tr('remove_banner_image')}}</a>

											      	</div>

											      	@endif

											        <div class="pull-right">
												        <button type="button" class="btn btn-default" data-dismiss="modal">{{tr('close')}}</button>

												        <button type="submit" class="btn btn-primary" onclick="return confirm(&quot;{{tr('set_banner_image_confirmation')}}&quot;);">{{tr('submit')}}</button>
												    </div>
											    	<div class="clearfix"></div>
										     	
										     	</div>

										    </div>
										
										</form>

								  	</div>
								</div>

								@endif

								@if ($video->genre_id > 0 && $video->is_approved && $video->status)

								<div id="video_{{$video->video_id}}" class="modal fade" role="dialog">
								  <div class="modal-dialog">
								  <form action="{{route('admin.save.video.position',['video_id'=>$video->video_id])}}" method="POST">
									    <!-- Modal content-->
									   	<div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									        <h4 class="modal-title">{{tr('change_position')}}</h4>
									      </div>

									      <div class="modal-body">
									        
								            <div class="row">
									        	<div class="col-lg-3">
									        		<label>{{tr('position')}}</label>
									        	</div>
								                <div class="col-lg-9">
								                       <input type="number" required value="{{$video->position}}" name="position" class="form-control" id="position" placeholder="{{tr('position')}}" pattern="[0-9]{1,}" title="Enter 0-9 numbers">
								                  <!-- /input-group -->
								                </div>
								            </div>
									      </div>
									      <div class="modal-footer">
									        <div class="pull-right">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										        <button type="submit" class="btn btn-primary">{{tr('submit')}}</button>
										    </div>
										    <div class="clearfix"></div>
									      </div>
									    </div>
									</form>
								  </div>
								</div>

								@endif
							@endforeach
						</tbody>
					
					</table>

					<div class="pagination-video-list">
						
						<div class="row">
		                    <div class="col-sm-5">
		                        <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">Showing {{ ($videos->currentpage()-1)*$videos->perpage()+1 }} to {{ ($videos->currentpage()*$videos->perpage() < $videos->total() ) ? $videos->currentpage()*$videos->perpage() : $videos->total() }} of {{ $videos->total() }} entries
		                        </div>
		                    </div>
		                    <div class="col-sm-7 text-right">
		                        <div>{{ $videos->links() }}</div>
		                    </div>
		                </div>

					</div>
				@else
					<h3 class="no-result">{{tr('no_video_found')}}</h3>
				@endif

				</div>
            </div>
          </div>
        </div>
    </div>

@endsection



@section('scripts')
<script type="text/javascript">
	
function loadFile(event, id){
    var reader = new FileReader();

    reader.onload = function(){
      var output = document.getElementById(id);
      output.src = reader.result;
    };
    reader.readAsDataURL(event.files[0]);
}

function change_banner_status(video_id, status)
{
	$.ajax({
		type:'POST',
		url:'{{ url("admin/videos/change-banner-status")}}',
		data: {
			video_id: video_id,
			status: status,
			_token: '<?php echo csrf_token(); ?>'
		},
		success: function(res){
			if(res.data != '') {
				$('.banner_status_'+res.video_id).html(res.data);
			} else {
				alert('Something wrong with request');
			}
		},
		error: function(){
			alert('Something wrong with request');
		}
	});
}
</script>
<script>
	// var table = $('#datatable-withoutpagination').DataTable( {
 //        orderCellsTop: true,
 //        fixedHeader: true,
 //    } );
	
	
</script>
@endsection
