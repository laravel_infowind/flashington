
<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.ratings.save')}}" method="POST" enctype="multipart/form-data" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">@yield('title')</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <input type="hidden" name="id" value="{{$model->id}}">
                                    <input type="hidden" name="admin_video_id" value="{{$model->admin_video_id}}">
                                    <div class="form-group">
                                        <label for="review">Subject*</label>
                                        <input required id="subject" name="subject" class="form-control" placeholder="Write your Subject or headline" value="{{$model->subject}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="rating">{{tr('rating')}}*</label>
                                        <div class="starRating">
                                            <input id="rating10" type="radio" name="rating" value="10"
                                            @if($model->rating == 10) checked @endif>
                                            <label for="rating10">10</label>
                                            <input id="rating9" type="radio" name="rating" value="9"
                                            @if($model->rating == 9) checked @endif>
                                            <label for="rating9">9</label>
                                            <input id="rating8" type="radio" name="rating" value="8"
                                            @if($model->rating == 8) checked @endif>
                                            <label for="rating8">8</label>
                                            <input id="rating7" type="radio" name="rating" value="7"
                                            @if($model->rating == 7) checked @endif>
                                            <label for="rating7">7</label>
                                            <input id="rating6" type="radio" name="rating" value="6"
                                            @if($model->rating == 6) checked @endif>
                                            <label for="rating6">6</label>
                                            <input id="rating5" type="radio" name="rating" value="5"
                                            @if($model->rating == 5) checked @endif>
                                            <label for="rating5">5</label>
                                            <input id="rating4" type="radio" name="rating" value="4"
                                            @if($model->rating == 4) checked @endif>
                                            <label for="rating4">4</label>
                                            <input id="rating3" type="radio" name="rating" value="3"
                                            @if($model->rating == 3) checked @endif>
                                            <label for="rating3">3</label>
                                            <input id="rating2" type="radio" name="rating" value="2"
                                            @if($model->rating == 2) checked @endif>
                                            <label for="rating2">2</label>
                                            <input id="rating1" type="radio" name="rating" value="1"
                                            @if($model->rating == 1) checked @endif>
                                            <label for="rating1">1</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="review">{{tr('review')}}*</label>
                                        <textarea required id="ckeditor" name="review" class="form-control" placeholder="Write your review here">{{$model->comment}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="spoilers" class="text-left">Does this review contain spoilers?*</label>
                                        <br>
                                        <input type="radio" value="1" @if($model->spoilers=='YES') checked @endif name="spoilers"/> Yes
                                        <input type="radio" value="0" @if($model->spoilers=='NO') checked @endif name="spoilers"/> No
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="publish_item mb-20">
                                <span id="timestamp">
                                <label>Published on: <span class="published_on">2020-05-12 at 10:05</span></label>
                                </span>
                                <a href="javascript:void(0)" class="edit-timestamp hide-if-no-js btn" role="button">
                                <span aria-hidden="true">Edit</span>
                                </a>
                                <fieldset id="timestampdiv" class="hide-if-js" style="display: none;">
                                    <div class="timestamp-wrap">
                                        <label>
                                            <select id="mm" name="mm">
                                                <option value="01" data-text="Jan">01-Jan</option>
                                                <option value="02" data-text="Feb">02-Feb</option>
                                                <option value="03" data-text="Mar">03-Mar</option>
                                                <option value="04" data-text="Apr">04-Apr</option>
                                                <option value="05" selected="" data-text="May">05-May</option>
                                                <option value="06" data-text="Jun">06-Jun</option>
                                                <option value="07" data-text="Jul">07-Jul</option>
                                                <option value="08" data-text="Aug">08-Aug</option>
                                                <option value="09" data-text="Sep">09-Sep</option>
                                                <option value="10" data-text="Oct">10-Oct</option>
                                                <option value="11" data-text="Nov">11-Nov</option>
                                                <option value="12" data-text="Dec">12-Dec</option>
                                            </select>
                                        </label>
                                        <label><input type="text" id="jj" name="jj" value="12" size="2" maxlength="2" autocomplete="off"></label>, <label><input type="text" id="aa" name="aa" value="2020" size="4" maxlength="4" autocomplete="off"></label> at <label><input type="text" id="hh" name="hh" value="10" size="2" maxlength="2" autocomplete="off"></label>:<label><input type="text" id="mn" name="mn" value="05" size="2" maxlength="2" autocomplete="off"></label>
                                    </div>
                                    <p>
                                        <a href="javascript:void(0)" class="save-timestamp hide-if-no-js button btn">OK</a>
                                        <a href="javascript:void(0)" class="cancel-timestamp hide-if-no-js button-cancel">Cancel</a>
                                    </p>
                                </fieldset>
                            </div>
                            <input type="hidden" name="created_at" id="created_at" value="2020-05-12 10:05:01">
                            <div class="publish_item mb-20">
                                <label>Modified on: <span class="published_on">2020-09-28 at 06:17</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="{{ url('admin/ratings/index') }}" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>{{tr('submit')}}</a>
                                @else
                                    <button type="submit" class="btn btn-primary">{{tr('submit')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


