<?php //echo '<pre>'; print_r($record);die;?>
<div class="card">
    <div class="card-header">
        <a class="" href="javascript:void(0)"> #{{$record->id}} {{$record->getTitle()}}</a>
    </div>
    <div class="card-body">
        <div class="card-text">
            {{$record->getDescription()}}
        </div>
    </div>
    <div class="card-footer">
        <label class="badge badge-secondary">{{$record->robot_index}}</label>
        <label class="badge badge-secondary">{{$record->robot_follow}}</label>
        <label class="badge badge-light">
            <a target="_blank" href="{{url($record->getFullUrl())}}">Visit page</a>
        </label>
        <div class="" style="float: right">
            <a href="{{route('seo::pages.edit',$record->id)}}">
                <span class="fa fa-pencil"></span>
            </a>
            &nbsp;&nbsp;
            @include('seo::forms.destroy',['route'=>route('seo::pages.destroy',$record->id)])
        </div>
    </div>
</div>
