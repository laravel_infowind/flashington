<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="@if(Auth::guard('admin')->user()->picture){{Auth::guard('admin')->user()->picture}} @else {{asset('placeholder.png')}} @endif" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::guard('admin')->user()->name}}</p>
                <a href="{{route('admin.profile')}}">{{ tr('admin') }}</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li id="dashboard">
              <a href="{{route('admin.dashboard')}}">
                <i class="fa fa-dashboard"></i> <span>{{tr('dashboard')}}</span>
              </a>
              
            </li>

            <li class="treeview" id="users">

                <a href="#">
                    <i class="fa fa-user"></i> <span>{{tr('users')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="view-user"><a href="{{route('admin.users')}}"><i class="fa fa-circle-o"></i>{{tr('view_users')}}</a></li>
                    <li id="add-user"><a href="{{route('admin.users.create')}}"><i class="fa fa-circle-o"></i>{{tr('add_user')}}</a></li>
                </ul>
    
            </li>

            <li class="treeview" id="moderators">
                
                <a href="#">
                    <i class="fa fa-users"></i> <span>{{tr('moderators')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="view-moderator"><a href="{{route('admin.moderators')}}"><i class="fa fa-circle-o"></i>{{tr('view_moderators')}}</a></li>
                    <li id="add-moderator"><a href="{{route('admin.add.moderator')}}"><i class="fa fa-circle-o"></i>{{tr('add_moderator')}}</a></li>
                </ul>
            
            </li>

            @if(Auth::guard('admin')->user()->user_type == 1)
            <li class="treeview" id="subadmins">
                
                <a href="#">
                    <i class="fa fa-support"></i> <span>{{tr('subadmins')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="view-subadmin"><a href="{{route('admin.subadmins')}}"><i class="fa fa-circle-o"></i>{{tr('view_subadmins')}}</a></li>
                    <li id="add-subadmin"><a href="{{route('admin.add.subadmin')}}"><i class="fa fa-circle-o"></i>{{tr('add_subadmin')}}</a></li>
                </ul>
            
            </li>
            @endif

            {{-- <li class="treeview" id="categories">
                <a href="{{route('admin.categories')}}">
                    <i class="fa fa-suitcase"></i> <span>{{tr('categories')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-category"><a href="{{route('admin.add.category')}}"><i class="fa fa-circle-o"></i>{{tr('add_category')}}</a></li>
                    <li id="view-categories"><a href="{{route('admin.categories')}}"><i class="fa fa-circle-o"></i>{{tr('view_categories')}}</a></li>
                    <li id="add-tag"><a href="{{route('admin.add.tag')}}"><i class="fa fa-circle-o"></i>{{tr('add_tag')}}</a></li>
                    <li id="view-tags"><a href="{{route('admin.tags')}}"><i class="fa fa-circle-o"></i>{{tr('view_tags')}}</a></li>
                </ul>

            </li> --}}

            <li class="treeview" id="cast-crews">

                <a href="#">
                    <i class="fa fa-male"></i><span>{{tr('cast_crews')}}</span><i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id ="cast-crew-index"><a href="{{route('admin.cast_crews.index')}}"><i class="fa fa-circle-o"></i>{{tr('view_cast_crew')}}</a></li>
                    <li id="cast-crew-add"><a href="{{route('admin.cast_crews.add')}}"><i class="fa fa-circle-o"></i>{{tr('add_cast_crew')}}</a></li>
                    <li id="cast-crew-add"><a href="{{route('admin.cast_crews.position')}}"><i class="fa fa-circle-o"></i>Position</a></li>
                </ul>
            </li>

            <li class="treeview" id="videos">

                <a href="{{route('admin.videos')}}">
                    <i class="fa fa-video-camera"></i> <span>{{tr('videos')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="view-videos">
                        <a href="{{route('admin.videos')}}"><i class="fa fa-circle-o"></i>{{tr('view_videos')}}</a>
                    </li>

                    <li id="admin_videos_create">
                        <a href="{{route('admin.videos.create')}}"><i class="fa fa-circle-o"></i>{{tr('add_video')}}</a>
                    </li>

                    @if(Setting::get('is_spam'))

                        <li id="spam_videos">
                            <a href="{{route('admin.spam-videos')}}">
                                <i class="fa fa-circle-o"></i><span>{{tr('spam_videos')}}</span>
                            </a>
                        </li>

                    @endif
                    
                    <li id="view-categories"><a href="{{route('admin.categories')}}"><i class="fa fa-circle-o"></i>{{tr('view_categories')}}</a></li>
                    
                    <li id="view-tags"><a href="{{route('admin.tags')}}"><i class="fa fa-circle-o"></i>{{tr('view_tags')}}</a></li>

                    {{-- <li id="view-banner-videos"><a href="{{route('admin.videos',['banner'=>BANNER_VIDEO])}}"><i class="fa fa-circle-o"></i>{{tr('banner_videos')}}</a></li> --}}
                </ul>

            </li>

            <li class="treeview" id="discussion">

                <a href="{{route('admin.discussions')}}">
                    <i class="fa fa-comments"></i> <span>{{tr('discussion')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="view-discussion-categories">
                        <a href="{{route('admin.discussions')}}"><i class="fa fa-circle-o"></i>{{tr('view_discussions')}}</a>
                    </li>
                    <li id="view-discussion-categories"><a href="{{route('admin.discussion.category')}}"><i class="fa fa-circle-o"></i>{{tr('discussion_category')}}</a></li>
                </ul>

            </li>

            @if(Auth::guard('admin')->user()->user_type == 1)
            <li class="treeview" id="ratings">

                <a href="#">
                    <i class="fa fa-comment"></i><span>{{tr('ratings')}}</span><i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    {{-- <li id="ratings-add"><a href="{{route('admin.ratings.add')}}"><i class="fa fa-circle-o"></i>{{tr('add_rating')}}</a></li> --}}
                    <li id ="ratings-index"><a href="{{route('admin.ratings.index')}}"><i class="fa fa-circle-o"></i>{{tr('view_rating')}}</a></li>
                </ul>
            </li>
            @endif

            <?php /*<li class="treeview" id="banner-videos">
                <a href="{{route('admin.banner.videos')}}">
                    <i class="fa fa-university"></i> <span>{{tr('banner_videos')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    @if(get_banner_count() < 6)
                        <li id="add-banner-video"><a href="{{route('admin.add.banner.video')}}"><i class="fa fa-circle-o"></i>{{tr('add_video')}}</a></li>
                    @endif
                    <li id="view-banner-videos"><a href="{{route('admin.banner.videos')}}"><i class="fa fa-circle-o"></i>{{tr('view_videos')}}</a></li>
                </ul>

            </li> */?>

            @if(Auth::guard('admin')->user()->user_type == 1)
            <li class="treeview" id="subscriptions">

                <a href="#">
                    <i class="fa fa-key"></i> <span>{{tr('subscriptions')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id="view-subscription"><a href="{{route('admin.subscriptions.index')}}"><i class="fa fa-circle-o"></i>{{tr('view_subscriptions')}}</a></li>
                    <li id="subscriptions-add"><a href="{{route('admin.subscriptions.create')}}"><i class="fa fa-circle-o"></i>{{tr('add_subscription')}}</a></li>
                </ul>
            </li>
            @endif
            
        <!-- Coupon Section-->
            <li class="treeview" id="coupons">

                <a href="#">
                    <i class="fa fa-gift"></i><span>{{tr('coupons')}}</span><i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id = "view_coupons"><a href="{{route('admin.coupon.list')}}"><i class="fa fa-circle-o"></i>{{tr('view_coupon')}}</a></li>
                    <li id="create"><a href="{{route('admin.add.coupons')}}"><i class="fa fa-circle-o"></i>{{tr('add_coupon')}}</a></li>
                </ul>
            </li>

            @if(Auth::guard('admin')->user()->user_type == 1)
            <li id="payments">
                <a href="{{route('admin.user.payments')}}">
                    <i class="fa fa-credit-card"></i> <span>{{tr('payments')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id="revenue_system"><a href="{{route('admin.revenue.system')}}"><i class="fa fa-circle-o"></i>{{tr('revenue_system')}}</a></li>
                    
                    <li id="user-payments">
                        <a href="{{route('admin.user.payments')}}">
                            
                            <i class="fa fa-circle-o"></i> <span>{{tr('subscription_payments')}}</span>
                        </a>
                    </li>


                    @if(Setting::get('is_payper_view'))
                    
                        <li id="video-subscription">
                            <a href="{{route('admin.user.video-payments')}}">
                                <i class="fa fa-circle-o"></i> <span>{{tr('ppv_payments')}}</span>
                            </a>
                        </li>

                    @endif
                    
                </ul>
            </li>

            @if(Setting::get('redeem_control'))

            <li id="redeems">
                <a href="{{route('admin.moderators.redeems')}}">
                    <i class="fa fa-trophy"></i> <span>{{tr('redeems')}}</span> 
                </a>
            </li>

            @endif

            @if(Setting::get('admin_language_control') == 0)
            <li id="languages">
                <a href="{{route('admin.languages.index')}}">
                    <i class="fa fa-globe"></i> <span>{{tr('languages')}}</span>
                </a>
            </li>
            @endif

            <!-- settings Section-->
            <li class="treeview" id="settings">

                <a href="#">
                    <i class="fa fa-gears"></i><span>{{tr('settings')}}</span><i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id="site_settings"><a href="{{route('admin.settings')}}"><i class="fa fa-circle-o"></i>{{tr('site_settings')}}</a></li>
                    <li id = "home_page_settings"><a href="{{route('admin.homepage.settings')}}"><i class="fa fa-circle-o"></i>{{tr('home_page_settings')}}</a></li>
                </ul>
            </li>
            @endif

            {{-- <li id="settings">
                <a href="{{route('admin.email.settings')}}">
                    <i class="fa fa-envelope"></i> <span>{{tr('email_settings')}}</span>
                </a>
            </li> --}}

           <?php  /*<li id="theme-settings">
                <a href="{{route('admin.theme.settings')}}">
                    <i class="fa fa-refresh"></i> <span>{{tr('theme_settings')}}</span>
                </a>
            </li> */?>
           
            <li id="custom-push">
                <a href="{{route('admin.push')}}">
                    <i class="fa fa-send"></i> <span>{{tr('custom_push')}}</span>
                </a>
            </li>

            <li id="email_templates">
                <a href="{{route('admin.templates')}}">
                    <i class="fa fa-envelope"></i> <span>{{tr('email_templates')}}</span>
                </a>
            </li>

            <li class="treeview {{ Request::is('admin/contacts') ? 'active' : '' }}" id="viewpages">

                <a href="#">
                    <i class="fa fa-picture-o"></i> <span>{{tr('pages')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id="view_pages"><a href="{{route('admin.pages.index')}}"><i class="fa fa-circle-o"></i>View Page</a></li>
                    <li id="add_page"><a href="{{route('admin.pages.create')}}"><i class="fa fa-circle-o"></i>Add Page</a></li>

                    @if(Auth::guard('admin')->user()->user_type == 1)
                    <li id="view_contacts" class="{{ Request::is('admin/contacts') ? 'active' : '' }}"><a href="{{route('admin.contacts')}}"><i class="fa fa-circle-o"></i>View Contacts</a></li>
                    @endif
                    <li id="contact_us">
                        <a href="{{route('admin.contact_us.setting')}}">
                            <i class="fa fa-circle-o"></i> <span>Edit Contact page</span>
                        </a>
                    </li>
                    <li id="subject">
                        <a href="{{route('admin.subject.list')}}">
                            <i class="fa fa-circle-o"></i> <span>View subject</span>
                        </a>
                    </li>
                </ul>
            </li> 

            <li id="profile">
                <a href="{{route('admin.profile')}}">
                    <i class="fa fa-diamond"></i> <span>{{tr('account')}}</span>
                </a>
            </li>


            <li class="treeview" id="mail_camp">

                <a href="#">
                    <i class="fa fa-envelope"></i> <span>{{tr('mail_camp')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li>
                        <a href="{{route('admin.add.mailcamp')}}">
                            <span>{{tr('send_mail')}}</span> 
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>{{tr('send_updates')}}</span> 
                        </a>
                    </li>
                </ul>
            </li>

            @if(Auth::guard('admin')->user()->user_type == 1)
            <li class="treeview {{ Request::is('admin/seo/*') ? 'active' : '' }}" id="seo_dashboard">

                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Seo</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    <li id="seo_dashboard" class="{{ Request::is('admin/seo/dashboard') ? 'active' : '' }}"><a href="{{url('admin/seo/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>

                    <li id="sitemap" class="{{ Request::is('admin/seo/dashboard/sitemap') ? 'active' : '' }}"><a href="{{url('admin/seo/dashboard/sitemap')}}"><i class="fa fa-code"></i>XML SiteMap</a></li>

                    <li id="advanced" class="{{ Request::is('admin/seo/dashboard/advanced') ? 'active' : '' }}"><a href="{{url('admin/seo/dashboard/advanced')}}"><i class="fa fa-arrow-up"></i>Advanced</a></li>

                    <li id="pages" class="{{ Request::is('admin/seo/pages') ? 'active' : '' }}"><a href="{{url('admin/seo/pages')}}"><i class="fa fa-file"></i>Seo Pages</a></li>

                    <li id="meta_tags" class="{{ Request::is('admin/seo/meta-tags') ? 'active' : '' }}"><a href="{{url('admin/seo/meta-tags')}}"><i class="fa fa-code"></i>Meta Tags</a></li>
                </ul>
            </li> 
            


            <li id="help">
                <a href="{{route('admin.help')}}">
                    <i class="fa fa-question-circle"></i> <span>{{tr('help')}}</span>
                </a>
            </li>
            @endif

            <li>
                <a href="{{route('admin.logout')}}">
                    <i class="fa fa-sign-out"></i> <span>{{tr('sign_out')}}</span>
                </a>
            </li>

        </ul>

    </section>

    <!-- /.sidebar -->

</aside>
