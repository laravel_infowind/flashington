@extends('layouts.admin')

@section('title', tr('add_subject'))

@section('content-header', tr('add_subject'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.categories')}}"><i class="fa fa-suitcase"></i> {{tr('categories')}}</a></li>
    <li class="active">{{tr('edit_category')}}</li> -->
@endsection


@section('styles')

    <link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">

@endsection

@section('content')

@include('notification.notify')

<div class="main-content">
    @if(Setting::get('admin_delete_control'))
    <form class="form-horizontal" role="form">
    @else
    <form class="form-horizontal" action="{{route('admin.save.discussion.subject')}}" method="POST" enctype="multipart/form-data" role="form">
    @endif
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_subject')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <input type="hidden" name="id" value="">
                                    <input type="hidden" value="{{$_GET['video_id']}}" name="video_id">
                                    <div class="form-group">
                                        <label for="name">{{tr('subject')}}*</label>
                                        <input type="text" required class="form-control"  pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" value="" id="name" name="subject" placeholder="{{tr('enter')}} {{tr('subject')}}">
                                    </div>
                                    <div class="row __web-inspector-hide-shortcut__">
                              

                                    <div class="form-group col-sm-12">
                                        <label for="page_robot_index">Discussion Category</label>
                                        <select class="form-control" name="category" id="page_robot_index">
                                            <option value="">Select Category
                                            </option>
                                            @foreach($discussion as $val)
                                            <option value="{{$val->id}}">
                                            {{$val->name}}
                                            </option>
                                            @endforeach
                                        </select>
                                       </div>
                                   </div>
                                    <div class="form-group">
                                        <label for="description">{{tr('message')}}*</label>
                                        <textarea required class="form-control" id="ckeditor" name="message" placeholder="{{tr('description')}}"></textarea>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($topic) !!}

                            @if($topic->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($topic->updated_at)). ' at '. date('H:i', strtotime($topic->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Update</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>

@endsection

@section('scripts')

<script src="{{asset('admin-css/plugins/iCheck/icheck.min.js')}}"></script>

    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

        $(document).ready(function() {
            $(document).on('keyup', '#name', function(){
                var heading = $(this).val();
                $('#page_title').val(heading);
                $('input[name="meta[5]"]').val(heading);
                $('input[name="meta[26]"]').val(heading);
            });
        });

    </script>
@endsection