@extends('layouts.admin')
@section('title', 'Edit Meta Tag')
@section('content-header', 'Meta Tags')
@section('breadcrumb')
<!-- <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> {{tr('home')}}</a>
</li>
<li class="breadcrumb-item"><a href="{{route('seo::meta-tags.index')}}"> <i class="fa fa-code"></i> Meta Tags</a></li>
<li class="breadcrumb-item"> Edit Meta Tag</li> -->
@endsection
@section('content')

<form action="{{ route('seo::meta-tags.store') }}/{{ $model->id }}" method="POST">
<div class="main-content">
    <div class="row">
        <div class="col-md-8">
            <div class="boxx">
                <div class="box-bodyy">
                    <div class="card mb-20">
                        <div class="card-header">
                            <label for="title">Edit Meta Tag</label>
                        </div>
                        <div class="card-body">
                            <div class="form-body">
                                @include('seo::forms.meta_tag',[
                                    'route'=>route('seo::meta-tags.update',$model->id),
                                    'method'=>'PUT'
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <div class="card mb-20">
                <div class="card-header">
                    <label for="">Publish</label>
                </div>
                <div class="card-body">
                    <div class="publish_card">
                    {!! App\Helpers\Helper::published_block($model) !!}
                    
                    @if($model->id)
                        <div class="publish_item mb-20">
                            <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($model->updated_at)). ' at '. date('H:i', strtotime($model->updated_at)) }}</span></label>
                        </div>
                    
                    @endif
                    </div>
                </div>
                <div class="card-footer">
                    <div class="publish_item">
                        <div class="box-tools pull-left" style="width: 30%">
                        <input type="submit" class="btn btn-primary" value="Update"/>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>
</form>
@endSection