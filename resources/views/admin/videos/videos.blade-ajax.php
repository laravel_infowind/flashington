@extends('layouts.admin')

@section('title', tr('view_videos'))

@section('content-header')

{{tr('view_videos')}}

@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-video-camera"></i> {{tr('view_videos')}}</li>
@endsection



@section('content')

    @include('notification.notify')

    


	<div class="row">

        <div class="col-lg-12">

          <div class="box box-primary">

          	<div class="box-header label-primary">

                <b style="font-size:18px;">{{tr('view_videos')}}</b> 

                <a href="{{route('admin.videos.create')}}" class="btn btn-default pull-right">{{tr('add_video')}}</a>

                 <!-- EXPORT OPTION START --> 

                

                <!-- EXPORT OPTION END -->
            </div>

            <div class="box-body">

            	<div class=" table-responsive"> 
            		

	              	<table id="" class="table table-bordered table-striped data-table">

							<thead>
						    <tr>
						    	<th style="vertical-align: 0px;">{{tr('id')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('title')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('revenue')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('ppv')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('category')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('sub_category')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('genre_name')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('viewers_cnt')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('is_banner')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('position')}}</th>
						    	
						    	<th style="vertical-align: 0px;">{{tr('slider_video')}}</th>
						    	
						    	<th style="vertical-align: 0px;">{{tr('uploaded_by')}}</th>
						    	<th style="vertical-align: 0px;">Publish Date</th>
						    	<th style="vertical-align: 0px;">{{tr('status')}}</th>
						    	<th style="vertical-align: 0px;">{{tr('action')}}</th>
						    </tr>
						</thead>

						<tbody>
							

				</div>
            </div>
          </div>
        </div>
    </div>

@endsection



@section('scripts')
<script type="text/javascript">
	
function loadFile(event, id){
    var reader = new FileReader();

    reader.onload = function(){
      var output = document.getElementById(id);
      output.src = reader.result;
    };
    reader.readAsDataURL(event.files[0]);
}

$(function () {
    getVideosList();


});

function getVideosList() {

    var table = $('.data-table').DataTable({
		
		processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url: "{{ url('admin/videos-list') }}",
            // data: function (d) {
            //     d.status = $('#status').val(),
            //         d.date_between = $('#filter-date-range').val()
            // }
        },
        columns: [
		    {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'title',
                name: 'title',
                searchable: true
            },
            {
                data: 'revenue',
                name: 'revenue',
                searchable: true
            },
			{
                data: 'ppv',
                name: 'ppv',
                orderable: false,
                searchable: false
            },
            {
                data: 'category',
                name: 'category',
                orderable: false,
                searchable: false
            },
			 {
                data: 'sub_category',
                name: 'sub_category',
                orderable: false,
                searchable: false
            },
			{
                data: 'genre_name',
                name: 'genre_name',
                orderable: false,
                searchable: false
            },
            {
                data: 'viewers_cnt',
                name: 'viewers_cnt',
                orderable: false,
                searchable: false
            },
            {
                data: 'is_banner',
                name: 'is_banner',
                orderable: false,
                searchable: false
            },
            {
                data: 'position',
                name: 'position',
                orderable: false,
                searchable: false
            },
            {
                data: 'slider_video',
                name: 'slider_video',
                orderable: false,
                searchable: false
            },
            {
                data: 'uploaded_by',
                name: 'uploaded_by',
                orderable: false,
                searchable: false
            },
            {
                data: 'release_date',
                name: 'release_date',
                orderable: false,
                searchable: false
            },
            {
                data: 'status',
                name: 'status',
                orderable: false,
                searchable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ],
     
    });
}
</script>
<script>
	// var table = $('#datatable-withoutpagination').DataTable( {
 //        orderCellsTop: true,
 //        fixedHeader: true
 //    } );
	
	
</script>
@endsection
