@extends('layouts.admin')

@section('title', tr('help'))

@section('content-header', tr('help'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-question-circle"></i> {{tr('help')}}</li>
@endsection

@section('content')

<div class="row">

    <div class="col-md-12">

    	<div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{tr('help')}}</h3>
            </div>

            <div class="box-body">

            	<div class="card">


			       	<div class="card-head style-primary help-heading">
			            <header>{{tr('hi_there')}}</header>
			        </div>

            		<div class="card-body help">
		                <p>
		                 {{tr('thanks_for_choosing_streamhash')}}
		                </p>

		                <p>
		                 {{tr('any_changes_your_site')}}
		                </p>

		                <a href="https://www.facebook.com/StreamHash/" target="_blank">
		                	<span><i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook</span>
		                </a>
		                &nbsp;

		                <a href="https://twitter.com/StreamHash" target="_blank">
		                	<span><i class="fa fa-twitter"></i>&nbsp;&nbsp;Twiter</span>
		                </a>
		                &nbsp;

		                <a href="skype:contact@streamhash.com?chat" target="_blank">
		                	<span><i class="fa fa-skype" aria-hidden="true"></i>&nbsp;&nbsp;Skype</span>
		                </a>
		                &nbsp;

		                <a href="mailto:contact@streamhash.com" target="_blank">
		                	<span><i class="fa fa-envelope"></i>&nbsp;&nbsp;Message</span>
		                </a>

			             &nbsp;


			             <p>{{tr('help_notes_streamhash')}}</p>

              			<a href="#" target="_blank">
              				<span><i class="fa fa-money"></i>&nbsp;&nbsp;Money Box</span>
              			</a>

						<p>{{tr('cheers')}}</p>

            		</div>

        		</div>

    		</div>
        </div>

    </div>

</div>
@endsection
