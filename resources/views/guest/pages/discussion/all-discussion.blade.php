@extends('guest.layout.app')

<?php 
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
        @section('title', Setting::get('site_name').' | '. 'Movies')
        @section('description', Setting::get('meta_description'))
        @section('keywords', Setting::get('meta_keywords'))
        @section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')
<section class="home-pg-movie ptb80">
    <div class="discussion-main">
        <div class="discussion-sidebar">
            @include('guest.pages.discussion.sidebar')
        </div>
        <div class="discussion-content">
            <div class="container container-main">
                <div class="row movie_heading">
                    <div class="col col-md-12 col-sm-12">
                        @if(!empty($post))
                        
                        <h3 class="mmt_title discussion_title"><a href="{{ url('/discuss/movies') }}">Discuss</a> → <a href="{{ url('/discuss/movies') }}">Movies</a></h3>
                        <div class="">
                            <div class="category-discussion">
                                <table width="100%" class="new space">
                                    <thead>
                                        <tr>
                                            <th>Subject</th>
                                            <th>Status</th>
                                            <th>Replies</th>
                                            <th>Last Reply</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($post as $value)
                                            <?php $replies = get_reply_on_topic($value->id);?>
                                            <tr class="open">
                                                <td class="subject">
                                                    <div class="post_info">
                                                        <div class="flex_wrapper">
                                                            <div class="avatar_wrapper">
                                                                <span class="avatar thirty-two">
                                                                    <a href="" alt="{{ $value->user->name }}">
                                                                        <img width="45px" loading="lazy" class="avatar"
                                                                            src="{{ url('placeholder.png') }}">
                                                                    </a>
                                                                </span>
                                                            </div>
                                    
                                                            <div class="link_wrapper">
                                                                <a class="topic"
                                                                    href="{{ url('movie/'.$value->admin_video_id.'/discuss/subject/'.$value->id) }}">{{ $value->subject }}
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="inner_item adult_false">
                                                        <p>
                                                            <span>↪︎</span>
                                                
                                                            </p><div class="image_wrapper">
                                                            <img loading="lazy" class="poster" src="{{ $value->video->default_image }}" alt="Aliens">
                                                        </div>
                                                        <a href="{{ url('movies/'.$value->video->unique_id) }}">{{ $value->video->title }}</a>
                                                        <p></p>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($value->is_closed == 0)
                                                        <p class="status discussion-open">Open</p>
                                                    @else
                                                        <p class="status discussion-closed">Closed</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    <p>{{ $replies->childs->count() }}</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        {{ (!empty($replies) ? date('M d, Y', strtotime($replies->created_at)) : '')  }}<br>by <span class="username"><a
                                                        href="">{{ (!empty($replies) ? $replies->user->name : '') }}</a></span>
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection