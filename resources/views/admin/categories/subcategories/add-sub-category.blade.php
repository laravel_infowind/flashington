@extends('layouts.admin')

@section('title', tr('add_sub_category'))

@section('content-header')

    <span style="color:#1d880c !important">{{$category->name}} </span> - {{tr('add_sub_category') }}

@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.categories')}}"><i class="fa fa-suitcase"></i> {{tr('categories')}}</a></li>
    <li><a href="{{route('admin.sub_categories' , array('category' => $category->id))}}"><i class="fa fa-suitcase"></i> {{tr('sub_categories')}}</a></li>
    <li class="active"><i class="fa fa-suitcase"></i> {{tr('add_sub_category')}}</li>
@endsection

@section('content')

@include('notification.notify')

    <div class="row">

        <div class="col-md-10">

            <div class="box box-primary">

                <div class="box-header label-primary">
                    <b style="font-size:18px;">{{tr('add_sub_category')}}</b>
                    <a href="{{route('admin.sub_categories' , array('category' => $category->id))}}" class="btn btn-default pull-right">{{tr('sub_categories')}}</a>
                </div>

                <form class="form-horizontal" action="{{route('admin.save.sub_category')}}" method="POST" enctype="multipart/form-data" role="form">

                    <div class="box-body">

                        <input type="hidden" name="category_id" value="{{$category->id}}">

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 control-label">*{{tr('name')}}</label>
                            <div class="col-sm-10">
                                <input type="text" required   pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" class="form-control" id="name" name="name" placeholder="{{tr('name')}}" value="{{old('name')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-sm-2 control-label">*{{tr('description')}}</label>
                            <div class="col-sm-10">
                                <textarea required class="form-control" id="ckeditor" name="description" placeholder="{{tr('description')}}" >{!! old('description') !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="picture1" class="col-sm-2 control-label">*{{tr('image')}}</label>

                            <div class="col-sm-10">
                                <img id="image_preview" style="width: 100px;height: 100px;display: none">

                                <input type="file" accept="image/jpeg, image/png" id="picture1" name="picture1" placeholder="{{tr('picture1')}}" onchange="loadFile(this,'image_preview')">
                                 <p class="help-block">{{tr('image_validate')}} {{tr('image_square')}}</p>
                            </div>
                        </div>

                        @seoForm($sub_category)
                    </div>

                    <div class="box-footer">
                        <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button>
                        @if(Setting::get('admin_delete_control'))
                            <a href="#" class="btn btn-success pull-right" disabled>{{tr('submit')}}</a>
                        @else
                            <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
                        @endif
                    </div>
                </form>
            
            </div>

        </div>

    </div>

@endsection

@section('scripts')

    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

        $(document).ready(function() {
            $(document).on('keyup', '#name', function(){
                var heading = $(this).val();
                $('#page_title').val(heading);
                $('input[name="meta[5]"]').val(heading);
                $('input[name="meta[26]"]').val(heading);
            });
        });

    </script>
@endsection