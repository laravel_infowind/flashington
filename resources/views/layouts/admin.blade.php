<!Doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="{{Setting::get('meta_description')}}">
    <meta name="keywords" content="{{Setting::get('meta_keywords')}}">

    <meta name="author" content="{{Setting::get('meta_author')}}">

    <link rel="icon" href="@if(Setting::get('site_icon')) {{ Setting::get('site_icon') }} @else {{asset('favicon.png') }} @endif">

    <link rel="stylesheet" href="{{ asset('admin-css/bootstrap/css/bootstrap.min.css')}}">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('admin-css/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin-css/plugins/datatables/dataTables.bootstrap.css')}}">
    <!-- Select Multiple dropdown -->
    @yield('mid-styles')

    <link rel="stylesheet" href="{{ asset('admin-css/plugins/select2/css/select2.min.css')}}">

     <!-- Theme style -->
    
    <link rel="stylesheet" href="{{ asset('admin-css/dist/css/AdminLTE.css') }}">
    <link rel="stylesheet" href="{{ asset('admin-css/dist/css/skins/_all-skins.min.css')}}">

    <link rel="stylesheet" href="{{ asset('admin-css/dist/css/custom.css')}}">
     <!-- Datepicker-->
    <link rel="stylesheet" href="{{asset('admin-css/plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/plugins/tokenize2-1.1-dist/tokenize2.min.css')}}">

    <link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">

    <style>
        .popover {
            max-width: 500px;
        }

        .popover-content {
            padding: 10px 5px;

        }

        .popover-list li{

            margin-bottom: 5px;

        }
        .column {

            position: fixed;top:0;left: 0;bottom: 0;right: 0;display: flex; justify-content: center; align-items: center;background: #fff !important;opacity: 0.6;
        }

        .loader-form {
            position: relative;width: 100%;height: 100%;z-index: 9999;;
        }
    </style>

    @yield('styles')


    <?php echo Setting::get('header_scripts'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

    <div class="loader-form" style="display: none;">
        <div class="column">
            <div class="loader-container animation-5">
              <div class="shape shape1"></div>
              <div class="shape shape2"></div>
              <div class="shape shape3"></div>
              <div class="shape shape4"></div>
            </div>
        </div>

    </div>


    <div class="wrapper">

         <!-- popup -->
         @include('layouts.admin.header')
         @include('layouts.admin.nav')

        <div class="content-wrapper">

            <section class="content-header">
                <h1>@yield('content-header')<small>@yield('content-sub-header')</small></h1>
                <ol class="breadcrumb">@yield('breadcrumb')</ol>
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>

        </div>
    </div>

    
    <!-- jQuery 2.2.0 -->
    <script src="{{asset('admin-css/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

    <!-- Bootstrap 4.1.3 -->
    <script src="{{asset('admin-css/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('admin-css/plugins/datatables/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('admin-css/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

    <!-- Select2 -->
    <script src="{{asset('admin-css/plugins/select2/js/select2.full.min.js')}}"></script>
    
    <!-- InputMask -->
    <script src="{{asset('admin-css/plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script src="{{asset('admin-css/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>

    <script src="{{asset('admin-css/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

    <!-- SlimScroll -->
    <script src="{{asset('admin-css/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('admin-css/plugins/fastclick/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('admin-css/dist/js/app.min.js')}}"></script>

    <!-- jvectormap -->
    <script src="{{asset('admin-css/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>

    <script src="{{asset('admin-css/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <script src="{{asset('admin-css/plugins/chartjs/Chart.min.js')}}"></script>

    <!-- Datapicker -->
    <script src = "{{asset('admin-css/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    <script src="{{asset('admin-css/plugins/tokenize2-1.1-dist/tokenize2.min.js')}}"></script>

    <script src="{{asset('admin-css/plugins/iCheck/icheck.min.js')}}"></script>

    <script src="{{asset('admin-css/dist/js/demo.js')}}"></script>

    <!-- page script -->
    <script>

        $(document).ready(function(){
            $('#help-popover').popover({
                html : true,
                content: function() {
                    return $('#help-content').html();
                }
            });

            $('.table-row-actions a').each(function () {
                this.className = this.textContent;
            });
        });

        $(function () {

            $("#example1").DataTable();

            // $("#datatable-withoutpagination").DataTable({
            //      "paging": false,
            //      "lengthChange": false,
            //      "language": {
            //            "info": ""
            //     }
            // });

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

    <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
    <script>
        var editor = CKEDITOR.replace( 'ckeditor' , {
            extraPlugins: 'colorbutton,colordialog',
            allowedContent: true
        });

        editor.on( "pluginsLoaded", function( event ){
            editor.on( 'contentDom', function( evt ) {
                var editable = editor.editable();                   
                editable.attachListener( editable, 'keyup', function( e ) { 
                    // do something
                    var seo_description = editable.getData(evt).replace(/<[^>]*>|&nbsp;/g, '');
                    $('#page_description').val(seo_description);
                    $('input[name="meta[6]"]').val(seo_description);
                    $('input[name="meta[25]"]').val(seo_description);
                });
            }); 
        });

        $(document).on('keyup', '#heading', function(){
			var heading = $(this).val();
			$('#page_title').val(heading);
			$('input[name="meta[5]"]').val(heading);
			$('input[name="meta[26]"]').val(heading);
		});

    </script>

    @yield('scripts')

    <script type="text/javascript">
        $("#{{$page}}").addClass("active");
        @if(isset($sub_page)) $("#{{$sub_page}}").addClass("active"); @endif
    </script>

    <script type="text/javascript">

        $(document).ready(function() {
        $('#expiry_date').datepicker({
            autoclose:true,
            format : 'dd-mm-yyyy',
            startDate: 'today',
        });
    });

    </script>
    <script type="text/javascript">
    var upload_video;
        $(function () {
            upload_video = '{{ (Route::currentRouteName() == "admin.videos.create" || Route::currentRouteName() == "admin.videos.edit") ? true : false }}';
            
            if (!upload_video) {
                //Initialize Select2 Elements
                $(".select2").select2();
            }

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd:mm:yyyy", {"placeholder": "hh:mm:ss"});
            //Datemask2 mm/dd/yyyy
            // $("#datemask2").inputmask("hh:mm:ss", {"placeholder": "hh:mm:ss"});
            //Money Euro
            $("[data-mask]").inputmask();

             //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
              checkboxClass: 'icheckbox_minimal-blue',
              radioClass: 'iradio_minimal-blue',
               increaseArea : '20%'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
              checkboxClass: 'icheckbox_minimal-red',
              radioClass: 'iradio_minimal-red',
               increaseArea : '20%'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
              checkboxClass: 'icheckbox_flat-green',
              radioClass: 'iradio_flat-green',
              increaseArea : '20%'

            });

             //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

        });

// published on timestamp
$(document).ready(function(){
    if (!upload_video) {
        $(document).on('click', '.edit-timestamp', function(){
            $(this).hide();
            $('#timestampdiv').slideDown('fast');
        });

        $(document).on('click', '.cancel-timestamp', function(){
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });

        $(document).on('click', '.save-timestamp', function(){
            var today_date = new Date();
            var aa = $('#aa').val(),
                mm = $('#mm').val(),
                jj = $('#jj').val(),
                hh = $('#hh').val(),
                mn = $('#mn').val(),
                newD = new Date(aa, mm - 1, jj, hh, mn);

            event.preventDefault();

            if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
                $('.timestamp-wrap').addClass('form-invalid');
                return;
            } else {
                $('.timestamp-wrap').removeClass('form-invalid');
            }

            var month = $('#mm option[value="' + mm + '"]').attr('data-text');
            var date = parseInt(jj, 10);
            var year = aa;
            var hour = ('00' + hh).slice(-2);
            var min = ('00' + mn).slice(-2);
            
            var date_string = month + ' '+ date + ',' + ' ' + year + ' at ' + hour + ':' + min; 
            var publish_time = year + '-'+ (1+ newD.getMonth()) + '-' + date +' ' + hour + ':' + min + ':00';
            if (today_date >= newD) {
                $('#created_at').val(publish_time);
                $('#timestamp').html('<label>Published on:'+ ' <span class="published_on">' + date_string + '</span></label>');
            } else {
                $('#created_at').val(publish_time);
                $('#timestamp').html('<label>Scheduled for:'+ ' <span class="published_on">' + date_string + '</span></label>');
            }
            
            // Move focus back to the Edit link.
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    }
});


    /** add active class and stay opened when selected */
    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.sidebar-menu a').filter(function() {
    return this.href == url;
    }).parent().addClass('active');

    // for treeview
    $('ul.treeview-menu a').filter(function() {
    return this.href == url;
    }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
</script>
    

    <?php echo Setting::get('body_scripts'); ?>


</body>

</html>
