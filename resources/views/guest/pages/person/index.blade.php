@extends('guest.layout.app')
@section('title', Setting::get('site_name'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')

<section class="search-result home-pg-movie ptb80">
    <div class="container container-main">
        <div class="row cat_top_head person_profile">
            <div class="col-md-3">
                <div class="image_content">
                    @if(!empty($person_detail->profile_path))
                    <img class="profile lazyload lazyloaded" src="{{ $person_detail->profile_path }}">
                    @else
                    <img class="channel_detailimg" src="{{ url('placeholder.png') }}"
                        alt="{{ Setting::get('site_name')}}">
                    @endif
                </div>
                <div class="column">

                    <div class="social_links">
                    </div>
                    <h3><bdi>Personal Info</bdi></h3>

                    <p><strong><bdi>Known For</bdi></strong> {{ (!empty($person_detail->known_for_department)) ? $person_detail->known_for_department : '-' }}</p>
                    <p><strong><bdi>Known Credits</bdi></strong> 
                        {{ $known_credits }}
                    </p>
                    <p><strong><bdi>Gender</bdi></strong>
                        {{ (!empty($person_detail->known_for_department) && $person_detail->gender == 1) ? 'Female' : '' }}
                        {{ (!empty($person_detail->known_for_department) && $person_detail->gender == 2) ? 'Male' : '' }}
                        {{ (!empty($person_detail->known_for_department) && $person_detail->gender == 0) ? '-' : '' }}
                    </p>
                    <p class="full">
                        <strong><bdi>Birthday</bdi></strong>
                        {{ (!empty($person_detail->birthday)) ? $person_detail->birthday : '-' }}
                    </p>
                    @if(!empty($person_detail->deathday))
                    <p class="full">
                        <strong><bdi>Day of Death</bdi></strong>
                        {{ (!empty($person_detail->deathday)) ? $person_detail->deathday : '-' }}
                    </p>
                    @endif
                    <p class="full"><strong><bdi>Place of Birth</bdi></strong>
                        {{ (!empty($person_detail->place_of_birth)) ? $person_detail->place_of_birth : '-' }}
                    </p>
                    <p class="full true">
                        <strong><bdi>Also Known As</bdi></strong>
                    </p>
                    @if(!empty($person_detail->also_known_as))
                    @if(!empty(json_decode($person_detail->also_known_as, true)) && is_array(json_decode($person_detail->also_known_as, true)))
                    <ul class="facts">
                        @foreach(json_decode($person_detail->also_known_as, true) as $value)
                        <li class="additionalName">{{$value}}</li>
                        @endforeach
                    </ul>
                    @else
                    <p>{{!empty($person_detail->also_known_as)?((empty(json_decode($person_detail->also_known_as, true)) && is_array(json_decode($person_detail->also_known_as, true)))?'-':$person_detail->also_known_as):'-'}}</p>
                    @endif
                    @else
                    <p>-</p>
                    @endif
                </div>
            </div>
            <div class="col-md-9">
                <div class="title" dir="auto">
                    <h2 class="title"><a href="">{{ (!empty($person_detail->name))  ? $person_detail->name : '-' }}</a></h2>
                </div>
                <div class="biography" dir="auto">
                    <h3 dir="auto">Biography</h3>
                    <div dir="auto" class="biography false">
                        @if(!empty($person_detail->biography))
                        <p>{!! $person_detail->biography !!}</p>
                        @else
                        <p>We don't have a biography for {{ (!empty($person_detail->name))  ? $person_detail->name : '-' }}.</p>
                        @endif
                    </div>
                </div>
                
                @if(!empty($known_for_movies))
                <div id="known_for">
                    <h3 dir="auto">Starring In</h3>
                    <div id="known_for_scroller" class="scroller_wrap should_fade is_fading">
                        <ul class="horizontal_media_list scroller">
                            @php $i = 0; $tmdb_movies_arr = []; @endphp
                            @foreach ($known_for_movies as $key=>$item)
                                <?php $tmdb_movie = check_tmdb_movie($item['id']); 
                                if ($tmdb_movie) {
                                    if(!in_array($tmdb_movie, $tmdb_movies_arr)) {
                                    $tmdb_movies_arr[] = $tmdb_movie;
                                    $poster_url = url('placeholder.png');
                                    $get_image = @GetImageSize($tmdb_movie->default_image);
                                    
                                    if($get_image) {
                                        $poster_url = $tmdb_movie->default_image;
                                    }
                                ?>
                                <li class="account_adult_false item_adult_false">
                                    <div class="image">
                                        <a href="{{ (!$tmdb_movie) ? 'javascript:void(0)' : url('movies/'.$tmdb_movie->unique_id) }}">
                                            <img loading="lazy" class="poster" src=" {{ $poster_url }}"  alt="{{ (!empty($item['title'])) ? $item['title'] : $item['original_name'] }}" width="130px" height="194px">
                                        </a>
                                    </div>
                                    <p><a class="title" href="{{ (!$tmdb_movie) ? 'javascript:void(0)' : url('movies/'.$tmdb_movie->unique_id) }}"><bdi>{{ (!empty($item['title'])) ? $item['title'] : $item['original_name'] }}</bdi></a></p>
                                </li>
                                <?php } }?>
                                @php $i++; @endphp
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                
                @if(!empty($department))
                    @foreach($department as $key => $value)
                    <div class="cast_credits">
                        <h3 dir="auto">{{$key}} ({{ count($value) }})</h3>
                        <ul>
                            <?php 
                                $price = array();
                                foreach ($value as $key => $row)
                                {
                                    if(!empty($row['release_date'])) {
                                        $release_date = date('Y', strtotime($row['release_date']));
                                    } else if(!empty($row['first_air_date'])) {
                                        $release_date = date('Y', strtotime($row['first_air_date']));
                                    } else {
                                        $release_date = '-';
                                    }
                                    
                                    $price[$key] = strtotime($release_date);
                                    $value[$key]['year'] = $release_date;
                                }
                                array_multisort($price, SORT_DESC, $value);    
                            ?>
                            @foreach($value as $row)
                            <?php $tmdb_movie = check_tmdb_movie($row['id']);?>
                            <li>
                                @if(!$tmdb_movie)
                                    <div class="working-movie-link">
                                        <span class="year">{{ $row['year'] }}</span>
                                        <span><i class="fa fa-circle-o"></i></span>
                                        <span>{{ (!empty($row['title'])) ? $row['title'] : $row['original_name'] }}</span>
                                        
                                    @else
                                    <a href="{{ url('movies/'.$tmdb_movie->unique_id) }}">
                                        <span class="year">{{ $row['year'] }}</span>
                                        <span><i class="fa fa-circle-o"></i></span>
                                        <span>{{ (!empty($row['title'])) ? $row['title'] : $row['original_name'] }}</span>
                                    </a>
                                @endif
                                <div class="k-tooltip-content">
                                    <div class="media_tooltip_popup">
                                        <div class="row m-0">
                                            <div class="col-md-3 p-2">
                                                <div class="media-image">
                                                    <a href="#"><img src="" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="col-md-9 p-2">
                                                <div class="media-content">
                                                    <div class="media-head">
                                                        <div class="media-title"><a href="#">Munich</a></div>
                                                        <div class="rounded rating_block">
                                                            <i class="fa fa-star"></i>
                                                            <span class="rating">0.0</span>
                                                        </div>
                                                    </div>
                                                    <div class="k-tooltip-overview">
                                                        <p>An adaptation of Robert Harris' thriller set on the eve of World War 2.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                @endif
            </div>

        </div>

    </div>
    </div>
</section>
@endsection