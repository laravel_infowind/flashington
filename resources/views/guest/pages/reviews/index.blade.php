@extends('guest.layout.app')

<?php
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
        @section('title', Setting::get('site_name').' | '. 'TV Shows')
        @section('description', Setting::get('meta_description'))
        @section('keywords', Setting::get('meta_keywords'))
        @section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')

<section class="home-pg-movie ptb80">
    <div class="container container-main">
        <div class="row cat_top_head">
            @if(!empty($videos))
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="cat_top_left_img">
                        @if (!empty($videos->default_image))
                            <a href="{{ url('movies/'.$videos->unique_id) }}"><img class="channel_detailimg" src="{{ $videos->default_image }}" alt="{{ Setting::get('site_name'). ' | ' .$videos->title}}"></a>
                        @else
                            <a href="{{ url('movies/'.$videos->unique_id) }}"><img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$videos->title}}"></a>
                        @endif
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-6">
                    <div class="cat_top_right_cont">
                        <h1 class="channel_name_heading"><a href="{{ url('movies/'.$videos->unique_id) }}">{{ $videos->title }}</a></h1>
                        <p class="cat_top_details">{!! $videos->details !!}</p>
                    </div>
                </div>
            
            @endif
    </div>

    <div class="user_reviews_section">
        <div class="row movie_heading">
            <div class="col col-md-6 col-sm-12">
            <h3 class="mmt_title title">{{ $user_reviews->total() }} Reviews</h3>
            </div>
        </div>

        <div class="review-filters">
            <form method="GET" action="">
                <div class="filters">
                    <label class="faceter-facet">
                        <input name="spoilers" value="hide" class="element-check" type="checkbox" onchange="this.form.submit()" {{ (Request::query('spoilers') == 'hide') ? 'checked' : '' }}>
                        <span class="lister-widget-sprite lister-checkbox"></span>
                        <span class="faceter-facets-text">Hide Spoilers</span>
                    </label>
                </div>
                <div class="rating-filter filters">
                    <div class="lab">Filter by Rating : </div>
                    <div>
                        <select class="lister-filter-by-select" name="rating" onchange="this.form.submit()">
                            <option value="0" selected="selected">Show All</option>
                            <option value="1" {{ (Request::query('rating') == 1) ? 'selected' : '' }}>1 Star</option>
                            <option value="2" {{ (Request::query('rating') == 2) ? 'selected' : '' }}>2 Stars</option>
                            <option value="3" {{ (Request::query('rating') == 3) ? 'selected' : '' }}>3 Stars</option>
                            <option value="4" {{ (Request::query('rating') == 4) ? 'selected' : '' }}>4 Stars</option>
                            <option value="5" {{ (Request::query('rating') == 5) ? 'selected' : '' }}>5 Stars</option>
                            <option value="6" {{ (Request::query('rating') == 6) ? 'selected' : '' }}>6 Stars</option>
                            <option value="7" {{ (Request::query('rating') == 7) ? 'selected' : '' }}>7 Stars</option>
                            <option value="8" {{ (Request::query('rating') == 8) ? 'selected' : '' }}>8 Stars</option>
                            <option value="9" {{ (Request::query('rating') == 9) ? 'selected' : '' }}>9 Stars</option>
                            <option value="10" {{ (Request::query('rating') == 10) ? 'selected' : '' }}>10 Stars</option>
                        </select>
                    </div>
                </div>
                <div class="sort-by filters">
                    <div class="lab">Sort by : </div>
                   <div>
                    <select name="sort" class="lister-sort-by" onchange="this.form.submit()">
                        <option value="submitted" {{ (Request::get('sort') == 'submitted') ? 'selected' : '' }}>Review Date</option>
                        <option value="review" {{ (Request::get('sort') == 'review') ? 'selected' : '' }}>Review Rating</option>
                    </select>
                   </div>
                </div>
            </form>
        </div>

            <div class="append-reviews">
                @if($user_reviews->count() > 0)
                @foreach($user_reviews as $review)
                <div class="user_reviews">
                    {{-- <div class="author_img">
                        <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$review->user->name}}">
                    </div> --}}
                    <div class="user-comments">
                        <div class="tinystarbar" title="{{ $review->rating }}/10">
                            <div class="rating text-left">
                                @if($review->rating >= 1)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 2)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 3)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 4)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 5)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 6)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 7)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 8)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 9)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                                @if($review->rating >= 10)
                                <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                @else
                                <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                @endif
                            </div>
                        </div>
                        <div class="comment-meta-div">
                            <div class="comment-meta">
                                <b><span>{{ $review->user->name }}</span></b> - {{ date('d F Y', strtotime($review->created_at)) }}
                            </div>
                            <div class="comment-subject">{{ $review->subject }}</div>
                            <div class="author-comment">
                                {!! $review->comment !!}
                            </div>
                            @if($review->spoilers == 'YES')
                            <span class="spoiler-warning"><strong>Warning: Spoilers</strong></span>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p>No record found</p>
                @endif
            </div>
            <div>
                <div class="all-pagination-reviews">{{ $user_reviews->links() }}</div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    .owl-prev span,
    .owl-next span {
        color: #FFF;
    }

    .owl-prev,
    .owl-next {
        position: absolute;
        top: 0;
        height: 100%;
    }

    .owl-prev {
        left: 7px;
    }

    .owl-next {
        right: 7px;
    }
</style>

@endsection