@extends('layouts.admin')

@section('title', tr('position_cast_crew'))

@section('content-header', tr('position_cast_crew'))

@section('breadcrumb')
    
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    
    <li class="active"><i class="fa fa-users"></i> {{tr('position_cast_crew')}}</li>
    
@endsection

@section('content')

    @include('notification.notify')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                
                <div class="box-header label-primary">
                    
                    <b style="font-size:18px;">{{tr('position_cast_crew')}}</b>

                    <a href="{{route('admin.cast_crews.add')}}" class="btn btn-default pull-right">{{tr('add_cast_crew')}}</a>

                </div>

                <div class="box-body">

                    <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Cast</th>
                                <th>Count</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Actors</td>
                                <td><a href="{{ url('admin/cast_crews/position/actors') }}">{{ $actor_count }}</a></td>
                            </tr>
                            <tr>
                                <td>Directors</td>
                                <td><a href="{{ url('admin/cast_crews/position/directors') }}">{{ $director_count }}</a></td>
                            </tr>
                            <tr>
                                <td>Writers</td>
                                <td><a href="{{ url('admin/cast_crews/position/writers') }}">{{ $writer_count }}</a></td>
                            </tr>
                        </tbody>

                    </table>
            </div>
        </div>
    </div>
@endsection