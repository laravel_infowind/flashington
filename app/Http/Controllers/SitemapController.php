<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminVideo;
use App\SubCategory;
use Carbon\Carbon;

class SitemapController extends Controller
{
    public function generate_sitemap(){
		header("Content-Type: application/xml; charset=utf-8"); 
		echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL; 
		echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' .PHP_EOL; 
		$date = Carbon::now()->format('Y-m-d\TH:i:sP');

			echo '<url>'.PHP_EOL; 
				echo '<loc>'.url('/').'</loc>'.PHP_EOL;
				echo '<lastmod>'.$date.'</lastmod>'.PHP_EOL;
				echo '<changefreq>weekly</changefreq>'.PHP_EOL;
				echo '<priority>0.80</priority>'.PHP_EOL;
			echo '</url>'.PHP_EOL;

			echo '<url>'.PHP_EOL; 
				echo '<loc>'.url('/contact-us').'</loc>'.PHP_EOL;
				echo '<lastmod>'.$date.'</lastmod>'.PHP_EOL;
				echo '<changefreq>weekly</changefreq>'.PHP_EOL;
				echo '<priority>0.80</priority>'.PHP_EOL;
			echo '</url>'.PHP_EOL;

		$videos = AdminVideo::all();
		foreach ($videos as $key => $video) {
			echo '<url>'.PHP_EOL; 
				echo '<loc>'.url('/movies/'.$video->unique_id).'</loc>'.PHP_EOL;
				echo '<lastmod>'.$date.'</lastmod>'.PHP_EOL;
				echo '<changefreq>weekly</changefreq>'.PHP_EOL;
				echo '<priority>0.80</priority>'.PHP_EOL;
			echo '</url>'.PHP_EOL;
		}

		$sub_categories = SubCategory::where('category_id', 8)->get();
		foreach ($sub_categories as $key => $sub_category) {
			echo '<url>'.PHP_EOL; 
				echo '<loc>'.url('search?sub-category='.$sub_category->name).'</loc>'.PHP_EOL;
				echo '<lastmod>'.$date.'</lastmod>'.PHP_EOL;
				echo '<changefreq>weekly</changefreq>'.PHP_EOL;
				echo '<priority>0.80</priority>'.PHP_EOL;
			echo '</url>'.PHP_EOL;
		}

		echo '</urlset>';
    }

}