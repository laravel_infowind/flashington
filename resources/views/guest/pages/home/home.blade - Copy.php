@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')

<section id="slider" class="full-slider">
    <!-- Main Slider Code -->
    
    <div class="otmd_div">
        <div class="latest-movies">
            <!-- Add Recently Added -->
            <div class="container container-main">
                <div class="row">
                    <div class="col col-md-9 col-sm-6">
                        <h3 class="recent_title title">Recently Added</h3>
                    </div>
                    <div class="col col-md-3 col-sm-6 align-self-center text-right">
                        <a href="{{ url('recently-added') }}" class="btn btn-icon btn-main btn-effect">View All
                            <i class="ti-angle-double-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container container-main">
                <div class="row">
                    @foreach($videos_data as $data)
                    <?php 
                //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
                $movie_description = App\Helpers\Helper::truncate($data['details'], 400, '...', true, true);
                $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
                ?>
                    <div hover-id="{{ $data->unique_id }}" class="col-lg-2 col-md-3 col-sm-4 col-6 poster_div">
                        <a href="{{ url('movies/'.$data['unique_id'])  }}">
                            <div class="movie-box-1">
                                <div id="{{ $data->unique_id }}" class="poster">
                                    <div class="movie_item">
                                        <img id="{{ $data->unique_id }}" class="poster_image"
                                            src="{{ $data['default_image'] }}"
                                            alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button"
                                            data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                            data-title="
                                    <span class='popover-movie-title'>{{ $data['title'] }}</span>
                                    <div class='movie-info-row'>
                                        <span class='movie-year'>{{ $data['release_date'] }}</span>
                                        <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                        <span class='movie-stars'>
                                        @if($data['ratings'] >= 1)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 2)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 3)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 4)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 5)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                        </span>
                                    </div>" data-content="
                                    <div class='movie-description'>
                                        <div>{!! $movie_description !!}</div>
                                    </div>" data-container="body" data-html="true" data-original-title="" title="" />
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Recently Added -->
</section>



<ul id="video_categories">
    <li>{{ $sub_categories->id }}</li>
</ul>

<!-- <div class="ajax-loading">
   <span><i class="fa fa-circle clr_red"></i></span>
   <span><i class="fa fa-circle clr_white"></i></span>
   <span><i class="fa fa-circle clr_red"></i></span>
</div> -->
@endsection