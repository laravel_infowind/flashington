<div class="header_section_fluid" id="header_section">

    <header class="header header-fixed header-transparent">
        <div class="container-fluid">
            <nav class="row navbar navbar-expand-md">

                <div class="col-md-7">

                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-5 col-6">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <img src="{{ Setting::get('site_logo') }}"
                                    alt="{{ Setting::get('site_name') }} white logo" width="150" class="logo-white">
                            </a>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-7 col-6">
                            <button id="navbar_btn" class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#collapsibleNavbar">Menu
                                <span class="fa fa-bars">
                                    <!--  <span class="hamburger-inner"></span> -->
                                </span>

                            </button>

                            <span class="sr-sec">
                                <i class="fa fa-search"></i>
                            </span>
                            
                            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                <ul class="navbar-nav" id="main-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/') }}">Home</a>
                                    </li>
                                    <li class="nav-item"> 
                                        <a class="nav-link" href="{{ url('/movies') }}">Movies</a>
                                    </li>
<!--
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/music') }}">Music</a>
                                    </li>
-->
                                    <li class="nav-item"> 
                                        <a class="nav-link" href="{{ url('/tv-shows') }}">TV Shows</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('channels') }}">Channels</a>
                                    </li>
                                    <li class="nav-item d-md-none">
                                        @if(Auth::user())
                                        @if( request()->get('logout') )
                                        <form name="member_signup" id="logout-form"
                                            action="{{ url('/user/logout/create') }}" method="Post"
                                            style="display: none;">@csrf
                                        </form>
                                        <script type="text/javascript">
                                            window.onload = function(event)
                          { 
                            event.preventDefault();
                            document.getElementById('logout-form').submit();
                            //window.location.href = "http://localhost:8080/#";
                          }
                                        </script>
                                        @else
                                        <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                            class="nav-link-btn nav-link btn btn-icon btn-main btn-effect">Logout</a>
                                        <form name="member_signup" id="logout-form"
                                            action="{{ url('/user/logout/create') }}" method="Post"
                                            style="display: none;">@csrf
                                        </form>
                                        @endif
                                        @else
                                        <a class="nav-link-btn nav-link btn btn-icon btn-main btn-effect signin_click"
                                            id="">Sign In</a>
                                        <a class="nav-link-btn nav-link btn btn-icon btn-success register_click"
                                            id="">Register for Free</a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="header-right">
                        <div class="search-right">
                            <!-- Search Button -->
                            <i id="btn_search" class="fa btn fa-search"></i>
                            <div id="div_typesearch">
                                <form action="{{ url('search') }}" method="GET" class="example">
                                    <input autocomplete="off" required="" class="form-control" type="text" placeholder="Search.." name="query" id="header_search"/>
                                    
                                    <button id="btn_searchclick" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <div id="header-search-result"></div>
                                </form>
                            </div>
                            
                            <!-- Search Button -->
                        </div>
                        <div class="sign-right d-none d-md-block">
                            @if(Auth::user())
                            @if( request()->get('logout') )
                            <form name="member_signup" id="logout-form" action="{{ url('/user/logout/create') }}"
                                method="Post" style="display: none;">@csrf
                            </form>
                            <script type="text/javascript">
                                window.onload = function(event)
                        { 
                          event.preventDefault();
                          document.getElementById('logout-form').submit();
                          //window.location.href = "http://localhost:8080/#";
                        }
                            </script>
                            @else
                            <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                class="nav-link-btn nav-link btn btn-icon btn-main btn-effect">Logout</a>
                            <form name="member_signup" id="logout-form" action="{{ url('/user/logout/create') }}"
                                method="Post" style="display: none;">@csrf
                            </form>
                            @endif
                            @else
                            <a class="nav-link-btn btn btn-icon btn-main btn-effect signin_click" id="">Sign In</a>
                            <a class="nav-link-btn btn btn-icon btn-success register_click" id="">Register for Free</a>
                            @endif
                        </div>
                    </div>
                </div>

            </nav>

        </div>
    </header>

</div>
<!-- Trigger/Open The Modal -->
<!-- The Modal -->
<div class="container">
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div id="myModal" class="modal">
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="content-panel">
                                        <div id="message_signup"></div>
                                        <div id="message_forgetpass"></div>
                                        <!-- Sign In Form Start ...................................... -->
                                        <button type="button" class="close"><span data-v-fb473e1c=""
                                                aria-hidden="true">×</span></button>
                                        <div class="login-form" id="sign_in">
                                            <div class="login-form-inner grey-bg">
                                                <h5 class="login-tit">Sign in to {{ Setting::get('site_name') }}</h5>
                                                <!--  <h6 style="font-size: 12px; font-weight: 500;margin-top: 10px;
                     margin-bottom: 10px;line-height: 1.1;color: #fff;">Sign in to start watching or restarting your membership</h6> -->
                                                <form action="{{ route('admin.login.save') }}" method="post"
                                                    id="btn_submit">
                                                    @csrf
                                                    <!-- get video id for vue js -->
                                                    <input type="hidden" id="get_videoid" name="video_id">
                                                    <input type="hidden" id="get_trailerid" name="video_trailerid">
                                                    <input type="hidden" id="video_trailer" name="video_trailer">
                                                    <div class="form-group userFormGroup">
                                                        <label class="form-control-placeholder">Email address</label>
                                                        <input style="padding: 25px 20px;" required="" id="username"
                                                            class="form-control form-control-input" type="text"
                                                            name="email">
                                                    </div>
                                                    <div class="form-group userFormGroup">
                                                        <label class="form-control-placeholder">Password</label>
                                                        <input style="padding: 25px 20px;" required="" id="pass"
                                                            class="form-control form-control-input" type="password"
                                                            name="password">
                                                    </div>
                                                    <div class="form-group userFormGroup">
                                                        <button type="submit" class="form-btn">Sign In</button>
                                                    </div>
                                                </form>
                                                <div class="forgot form-txt-block">
                                                    <a class="pull-left for_password">Forgot your password?</a>
                                                </div>
                                                <div class="form-txt-block">
                                                    <a class="pull-right for_signup">New to
                                                        {{ Setting::get('site_name') }}?</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Sign In Form End........................................... -->
                                        <!-- Sign Up Form Start.......................................... -->
                                        <div class="login-form-inner" id="sign_up">
                                            <h3 class="login-tit">Register for SassyFlix</h3>
                                            <form id="signup_submit">
                                                <div class="form-group userFormGroup">
                                                    <label class="form-control-placeholder">Your Name</label>
                                                    <input style="padding: 25px 20px;"
                                                        class="form-control form-control-input" type="text" name="name"
                                                        id="name" required="">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <label class="form-control-placeholder">Your Email</label>
                                                    <input style="padding: 25px 20px;"
                                                        class="form-control form-control-input" type="email"
                                                        name="email" id="email" required="">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <label class="form-control-placeholder">Password</label>
                                                    <input style="padding: 25px 20px;"
                                                        class="form-control form-control-input" type="password"
                                                        name="password" id="password" required="">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <label class="form-control-placeholder">Confirm Password</label>
                                                    <input style="padding: 25px 20px;"
                                                        class="form-control form-control-input" type="password"
                                                        name="confirm_password" id="confirm_password" required="">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <input type="hidden" class="form-control" type="text"
                                                        name="login_by" id="login_by" value="manual">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <input type="hidden" class="form-control" type="text"
                                                        name="device_type" id="device_type" value="web">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <input type="hidden" class="form-control" type="text"
                                                        name="device_token" id="device_token" value="123456">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <button type="submit" id="sign_in_button"
                                                        class="form-btn">Register</button>
                                                </div>
                                            </form>

                                            <div class="forgot form-txt-block">
                                                <a class="pull-left for_password">Forgot your password?</a>
                                            </div>
                                            <div class="form-txt-block">
                                                <a class="pull-right for_signin"> Sign In</a>
                                            </div>
                                        </div>
                                        <!-- Sign Up Form End............................................ -->
                                        <!-- Forget Password Start....................................... -->
                                        <div class="login-form-inner" id="forget_password">
                                            <h3 class="login-tit">Forget Password</h3>
                                            <form id="password_forget">
                                                <div class="form-group userFormGroup">
                                                    <label class="form-control-placeholder">Email</label>
                                                    <input style="padding: 25px 20px;" required=""
                                                        class="form-control form-control-input" type="email"
                                                        id="useremail">
                                                </div>
                                                <div class="form-group userFormGroup">
                                                    <button type="submit" id="sign_in_button"
                                                        class="form-btn">Send</button>
                                                </div>
                                            </form>
                                            <div class="form-txt-block">
                                                <a class="pull-right for_signin"> Sign In</a>
                                            </div>
                                        </div>
                                        <!-- Forget Password End......................................... -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
