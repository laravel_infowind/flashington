<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class Subject extends Model
{
    protected $fillable = array('subject');
}
