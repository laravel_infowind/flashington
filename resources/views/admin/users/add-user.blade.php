@extends('layouts.admin')

@section('title', tr('add_user'))

@section('content-header', tr('add_user'))

@section('breadcrumb')
<!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.users')}}"><i class="fa fa-user"></i> {{tr('users')}}</a></li>
    <li class="active"><i class="fa fa-user-plus"></i> {{tr('add_user')}}</li> -->
@endsection

@section('content')

@include('notification.notify')


<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.user')}}" method="POST" enctype="multipart/form-data" role="form">
    <div class="row">
        
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_user')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">

                                    <div class="form-group">
                                        <label for="username">* {{tr('username')}}</label>
                                        <input type="text" required name="name" pattern="[a-zA-Z,0-9\s\-\.]{2,100}"
                                            title="{{tr('only_alphanumeric')}}" class="form-control" id="username"
                                            placeholder="{{tr('name')}}" value="{{old('name')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">* {{tr('email')}}</label>
                                        <input type="email" maxlength="255" required class="form-control" id="email"
                                            name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$"
                                            placeholder="{{tr('email')}}" value="{{old('email')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="mobile">* {{tr('mobile')}}</label>
                                        <input type="text" required name="mobile" class="form-control" id="mobile"
                                            placeholder="{{tr('mobile')}}" minlength="4" maxlength="16"
                                            pattern="[0-9]{4,16}" value="{{old('mobile')}}">
                                        <small style="color:brown">{{tr('mobile_note')}}</small>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">* {{tr('password')}}</label>
                                        <input type="password" required name="password" pattern=".{6,}"
                                            title="{{tr('password_notes')}}" class="form-control" id="password"
                                            placeholder="{{tr('password')}}" value="{{old('password')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="username" class=" control-label">*
                                            {{tr('password_confirmation')}}</label>
                                        <input type="password" required pattern=".{6,}" title="{{tr('password_notes')}}"
                                            name="password_confirmation" class="form-control" id="username"
                                            placeholder="{{tr('password_confirmation')}}"
                                            value="{{old('password_confirmation')}}">
                                    </div>

                                </div>

                                <input type="hidden" name="timezone" value="" id="userTimezone">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="publish_item mb-20">
                                {{-- <label>Modified on: <span class="published_on">immediately</span></label>
                                <a href="javascript:void(0)" class="edit-timestamp hide-if-no-js btn" role="button"
                                    style="display: inline-block;">
                                    <span aria-hidden="true">Edit</span>
                                </a> --}}
                                {!! App\Helpers\Helper::published_block($user) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                                @if(Setting::get('admin_delete_control'))
                                <a href="#" class="btn btn-primary" disabled>{{tr('submit')}}</a>
                                @else
                                <button type="submit" class="btn btn-primary">Publish</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>


@endsection

@section('scripts')

<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });

</script>

@endsection