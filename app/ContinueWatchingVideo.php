<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContinueWatchingVideo extends Model
{
    protected $fillable = [
        "user_id", "sub_profile_id", "admin_video_id", "duration",	"is_genre",	"position", "genre_position", "status"
    ];
}
