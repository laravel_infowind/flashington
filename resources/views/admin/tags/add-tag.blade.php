@extends('layouts.admin')

@section('title', tr('add_tag'))

@section('content-header', tr('add_tag'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.categories')}}"><i class="fa fa-suitcase"></i> {{tr('categories')}}</a></li>
    <li class="active">{{tr('add_category')}}</li> -->
@endsection

@section('styles')

    <link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">

@endsection

@section('content')

@include('notification.notify')


<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.tag')}}" method="POST" enctype="multipart/form-data" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_tag')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">

                                    <div class="form-group">
                                        <label for="name">{{tr('name')}}*</label>
                                        <input type="text" required class="form-control" pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" id="name" name="name" placeholder="{{tr('tag_name')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                        {!! App\Helpers\Helper::published_block($tag) !!}

                        
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Publish</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Publish</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>

@endsection