<?php

namespace App\Exports;

use App\PayPerView;
use Maatwebsite\Excel\Concerns\FromCollection;

class PayPerViewsExport implements FromCollection
{
    public function collection()
    {
        return PayPerView::all();
    }
}