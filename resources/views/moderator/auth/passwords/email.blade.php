@extends('layouts.admin.focused')

@section('title', tr('reset_password'))

@section('content')

    <div class="login-box-body" style="height:300px">

        <form class="form-layout" role="form" method="POST" action="{{ url('/moderator/password/email') }}">
            {{ csrf_field() }}

            <p class="text-center mb25">{{tr('enter_email_address')}}</p></br>

            <div class="form-inputs">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{tr('email_add')}}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-warning btn-block text-white" type="submit">
                            <i class="fa fa-btn fa-envelope"></i> {{tr('reset')}}
                        </button>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('moderator.dashboard')}}" class="btn btn-info btn-block">
                            <i class="fa fa-btn fa-user"></i> {{tr('login')}}
                        </a>
                    </div>
                </div>

            </div>
        </form>

    </div>

@endsection
