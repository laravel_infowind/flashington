<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Setting;

use DB;

class AdminVideoTag extends Model
{
    public function adminVideo()
    {
        return $this->belongsTo('App\AdminVideo');
    }

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }
}
