@extends('layouts.admin')

@section('title', tr('pages'))

@section('content-header', tr('pages'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-book"></i> {{tr('pages')}}</li> -->
@endsection

@section('content')

	@include('notification.notify')
	
<div class="main-content">
	<form  action="{{route('admin.pages.save')}}" method="POST" enctype="multipart/form-data" role="form">
      <div class="row">
         <div class="col-md-8">
            <div class="boxx">
               <div class="box-bodyy">
                  <div class="card mb-20">
                     <div class="card-header">
                        <label for="title">{{tr('add_page')}}</label>
                     </div>
                     <div class="card-body">
                        <div class="form-body">
							<div class="form-group">
								<label for="heading">{{tr('heading')}}*</label>
								<input type="text" class="form-control" name="heading" required id="heading" placeholder="{{tr('enter_heading')}}">
							</div>
							<div class="form-group">
								<label for="description">{{tr('description')}}*</label>
								<textarea id="ckeditor" name="description" class="form-control" required placeholder="{{tr('enter_text')}}"></textarea>
							</div>
							<div class="form-group">
								<label for="heading">Is Error Page</label>
								<div class="form-check">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" name="is_error_page" value="0">YES
									</label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" checked name="is_error_page" value="1">NO
									</label>
								</div>
							</div>
                        </div>
                     </div>
				  </div>
				  @seoForm($model)
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card mb-20">
               <div class="card-header">
                  <label for="">Publish</label>
               </div>
               <div class="card-body">
                  <div class="publish_card">
                     {!! App\Helpers\Helper::published_block($page) !!}
                  </div>
               </div>
               <div class="card-footer">
                  <div class="publish_item">
                     <div class="box-tools pull-left" style="width: 30%">
					 	<!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
	                    <button type="submit" class="btn btn-primary">{{tr('submit')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>

@endsection

