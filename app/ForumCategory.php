<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class ForumCategory extends Model
{
    public $table = "forum_category";

	public function topics()
    {
        return $this->hasMany('App\ForumTopic');
    }
}
