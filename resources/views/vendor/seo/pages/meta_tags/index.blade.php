@extends('layouts.admin')
@section('title', 'Meta Tags')

@section('content-header', 'Meta Tags')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> {{tr('home')}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('seo::meta-tags.index')}}"> <i class="fa fa-code"></i> Meta Tags</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
          <div class="box box-primary">
            <div class="box-header label-primary">
                <b style="font-size:18px;">Meta Tags</b>
                <a href="{{route('seo::meta-tags.create')}}" class="btn btn-default pull-right">Add Meta Tag</a>
            </div>
            
            <div class="box-body">
                @include('seo::tables.meta_tag_details')
            </div>

          </div>
    </div>
</div>    
@endSection