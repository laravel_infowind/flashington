@extends('layouts.admin')

@section('title', 'SEO Pages')

@section('content-header', 'SEO Pages')

@section('tools')
&nbsp;&nbsp;
@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-file"></i> SEO Pages</li>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            
            <div class="box-header label-primary">
                
                <b style="font-size:18px;">SEO Pages</b>

                <a href="{{route('seo::pages.create')}}" class="btn btn-default pull-right">Add page</a>

            </div>

            <div class="box-body">

                <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                    <div class="search-table-top"> 
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <!-- <label>Show
                                        <form method="get" action="{{ url('admin/cast-crews/index') }}" class="videos-change-pagination">
                                            <input type="hidden" name="column" value="">
                                            <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="">


                                            <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
                                                <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
                                                <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
                                                <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
                                                <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
                                            </select>
                                        </form>
                                    entries</label> -->
                                </div>
                            </div>



                            <div class="col-sm-6">
                            <form method="get" class="m-form">
                                
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control input-video-search" value="{{$_GET['search']??''}}"
                                        placeholder="Search by Page title">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-success" type="submit">Search
                                        </button>
                                    </div>
                                </div>
                                    
                            </form>
                        </div>
                    </div>
                </div>

                    <thead>
                        <tr>
                            
                            <th>
                                <a href="{{ url('admin/seo/pages?column=title&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">Title <i class="fa fa-sort<?php echo $column == 'title' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th>
                                Visit Page
                            </th>
                            <th>
                                Created at
                            </th>
                                
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($records as $record)
                        <tr>
                        <td>{{$record->getTitle()}}
                            <ul class="table-row-actions">
                                <li role="presentation">
                                    @if(Setting::get('admin_delete_control'))
                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                    @else
                                    <a role="button" href="{{route('seo::pages.edit',$record->id)}}" class="btn disabled" style="text-align: left">{{tr('edit')}}
                                    </a>
                                    @endif
                                </li> 
                                <li role="presentation"> 
                                    @if(Setting::get('admin_delete_control'))
                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('view')}}</a>
                                    @else
                                    @include('seo::forms.destroy',['route'=>route('seo::pages.destroy',$record->id)])
                                    @endif 
                                </li>
                            </ul>
                        </td>
                        <td><a target="_blank" href="{{url($record->getFullUrl())}}">Visit page</a></td>
                        <td>{{$record->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <div class="pagination-video-list">
                    
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">Showing {{ ($records->currentpage()-1)*$records->perpage()+1 }} to {{ ($records->currentpage()*$records->perpage() < $records->total() ) ? $records->currentpage()*$records->perpage() : $records->total() }} of {{ $records->total() }} entries
                            </div>
                            </div>
                        <div class="col-sm-7 text-right">
                            <div>{!! $records->render() !!}</div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>

<!-- <div class="card-body">
    <form method="get" class="m-form">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group m-form__group">
                    <div class="input-group">
                        <div class="input-group-append">
                            <select name="object" class="form-control">
                                <option value="">All</option>
                                @foreach($objects as $object)
                                <option value="{{$object}}" {{request('object')==$object?'selected':''}}>
                                    {{$object}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <input type="text" name="search" class="form-control" value="{{$_GET['search']??''}}"
                            placeholder="Search for .. product name, brand, model">
                        <div class="input-group-append">
                            <button class="btn btn-default text-primary m--font-boldest" type="submit"><i
                                    class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <div class="btn-group">
                    @if(count(config('seo.linkProviders'))>0)
                    <a class="btn btn-default btn-outline-secondary" href="{{route('seo::pages.generate')}}">Generate Page</a>
                    @endif
                
                    <a href="{{route('seo::pages.bulkEdit')}}" class="btn btn-default btn-outline-secondary">
                        <i class="fa fa-pencil-square-o"></i> Bulk Edit
                    </a>
                    <a class="btn btn-default btn-outline-secondary" href="{{route('seo::pages.create')}}"><i class="fa fa-plus"></i></a>
                </div>
            </div>
        </div>
    </form>
    <div class="seo-page-row">
        <div class="row">
            @foreach($records as $record)
            <div class="col-sm-6 mb-4">
                @include('seo::cards.page')
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="nav-right text-right seo-pages-pagination">
    {!! $records->render() !!}
</div> -->
@include('seo::modals.page_upload')
@endSection