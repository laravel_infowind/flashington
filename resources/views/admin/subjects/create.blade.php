@extends('layouts.admin')

@section('title',tr('add_subject'))

@section('content-header',tr('add_subject'))

@section('breadcrumb')

	
@endsection

@section('content')

@include('notification.notify')



<div class="main-content">
    <form action="{{route('admin.save.subject')}}" method="POST" class="form-horizontal" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_subject')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    
									<div class="form-group">
										<label for = "title">{{tr('subject')}}*</label>
										<input type="text" name="subject" role="title" min="5" max="20" class="form-control" value="{{ old('subject') }}" required placeholder="Enter subject">
									</div>

                                    <div class="form-group" style="width: 30%">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </form>

</div>


	
@endsection

