@extends('guest.layout.app')

@section('title', Setting::get('site_name').' | '. 'Contact Us')
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<section class="movie-detail-intro overlay-gradient" style="background: url({{ Setting::get('home_page_bg_image') }}); min-height: 250px; position: relative; padding-top: 165px;">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="title" style="text-align: center;color: #ffffff;">Contact Us</h1>
            @if (isset($errors) && $errors->any())
               <div class="alert alert-danger">
                  <ul>
                        @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                        @endforeach
                  </ul>
               </div>
            @endif
            
            @if (session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
            @endif
			</div>
		</div>
	</div>
</section>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<main class="contact-page ptb100">
   <div class="container">
   
      <div class="row">
 
         <div class="col-md-6 col-sm-12">
            <h3 class="title">Info</h3>
            <div class="details-wrapper">
               <p>{!! Setting::get('contact_description') !!}</p>
               <ul class="contact-details">
                  <li>
                     <i class="fa fa-phone"></i>
                     <strong>Phone:</strong>
                     <span>{{ Setting::get('contact_mobile') }}</span>
                  </li>                  
                  <li>
                     <i class="fa fa-envelope-o"></i>
                     <strong>E-Mail:</strong>
                     <span><a href="#"><span class="__cf_email__" data-cfemail="fb92959d94bb96948d929d82d5989496">{{ Setting::get('contact_email') }}</span></a></span>
                  </li>
                  <li>
                     <i class="fa fa-location-arrow"></i>
                     <strong>Address:</strong>
                     <span>{{ Setting::get('contact_address') }}</span>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-md-6 col-sm-12">
        
            <form method="POST" action="{{ url('contact-us') }}">
            	@csrf
               <div id="contact-result"></div>
               <div class="form-group">
                  <input class="form-control input-box" type="text" name="name" placeholder="Your Name" autocomplete="off" required="required">
               </div>
               <div class="form-group">
                  <input class="form-control input-box" type="email" name="email" placeholder="john@example.com" autocomplete="off" required="required">
               </div>
               <div class="form-group">
                     <select class="form-control input-box"  name="subject" autocomplete="off" required="required">
                     <option value="">Select Subject</option>
                     @foreach($subjects as $subject)
                     <option value="{{$subject->subject}}">{{$subject->subject}}</option>
                     @endforeach
                     </select>
               </div>
               <div class="form-group mb20">
                  <textarea class="form-control textarea-box" rows="8" name="message" placeholder="Type your message..." ></textarea>
               </div>
               @if (!Auth::user())
               <div class="form-group mb20">
                  <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{Setting::get('GOOGLE_RECAPTCHA_SITE_KEY')}}"></div>
               </div>
               @endif
               <div class="form-group text-center">
                  <button class="btn btn-main btn-effect" type="submit">Send message</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</main>
@endsection