<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">

    <?php 
    $current_url = $_SERVER['REQUEST_URI'];
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if($check_seo_tags) { ?>
        @seoTags()
    <?php } else {
    ?>
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="@yield('author')">
    <meta charset="UTF-8" name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8" name="site-url" content="{{ url('/') }}">

    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:site_name" content="{{ Setting::get('site_name') }}">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="en_US">
    <meta property="og:image" content="{{ asset(Setting::get('site_icon')) }}">
    <link rel="canonical" href="{{ url()->current() }}">

    <?php } ?>
    {{ Setting::get('google_analytics') }}

    <link rel="icon" href="{{ asset(Setting::get('site_icon')) }}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/revolution/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/revolution/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/jquery.mmenu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/responsive.css') }}">
</head>

<body>
    <div class="loading">
        <div class="loading-inner">
            <div class="loading-effect">
                <div class="object"></div>
            </div>
        </div>
    </div>
    <nav id="main-mobile-nav"></nav>
    <div class="wrapper">

        @include('guest.include.header')

        @yield('content')

        @include('guest.include.footer')

    </div>
    <div class="general-search-wrapper">
        <form class="general-search" role="search" method="get" action="#">
            <input type="text" placeholder="Type and hit enter...">
            <span id="general-search-close" class="ti-close toggle-search"></span>
        </form>
    </div>

    <div id="backtotop">
        <a href="#"></a>
    </div>

    @if(auth()->check())
    <script>
        var authuser = @JSON(auth()->user());
    </script>
    @endif

    <script src="{{ asset('front/assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.ajaxchimp.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.mmenu.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.inview.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.countTo.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/headroom.js') }}"></script>
    <script src="{{ asset('front/assets/js/custom.js') }}"></script>

    <script type="text/javascript" src="{{ asset('front/assets/revolution/js/jquery.themepunch.tools.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('front/assets/revolution/js/jquery.themepunch.revolution.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('front/assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

    <?php echo Setting::get('body_scripts'); ?>
    <script>
    var ADMIN_API_URL = "{{ env('ADMIN_API_URL') }}";
    var APP_URL = "{{ url('/') }}";
        $(document).ready(function(){
            $('#header_search').keyup(function(){ 
                var query = $(this).val();
                if(query != '' && query.length >= 3)
                {
                    var _token = $('input[name="_token"]').val();
                        $.ajax({
                        url:"{{ route('autocomplete.search') }}",
                        method:"POST",
                        data:{query:query, _token:_token},
                        success:function(data){
                        $('#header-search-result').fadeIn();  
                            $('#header-search-result').html(data);
                        }
                    });
                } else {
                    $('#header-search-result').fadeOut();
                }
            }); 
        });
    </script>
@if (\Request::is('/'))
<!-- Setup slider -->
<script type="text/javascript" src="{{ url('slider/java/FWDSISC.js') }}"></script>
<script type="text/javascript" src="{{ url('slider/java/fwdparams.js') }}"></script>
@endif
</body>

</html>