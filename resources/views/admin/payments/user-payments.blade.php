@extends('layouts.admin')
@section('title', tr('subscription_payments'))
@section('content-header',tr('subscription_payments'))
@section('breadcrumb')
<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
<li class="active"><i class="fa fa-credit-card"></i> {{tr('subscription_payments')}}</li>
@endsection
@section('content')
@include('notification.notify')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header label-primary">
                <b style="font-size:18px;">{{tr('subscription_payments')}}</b>
            </div>
            <div class="box-body  table-responsive">
                <div>
                <div class="search-table-top">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_length">
                                <label>
                                    Show
                                    <form method="get" action="{{ url('admin/user/payments') }}" class="videos-change-pagination">
                                        <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'desc')) ? 'desc' : 'asc'; ?>">
                                        <input type="hidden" name="search" value="{{ $search }}">
                                        <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
                                            <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
                                            <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
                                            <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
                                            <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
                                        </select>
                                    </form>
                                    entries
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <form action="{{ url ('admin/user/payments') }}" class="form" method="get">
                                <div class="input-group">
                                    <input type="text" placeholder="Search by Username" class="form-control input-video-search" name="search" value="{{ $search }}">
                                    <div class="input-group-prepend">
                                        <button type="submit" class="btn btn-success">search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if(count($data) > 0)
                <table id="datatable-withoutpagination" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>{{tr('id')}}</th>
                            <th>{{tr('username')}}</th>
                            <th>{{tr('subscriptions')}}</th>
                            <th>{{tr('payment_mode')}}</th>
                            <th>{{tr('payment_id')}}</th>
                            <th>{{tr('coupon_code')}}</th>
                            <th>{{tr('coupon_amount')}}</th>
                            <th>{{tr('plan_amount')}}</th>
                            <th>{{tr('final_amount')}}</th>
                            <th>{{tr('expiry_date')}}</th>
                            <th>{{tr('is_coupon_applied')}}</th>
                            <th>{{tr('coupon_reason')}}</th>
                            <th>{{tr('status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $i => $payment)
                        <tr>
                            <td>{{showEntries($_GET, $i+1)}}</td>
                            <td><a href="{{route('admin.users.view' , $payment->user_id)}}"> {{($payment->user) ? $payment->user->name : ''}} </a></td>
                            <td>
                                @if($payment->subscription)
                                <a href="{{route('admin.subscriptions.view' , $payment->subscription->unique_id)}}" target="_blank">{{$payment->subscription ? $payment->subscription->title : ''}}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td class="text-capitalize">{{$payment->payment_mode ? $payment->payment_mode : 'free-plan'}}</td>
                            <td>{{$payment->payment_id}}</td>
                            <td>{{$payment->coupon_code}}</td>
                            <td>{{Setting::get('currency')}} {{$payment->coupon_amount? $payment->coupon_amount : "0.00"}}</td>
                            <td>{{Setting::get('currency')}} {{$payment->subscription ? $payment->subscription_amount : "0.00"}}</td>
                            <td>{{Setting::get('currency')}} {{$payment->amount ? $payment->amount : "0.00" }}</td>
                            <td>{{date('d M Y',strtotime($payment->expiry_date))}}</td>
                            <td>
                                @if($payment->is_coupon_applied)
                                <span class="label label-success">{{tr('yes')}}</span>
                                @else
                                <span class="label label-danger">{{tr('no')}}</span>
                                @endif
                            </td>
                            <td>
                                {{$payment->coupon_reason ? $payment->coupon_reason : '-'}}
                            </td>
                            <td>
                                @if($payment->status)
                                <span class="label label-success">{{tr('paid')}}</span>
                                @else
                                <span class="label label-danger">{{tr('not_paid')}}</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="pagination-video-list">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">Showing {{ ($data->currentpage()-1)*$data->perpage()+1 }} to {{ ($data->currentpage()*$data->perpage() < $data->total() ) ? $data->currentpage()*$data->perpage() : $data->total() }} of {{ $data->total() }} entries
                            </div>
                        </div>
                        <div class="col-sm-7 text-right">
                            <div align="right" id="paglink"><?php echo $data->links(); ?></div>
                        </div>
                    </div>
                </div>
                
                @else
                <h3 class="no-result">{{tr('no_result_found')}}</h3>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#example3').DataTable( {
            "processing": true,
            "serverSide": true,
            "bLengthChange": false,
            "ajax": "{{route('admin.ajax.user-payments')}}",
            "deferLoading": "{{$payment_count > 0 ? $payment_count : 0}}"
        } );
    } );
</script>
@endsection