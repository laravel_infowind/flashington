@extends('guest.layout.app')
<?php 
$current_url = Request::url();
$check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
if(!$check_seo_tags) { ?>
@section('title', Setting::get('site_name').' | '. $data->heading)
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
<?php } ?>
@section('content')
<section class="static-pages-section-main">
   <div class="movie-detail-intro overlay-gradient"
      style="background: url({{ Setting::get('home_page_bg_image') }}); min-height: 250px; position: relative; padding-top: 165px;">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <h1 class="title" style="text-align: center;color: #ffffff;">{!! $data->heading !!}</h1>
            </div>
         </div>
      </div>
   </div>
   <main class="contact-page ptb100">
      <div class="container">
         <div class="pages-content" style="color: #fff">
            {!! $data->description !!}
         </div>
      </div>
   </main>
</section>
@endsection