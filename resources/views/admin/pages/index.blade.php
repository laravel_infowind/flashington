@extends('layouts.admin')

@section('title', tr('pages'))

@section('content-header', tr('pages'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-book"></i> {{tr('pages')}}</li>
@endsection

@section('content')

    @include('notification.notify')

    <div class="row">
        <div class="col-lg-12">

          <div class="box box-primary">

            <div class="box-header label-primary">
                <b>{{tr('pages')}}</b>
                <a href="{{route('admin.pages.create')}}" style="float:right" class="btn btn-default">{{tr('add_page')}}</a>
            </div>

            <div class="box-body table-responsive">

                <table id="datatable-withoutpagination" class="table table-bordered table-striped">

                    <thead>
                        <tr>                          
                          <th>{{tr('heading')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($data as $i => $result)
                
                            <tr>                                
                                <td><a href="{{route('admin.pages.view', array('id' => $result->id))}}">{{$result->heading}}</a>
                                     <ul class="table-row-actions">
                                        <li>                                               
                                            <a href="{{route('admin.pages.view', array('id' => $result->id))}}"> {{tr('view')}} </a>
                                        </li>

                                        <li>
                                            @if(Setting::get('admin_delete_control'))
                                                <a href="javascript:;" class="btn disabled" style="text-align: left"> {{tr('edit')}} </a>
                                            @else
                                                <a href="{{route('admin.pages.edit', array('id' => $result->id))}}"> {{tr('edit')}} </a>
                                            @endif
                                        </li>

                                        @if($result->page_slug != '404-page')

                                        <li>
                                            @if(Setting::get('admin_delete_control'))
                                                <a href="javascript:;" class="btn disabled" style="text-align: left"> {{tr('delete')}} </a> 

                                            @else
                                                <a onclick="return confirm('Are you sure?')" href="{{route('admin.pages.delete',array('id' => $result->id))}}"> {{tr('delete')}} </a>

                                            @endif

                                        </li> 
                                        @endif                           

                                    </ul>
                                </td>
                            
                            </tr>

                        @endforeach

                    </tbody>
                
                </table>

                @if(count($data) > 0) <div align="right" id="paglink"><?php echo $data->links(); ?></div> @endif

            </div>
          </div>
        </div>
    </div>

@endsection