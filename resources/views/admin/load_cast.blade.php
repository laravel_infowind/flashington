@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" id="load_cast">Load Cast</button>
    </div>
</div>

<div class="row" style="margin-top: 10px;">
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            <div id="result"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="script-loading"></div>
            <ul class="batch_count"></ul>
        </div>

        <div class="col-md-8">
            <h4>Here are the report listing for which movies who has not tmdb movie ID or release date correct in our database.</h4>
            <table class="load-aka-report" border="1px solid #000" cellpadding="10px" style="border-collapse: collapse">
                <tr>
                    <td>ID</td>
                    <td>Movie Name</td>
                </tr>
                <tbody id="reported">

                </tbody>
            </table>
        </div>
    </div>



@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#load_cast', function(){
        load_cast(1);
    });
});

function load_cast(page = 1)
{
	$.ajax({
		type: 'GET',
		url: '?page=' + page,
        datatype: "json",
        beforeSend: function() {
            $('#load_cast').attr('disabled', true);
            $('.script-loading').css('display', 'block');
        },
        success: function(res){
            //$('#load_cast').attr('disabled', false);
            if(res.success) {
                page++;
                if (res.current_page <= res.last_page) {
                    $('ul.batch_count').append('<li> Batch Count => ' + res.current_page + '</li>')
                    $('#reported').append(res.reported);
                    load_cast(page);
                } else {
                    $('#result').append(res.msg);
                    $('#reported').append(res.reported);
                    $('.script-loading').css('display', 'none');
                }
            } else {
                alert('Something wrong with request');
                $('#reported').append('<tr><td>'+ res.reported +'</td></tr>');
                $('.script-loading').css('display', 'none');
			}
		},
		error: function(){
            //$('#load_cast').attr('disabled', false);
            $('.script-loading').css('display', 'none');
			alert('Something wrong with request');
		}
	});
}
</script>
@endsection