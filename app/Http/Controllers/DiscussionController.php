<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\ForumCategory;
use Validator;
use App\AdminVideo;
use App\UserReview;
use App\ForumTopic;
use Illuminate\Support\Collection;
use App\ForumPost;
use Auth;

class DiscussionController extends Controller
{
    public function categories()
    {
       $forumCategory = ForumCategory::orderBy('forum_category.created_at', 'desc')
            ->distinct('forum_category.id')
            ->paginate(10);
        return view('admin.discussion.categories')->with('categories', $forumCategory)->withPage('categories')->with('sub_page', 'view-categories');
    }

    public function add_category()
    {
        $category = new ForumCategory;
        return view('admin.discussion.add-category')->with('page', 'categories')->with('sub_page', 'add-category')->with('category', $category);
    }

    public function edit_category($id)
    {
        $category = ForumCategory::find($id);

        return view('admin.discussion.edit-category')->with('category', $category)->with('page', 'categories')->with('sub_page', 'edit-category');
    }

    public function add_category_process(Request $request)
    {
    
        if ($request->id != '') {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'description' => 'required'
                )
            );
        } else {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100|unique:categories,name',
                    'description' => 'required'
                )
            );

        }
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {
            if ($request->id != '') {
                $category = ForumCategory::find($request->id);
                $message = tr('admin_not_category');
            } else {
                $message = tr('admin_add_category');
                //Add New User
                $category = new ForumCategory();
            }
            $category->name = $request->has('name') ? $request->name : '';
            $category->description = $request->has('description') ? $request->description : '';
            $category->status = 1;
            $category->save();
            if ($category) {
                return redirect('admin/discussion')->with('flash_success', $message);
                // return back()->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function delete_category(Request $request)
    {

        $category = ForumCategory::where('id', $request->category_id)->first();
        if ($category) {
            $category->delete();
            return back()->with('flash_success', tr('admin_not_category_del'));
        } else {
            return back()->with('flash_error', tr('admin_not_error'));

        }
    }

    public function approve_category(Request $request)
    {
        $category = ForumCategory::find($request->id);

        $category->status = $request->status;

        $category->save();
        
        $message = ($request->status)?tr('admin_not_category_approve') :tr('admin_not_category_decline');
        return back()->with('flash_success', $message);

    }

    public function discussion_videos(Request $request)
    {
        
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $request->session()->forget('rated_by_user_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $rated_by_user_search = $request->session()->get('rated_by_user_search');
            }
        }

        $columns = array('admin_videos.title', 'forum_topics.admin_video_id', 'topic_count', 'admin_videos.vote_count', 'admin_videos.vote_rating_total');
        
        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (!empty($request->get('search'))) {
            $model = ForumTopic::leftJoin('admin_videos', 'forum_topics.admin_video_id', '=', 'admin_videos.id')
                ->selectRaw('forum_topics.admin_video_id, admin_videos.title, admin_videos.vote_rating_total, admin_videos.vote_count, count(forum_topics.forum_category_id) as topic_count')
                ->Where('admin_videos.title', 'Like', '%' . $rated_by_user_search . '%')
                ->groupBy('forum_topics.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = $rated_by_user_search;
        } else {
            $model = ForumTopic::leftJoin('admin_videos', 'forum_topics.admin_video_id', '=', 'admin_videos.id')
                ->selectRaw('forum_topics.admin_video_id, admin_videos.title, admin_videos.vote_rating_total, admin_videos.vote_count, count(forum_topics.forum_category_id) as topic_count')
                ->groupBy('forum_topics.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = '';
        }
        
        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.discussion.discussions')->with('page', 'discussions')
            ->with('sub_page', 'rating-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('rated_by_user_search', $rated_by_user_search)
            ->with('per_page', $per_page);
    }

    public function discussion_subjects(Request $request){
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $request->session()->forget('rated_by_user_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $rated_by_user_search = $request->session()->get('rated_by_user_search');
            }
        }

        $columns = array('forum_topics.id', 'forum_category.name','review_count', 'forum_category.is_closed', 'admin_videos.vote_rating_total');
        
        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (!empty($request->get('search'))) {
            $model = ForumTopic::leftJoin('admin_videos', 'forum_topics.admin_video_id', '=', 'admin_videos.id')
                ->leftJoin('forum_category', 'forum_topics.forum_category_id', '=', 'forum_category.id')
                // ->leftJoin('forum_posts', 'forum_topics.id', '=', 'forum_posts.forum_topic_id')
                ->selectRaw('forum_topics.id, admin_videos.title , forum_topics.admin_video_id,forum_topics.subject, forum_category.name as category,forum_topics.is_closed')
                ->Where('forum_topics.subject', 'Like', '%' . $rated_by_user_search . '%')
                ->where('forum_topics.admin_video_id', $request->video_id)
                // ->groupBy('forum_topics.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = $rated_by_user_search;
        } else {
            
            $model = ForumTopic::leftJoin('admin_videos', 'forum_topics.admin_video_id', '=', 'admin_videos.id')
                ->rightJoin('forum_category', 'forum_topics.forum_category_id', '=', 'forum_category.id')
                // ->leftJoin('forum_posts', 'forum_topics.id', '=', 'forum_posts.forum_topic_id')
                ->selectRaw('forum_topics.id, admin_videos.title , forum_topics.admin_video_id,forum_topics.subject, forum_category.name as category,forum_topics.is_closed')
                ->where('forum_topics.admin_video_id', $request->video_id)
                // ->groupBy('forum_topics.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = '';
        }
        
        // echo "<pre>";
        // print_r($model);die;
        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';
        return view('admin.discussion.discussion-topics')->with(['page'=>'discussion-topics' , 'video'=>(count($model)>0)?$model[0]->title:'' ])
            ->with('sub_page', 'rating-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('rated_by_user_search', $rated_by_user_search)
            ->with('per_page', $per_page);
    }

    public function discussion_subject_post(Request $request)
    {

        if (!isset($_GET['page'])) {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $request->session()->forget('rated_by_user_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $rated_by_user_search = $request->session()->get('rated_by_user_search');
            }
        }

        $columns = array('forum_topics.id', 'forum_category.name','review_count', 'forum_category.is_closed', 'admin_videos.vote_rating_total');
        
        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (!empty($request->get('search'))) {
            $model = ForumPost::leftJoin('forum_topics', 'forum_posts.forum_topic_id', '=', 'forum_topics.id')
                ->rightJoin('admin_videos', 'admin_videos.id', '=', 'forum_topics.admin_video_id')
                ->leftJoin('users', 'forum_posts.user_id', '=', 'users.id')
                ->leftJoin('forum_posts as fp', 'fp.forum_post_id', '=', 'forum_posts.id')
                ->selectRaw('fp.id, forum_posts.message,admin_videos.title ,users.name , forum_topics.subject , fp.message as reply')
                ->Where('fp.message', 'Like', '%' . $rated_by_user_search . '%')
                ->where('fp.forum_topic_id', $request->forum_topic_id)
                ->where('fp.forum_post_id' , '!=' , 0)
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = $rated_by_user_search;
        } else {
            
            $model =  ForumPost::leftJoin('forum_topics', 'forum_posts.forum_topic_id', '=', 'forum_topics.id')
            ->rightJoin('admin_videos', 'admin_videos.id', '=', 'forum_topics.admin_video_id')
                ->leftJoin('users', 'forum_posts.user_id', '=', 'users.id')
                ->leftJoin('forum_posts as fp', 'fp.forum_post_id', '=', 'forum_posts.id')
                ->selectRaw('fp.id, forum_posts.message,admin_videos.title ,users.name , forum_topics.subject , fp.message as reply')
                ->where('forum_posts.forum_topic_id', $request->forum_topic_id)
                ->where('fp.forum_post_id' , '!=' , 0)
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = '';
    }
   
        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';
        return view('admin.discussion.discussion-post')->with('page', 'discussion-post')->with('video',(count($model)>0)?$model[0]->title:'')
            ->with('sub_page', 'rating-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('rated_by_user_search', $rated_by_user_search)
            ->with('per_page', $per_page);
   }

   public function delete_subject($id){
        $category = ForumTopic::where('id', $id)->first();
        if ($category) {
            $category->delete();
            return back()->with('flash_success', tr('delete_subject'));
        } else {
            return back()->with('flash_error', tr('admin_not_error'));

        }
   }

   public function subject_open_close($id,$status)
   {
        $topic = ForumTopic::where('id', $id)->first();
        if ($topic) {
            $topic->is_closed = $status;
            $topic->save();
            $message = ($status)?tr('subject_open'):tr('subject_close');
            return back()->with('flash_success', $message);
        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
   }

   public function edit_subject($id){

         $topic = ForumTopic::find($id);
        return view('admin.discussion.edit-topic')->with('topic', $topic)->with('page', 'topics')->with('sub_page', 'edit-topic');
   }

   public function add_subject(){
    $topic = new ForumTopic();
    $discussion = ForumCategory::get();
    return view('admin.discussion.add-topic')->with('topic', $topic)->with('page', 'topics')->with('sub_page', 'edit-topic')->with('discussion' , $discussion);
   }

   public function save_subject(Request $request){

        if($request->id != ''){
        $validator = Validator::make($request->all(), array(
                'subject' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100|unique:forum_topics,subject,'.$request->id,
                'message' => 'required'
            )
        );
        }else{
                $validator = Validator::make($request->all(), array(
                    'subject' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100|unique:forum_topics,subject',
                    'message' => 'required',
                    'category' => 'required'
                )
            );
        }
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {
            if($request->id != ''){
            ForumTopic::where('id',  $request->id)->update(['subject' => $request->subject]);
            ForumPost::where('forum_topic_id',  $request->id)->where('forum_post_id',0)->update(['message' => $request->message]);
            return back()->with('flash_success', 'Topic update successfully.');
            }else{
                $forumTopic = new ForumTopic();
                $forumTopic->subject  = $request->subject;
                $forumTopic->forum_category_id  = $request->category;
                $forumTopic->admin_video_id  = $request->video_id;
                $forumTopic->user_id  = Auth::guard('admin')->user()->id;
                if($forumTopic->save()){
                    $forumPost = new ForumPost();
                    $forumPost->message =  $request->message;
                    $forumPost->forum_topic_id = $forumTopic->id;
                    $forumPost->user_id = Auth::guard('admin')->user()->id;
                    $forumPost->forum_post_id = 0;
                    $forumPost->save();
                }
                return back()->with('flash_success', 'Topic created successfully.');  
            }
        }


   }

   public function delete_post($id){
       
    $post = ForumPost::where('id', $id)->first();
    if ($post) {
        $post->delete();
        return back()->with('flash_success', tr('message_delete'));
    } else {
        return back()->with('flash_error', tr('admin_not_error'));

    }
}
}
