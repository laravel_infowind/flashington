@extends('layouts.admin')
@section('title', 'Add Meta Tag')

@section('content-header', 'Meta Tags')
@section('breadcrumb')
<!-- <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> {{tr('home')}}</a>
</li>
<li class="breadcrumb-item"><a href="{{route('seo::meta-tags.index')}}"> <i class="fa fa-code"></i> Meta Tags</a></li>
<li class="breadcrumb-item"> Add Meta Tag</li> -->
@endsection
@section('content')


<form action="{{isset($route)?$route : route('seo::meta-tags.store')}}" method="POST">
<div class="main-content">
    <div class="row">
        <div class="col-md-8">
            <div class="boxx">
                <div class="box-bodyy">
                    <div class="card mb-20">
                        <div class="card-header">
                            <label for="title">Add Meta Tag</label>
                        </div>
                        <div class="card-body">
                            @include('seo::forms.meta_tag')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <div class="card mb-20">
                <div class="card-header">
                    <label for="">Publish</label>
                </div>
                <div class="card-body">
                    <div class="publish_card">
                    {!! App\Helpers\Helper::published_block($model) !!}
                    </div>
                </div>
                <div class="card-footer">
                    <div class="publish_item">
                        <div class="box-tools pull-left" style="width: 30%">
                        <input type="submit" class="btn btn-primary" value="Publish"/>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>
</form>
@endSection