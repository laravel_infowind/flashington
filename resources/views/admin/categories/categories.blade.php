@extends('layouts.admin')

@section('title', tr('categories'))

@section('content-header', tr('categories'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-suitcase"></i> {{tr('categories')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
        <div class="col-lg-12">
          	<div class="box box-primary">
	          	<div class="box-header label-primary">
	                <b style="font-size:18px;">{{tr('categories')}}</b>
	                <a href="{{route('admin.add.category')}}" class="btn btn-default pull-right">{{tr('add_category')}}</a>
	            </div>
	            
	            <div class="box-body">

	            	@if(count($categories) > 0)

		              	<table id="datatable-withoutpagination" class="table table-bordered table-striped">

							<thead>
							    <tr>
							      {{-- <th>{{tr('id')}}</th> --}}
							      <th>{{tr('category')}}</th>
							      <th>{{tr('picture')}}</th>
							      <th>{{tr('sub_categories')}}</th>
							      <th>{{tr('is_series')}}</th>
							      <th>{{tr('status')}}</th>							      
							    </tr>
							</thead>

							<tbody>
								@foreach($categories as $i => $category)

								    <tr>
								      	{{-- <td>{{showEntries($_GET, $i+1)}}</td> --}}
								      	<td>{{$category->name}}

								      		<ul class="table-row-actions"> 
	            								
						                  	<li role="presentation">
                                                @if(Setting::get('admin_delete_control'))
                                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                                @else
                                                    <a role="menuitem" tabindex="-1" href="{{route('admin.edit.category' , array('id' => $category->id))}}">{{tr('edit')}}</a>
                                                @endif
                                            </li>
                                            
						                  	<li role="presentation">

							                  	@if(Setting::get('admin_delete_control'))

								                  	<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>

								                @else

						                  			<a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_delete_confirmation' , $category->name)}}&quot;);" href="{{route('admin.delete.category' , array('category_id' => $category->id))}}">{{tr('delete')}}</a>
						                  		@endif
						                  	</li>
											

						                  	@if($category->is_approved)
						                  		<li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_decline_confirmation' , $category->name)}}&quot;);" href="{{route('admin.category.approve' , array('id' => $category->id , 'status' =>0))}}">{{tr('decline')}}</a></li>
						                  	@else
						                  		<li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('category_approve_confirmation' , $category->name)}}&quot;);" href="{{route('admin.category.approve' , array('id' => $category->id , 'status' => 1))}}">{{tr('approve')}}</a></li>
						                  	@endif
						                  	

						                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.add.sub_category' , array('category' => $category->id))}}">{{tr('add_sub_category')}}</a></li>

						                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.sub_categories' , array('category' => $category->id))}}">{{tr('view_sub_categories')}}</a></li>				                  

						                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.videos' , array('category_id' => $category->id))}}">{{tr('videos')}}</a></li>
									              
	            							</ul>
								      	</td>
								      	<td>
		                                	<img style="height: 30px;" src="{{$category->picture}}">
		                            	</td>
		                            	<td>
		                            		<a href="{{route('admin.sub_categories' , array('category' => $category->id))}}">
		                            		{{count($category->subCategory)}}</a>
		                            	</td>
		                            	<td>
								      		@if($category->is_series)
								      			<span class="label label-success">{{tr('yes')}}</span>
								       		@else
								       			<span class="label label-warning">{{tr('no')}}</span>
								       		@endif
								       	</td>

								      <td>
								      		@if($category->is_approved)
								      			<span class="label label-success">{{tr('approved')}}</span>
								       		@else
								       			<span class="label label-warning">{{tr('pending')}}</span>
								       		@endif
								       </td>
								       
								    </tr>
								@endforeach
							</tbody>
						
						</table>

						<div align="right" id="paglink"><?php echo $categories->links(); ?></div>


					@else
						<h3 class="no-result">{{tr('no_result_found')}}</h3>
					@endif
	            
	            </div>

          	</div>
        </div>
    </div>

@endsection
