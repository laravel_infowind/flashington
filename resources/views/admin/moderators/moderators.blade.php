@extends('layouts.admin')

@section('title', tr('moderators'))

@section('content-header', tr('moderators'))

@section('breadcrumb')
<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

<li class="active"><i class="fa fa-users"></i> {{tr('moderators')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">

    <div class="col-lg-12">

        <div class="box box-primary">

            <div class="box-header label-primary">
                <b style="font-size:18px;">{{tr('moderators')}}</b>
                <a href="{{route('admin.add.moderator')}}"
                    class="btn btn-default pull-right">{{tr('add_moderator')}}</a>
            </div>

            <div class="box-body">
                <div>

                    <div class="search-table-top">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>Show
                                        <form method="get" action="{{ url('admin/moderators') }}"
                                            class="videos-change-pagination">
                                            <input type="hidden" name="order_by"
                                                value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="{{ $search }}">


                                            <select name="per_page" aria-controls="datatable"
                                                class="form-control input-sm" onchange="this.form.submit()">
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
                                                    value="10">10</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
                                                    value="25">25</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
                                                    value="50">50</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
                                                    value="100">100</option>
                                            </select>
                                        </form>
                                        entries</label>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <form action="{{route('admin.moderators')}}" class="form" method="get">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search by Username"
                                            class="form-control input-video-search" name="search" value="{{ $search }}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    @if(count($moderators) > 0)

                    <table id="datatable-withoutpagination" class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                <th>
                                    <a
                                        href="{{ url('admin/moderators?column=name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('username')}}
                                        <i
                                            class="fa fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th>
                                    <a
                                        href="{{ url('admin/moderators?column=email&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('email')}}
                                        <i
                                            class="fa fa-sort<?php echo $column == 'email' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th>{{tr('mobile')}}</th>
                                <th>{{tr('address')}}</th>
                                <th>{{tr('total_videos')}}</th>
                                <th>{{tr('total')}}</th>
                                <th>
                                    <a
                                        href="{{ url('admin/moderators?column=is_activated&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('status')}}
                                        <i
                                            class="fa fa-sort<?php echo $column == 'is_activated' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>                               
                            </tr>
                        </thead>

                        <tbody>

                            @php($count = ($moderators->perPage() * ($moderators->currentPage() - 1)) + 1)
                            @foreach($moderators as $i => $moderator)
                            <tr>
                                <td><a href="{{route('admin.moderator.view',$moderator->id)}}">{{$moderator->name}}</a>
                                    <ul class=" table-row-actions">
                                        <li role="presentation">
                                            @if(Setting::get('admin_delete_control'))
                                            <a role="button" href="javascript:;" class="btn disabled"
                                                style="text-align: left">{{tr('edit')}}</a>
                                            @else
                                            <a role="menuitem" tabindex="-1"
                                                href="{{route('admin.edit.moderator',$moderator->id)}}">{{tr('edit')}}</a>
                                            @endif
                                        </li> 

                                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                href="{{route('admin.moderator.view',$moderator->id)}}">{{tr('view')}}</a>
                                        </li>
                                        
                                        @if($moderator->is_activated)
                                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                onclick="return confirm(&quot;{{tr('moderator_decline_confirmation' , $moderator->name)}}&quot;);"
                                                href="{{route('admin.moderator.decline',$moderator->id)}}">{{tr('decline')}}</a>
                                        </li>
                                        @else
                                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                onclick="return confirm(&quot;{{tr('moderator_approve_confirmation' , $moderator->name)}}&quot;);"
                                                href="{{route('admin.moderator.approve',$moderator->id)}}">{{tr('approve')}}</a>
                                        </li>
                                        @endif

                                        <li role="presentation">

                                            @if(Setting::get('admin_delete_control'))

                                            <a role="button" href="javascript:;" class="btn disabled"
                                                style="text-align: left">{{tr('delete')}}</a>

                                            @else

                                            <a role="menuitem" tabindex="-1"
                                                onclick="return confirm(&quot;{{tr('admin_moderator_delete_confirmation' , $moderator->name)}}&quot;);"
                                                href="{{route('admin.delete.moderator', $moderator->id)}}">{{tr('delete')}}</a>
                                            @endif

                                        </li>  
                                        
                                        <?php $user = App\User::where('moderator_id', $moderator->id)->first();
                                        //print_r($user); ?>
                                        @if($user)
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="{{route('admin.users.password_reset',  $user->id)}}">Password Reset</a>
                                        </li>
                                        @endif

                                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                href="{{route('admin.moderators.redeems',['id'=>$moderator->id])}}">{{tr('redeems')}}</a>
                                        </li>                                               

                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1"
                                                href="{{route('admin.videos' , array('moderator_id' => $moderator->id))}}">{{tr('videos')}}</a>
                                        </li>
                                    </ul>
                                </td>
                                <td>{{$moderator->email}}</td>
                                <td>{{$moderator->mobile}}</td>
                                <td>{{$moderator->address}}</td>
                                <td><a
                                        href="{{route('admin.videos' , array('moderator_id' => $moderator->id))}}">{{$moderator->moderatorVideos ? $moderator->moderatorVideos->count() : "0.00"}}</a>
                                </td>
                                <td>{{Setting::get('currency')}} {{$moderator->total}}</td>

                                <td>
                                    @if($moderator->is_activated)
                                    <span class="label label-success">{{tr('approved')}}</span>
                                    @else
                                    <span class="label label-warning">{{tr('pending')}}</span>
                                    @endif
                                </td>
                                 
                            </tr>
                            @php($count++)
                            @endforeach
                        </tbody>

                    </table>
                    <div class="pagination-video-list">

                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="datatable-checkbox_info" role="status"
                                    aria-live="polite">Showing
                                    {{ ($moderators->currentpage()-1)*$moderators->perpage()+1 }} to
                                    {{ ($moderators->currentpage()*$moderators->perpage() < $moderators->total() ) ? $moderators->currentpage()*$moderators->perpage() : $moderators->total() }}
                                    of {{ $moderators->total() }} entries
                                </div>
                            </div>
                            <div class="col-sm-7 text-right">
                                <div align="right" id="paglink"><?php echo $moderators->links(); ?></div>
                            </div>
                        </div>

                    </div>
                    @else
                    <h3 class="no-result">{{tr('no_result_found')}}</h3>
                    @endif

                </div>

            </div>

        </div>
    </div>
    <script>
        function sort(data){
		var search = "<?php echo session('search');?>";console.log(search);
		var sort = data.id;console.log(sort);
		var direction = "<?php echo session('direction');?>";

		if(direction == "asc")
       {
        direction = "desc"
    }
    else{
       direction = "asc"
   }
   window.location.href="{{url('admin/videos')}}?search="+search+"&sort="+sort+"&direction="+direction;
}


    </script>
    @endsection