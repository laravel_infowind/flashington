<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{

	protected $table = 'user_histories';
	protected $fillable = ['user_id', 'admin_video_id', 'status'];
    public function adminVideo() {
        return $this->belongsTo('App\adminVideo');
    }
}
