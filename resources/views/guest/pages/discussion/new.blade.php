@extends('guest.layout.app')

<?php 
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
        @section('title', Setting::get('site_name').' | '. 'Movies')
        @section('description', Setting::get('meta_description'))
        @section('keywords', Setting::get('meta_keywords'))
        @section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')
<section class="home-pg-movie ptb80">
    <div class="discussion-main">
        <div class="discussion-sidebar">
            <div class="new-discussion-btn">
                
            </div>
            <h5>Categories</h5>
            <ul class="discussion-category-list">
                @if(!empty($forum_category))
                    @foreach($forum_category as $value)
                    <li><a href="{{ url('movie/'.$movie->id.'/discuss/'.$value->id) }}">{{ $value->name }}</a></li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="discussion-content">
            <div class="container container-main">
                <div class="row movie_heading">
                    <div class="col col-md-12 col-sm-12">
                        <h3 class="mmt_title discussion_title">Discuss {{ $movie->title }}</h3>
                        <div class="">
                            @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="new-discuss-form">
                                <form class="form-horizontal" action="{{ url('save-new-discussion') }}" method="post">
                                    <input type="hidden" name="csrf_token" value="{{ csrf_token() }}"/>
                                    <input type="hidden" name="movie_id" value="{{ $movie_id }}"/>
                                    <input type="hidden" name="user_id" value="{{ $user_id }}"/>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-2" for="category">Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="category">
                                                <option value="">Select Category</option>
                                                @foreach($forum_category as $value)
                                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-2" for="for_item">For Movie:</label>
                                        <div class="col-sm-10">
                                            {{ $movie->title }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-2" for="subject">Subject</label>
                                        <div class="col-sm-10">
                                            <input type="text" value="" name="subject" id="subject"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-2" for="message">Message</label>
                                        <div class="col-sm-10">
                                            <textarea value="" name="message" id="message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-main btn-effect nomargin">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection