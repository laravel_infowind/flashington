@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" id="load_cast_profile">Load Cast Profile</button>
    </div>
</div>

<div class="row" style="margin-top: 10px;">
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            <div id="result"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="script-loading"></div>
            <ul class="batch_count"></ul>
        </div>

        <div class="col-md-8">
            <h4>Here are the report listing for whose cast profile is not in our database.</h4>
            <table class="load-aka-report" border="1px solid #000" cellpadding="10px" style="border-collapse: collapse">
                <tr>
                    <td>Cast ID</td>
                    <td>Cast Name</td>
                </tr>
                <tbody id="reported">

                </tbody>
            </table>
        </div>
    </div>



@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#load_cast_profile', function(){
        load_cast_profile(1);
    });
});

function load_cast_profile(page = 1)
{
	$.ajax({
		type: 'GET',
		url: '?page=' + page,
        datatype: "json",
        beforeSend: function() {
            $('#load_cast_profile').attr('disabled', true);
            $('.script-loading').css('display', 'block');
        },
        success: function(res){
            //$('#load_cast_profile').attr('disabled', false);
            if(res.success) {
                page++;
                if (res.current_page <= res.last_page) {
                    $('ul.batch_count').append('<li> Batch Count => ' + res.current_page + '</li>')
                    $('#reported').append(res.reported);
                    load_cast_profile(page);
                } else {
                    $('#result').append(res.msg);
                    $('#reported').append(res.reported);
                    $('.script-loading').css('display', 'none');
                }
            } else {
                alert('Something wrong with request');
                $('#reported').append('<tr><td>'+ res.reported +'</td></tr>');
                $('.script-loading').css('display', 'none');
			}
		},
		error: function(){
            //$('#load_cast_profile').attr('disabled', false);
            $('.script-loading').css('display', 'none');
			alert('Something wrong with request');
		}
	});
}
</script>
@endsection