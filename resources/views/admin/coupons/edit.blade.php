@extends('layouts.admin')

@section('title',tr('edit_coupon'))

@section('content-header',tr('edit_coupon'))

@section('breadcrumb')

	<!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

	<li><a href="{{route('admin.coupon.list')}}"><i class="fa fa-gift"></i>{{tr('coupons')}}</a></li>

	<li class="active">{{tr('edit_coupon')}}</li> -->

@endsection

@section('content')

	@include('notification.notify')



<div class="main-content">
    <form action="{{route('admin.save.coupon')}}" method="POST" class="form-horizontal" role="form">
	<input type="hidden" name="id" value="{{$edit_coupon->id}}">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('edit_coupon')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    
									<div class="form-group">
										<label for="title">{{tr('title')}}*</label>
										<input type="text" name="title" role="title" min="5" max="20" class="form-control" placeholder="{{tr('enter_coupon_title')}}" value="{{$edit_coupon->title ?$edit_coupon->title : old('title') }}">
									</div>
									<div class="form-group">
										<label for="coupon_code">{{tr('coupon_code')}}*</label>
										<input type="text" name="coupon_code" min="5" max="10" class="form-control" pattern="[A-Z0-9]{1,10}" placeholder="{{tr('enter_coupon_code')}}" value="{{$edit_coupon->coupon_code ? $edit_coupon->coupon_code : old('coupon_code') }}" title="{{tr('validation')}}">
										<p class="help-block">{{tr('note')}} : {{tr('coupon_code_note')}}</p>
									</div>
									<div class="form-group floating-label">
										<label for = "amount_type">{{tr('amount_type')}}*</label>
										<select id ="amount_type" name="amount_type" class="form-control select2">
											<option value="{{PERCENTAGE}}" {{$edit_coupon->amount_type == 0 ?'selected="selected"':''}}>{{tr('percentage_amount')}}</option>
											<option value="{{ABSOULTE}}" {{$edit_coupon->amount_type == 1 ?'selected="selected"':''}}>{{tr('absoulte_amount')}}</option>
										</select> 
									</div>
									<div class="form-group">
										<label for="amount">{{tr('amount')}}*</label>
										<input type="number" name="amount" min="1" max="5000" step="any" class="form-control" placeholder="{{tr('amount')}}" value="{{$edit_coupon->amount ? $edit_coupon->amount : old('amount')}}">
									</div>
									<div class="form-group">
										<label for="expiry_date">{{tr('expiry_date')}}*</label>
										<input type="text" id="expiry_date" name="expiry_date"  class="form-control" placeholder="{{tr('expiry_date_coupon')}}" value="{{ $edit_coupon->expiry_date ? date('d-m-Y',strtotime($edit_coupon->expiry_date)) : old('expiry_date')}}" required readonly>
									</div>
									<div class="form-group">
										<label for="no_of_users_limit">{{tr('no_of_users_limit')}}*</label>
										<input type="text" pattern="[0-9]{1,4}" name="no_of_users_limit" class="form-control" placeholder="{{tr('no_of_users_limit')}}" value="{{$edit_coupon->no_of_users_limit}}" required title="{{tr('no_of_users_limit_notes')}}">
									</div>
									<div class="form-group">
										<label for="amount">{{tr('per_users_limit')}}*</label>
										<input type="text" pattern="[0-9]{1,2}" name="per_users_limit" class="form-control" placeholder="{{tr('per_users_limit')}}" value="{{$edit_coupon->per_users_limit}}" required title="{{tr('per_users_limit_notes')}}">
									</div>
									<div class="form-group">
										<label for="description">{{tr('description')}}</label>
										<textarea name="description" class="form-control" max="255" style="resize: none;">{{$edit_coupon->description}}</textarea>
									</div>
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($edit_coupon) !!}

                            @if($edit_coupon->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($edit_coupon->updated_at)). ' at '. date('H:i', strtotime($edit_coupon->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
								<!-- <a href="" class="btn btn-danger">{{tr('reset')}}</a> -->
								<button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>

@endsection