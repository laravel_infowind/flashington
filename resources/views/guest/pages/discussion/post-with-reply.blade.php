@extends('guest.layout.app')
<?php 
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
@section('title', Setting::get('site_name').' | '. 'Movies')
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
<?php } ?>
@section('content')
<section class="home-pg-movie ptb80">
    <div class="discussion-main">
        <div class="discussion-sidebar">
            @include('guest.pages.discussion.sidebar')
            @if($post->topic->is_closed == 0)
                <div><a href="{{ url('close-discussion/'. $post->topic->id) }}">Close</a></div>
            @else
                <div><a href="">Re-Open Discussion</a></div>
            @endif
        </div>
        <div class="discussion-content">
            <div class="container container-main">
                <div class="row movie_heading">
                    <div class="col col-md-12 col-sm-12">
                        <h3 class="mmt_title discussion_title"><a href="{{ url('discuss/movies') }}">Discuss</a> → <a href="{{ url('discuss/movies') }}">Movies</a> → <a href="{{ url('movies/'.$movie->unique_id.'/discuss') }}">{{ $movie->title }}</a> → <a href="{{ url('movie/'.$movie->id.'/discuss/'.$post->topic->forum_category_id) }}">{{ $post->topic->category->name }}</a></h3>
                        <div class="">
                            <div class="category-discussion">
                                <div id="{{ $post->id }}" class="primary carton">
                                    <div class="post_info">
                                        <div class="flex_wrapper">
                                            <div class="title">
                                                <div class="avatar_wrapper">
                                                    <span class="avatar ">
                                                    <a href="" alt="Discussion started by {{ $post->user->name }}">
                                                    <img loading="lazy" class="avatar" src="{{ url('placeholder.png') }}" width="45" height="45">
                                                    </a>
                                                    </span>
                                                </div>
                                                <div class="subject one">
                                                    <h2 class="no_capitalize bigger_image">
                                                        <a href="">{{ $post->topic->subject }}</a>
                                                    </h2>
                                                    <h3 class="by">
                                                        posted by <a href="#">{{ $post->user->name }}</a>
                                                        on {{ date('F d, Y', strtotime($post->created_at)) }} at {{ date('H:i A', strtotime($post->created_at)) }}
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content post_id_{{ $post->id }} convert_emoji">
                                        {!! $post->message !!}
                                    </div>
                                    <div id="likes_60129429b5bc21003b98742e" class="like_tooltip hide"></div>
                                    <div class="toolbar">
                                        <div class="post_actions">
                                            <div class="tools">
                                                <div class="tools_wrapper">
                                                    @if(Auth::check())
                                                        <?php $like_val = user_post_like($post->id, Auth::user()->id ); ?>
                                                        <a class="no_click like liked_false false" data-user="{{ $post->user->id }}" data-type="discussion" id="like_{{ $post->id }}" data-action="like" href="javascript:void(0)" data-value="{{ ($like_val) ? '0' : '1'  }}"><i class="fa fa-thumbs-up"></i> <span class="text like_count_{{ $post->id }}">{{ post_like_count($post->id) }}</span></a>
                                                    
                                                        <a class="no_click quote" data-user="{{ $post->user->name }}" data-type="discussion" data-id="{{ $post->id }}" data-action="quote" href="javascript:void(0)"><i class="fa fa-comments"></i> Quote</a>
                                                    @else
                                                        <a class="no_click liked_false false"  data-type="discussion" id="like_{{ $post->id }}" data-action="like" href="javascript:void(0)" ><i class="fa fa-thumbs-up"></i> <span class="text like_count_{{ $post->id }}">{{ post_like_count($post->id) }}</span></a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tools">
                                            <div class="tools_wrapper">
                                                <div class="actions">
                                                </div>
                                                <div class="settings">
                                                    <a class="no_click settings" href="#" data-comment-id="60129429b5bc21003b98742e" data-role="tooltip">
                                                    <span class="glyphicons_v2 cogwheel"></span>
                                                    </a>
                                                    <div id="comment_settings_tooltip" class="hide 60129429b5bc21003b98742e">
                                                        <div class="settings_content">
                                                            <div class="group no_pad">
                                                                <p><a class="no_click edit_discussion" href="#">Edit</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(count($childs) > 0)
                                <div class="inner_pagination">
                                    <div class="group_one">
                                        <h3 class="">{{ $childs->count() }} reply (on page {{ $childs->currentPage() }} of {{ $childs->lastPage() }})</h3>
                                        {{-- <h4> <a id="jump_to_last" class="jump" href="#last">Jump to last post</a></h4> --}}
                                    </div>
                                </div>
                                <div class="comments">
                                    @if(!empty($childs) && count($childs) > 0)
                                    @foreach($childs as $value)
                                    <div id="{{ $value->id }}" class="carton">
                                        <a name="6012944d1b70ae003e5c1864"></a>
                                        <div class="post_info">
                                            <div class="title">
                                                <span class="avatar ">
                                                <a href="#" alt="Discussion started by {{ $value->user->name }}">
                                                <img loading="lazy" class="avatar" src="{{ url('placeholder.png') }}" width="45" height="45">
                                                </a>
                                                </span>
                                                <div class="subject one">
                                                    <div>
                                                        <h2 class="no_capitalize bigger_image">
                                                            Reply by <a href="https://www.themoviedb.org/movie/10823-children-of-the-corn/discuss/60129429b5bc21003b98742e#6012944d1b70ae003e5c1864">&nbsp;{{ $value->user->name }}</a>
                                                        </h2>
                                                    </div>
                                                    <h3 class="by">
                                                        on {{ date('F d, Y', strtotime($value->created_at)) }} at {{ date('H:i A', strtotime($value->created_at)) }}
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content post_id_{{ $value->id }} convert_emoji">
                                            {!! $value->message !!}
                                        </div>
                                        <div id="likes_6012944d1b70ae003e5c1864" class="like_tooltip hide"></div>
                                        <div class="toolbar">
                                            <div class="post_actions">
                                                <div class="tools">
                                                    <div class="tools_wrapper">
                                                        @if(Auth::check())
                                                            <?php $like_val_inner = user_post_like($value->id, Auth::user()->id); ?>
                                                            <a class="no_click like liked_false false" data-user="{{ $value->user->id }}" data-type="discussion" id="like_{{ $value->id }}" data-action="like" data-value="{{ ($like_val_inner) ? '0' : '1'  }}" href="javascript:void(0)"><i class="fa fa-thumbs-up"></i> <span class="text like_count_{{ $value->id }}">{{ post_like_count($value->id) }}</span></a>
                                                            
                                                            <a class="no_click quote" data-user="{{ $value->user->name }}" data-type="comment" data-id="{{ $value->id }}" data-action="quote" href="javascript:void(0)"><i class="fa fa-comments"></i> Quote</a>
                                                        @else
                                                            <a class="no_click liked_false false" data-type="discussion" id="like_{{ $value->id }}" data-action="like"  href="javascript:void(0)"><i class="fa fa-thumbs-up"></i> <span class="text like_count_{{ $value->id }}">{{ post_like_count($value->id) }}</span></a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tools">
                                                <div class="tools_wrapper">
                                                    <div class="actions">
                                                    </div>
                                                    <div class="settings">
                                                        <a class="no_click settings" href="#" data-comment-id="6012944d1b70ae003e5c1864" data-role="tooltip">
                                                        <span class="glyphicons_v2 cogwheel"></span>
                                                        </a>
                                                        <div id="comment_settings_tooltip" class="hide 6012944d1b70ae003e5c1864">
                                                            <div class="settings_content">
                                                                <div class="group no_pad">
                                                                    <p><a class="no_click edit_comment" data-id="6012944d1b70ae003e5c1864" href="#">Edit</a></p>
                                                                    <p><a class="no_click delete_comment" href="#" data-id="6012944d1b70ae003e5c1864" data-redirect="/movie/10823-children-of-the-corn/discuss/60129429b5bc21003b98742e">Delete</a></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                @endif
                                <div id="reply">
                                    @if(Auth::check() && $post->topic->is_closed == 0)
                                        <button type="button" id="reply_to_discussion" class="k-button k-primary btn btn-primary">Reply</button>
                                    @elseif(Auth::check() && $post->topic->is_closed == 1)
                                        <a type="button" href="{{ url('reopen-discussion/'.$post->topic->id) }}" class="k-button k-primary btn btn-primary">Re-Open & Reply</a>
                                    @endif
                                </div>
                                <div class="message-area-wrap">
                                    <div class="errors" style="display:none">
                                        <div class="alert alert-danger">
                                            <ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <form class="" id="submit_reply" method="post" action="{{ url('save-reply') }}">
                                        <input type="hidden" name="forum_topic_id" value="{{ $post->forum_topic_id }}"/>
                                        <input type="hidden" name="forum_post_id" value="{{ $post->id }}"/>
                                        <input type="hidden" name="is_quote" id="is_quote" value="0"/>
                                        <textarea id="message_area" name="post"></textarea>
                                        <label>
                                        <input type="button" class="submit_reply btn btn-main btn-effect nomargin" name="submit" value="submit"/>
                                        <button type="button" class="btn btn-default cancel" name="cancel">cancel</button>
                                        </label>
                                    </form>
                                </div>
                            </div>
                            <div>
                                <div class="all-pagination-subcategory">{{ $childs->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'message_area' );
    
    CKEDITOR.on("instanceReady", function(event)
    {
        $('#cke_message_area').hide();
    });
</script>
@endsection