{{-- @extends('guest.layout.app')
@section('title', Setting::get('site_name').' | '. 'Channels')
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h1 id="channels_heading" class="text-center">What are Premium Channels</h1>
            <h3 id="channels_subheading">Premium channels are curated collections of movies which let you play all
                titles in that channel for a singlemonthly subscription price.</h3>
        </div>
    </div>
</div>
<section class="home-pg-movie ptb80">
    <div class="container">
        <div class="related-movies row mt20">
            @foreach($channels as $data)
            <div class="item col-lg-3 col-md-4 col-sm-6">
                <div class="movie-box-1">
                    <div class="poster"> 
                        <img src="{{ $data['picture'] }}" alt="{{ Setting::get('site_name'). ' | ' .$data['name']}}">
                    </div>
                </div>
                <div class="channels_name">{{ $data['name'] }}</div>
                <div class="desc channels_desc">{!! $data['description'] !!}</div>
                <div class="bottom">
                    <a href="{{ url('/singlechannels/'.$data->id) }}" class="btn form-btn">View Details</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@endsection --}}


@extends('guest.layout.app')

<?php 
    $current_url = Request::url();
    $check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
    if(!$check_seo_tags) { ?>
        @section('title', Setting::get('site_name').' | '. 'Channels')
        @section('description', Setting::get('meta_description'))
        @section('keywords', Setting::get('meta_keywords'))
        @section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')
<section class="home-pg-movie ptb80">
    {{-- <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1 id="channels_heading" class="text-center">What are Premium Channels</h1>
                <h3 id="channels_subheading">Premium channels are curated collections of movies which let you play all
                    titles in that channel for a singlemonthly subscription price.</h3>
            </div>
        </div>
    </div> --}}
    <div class="container container-main">
        <div class="row cat_top_head">
            @if(!empty($category))
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="cat_top_left_img">
                            @if (!empty($category->picture))
                                <img class="channel_detailimg" src="{{ $category->picture }}" alt="{{ Setting::get('site_name'). ' | ' .$category->name}}">
                            @else
                                <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$category->name}}">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-6">
                        <div class="cat_top_right_cont">
                            <h1 class="channel_name_heading">{{ $category->name }}</h1>
                            <p class="cat_top_details">{!! $category->description !!}</p>
                        </div>
                    </div>
                
                @endif
        </div>
        <div class="row movie_heading" style="padding:0px;">
            <div class="col col-lg-6 col-md-9 col-sm-12">
                <h3 class="mmt_title title">Channels</h3>
                <span id="Genres">
                    <span>Channel: <i class="fa fa-caret-down" aria-hidden="true">
                        </i>
                    </span>
                </span>
                <div class="row"
                    style="display:none; margin-left: 73px; position: absolute; z-index: 940; background-color: rgba(0, 0, 0, .9);border: solid 1px rgba(255, 255, 255, .15); top: 38px; padding: 5px;"
                    id="Genresdata">
                    @foreach($channels as $data)
                    <div class="col-md-3 col-6 col-xs-6 genredata_div">
                        <a href="{{ url('/channels/'.$data->slug) }}">{{ $data['name'] }}</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @foreach($channels as $data)
        <div class="row sub-category">
            <div class="col col-md-9 col-sm-6">
                <h3 class="mmt_subtitle title">{{ $data['name'] }}</h3>
            </div>
            <div class="col col-md-3 col-sm-2 align-self-center text-right">
                <a href="{{ url('/channels/'.$data->slug) }}" class="btn btn-icon btn-main btn-effect">View Details
                    <i class="ti-angle-double-right"></i>
                </a>
            </div>
        </div>
        <div class="related-movies row">
            <div class="col-lg-2 col-md-3 col-sm-4 col-12 related-movies-column">
                <div class="movie-box-1">
                    <div class="poster ">
                        <div class="movie_item">
                            <img src="{{ $data['picture'] }}" alt="{{ Setting::get('site_name'). ' | ' .$data['name']}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-6 col-12 related-movies-column">
                <div class="desc channels_desc">{!! $data['description'] !!}</div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<style type="text/css">
    .owl-prev span,
    .owl-next span {
        color: #FFF;
    }

    .owl-prev,
    .owl-next {
        position: absolute;
        top: 0;
        height: 100%;
    }

    .owl-prev {
        left: 35px;
    }

    .owl-next {
        right: 35px;
    }
</style>


@endsection