<table class="table sitemap-sec table-secondary table-bordered table-striped">
    <thead>
    <tr>
        <th>Title</th>
        <th>Info</th>
        <th>Name</th>
        <th>Property</th>
        <th>Status</th>
        <th>Group</th>
        <th>Visibility</th>
        <!-- <th>Action</th> -->
    </tr>
    </thead>
    <tbody>
    @foreach($records as $record)
        <tr>
            <td> {{$record->input_label }}
            <ul class=" table-row-actions">
                <li role="presentation">
                    @if(Setting::get('admin_delete_control'))
                    <a role="button" href="javascript:;" class="btn disabled"
                        style="text-align: left">{{tr('edit')}}</a>
                    @else
                        <a role="menuitem" tabindex="-1" href="{{route('seo::meta-tags.edit',$record->id)}}">{{tr('edit')}}</a>
                    @endif
                </li> 
            </ul>
            </td>
            <td> {{$record->input_info }} </td>
            <td> {{$record->name }} </td>
            <td> {{$record->property }} </td>
            <td> {{$record->status }} </td>
            <td> {{$record->group }} </td>
            <td> {{$record->visibility }} </td>
            <!-- <td>
                <a href="{{route('seo::meta-tags.edit',$record->id)}}">
                    <span class="fa fa-pencil"></span>
                </a>
                {{-- @include('seo::forms.destroy',['route'=>route('seo::meta-tags.destroy',$record->id)]) --}}
            </td> -->
        </tr>

    @endforeach
    </tbody>
</table>

<div align="right" id="paglink"><?php echo $records->render(); ?></div>