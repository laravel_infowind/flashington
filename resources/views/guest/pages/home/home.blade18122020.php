@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')

<!--  Import required css and js files  -->
<link rel="stylesheet" type="text/css" href="slider/content/global.css" />

<section id="slider" class="full-slider">
    <div class="welcome-block text-justify">
        <div class="container">
            <h1 class="text-center">Welcome To {{ Setting::get('site_name') }}</h1>
            <p>{!! Setting::get('home_welcome') !!}</p>
        </div>
    </div>
    <!-- Main Slider Code -->
    <!-- Div holder -->
    <div id="myDiv"></div>

    <ul id="myPlaylist1" style="display:none;">
        @foreach($videos as $video)
        <li data-source="{{isset($video->default_image) ? $video->default_image : ''}}" data-width="977" data-height="1500" data-link="{{ url('movies/'.$video->unique_id)  }}" data-target="_self">
            <!-- <div class="main-holder-1">
                    <h4><a href="{{ url('movies/'.$video->unique_id)  }}">{{ $video->title }}</a></h4>
                    <?php 
                        $details = App\Helpers\Helper::truncate($video->details, 100, '...', true, true);
                    ?>
					<p>{!! $details !!}</p>
				</div> -->
        </li>
        @endforeach
    </ul>
    
    <div class="otmd_div">
        <div class="latest-movies">
            <!-- Add Recently Added -->
            <div class="container container-main">
                <div class="row">
                    <div class="col col-md-9 col-sm-6">
                        <h3 class="recent_title title">Recently Added</h3>
                    </div>
                    <div class="col col-md-3 col-sm-6 align-self-center text-right">
                        <a href="{{ url('recently-added') }}" class="btn btn-icon btn-main btn-effect">View All
                            <i class="ti-angle-double-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container container-main">
                <div class="row">
                    @foreach($videos_data as $data)
                    <?php 
                //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
                $movie_description = App\Helpers\Helper::truncate($data['details'], 400, '...', true, true);
                $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
                ?>
                    <div hover-id="{{ $data->unique_id }}" class="col-lg-2 col-md-3 col-sm-4 col-6 poster_div">
                        <a href="{{ url('movies/'.$data['unique_id'])  }}">
                            <div class="movie-box-1">
                                <div id="{{ $data->unique_id }}" class="poster">
                                    <div class="movie_item">
                                        <img id="{{ $data->unique_id }}" class="poster_image"
                                            src="{{ $data['default_image'] }}"
                                            alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button"
                                            data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                            data-title="
                                    <span class='popover-movie-title'>{{ $data['title'] }}</span>
                                    <div class='movie-info-row'>
                                        <span class='movie-year'>{{ $data['release_date'] }}</span>
                                        <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                        <span class='movie-stars'>
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            {{ $data['ratings'] }}/<span class='rating_out_of'>10</span> <span class='vote_total'>{{ (!empty($data['vote_count'])) ? $data['vote_count'] : 0 }} votes</span>
                                        {{-- @if($data['ratings'] >= 1)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 2)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 3)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 4)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif
                                            @if($data['ratings'] >= 5)
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            @else
                                            <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                            @endif --}}
                                        </span>
                                    </div>" data-content="
                                    <div class='movie-description'>
                                        <div>{!! $movie_description !!}</div>
                                    </div>" data-container="body" data-html="true" data-original-title="" title="" />
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Recently Added -->
</section>



<ul id="video_categories">
    <li>{{ $sub_categories->id }}</li>
</ul>

<!-- <div class="ajax-loading">
   <span><i class="fa fa-circle clr_red"></i></span>
   <span><i class="fa fa-circle clr_white"></i></span>
   <span><i class="fa fa-circle clr_red"></i></span>
</div> -->
@endsection