<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;
use App\Moderator;
use Validator;
use Auth;
use App\Notifications\ResetPasswordNotification;
use App\Helpers\Helper;

class ModeratorPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;


    /**
     * Where to redirect users after successful change of password.
     *
     * @var string
     */

    protected $guard = 'moderator';

    protected $broker = 'moderators';

    protected $redirectTo = '/moderator';

    /**
     * The password reset request view that should be used.
     *
     * @var string
     */

    protected $linkRequestView = 'moderator.auth.passwords.email';

    /**
     * The password reset view that should be used.
     *
     * @var string
     */

    protected $resetView = 'moderator.auth.passwords.reset';


    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
       return view('moderator.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $user = DB::table('users')->where('email', '=', $request->email)->where('is_moderator', 1)
            ->count();
        //Check if the user exists
        if ($user < 1) {
            return redirect()->back()->withErrors(['email' => trans('User does not exist')]);
        }

        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();

        if ($this->sendResetEmail($request->email, $tokenData->token)) {
            return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
        } else {
            return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
        }
    }

    private function sendResetEmail($email, $token)
    {
        //Retrieve the user from the database
        $user = DB::table('users')->where('email', $email)->select('name', 'email')->first();
        //Generate, the password reset link. The token generated is embedded in the link
        $link = url('/') . '/moderator/password/reset/' . $token . '?email=' . urlencode($user->email);

        try {
            //Here send the link with CURL with an external email API 
            $email_data = array();
            $page = "emails.moderator_forgot_password";
            $email = $user->email;
            $email_data['template_type'] = 'moderator_forgot_password';
            $email_data['name'] = $user->name;
            $email_data['email'] = $user->email;
            $email_data['link'] = $link;

            Helper::send_email($page, $subject = null, $email, $email_data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function showResetForm($token = null, Request $request)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }
        $email = $request->get('email');
        return view('moderator.auth.passwords.reset')->with('token', $token)->with('email', $email);
    }

    public function resetPassword(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed',
            'token' => 'required' ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['email' => 'Please complete the form']);
        }

        $password = $request->password;
        // Validate the token
        $tokenData = DB::table('password_resets')
        ->where('token', $request->token)->first();
        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) return view('moderator.auth.passwords.email');

        $user = User::where('email', $tokenData->email)->first();
        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
        //Hash and update the new password
        $user->password = \Hash::make($password);
        $user->update(); //or $user->save();

        // update password in moderator table
        Moderator::where('id', $user->moderator_id)->update(['password'=> \Hash::make($password)]);

        //login the user immediately they change password successfully
        //Auth::login($user);

        //Delete the token
        DB::table('password_resets')->where('email', $user->email)
        ->delete();

        //Send Email Reset Success Email
        if ($tokenData->email) {
            return redirect('moderator/login');
        } else {
            return redirect()->back()->withErrors(['email' => trans('A Network Error occurred. Please try again.')]);
        }

    }
}
