<footer class="footer1 bg-dark">
   <div class="footer-copyright-area ptb30">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="d-flex">
                  <div class="links">
                     <ul class="list-inline">
                        <?php $footer_menu = App\Helpers\Helper::getStaticPages(); ?>
                        @if(!empty($footer_menu))
                            @foreach($footer_menu as $row)
                              @if($row->is_error_page == 0))
                              <li class="list-inline-item">
                                 <a class="footer_anchor" href="{{ url($row->page_slug) }}">{{ $row->heading }}</a>
                              </li>
                              @endif
                            @endforeach
                        @endif
                        <li class="list-inline-item">
                            <a class="footer_anchor" href="{{ url('contact-us') }}">Contact Us</a>
                        </li>
                     </ul>
                  </div>
                  <div class="copyright ml-auto">{{ Setting::get('site_name') }} &copy; {{ date('Y') }} {{ Setting::get('copyright_content') }}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<style type="text/css">
  .footer_anchor{
    cursor: pointer;
    font-size: 15px;
    font-weight: 400;
    color:#fff;
  }
  .footer_anchor:hover{
    color: #E50914 !important;
  }
</style>

