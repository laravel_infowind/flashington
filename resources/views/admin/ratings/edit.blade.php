@extends('layouts.admin')

@section('title', tr('edit_rating'))

@section('content-header', tr('edit_rating'))

@section('content-sub-header')
    <span> - In response to </span><a href="{{ url('admin/videos/edit').'/'.$model->admin_video_id }}">{{ $model->adminVideo->title }}</a>
@endsection

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.ratings.index')}}"><i class="fa fa-users"></i> {{tr('ratings')}}</a></li>
    <li class="active">{{tr('edit_rating')}}</li> -->
@endsection


@section('content')

@include('notification.notify')

@include('admin.ratings._form')

@endsection

@section('scripts')
    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

    </script>
@endsection
