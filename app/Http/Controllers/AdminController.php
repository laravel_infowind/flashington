<?php

namespace App\Http\Controllers;

use App\Admin;
use App\AdminVideo;
use App\AdminVideoImage;
use App\CastCrew;
use App\CastProfile;
use App\Category;
use App\Channel;
use App\ContactUs;
use App\Coupon;
use App\EmailTemplate;
use App\Flag;
use App\Genre;
use App\Helpers\EnvEditorHelper;
use App\Helpers\Helper;
use App\Jobs\SendMailCamp;
use App\Language;
use App\Moderator;
use App\Page;
use App\PayPerView;
use App\Redeem;
use App\RedeemRequest;
use App\Repositories\PushNotificationRepository as PushRepo;
use App\Repositories\VideoRepository as VideoRepo;
use App\Settings;
use App\SubCategory;
use App\SubCategoryImage;
use App\SubProfile;
use App\Subscription;
use App\Tag;
use App\AdminVideoTag;
use App\User;
use App\UserCoupon;
use App\UserHistory;
use App\UserLoggedDevice;
use App\UserPayment;
use App\UserReview;
use App\VideoCastCrew;
use App\Wishlist;
use Auth;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Log;
use Mail;
use Redirect;
use Setting;
use Validator;
use SEO\Seo;
use Response;
use View;
use App\Subject;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Function: login()
     *
     * @param -
     *
     * @return login view page
     * @uses used to display the login page
     *
     * @created vidhya R
     *
     * @edited vidhya R
     *
     */

    public function login()
    {

        return view('admin.login')->withPage('admin-login')->with('sub_page', '');
    }

    public function channels()
    {
        $channels = Channel::all();
        return view('admin.channels.all')->with('channels', $channels)->withPage('all')->with('sub_page', '');
    }

    public function add_channel()
    {
        return view('admin.channels.add')->withPage('add')->with('sub_page', '');
    }

    public function edit_channel($id)
    {
        $channel = Channel::find($id);
        return view('admin.channels.edit')->with('channel', $channel)->withPage('edit')->with('sub_page', '');
    }

    public function save_channel(Request $request)
    {

        $channel['name'] = $request->channel_name;
        $channel['description'] = $request->channel_desc;
        if ($request->hasFile('channel_img')) {
            Helper::delete_picture($request->img, "/uploads/channels/");
            $url = Helper::normal_upload_picture($request->channel_img, '/uploads/channels/');
            $channel['channel_img'] = $url;
        }

        if ($request->id) {
            Channel::where('id', $request->id)->update($channel);
            $id = $request->id;
        } else {
            $id = Channel::insertGetId($channel);
        }

        return back()->with('flash_success', 'Successfully Saved');

    }

    public function delete_channel($id)
    {
        $c = Channel::find($id);
        $c->delete();
        return back()->with('flash_success', 'Successfully Deleted');
    }

    /**
     * Function: dashboard()
     *
     * @param -
     *
     * @return view page
     * @uses used to display analytics of the website
     *
     * @created vidhya R
     *
     * @edited vidhya R
     *
     */

    public function dashboard()
    {

        $id = Auth::guard('admin')->user()->id;

        $admin = Admin::find($id);

        $admin->token = Helper::generate_token();

        $admin->token_expiry = Helper::generate_token_expiry();

        $admin->save();

        $user_count = User::count();

        $provider_count = Moderator::count();

        $video_count = AdminVideo::count();

        $recent_videos = Helper::recently_added();

        $get_registers = get_register_count();

        $recent_users = get_recent_users();

        $total_revenue = total_revenue();

        $view = last_days(10);

        if (Setting::get('track_user_mail')) {

            user_track("StreamHash - New Visitor");

        }

        return view('admin.dashboard.dashboard')->withPage('dashboard')
            ->with('sub_page', '')
            ->with('user_count', $user_count)
            ->with('video_count', $video_count)
            ->with('provider_count', $provider_count)
            ->with('get_registers', $get_registers)
            ->with('view', $view)
            ->with('total_revenue', $total_revenue)
            ->with('recent_users', $recent_users)
            ->with('recent_videos', $recent_videos);

    }

    /**
     * Function: profile()
     *
     * @param -
     *
     * @return view page
     * @uses admin profile details
     *
     * @created vidhya R
     *
     * @edited vidhya R
     *
     */

    public function profile()
    {

        $id = Auth::guard('admin')->user()->id;

        $admin = Admin::find($id);

        return view('admin.account.profile')->with('admin', $admin)->withPage('profile')->with('sub_page', '');
    }

    /**
     * Function: profile_save()
     *
     * @param -
     *
     * @return view page
     * @uses save admin updated profile details
     *
     * @created vidhya R
     *
     * @edited vidhya R
     *
     */

    public function profile_save(Request $request)
    {

        $validator = Validator::make($request->all(), array(
                'name' => 'regex:/^[a-zA-Z]*$/|max:100',
                'email' => $request->id ? 'email|max:255|unique:admins,email,' . $request->id : 'required|email|max:255|unique:admins,email,NULL',
                'mobile' => 'digits_between:4,16',
                'address' => 'max:300',
                'id' => 'required|exists:admins,id',
                'picture' => 'mimes:jpeg,jpg,png',
            )
        );

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);
        } else {

            $admin = Admin::find($request->id);

            $admin->name = $request->has('name') ? $request->name : $admin->name;

            $admin->email = $request->has('email') ? $request->email : $admin->email;

            $admin->mobile = $request->has('mobile') ? $request->mobile : $admin->mobile;

            $admin->gender = $request->has('gender') ? $request->gender : $admin->gender;

            $admin->address = $request->has('address') ? $request->address : $admin->address;

            if ($request->hasFile('picture')) {
                Helper::delete_picture($admin->picture, "/uploads/");
                $admin->picture = Helper::normal_upload_picture($request->picture);
            }

            $admin->remember_token = Helper::generate_token();
            $admin->is_activated = 1;
            $admin->save();

            return back()->with('flash_success', tr('admin_not_profile'));

        }

    }

    /**
     * Function: change_password()
     *
     * @param -
     *
     * @return redirect with success/ error message
     * @uses change the admin password
     *
     * @created vidhya R
     *
     * @edited vidhya R
     *
     */

    public function change_password(Request $request)
    {

        $old_password = $request->old_password;
        $new_password = $request->password;
        $confirm_password = $request->confirm_password;

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'old_password' => 'required',
            'confirm_password' => 'required|min:6',
            'id' => 'required|exists:admins,id',
        ]);

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);

        } else {

            $admin = Admin::find($request->id);

            if (Hash::check($old_password, $admin->password)) {
                $admin->password = Hash::make($new_password);
                $admin->save();

                return back()->with('flash_success', tr('password_change_success'));

            } else {
                return back()->with('flash_error', tr('password_mismatch'));
            }
        }

        $response = response()->json($response_array, $response_code);

        return $response;

    }

    /**
     * Function: users()
     *
     * @param -
     *
     * @return users management view page
     * @uses used to list the users
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function contacts_index(Request $request)
    {
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('id', 'subject');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('search')) {
            $contacts = ContactUs::where(function($q)use ($search){
                    $q->where('name', 'LIKE', '%' . $search . '%')
                      ->orWhere('subject', 'LIKE', '%' . $search . '%');
                })
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $contact_name = $search;
        } else {
            $contacts = ContactUs::orderBy($column, $order_by)->paginate($per_page)->appends(request()->query());

            $contact_name = '';
        }

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.contact-us.index')
            ->with('page', 'contacts')
            ->with('contacts', $contacts)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('contact_name', $contact_name)
            ->with('per_page', $per_page);
    }

    public function contacts_show($id)
    {
        $contact = ContactUs::find($id);
        return view('admin.contact-us.show')->with('contact', $contact)->with('page', 'contacts');
    }

    public function delete_contact(Request $request)
    {
        $contact = ContactUs::where('id',$request->id)->first();
        if(!empty($contact)){
            $contact->delete();
            return back()->with('flash_success', tr('contact_us_delete'));
        }
        
    }
    /**
     * Function: users()
     *
     * @param -
     *
     * @return users listing
     * @uses used to list the users
     *
     * @created vidhya R
     *
     * @edited Vedgupt S
     *
     */

    public function users(Request $request)
    {

        if (isset($_GET['direction'])) {
            $direction = $_GET['direction'];
            $request->session()->put('direction', $direction);

        } else {
            $direction = 'asc';
        }

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('created_at', 'name', 'email', 'is_activated');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if ($request->session()->get('search')) {
            $query = User::where('name', $request->session()->get('search'))->orWhere('name', 'LIKE', '%' . $request->session()->get('search') . '%')->orderBy($column, $order_by);
            $search = $request->session()->get('search');
        } else {
            $query = User::orderBy($column, $order_by);
            $search = '';
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        $users = $query->paginate($per_page)->appends(request()->query());
        return view('admin.users.users')->withPage('users')
            ->with('users', $users)
            ->with('per_page', $per_page)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('search', $search)
            ->with('sub_page', 'view-user');
    }

    /**
     * Function: users_create()
     *
     * @param -
     *
     * @return view page
     * @uses used to list the users
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_create()
    {
        $user = new User;
        return view('admin.users.add-user')->with('page', 'users')->with('sub_page', 'add-user')->with('user', 'user');
    }

    /**
     * Function: users_edit()
     *
     * @param -
     *
     * @return view page
     * @uses used to display the edit page for the selected user
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_edit(Request $request)
    {
        $data = Subscription::orderBy('created_at', 'desc')->whereNotIn('status', [DELETE_STATUS])->get();
        $user = User::find($request->id);
        $array = (array)$user;
        if (count($array) == 0) {

            return redirect()->route('admin.users')->with('flash_error', tr('user_not_found'));
        }

        return view('admin.users.edit-user')->withUser($user)->with('sub_page', 'view-user')->with('page', 'users')
            ->withPage('subscriptions')
            ->with('data', $data)
            ->with('sub_page', 'view-user')
            ->with('page', 'users');
    }

    /**
     * Function: users_save()
     *
     * @param -
     *
     * @return redirect to user view page
     * @uses used to add /update the user details
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_save(Request $request)
    {
        if ($request->id != '') {

            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:users,email,' . $request->id,
                    'mobile' => 'required|digits_between:4,16',
                )
            );

        } else {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:users,email',
                    'mobile' => 'required|digits_between:4,16',
                    'password' => 'required|min:6|confirmed',
                )
            );

        }

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);
        } else {

            $new_user = 0;

            if ($request->id != '') {
                $plan_data = Subscription::where('id', $request->choose_plan)->first();
                $user = User::find($request->id);
                $message = tr('admin_not_user');
                if ($request->hasFile('picture')) {
                    Helper::delete_picture($user->picture, "/uploads/images/"); // Delete the old pic
                    $user->picture = Helper::normal_upload_picture($request->file('picture'));
                }
            } else {
                $new_user = 1;

                //Add New User
                $user = new User;
                $new_password = $request->password;
                $user->password = Hash::make($new_password);
                $message = tr('admin_add_user');
                $user->login_by = 'manual';
                $user->device_type = 'web';

                $user->picture = asset('placeholder.png');
            }

            $user->timezone = $request->has('timezone') ? $request->timezone : '';
            $user->name = $request->has('name') ? $request->name : '';
            $user->email = $request->has('email') ? $request->email : '';
            $user->mobile = $request->has('mobile') ? $request->mobile : '';

            $user->token = Helper::generate_token();
            $user->token_expiry = Helper::generate_token_expiry();
            $user->is_activated = 1;
            $user->no_of_account = 1;
            $user->status = 1;
            $user->created_at = $request->created_at;

            if ($request->id == '') {
                $email_data['name'] = $user->name;
                $email_data['password'] = $new_password;
                $email_data['email'] = $user->email;
                $email_data['template_type'] = ADMIN_USER_WELCOME;

                // $subject = tr('user_welcome_title').' '.Setting::get('site_name');
                $page = "emails.admin_user_welcome";
                $email = $user->email;
                Helper::send_email($page, $subject = null, $email, $email_data);
            }
            // Plan Active in User_Paymnet Table
            $user->save();
            // Plan Active in User_Paymnet Table
            if ($new_user) {
                $sub_profile = new SubProfile;
                $sub_profile->user_id = $user->id;
                $sub_profile->name = $user->name;
                $sub_profile->picture = $user->picture;
                $sub_profile->status = DEFAULT_TRUE;
                $sub_profile->save();
            } else {

                $sub_profile = SubProfile::where('user_id', $request->id)->first();

                if (!$sub_profile) {

                    $sub_profile = new SubProfile;

                    $sub_profile->user_id = $user->id;

                    $sub_profile->name = $user->name;

                    $sub_profile->picture = $user->picture;

                    $sub_profile->status = DEFAULT_TRUE;

                    $sub_profile->save();
                }
            }

            $user->is_verified = 1;
            $user->save();

            // Check the default subscription and save the user type
            if ($request->id == '') {

                user_type_check($user->id);

            }

            if ($user) {

                $moderator = Moderator::where('email', $user->email)->first();

                // If the user already registered as moderator, atuomatically the status will update.
                if ($moderator && $user) {
                    $user->is_moderator = DEFAULT_TRUE;
                    $user->moderator_id = $moderator->id;
                    $user->save();
                    $moderator->is_activated = DEFAULT_TRUE;
                    $moderator->is_user = DEFAULT_TRUE;
                    $moderator->save();

                }

                register_mobile('web');

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - New User Created");

                }

                return redirect()->route('admin.users.view', $user->id)->with('flash_success', $message);

            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    /**
     * Function: users_delete()
     *
     * @param -
     *
     * @return redirect to users management page with success/error response
     * @uses used to delete the selected user details
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_delete(Request $request)
    {

        if ($user = User::where('id', $request->id)->first()) {

            // Check User Exists or not

            if ($user) {

                if ($user->device_type) {

                    // Load Mobile Registers

                    subtract_count($user->device_type);
                }

                if ($user->picture) {
                    Helper::delete_picture($user->picture, "/uploads/images/");
                }
                // Delete the old pic

                // After reduce the count from mobile register model delete the user

                if ($user->is_moderator) {

                    $moderator = Moderator::where('email', $user->email)->first();

                    if ($moderator) {

                        $moderator->is_user = 0;

                        $moderator->save();
                    }
                }

                if ($user->delete()) {

                    return back()->with('flash_success', tr('admin_not_user_del'));
                }

            }

        }

        return redirect()->route('admin.users')->with('flash_error', tr('user_not_found'));

    }

    /**
     * Function: users_status_change()
     *
     * @param -
     *
     * @return redirect to users management page with success/error response
     * @uses used to approve/decline the selected user details
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_status_change(Request $request)
    {

        $user_details = User::find($request->id);
        $user_details->is_activated = $user_details->is_activated ? DEFAULT_FALSE : DEFAULT_TRUE;

        $user_details->save();

        if ($user_details->is_activated) {

            $message = tr('user_approve_success');

        } else {

            $message = tr('user_decline_success');
        }

        return back()->with('flash_success', $message);
    }

    /**
     * @param $user_id
     *
     * @return redirect back page with status of the email verification
     * @uses Email verify for the user
     *
     */

    /**
     * Function: users_verify_status()
     *
     * @param $user_id
     *
     * @return redirect to users management page with success/error response
     * @uses Email verify for the user
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function users_verify_status($id)
    {

        if ($data = User::find($id)) {

            $data->is_verified = $data->is_verified ? 0 : 1;

            $data->save();

            return back()->with('flash_success', $data->is_verified ? tr('user_verify_success') : tr('user_unverify_success'));

        } else {

            return back()->with('flash_error', tr('admin_not_error'));

        }
    }

    public function users_view($id)
    {

        if ($user = User::find($id)) {

            return view('admin.users.user-details')
                ->with('user', $user)
                ->withPage('users')
                ->with('sub_page', 'users');

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function users_upgrade($id)
    {

        if ($user = User::find($id)) {

            // Check the user is exists in moderators table

            if (!$moderator = Moderator::where('email', $user->email)->first()) {

                $moderator_user = new Moderator;
                $moderator_user->name = $user->name;
                $moderator_user->email = $user->email;
                if ($user->login_by == "manual") {
                    $moderator_user->password = $user->password;
                    $new_password = tr('user_login_password');

                } else {
                    $new_password = time();
                    $new_password .= rand();
                    $new_password = sha1($new_password);
                    $new_password = substr($new_password, 0, 8);
                    $moderator_user->password = Hash::make($new_password);
                }

                $moderator_user->picture = $user->picture;
                $moderator_user->mobile = $user->mobile;
                $moderator_user->address = $user->address;
                $moderator_user->save();

                $email_data = array();
                $page = "emails.moderator_welcome";
                $email = $user->email;
                $email_data['template_type'] = MODERATOR_WELCOME;
                $email_data['name'] = $moderator_user->name;
                $email_data['email'] = $moderator_user->email;
                $email_data['password'] = $new_password;

                Helper::send_email($page, $subject = null, $email, $email_data);

                $moderator = $moderator_user;
            }

            if ($moderator) {
                $user->is_moderator = 1;
                $user->moderator_id = $moderator->id;
                $user->save();

                $moderator->is_activated = 1;
                $moderator->is_user = 1;
                $moderator->save();

                return back()->with('flash_success', tr('admin_user_upgrade'));

            } else {

                return back()->with('flash_error', tr('admin_not_error'));
            }

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }

    }

    public function users_upgrade_disable(Request $request)
    {

        if ($moderator = Moderator::find($request->moderator_id)) {

            if ($user = User::find($request->id)) {
                $user->is_moderator = 0;
                $user->save();
            }

            $moderator->is_activated = 0;

            $moderator->save();

            return back()->with('flash_success', tr('admin_user_upgrade_disable'));

        } else {

            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function view_history($sub_profile_id)
    {

        if ($sub_profile_details = SubProfile::find($sub_profile_id)) {

            $user_history = UserHistory::where('user_id', $sub_profile_id)
                ->leftJoin('users', 'user_histories.user_id', '=', 'users.id')
                ->leftJoin('admin_videos', 'user_histories.admin_video_id', '=', 'admin_videos.id')
                ->select(
                    'users.name as username',
                    'users.id as user_id',
                    'user_histories.admin_video_id',
                    'user_histories.id as user_history_id',
                    'admin_videos.title',
                    'user_histories.created_at as date'
                )
                ->paginate(10);

            return view('admin.users.user-history')
                ->with('data', $user_history)
                ->with('sub_profile_details', $sub_profile_details)
                ->withPage('users')
                ->with('sub_page', 'users');

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function delete_history($id)
    {

        if ($user_history = UserHistory::find($id)) {

            $user_history->delete();

            return back()->with('flash_success', tr('admin_not_history_del'));

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function view_wishlist($id)
    {

        if ($user = SubProfile::find($id)) {

            $user_wishlist = Wishlist::where('user_id', $id)
                ->leftJoin('users', 'wishlists.user_id', '=', 'users.id')
                ->leftJoin('admin_videos', 'wishlists.admin_video_id', '=', 'admin_videos.id')
                ->select(
                    'users.name as username',
                    'users.id as user_id',
                    'wishlists.admin_video_id',
                    'wishlists.id as wishlist_id',
                    'admin_videos.title',
                    'wishlists.created_at as date'
                )
                ->paginate(10);

            return view('admin.users.user-wishlist')
                ->with('data', $user_wishlist)
                ->with('user', $user)
                ->withPage('users')
                ->with('sub_page', 'users');

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function delete_wishlist($id)
    {

        if ($user_wishlist = Wishlist::find($id)) {

            $user_wishlist->delete();

            return back()->with('flash_success', tr('admin_not_wishlist_del'));

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    /**
     * Function: moderators()
     *
     * @param -
     *
     * @return moderators management view page
     * @uses used to list the moderators
     *
     * @created vidhya R
     *
     * @edited Vedgupt S
     *
     */

    public function moderators(Request $request)
    {

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('created_at', 'name', 'email', 'is_activated');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('search')) {
            $query = Moderator::where('name', $request->session()->get('search'))->orWhere('name', 'LIKE', '%' . $request->session()->get('search') . '%')->orderBy($column, $order_by);
            $search = $request->session()->get('search');
        } else {
            $query = Moderator::orderBy($column, $order_by);
            $search = '';
        }

        $moderators = $query->paginate($per_page)->appends(request()->query());

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.moderators.moderators')->with('moderators', $moderators)->withPage('moderators')->with('sub_page', 'view-moderator')
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('search', $search)
            ->with('per_page', $per_page);
    }

    public function add_moderator()
    {
        $moderator = new Moderator;
        return view('admin.moderators.add-moderator')->with('page', 'moderators')->with('sub_page', 'add-moderator')->with('moderator', 'moderator');
    }

    public function edit_moderator($id)
    {

        $moderator = Moderator::find($id);

        return view('admin.moderators.edit-moderator')->with('moderator', $moderator)->with('page', 'moderators')->with('sub_page', 'edit-moderator');
    }

    public function add_moderator_process(Request $request)
    {
        $mobile_rule = (!empty($request->mobile)) ? 'digits_between:4,16' : '';
        if ($request->id != '') {

            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:moderators,email,' . $request->id,
                    'mobile' => $mobile_rule,
                )
            );
        } else {

            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:moderators,email,NULL',
                    'mobile' => $mobile_rule,
                    'password' => 'required|min:6|confirmed',
                )
            );

        }

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {

            $changed_email = DEFAULT_FALSE;

            $email = "";

            if ($request->id != '') {
                $moderator = Moderator::find($request->id);
                $message = tr('admin_not_moderator');

                if ($moderator->email != $request->email) {

                    $changed_email = DEFAULT_TRUE;

                    $email = $moderator->email;

                }

                if (empty($moderator->slug)) {
                    // channel slug
                    $slug = seoUrl($request->name);

                    // check to see if any other slugs exist that are the same & count them
                    $slug_count = Moderator::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

                    // if other slugs exist that are the same, append the count to the slug
                    $channel_slug = ($slug_count) ? $slug . '-' . $slug_count : $slug;

                    $moderator->slug = $channel_slug;
                }

            } else {
                $message = tr('admin_add_moderator');
                $moderator = new Moderator;
                $new_password = $request->password;
                $moderator->password = Hash::make($new_password);

                $moderator->is_activated = 1;

                // channel slug
                $slug = seoUrl($request->name);

                // check to see if any other slugs exist that are the same & count them
                $slug_count = Moderator::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $channel_slug = ($slug_count) ? $slug . '-' . $slug_count : $slug;

                $moderator->slug = $channel_slug;

            }
            if ($request->hasFile('picture')) {
                //$moderator->picture = Helper::normal_upload_picture($request->file('picture'));
                $moderator->picture = Helper::upload_file_to_digitalocean_spaces_image($request->file('picture'), 'images');
            }

            $moderator->timezone = $request->has('timezone') ? $request->timezone : '';
            $moderator->name = $request->has('name') ? $request->name : '';
            $moderator->email = $request->has('email') ? $request->email : '';
            $moderator->mobile = (!empty($request->mobile)) ? $request->mobile : '';
            $moderator->description = $request->has('description') ? $request->description : '';

            $moderator->token = Helper::generate_token();
            $moderator->token_expiry = Helper::generate_token_expiry();
            $moderator->created_at = $request->created_at;

            if ($request->id == '') {
                $email_data['name'] = $moderator->name;
                $email_data['password'] = $new_password;
                $email_data['email'] = $moderator->email;
                $email_data['template_type'] = MODERATOR_WELCOME;
                $page = "emails.moderator_welcome";
                $email = $moderator->email;
                Helper::send_email($page, $subject = null, $email, $email_data);

            }

            $moderator->save();

            if ($moderator) {

                $user = User::where('email', $moderator->email)->first();

                // if the moderator already exists in user table, the status will change automatically
                if ($moderator && $user) {

                    $user->is_moderator = DEFAULT_TRUE;
                    $user->moderator_id = $moderator->id;
                    $user->save();

                    $moderator->is_activated = DEFAULT_TRUE;
                    $moderator->is_user = DEFAULT_TRUE;
                    $moderator->save();

                }

                if ($changed_email) {

                    if ($email) {

                        $email_data = array();
                        $page = "emails.moderator_update_profile";
                        $email_data['template_type'] = MODERATOR_UPDATE_MAIL;
                        $email_data['name'] = $moderator->name;
                        $email_data['email'] = $moderator->email;

                        Helper::send_email($page, $subject = null, $email, $email_data);
                    }

                }

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - Moderator Created");

                }

                return redirect('/admin/view/moderator/' . $moderator->id)->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function delete_moderator(Request $request)
    {

        if ($moderator = Moderator::find($request->id)) {

            if ($moderator->picture) {

                Helper::delete_picture($moderator->picture, '/uploads/images/');
                Helper::spaces_delete_picture($moderator->picture, 'images');

            }

            if ($moderator->is_user) {

                $user = User::where('email', $moderator->email)->first();

                if ($user) {

                    $user->is_moderator = 0;

                    $user->save();
                }

            }

            $moderator->delete();

            if ($moderator->id) {

                $videos = AdminVideo::where('uploaded_by', $moderator->id)->first();

                if ($videos) {

                    $videos->delete();
                }

            }

            return back()->with('flash_success', tr('admin_not_moderator_del'));

        } else {

            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function moderator_approve(Request $request)
    {

        $moderator = Moderator::find($request->id);

        $moderator->is_activated = 1;

        $moderator->save();

        if ($moderator->is_activated == 1) {

            $message = tr('admin_not_moderator_approve');

        } else {

            $message = tr('admin_not_moderator_decline');
        }

        return back()->with('flash_success', $message);
    }

    public function moderator_decline(Request $request)
    {

        if ($moderator = Moderator::find($request->id)) {

            $moderator->is_activated = 0;

            $moderator->save();

            $message = tr('admin_not_moderator_decline');

            return back()->with('flash_success', $message);
        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }

    }

    public function moderator_view_details($id)
    {

        if ($moderator = Moderator::find($id)) {

            return view('admin.moderators.moderator-details')
                ->with('moderator', $moderator)
                ->withPage('moderators')
                ->with('sub_page', 'view-moderator');
        } else {

            return back()->with('flash_error', tr('admin_not_error'));
        }
    }


    /**
     * Function: subadmins()
     *
     * @param -
     *
     * @return subadmins management view page
     * @uses used to list the subadmins
     *
     * @created vidhya R
     *
     * @edited Vedgupt S
     *
     */

    public function subadmins(Request $request)
    {

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('created_at', 'name', 'email', 'is_activated');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('search')) {
            $query = Admin::where('name', $request->session()->get('search'))->orWhere('name', 'LIKE', '%' . $request->session()->get('search') . '%')->where('user_type', 2)->orderBy($column, $order_by);
            $search = $request->session()->get('search');
        } else {
            $query = Admin::where('user_type', 2)->orderBy($column, $order_by);
            $search = '';
        }

        $subadmins = $query->paginate($per_page)->appends(request()->query());

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.subadmins.subadmins')->with('subadmins', $subadmins)->withPage('subadmins')->with('sub_page', 'view-subadmin')
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('search', $search)
            ->with('per_page', $per_page);
    }

    public function add_subadmin()
    {
        $subadmin = new Admin;
        return view('admin.subadmins.add-subadmin')->with('page', 'subadmins')->with('sub_page', 'add-subadmin')->with('subadmin', 'subadmin');
    }

    public function edit_subadmin($id)
    {
        $subadmin = Admin::where('id', $id)->where('user_type', 2)->first();
        //dd($subadmin);die;
        if ($subadmin) {
            return view('admin.subadmins.edit-subadmin')->with('subadmin', $subadmin)->with('page', 'subadmins')->with('sub_page', 'edit-subadmin');
        } else {
            return redirect('admin/subadmins');
        }
    }

    public function add_subadmin_process(Request $request)
    {
        //print_r($request->all());die;
        $mobile_rule = (!empty($request->mobile)) ? 'digits_between:4,16' : '';
        if ($request->id != '') {

            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:admins,email,' . $request->id,
                    'mobile' => $mobile_rule,
                )
            );
        } else {

            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'email' => 'required|email|max:255|unique:admins,email,NULL',
                    'mobile' => $mobile_rule,
                    'password' => 'required|min:6|confirmed',
                )
            );

        }

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {

            $changed_email = DEFAULT_FALSE;

            $email = "";

            if ($request->id != '') {
                $subadmin = Admin::find($request->id);
                $message = tr('admin_not_subadmin');

                if ($subadmin->email != $request->email) {

                    $changed_email = DEFAULT_TRUE;

                    $email = $subadmin->email;

                }
            } else {
                $message = tr('admin_add_subadmin');
                $subadmin = new Admin;
                $new_password = $request->password;
                $subadmin->password = Hash::make($new_password);

                $subadmin->is_activated = 1;

            }
            if ($request->hasFile('picture')) {
                //$subadmin->picture = Helper::normal_upload_picture($request->file('picture'));
                $subadmin->picture = Helper::upload_file_to_digitalocean_spaces_image($request->file('picture'), 'images');
            }

            $subadmin->timezone = $request->has('timezone') ? $request->timezone : '';
            $subadmin->name = $request->has('name') ? $request->name : '';
            $subadmin->email = $request->has('email') ? $request->email : '';
            $subadmin->mobile = (!empty($request->mobile)) ? $request->mobile : '';
            $subadmin->description = $request->has('description') ? $request->description : '';

            $subadmin->token = Helper::generate_token();
            $subadmin->token_expiry = Helper::generate_token_expiry();
            $subadmin->created_at = $request->created_at;

            if ($request->id == '') {
                $email_data['name'] = $subadmin->name;
                $email_data['password'] = $new_password;
                $email_data['email'] = $subadmin->email;
                $email_data['template_type'] = SUBADMIN_WELCOME;
                $page = "emails.subadmin_welcome";
                $email = $subadmin->email;
                Helper::send_email($page, $subject = null, $email, $email_data);

            }

            $subadmin->save();

            if ($subadmin) {

                // if the moderator already exists in user table, the status will change automatically
                $subadmin->is_activated = DEFAULT_TRUE;
                $subadmin->user_type = 2;
                $subadmin->save();


                if ($changed_email) {

                    // if ($email) {

                    //     $email_data = array();
                    //     $page = "emails.moderator_update_profile";
                    //     $email_data['template_type'] = MODERATOR_UPDATE_MAIL;
                    //     $email_data['name'] = $subadmin->name;
                    //     $email_data['email'] = $subadmin->email;

                    //     Helper::send_email($page, $subject = null, $email, $email_data);
                    // }

                }

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - Subadmin Created");

                }

                return redirect('/admin/view/subadmin/' . $subadmin->id)->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function delete_subadmin(Request $request)
    {

        if ($subadmin = Admin::find($request->id)) {

            if ($subadmin->picture) {

                Helper::delete_picture($subadmin->picture, '/uploads/images/');
                Helper::spaces_delete_picture($subadmin->picture, 'images');

            }

            $subadmin->delete();

            return redirect('admin/subadmins')->with('flash_success', tr('admin_not_subadmin_del'));

        } else {

            return redirect('admin/subadmins')->with('flash_error', tr('admin_not_error'));
        }
    }

    public function subadmin_approve(Request $request)
    {

        $subadmin = Admin::find($request->id);

        $subadmin->is_activated = 1;

        $subadmin->save();

        if ($subadmin->is_activated == 1) {

            $message = tr('admin_not_subadmin_approve');

        } else {

            $message = tr('admin_not_subadmin_decline');
        }

        return back()->with('flash_success', $message);
    }

    public function subadmin_decline(Request $request)
    {

        if ($subadmin = Admin::find($request->id)) {

            $subadmin->is_activated = 0;

            $subadmin->save();

            $message = tr('admin_not_subadmin_decline');

            return back()->with('flash_success', $message);
        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }

    }

    public function subadmin_view_details($id)
    {

        if ($subadmin = Admin::find($id)) {

            return view('admin.subadmins.subadmin-details')
                ->with('subadmin', $subadmin)
                ->withPage('subadmin')
                ->with('sub_page', 'view-subadmin');
        } else {

            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    /**
     * Function: categories()
     *
     * @param -
     *
     * @return categories management view page
     * @uses used to list the categories
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function categories()
    {

        $categories = Category::select('categories.id',
            'categories.name',
            'categories.picture',
            'categories.is_series',
            'categories.status',
            'categories.is_approved',
            'categories.created_by'
        )
            ->orderBy('categories.created_at', 'desc')
            ->distinct('categories.id')
            ->paginate(10);

        return view('admin.categories.categories')->with('categories', $categories)->withPage('categories')->with('sub_page', 'view-categories');
    }

    public function add_category()
    {
        $category = new Category;
        return view('admin.categories.add-category')->with('page', 'categories')->with('sub_page', 'add-category')->with('category', $category);
    }

    public function edit_category($id)
    {

        $category = Category::find($id);

        return view('admin.categories.edit-category')->with('category', $category)->with('page', 'categories')->with('sub_page', 'edit-category');
    }

    public function add_category_process(Request $request)
    {

        if ($request->id != '') {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'picture' => 'mimes:jpeg,jpg,bmp,png',
                    'description' => 'required'
                )
            );
        } else {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100|unique:categories,name',
                    'picture' => 'required|mimes:jpeg,jpg,bmp,png',
                    'description' => 'required'
                )
            );

        }

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {

            if ($request->id != '') {
                $category = Category::find($request->id);
                $message = tr('admin_not_category');
                if ($request->hasFile('picture')) {
                    Helper::delete_picture($category->picture, "/uploads/images/");
                }

                // if (($category->name != $request->name) || ($category->slug == '')) {
                //     $category_slug = seoUrl($request->name);

                //     // check to see if any other slugs exist that are the same & count them
                //     $slug_count = Category::whereRaw("category_slug RLIKE '^{$category_slug}(-[0-9]+)?$'")->count();

                //     // if other slugs exist that are the same, append the count to the slug
                //     $slug = ($slug_count) ? $category_slug . '-' . $slug_count : $category_slug;

                //     $category->category_slug = $slug;
                // }
            } else {
                $message = tr('admin_add_category');

                //Add New User
                $category = new Category;
                $category->is_approved = DEFAULT_TRUE;
                $category->created_by = ADMIN;

                // $category_slug = seoUrl($request->name);

                // // check to see if any other slugs exist that are the same & count them
                // $slug_count = Category::whereRaw("category_slug RLIKE '^{$category_slug}(-[0-9]+)?$'")->count();

                // // if other slugs exist that are the same, append the count to the slug
                // $slug = ($slug_count) ? $category_slug . '-' . $slug_count : $category_slug;

                // $category->category_slug = $slug;
            }

            $category->name = $request->has('name') ? $request->name : '';
            $category->description = $request->has('description') ? $request->description : '';
            $category->is_series = $request->has('is_series') ? $request->is_series : 0;
            $category->status = 1;

            if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
                //$category->picture = Helper::normal_upload_picture($request->file('picture'));
                $category->picture = Helper::upload_file_to_digitalocean_spaces_image($request->file('picture'), 'images');
            }

            $category->save();

            // save seo tags
            if (!empty($category->picture)) {
                \SEO\Seo::save($category, url($category->category_slug), [
                    'title' => $category->name,
                    'images' => [
                        $category->picture
                    ]
                ]);
            } else {
                \SEO\Seo::save($category, url($category->category_slug), [
                    'title' => $category->name
                ]);
            }


            if ($category) {

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - Category Created");

                }

                return back()->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function approve_category(Request $request)
    {

        $category = Category::find($request->id);

        $category->is_approved = $request->status;

        $category->save();

        if ($request->status == 0) {
            foreach ($category->subCategory as $sub_category) {
                $sub_category->is_approved = $request->status;
                $sub_category->save();
            }

            foreach ($category->adminVideo as $video) {
                $video->is_approved = $request->status;
                $video->save();
            }

            foreach ($category->genre as $genre) {
                $genre->is_approved = $request->status;
                $genre->save();
            }
        }

        $message = tr('admin_not_category_decline');

        if ($category->is_approved == DEFAULT_TRUE) {

            $message = tr('admin_not_category_approve');
        }

        return back()->with('flash_success', $message);

    }

    public function delete_category(Request $request)
    {

        $category = Category::where('id', $request->category_id)->first();

        if ($category) {

            Helper::delete_picture($category->picture, "/uploads/images/");
            Helper::spaces_delete_picture($category->picture, "images");

            $category->delete();

            return back()->with('flash_success', tr('admin_not_category_del'));

        } else {

            return back()->with('flash_error', tr('admin_not_error'));

        }
    }

    // Methods for Tags
    public function tags(Request $request)
    {

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $tag_search = $_GET['search'];
                $request->session()->put('tag_search', $tag_search);
            } else {
                $request->session()->forget('tag_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $tag_search = $_GET['search'];
                $request->session()->put('tag_search', $tag_search);
            } else {
                $tag_search = $request->session()->get('tag_search');
            }
        }

        $columns = array('id', 'name');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('tag_search')) {
            $tags = Tag::where('name', $tag_search)
                ->orWhere('name', 'LIKE', '%' . $tag_search . '%')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $tag_name = $tag_search;
        } else {
            $tags = Tag::orderBy($column, $order_by)->paginate($per_page)->appends(request()->query());

            $tag_name = '';
        }

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        $tag = new Tag;
        return view('admin.tags.tags')->with('page', 'tags')
            ->with('sub_page', 'view-tags')->with('tags', $tags)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('tag_name', $tag_name)
            ->with('per_page', $per_page)
            ->with('tag', $tag);
    }

    public function add_tag()
    {
        $tag = new Tag;
        return view('admin.tags.add-tag')->with('page', 'tags')->with('sub_page', 'add-tag')->with('tag', $tag);
    }

    public function edit_tag($id)
    {

        $tag = Tag::find($id);

        return view('admin.tags.edit-tag')->with('tag', $tag)->with('page', 'tags')->with('sub_page', 'edit-tag');
    }

    public function add_tag_process(Request $request)
    {

        if ($request->id != '') {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:1|max:300|unique:tags,name,' . $request->id,
                    'slug' => 'unique:tags,slug,' . $request->id
                )
            );
        } else {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100|unique:tags,name'
                )
            );

        }

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {
            if ($request->id != '') {
                $tag = Tag::find($request->id);
                $slug = trim($request->slug);

                if (empty($slug)) {
                    $tag_slug = seoUrl($request->name);
                    $tag->slug = $tag_slug;
                } else {
                    $tag_slug = seoUrl($slug);
                    $tag->slug = $slug;
                }

                $tag->name = $request->has('name') ? $request->name : $tag->name;
                $message = tr('admin_not_tag');
            } else {
                $message = tr('admin_add_tag');

                //Add New User
                $tag = new Tag;
                $tag->name = $request->has('name') ? $request->name : '';

                $tag_slug = seoUrl($request->name);

                // check to see if any other slugs exist that are the same & count them
                $slug_count = Tag::whereRaw("slug RLIKE '^{$tag_slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $slug = ($slug_count) ? $tag_slug . '-' . $slug_count : $tag_slug;

                $tag->slug = $slug;
            }

            $tag->created_at = $request->has('created_at') ? $request->created_at : date('Y-m-d H:i:s');

            $tag->save();


            if ($tag) {
                return redirect('admin/tags')->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function delete_tag(Request $request)
    {

        $tag = Tag::where('id', $request->tag_id)->first();

        if ($tag) {

            $tag->delete();

            return back()->with('flash_success', tr('admin_not_tag_del'));

        } else {

            return back()->with('flash_error', tr('admin_not_error'));

        }
    }
    // Methods for Tags

    /**
     * Function: sub_categories()
     *
     * @param -
     *
     * @return sub_categories management view page
     * @uses used to list the sub_categories
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function sub_categories($category_id)
    {

        $category = Category::find($category_id);

        $sub_categories = SubCategory::where('category_id', $category_id)
            ->select(
                'sub_categories.id as id',
                'sub_categories.name as sub_category_name',
                'sub_categories.description',
                'sub_categories.is_approved',
                'sub_categories.created_by'
            )
            ->orderBy('sub_categories.created_at', 'desc')
            ->paginate(10);
        return view('admin.categories.subcategories.sub-categories')
            ->with('category', $category)
            ->with('data', $sub_categories)
            ->withPage('categories')
            ->with('sub_page', 'view-categories');
    }

    public function add_sub_category($category_id)
    {

        $category = Category::find($category_id);
        $sub_category = new SubCategory;
        return view('admin.categories.subcategories.add-sub-category')->with('category', $category)->with('page', 'categories')->with('sub_page', 'add-category')->with('sub_category', $sub_category);
    }

    public function edit_sub_category(Request $request)
    {

        $category = Category::find($request->category_id);

        $sub_category = SubCategory::find($request->sub_category_id);

        $sub_category_images = SubCategoryImage::where('sub_category_id', $request->sub_category_id)
            ->orderBy('position', 'ASC')->get();

        $genres = Genre::where('sub_category_id', $request->sub_category_id)
            ->orderBy('position', 'asc')
            ->get();

        return view('admin.categories.subcategories.edit-sub-category')
            ->with('category', $category)
            ->with('sub_category', $sub_category)
            ->with('sub_category_images', $sub_category_images)
            ->with('genres', $genres)
            ->with('page', 'categories')
            ->with('sub_page', '');
    }

    public function add_sub_category_process(Request $request)
    {

        if ($request->id != '') {
            $validator = Validator::make($request->all(), array(
                    'category_id' => 'required|integer|exists:categories,id',
                    'id' => 'required|integer|exists:sub_categories,id',
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'picture1' => 'mimes:jpeg,jpg,bmp,png',
                )
            );
        } else {
            $validator = Validator::make($request->all(), array(
                    'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                    'description' => 'required|max:255',
                    'picture1' => 'required|mimes:jpeg,jpg,bmp,png',
                )
            );

        }

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {

            if ($request->id != '') {

                $sub_category = SubCategory::find($request->id);

                $message = tr('admin_not_sub_category');

                if ($request->hasFile('picture1')) {
                    Helper::delete_picture($sub_category->picture1, "/uploads/images/");
                    Helper::spaces_delete_picture($sub_category->picture1);
                }

            } else {
                $message = tr('admin_add_sub_category');
                //Add New User
                $sub_category = new SubCategory;

                $sub_category->is_approved = DEFAULT_TRUE;
                $sub_category->created_by = ADMIN;
            }

            $sub_category->category_id = $request->has('category_id') ? $request->category_id : '';

            if ($request->has('name')) {
                $sub_category->name = $request->name;
            }

            if ($request->has('description')) {
                $sub_category->description = $request->description;
            }

            $sub_category->save();

            if ($request->hasFile('picture1')) {
                $picture1 = sub_category_image($request->file('picture1'), $sub_category->id, 1);
            }

            if ($request->hasFile('picture2')) {
                $picture2 = sub_category_image($request->file('picture2'), $sub_category->id, 2);
            }

            if ($request->hasFile('picture3')) {
                $picture3 = sub_category_image($request->file('picture3'), $sub_category->id, 3);
            }

            $check_image = SubCategoryImage::where('sub_category_id', $sub_category->id)->first();

            // save seo tags
            \SEO\Seo::save($sub_category, url('sub-category/' . $sub_category->name), [
                'title' => $sub_category->name,
                'images' => [
                    $check_image->picture
                ]
            ]);


            if ($sub_category) {

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - Sub category Created");

                }

                return back()->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }

        }

    }

    public function approve_sub_category(Request $request)
    {

        $sub_category = SubCategory::find($request->id);

        $sub_category->is_approved = $request->status;

        $sub_category->save();

        if ($request->status == 0) {

            foreach ($sub_category->adminVideo as $video) {
                $video->is_approved = $request->status;
                $video->save();
            }

            foreach ($sub_category->genres as $genre) {
                $genre->is_approved = $request->status;
                $genre->save();
            }

        }

        $message = tr('admin_not_sub_category_decline');

        if ($sub_category->is_approved == DEFAULT_TRUE) {

            $message = tr('admin_not_sub_category_approve');
        }

        return back()->with('flash_success', $message);

    }

    public function delete_sub_category(Request $request)
    {

        $sub_category = SubCategory::where('id', $request->id)->first();

        if ($sub_category) {

            Helper::delete_picture($sub_category->picture1, "/uploads/images/");
            Helper::spaces_delete_picture($sub_category->picture1, "images");

            $sub_category->delete();

            return back()->with('flash_success', tr('admin_not_sub_category_del'));
        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    public function add_genre($sub_category)
    {

        $subcategory = SubCategory::find($sub_category);

        if ($subcategory) {
            $genre = new Genre;

            return view('admin.categories.subcategories.genres.create')->with('subcategory', $subcategory)->with('page', 'categories')->with('sub_page', 'add-category')->with('genre', $genre);

        } else {

            return back()->with('flash_error', tr('sub_category_not_found'));
        }

    }

    /**
     * Function: genres_edit()
     *
     * @param -
     *
     * @return redirect to view page
     * @uses used to store or update the genre details
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function genres_save(Request $request)
    {

        $validator = Validator::make($request->all(), array(
                'category_id' => 'required|integer|exists:categories,id',
                'sub_category_id' => 'required|integer|exists:sub_categories,id',
                'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:100',
                'video' => ($request->id) ? 'mimes:mkv,mp4,qt' : 'required|mimes:mkv,mp4,qt',
                'image' => ($request->id) ? 'mimes:jpeg,jpg,bmp,png' : 'required|mimes:jpeg,jpg,bmp,png',
            )
        );

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);

        } else {

            $genre = $request->id ? Genre::find($request->id) : new Genre;

            if ($genre->id) {

                $position = $genre->position;

            } else {

                // To order the position of the genres
                $position = 1;

                if ($check_position = Genre::where('sub_category_id', $request->sub_category_id)->orderBy('position', 'desc')->first()) {
                    $position = $check_position->position + 1;
                }

            }

            $genre->category_id = $request->category_id;
            $genre->sub_category_id = $request->sub_category_id;
            $genre->name = $request->name;

            $genre->position = $position;
            $genre->status = DEFAULT_TRUE;
            $genre->is_approved = DEFAULT_TRUE;
            $genre->created_by = ADMIN;

            if ($request->hasFile('video')) {

                if ($genre->id) {

                    if ($genre->video) {

                        Helper::delete_picture($genre->video, '/uploads/videos/original/');

                    }
                }

                $video = Helper::video_upload($request->file('video'), 1);

                $genre->video = $video['db_url'];
            }

            if ($request->hasFile('image')) {

                if ($genre->id) {

                    if ($genre->image) {

                        Helper::delete_picture($genre->image, '/uploads/images/');

                    }
                }

                $genre->image = Helper::normal_upload_picture($request->file('image'), '/uploads/images/');
            }

            if ($request->hasFile('subtitle')) {

                if ($genre->id) {

                    if ($genre->subtitle) {

                        Helper::delete_picture($genre->subtitle, "/uploads/subtitles/");

                    }
                }

                $genre->subtitle = Helper::subtitle_upload($request->file('subtitle'));

            }

            $genre->save();

            $message = ($request->id) ? tr('admin_edit_genre') : tr('admin_add_genre');

            if ($genre) {

                if (!$request->id) {

                    $genre->unique_id = $genre->id;

                    $genre->save();
                }

                if (Setting::get('track_user_mail')) {

                    user_track("StreamHash - Genre Created");

                }
                return back()->with('flash_success', $message);
            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }
        }

    }

    /**
     * Function: genres_edit()
     *
     * @param -
     *
     * @return view page
     * @uses used to display the edit page for the selected genre
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function genres_edit($sub_category_id, $genre_id)
    {

        $subcategory = SubCategory::find($sub_category_id);

        $genre = Genre::find($genre_id);

        return view('admin.categories.subcategories.genres.edit')->with('subcategory', $subcategory)->with('page', 'categories')->with('sub_page', 'add-category')->with('genre', $genre);
    }

    /**
     * Function: genres()
     *
     * @param -
     *
     * @return genres management view page
     * @uses used to list the genres
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function genres($sub_category)
    {

        $subcategory = SubCategory::find($sub_category);

        $genres = Genre::where('sub_category_id', $sub_category)
            ->leftjoin('sub_categories', 'sub_categories.id', '=', 'genres.sub_category_id')
            ->leftjoin('categories', 'categories.id', '=', 'genres.category_id')
            ->select(
                'genres.id as genre_id',
                'categories.name as category_name',
                'sub_categories.name as sub_category_name',
                'genres.name as genre_name',
                'genres.video',
                'genres.subtitle',
                'genres.image',
                'genres.is_approved',
                'genres.created_at',
                'sub_categories.id as sub_category_id',
                'sub_categories.category_id as category_id',
                'genres.position as position'
            )
            ->orderBy('genres.created_at', 'desc')
            ->paginate(10);

        return view('admin.categories.subcategories.genres.index')
            ->with('sub_category', $subcategory)
            ->with('data', $genres)
            ->withPage('categories')
            ->with('sub_page', 'view-categories');

    }

    public function approve_genre(Request $request)
    {

        try {

            DB::beginTransaction();

            $genre = Genre::find($request->id);

            if ($genre) {

                $genre->is_approved = $request->status;

                //$genre->save();

                $position = $genre->position;

                $sub_category_id = $genre->sub_category_id;

                if ($request->status == 0) {

                    foreach ($genre->adminVideo as $video) {

                        $video->is_approved = $request->status;

                        $video->save();

                    }

                    $next_genres = Genre::where('sub_category_id', $sub_category_id)
                        ->where('position', '>', $position)
                        ->orderBy('position', 'asc')
                        ->where('is_approved', DEFAULT_TRUE)
                        ->get();

                    if (count($next_genres) > 0) {

                        foreach ($next_genres as $key => $value) {

                            $value->position = $value->position - 1;

                            if ($value->save()) {

                            } else {

                                throw new Exception(tr('genre_not_saved'));

                            }

                        }

                    }

                    $genre->position = 0;

                } else {

                    $get_genre_position = Genre::where('sub_category_id', $sub_category_id)
                        ->orderBy('position', 'desc')
                        ->where('is_approved', DEFAULT_TRUE)
                        ->first();

                    if ($get_genre_position) {

                        $genre->position = $get_genre_position->position + 1;

                    }

                }

                if ($genre->save()) {

                } else {

                    throw new Exception(tr('genre_not_saved'));

                }

                $message = tr('admin_not_genre_decline');

                if ($genre->is_approved == DEFAULT_TRUE) {

                    $message = tr('admin_not_genre_approve');
                }

                DB::commit();

                return back()->with('flash_success', $message);

            } else {

                throw new Exception(tr('genre_not_found'));

            }

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }

    }

    /**
     * Function Name : genres_view()
     *
     * Used to display the selected genre details
     *
     * @created Vidhya R
     *
     * @edited Vidhya R
     *
     * @param -
     *
     * @return view page
     */

    public function genres_view($id)
    {

        $genre = Genre::where('genres.id', $id)
            ->leftJoin('categories', 'genres.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'genres.sub_category_id', '=', 'sub_categories.id')
            ->select('genres.id as genre_id', 'genres.name as genre_name',
                'genres.position', 'genres.status',
                'genres.is_approved', 'genres.created_at as genre_date',
                'genres.created_by',
                'genres.video',
                'genres.image',
                'genres.category_id as category_id',
                'genres.sub_category_id',
                'categories.name as category_name',
                'genres.unique_id',
                'genres.subtitle',
                'sub_categories.name as sub_category_name')
            ->orderBy('genres.position', 'asc')
            ->first();

        if ($genre) {

            return view('admin.categories.subcategories.genres.view-genre')->with('genre', $genre)
                ->withPage('categories')
                ->with('sub_page', 'view-categories');

        } else {
            return redirect()->route('admin.categories')->with('flash_error', tr('genre_not_found'));
        }

    }

    /**
     * Function Name : genres_delete()
     *
     * Used to delete the selected genre
     *
     * @created Vidhya R
     *
     * @edited Vidhya R
     *
     * @param -
     *
     * @return view page
     */

    public function genres_delete(Request $request)
    {

        try {

            DB::beginTransaction();

            if ($genre = Genre::where('id', $request->id)->first()) {

                Helper::delete_picture($genre->image, '/uploads/images/');

                if ($genre->video) {

                    Helper::delete_picture($genre->video, '/uploads/videos/original/');

                }

                if ($genre->subtitle) {

                    Helper::delete_picture($genre->subtitle, "/uploads/subtitles/");

                }

                $position = $genre->position;

                $sub_category_id = $genre->sub_category_id;

                if ($genre->delete()) {

                    $next_genres = Genre::where('sub_category_id', $sub_category_id)
                        ->where('position', '>', $position)
                        ->orderBy('position', 'asc')
                        ->where('is_approved', DEFAULT_TRUE)
                        ->get();

                    if (count($next_genres) > 0) {

                        foreach ($next_genres as $key => $value) {

                            $value->position = $value->position - 1;

                            $value->save();

                        }

                    }

                } else {

                    throw new Exception(tr('genre_not_saved'));

                }

            } else {

                throw new Exception(tr('genre_not_found'));

            }

            DB::commit();

            return back()->with('flash_success', tr('admin_not_genre_del'));

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }

    }

    /**
     * Function Name: videos()
     *
     * @param Get the video list in table
     *
     * @return Videos list
     * @uses: get the videos list
     *
     * @created vidhya R
     *
     * @edited Vedgupt S
     *
     */

    public function videos(Request $request)
    {
        $title = $request->get('title');
        $categoy = $request->get('category_name');
        $tag = $request->get('tag');

        $title = (!empty($title)) ? $title : '';
        $tag = (!empty($tag)) ? $tag : '';
        $category = (!empty($category)) ? $category : '';

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('video_id', 'title', 'alternative_titles', 'amount', 'admin_amount', 'category_name', 'watch_count', 'publish_time', 'is_banner', 'uploaded_by', 'is_approved', 'created_at');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if ($request->session()->get('search')) {
            $search_string = $request->session()->get('search');
            $query = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
                ->select('admin_videos.id as video_id', 'admin_videos.sub_category_id', 'admin_videos.title',
                    'admin_videos.description', 'admin_videos.ratings',
                    'admin_videos.reviews', 'admin_videos.created_at as video_date',
                    'admin_videos.default_image',
                    'admin_videos.banner_image',
                    'admin_videos.amount',
                    'admin_videos.admin_amount',
                    'admin_videos.user_amount',
                    'admin_videos.unique_id',
                    'admin_videos.type_of_user',
                    'admin_videos.type_of_subscription',
                    'admin_videos.category_id as category_id',
                    'admin_videos.sub_category_id',
                    'admin_videos.genre_id',
                    'admin_videos.is_home_slider',
                    'admin_videos.watch_count',
                    'admin_videos.compress_status',
                    'admin_videos.trailer_compress_status',
                    'admin_videos.main_video_compress_status',
                    'admin_videos.status', 'admin_videos.uploaded_by',
                    'admin_videos.edited_by', 'admin_videos.is_approved',
                    'admin_videos.video_subtitle',
                    'admin_videos.trailer_subtitle',
                    'admin_videos.release_date',
                    'admin_videos.publish_time',
                    'categories.name as category_name', 'sub_categories.name as sub_category_name',
                    'genres.name as genre_name',
                    'admin_videos.is_banner',
                    'admin_videos.position')
                ->orWhere('admin_videos.alternative_titles', 'LIKE', '%' . $request->session()->get('search') . '%')
                ->orWhere('admin_videos.title', $request->session()->get('search'))
                ->orWhere('admin_videos.title', 'LIKE', '%' . $request->session()->get('search') . '%')
                ->orderBy($column, $order_by);

            $title_search = $request->session()->get('search');
        } else if ($tag != '') {
            $query = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
                ->leftJoin('admin_video_tags', 'admin_videos.id', '=', 'admin_video_tags.admin_video_id')
                ->select('admin_videos.id as video_id', 'admin_videos.sub_category_id', 'admin_videos.title',
                    'admin_videos.description', 'admin_videos.ratings',
                    'admin_videos.reviews', 'admin_videos.created_at as video_date',
                    'admin_videos.default_image',
                    'admin_videos.banner_image',
                    'admin_videos.amount',
                    'admin_videos.admin_amount',
                    'admin_videos.user_amount',
                    'admin_videos.unique_id',
                    'admin_videos.type_of_user',
                    'admin_videos.type_of_subscription',
                    'admin_videos.category_id as category_id',
                    'admin_videos.sub_category_id',
                    'admin_videos.genre_id',
                    'admin_videos.is_home_slider',
                    'admin_videos.watch_count',
                    'admin_videos.compress_status',
                    'admin_videos.trailer_compress_status',
                    'admin_videos.main_video_compress_status',
                    'admin_videos.status', 'admin_videos.uploaded_by',
                    'admin_videos.edited_by', 'admin_videos.is_approved',
                    'admin_videos.video_subtitle',
                    'admin_videos.trailer_subtitle',
                    'admin_videos.release_date',
                    'admin_videos.publish_time',
                    'categories.name as category_name', 'sub_categories.name as sub_category_name',
                    'genres.name as genre_name',
                    'admin_videos.is_banner',
                    'admin_videos.position')
                ->where('admin_video_tags.tag_id', $tag)
                ->orderBy($column, $order_by);

            $title_search = $tag;
        } else {
            $query = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
                ->select('admin_videos.id as video_id', 'admin_videos.sub_category_id', 'admin_videos.title',
                    'admin_videos.description', 'admin_videos.ratings',
                    'admin_videos.reviews', 'admin_videos.created_at as video_date',
                    'admin_videos.default_image',
                    'admin_videos.banner_image',
                    'admin_videos.amount',
                    'admin_videos.admin_amount',
                    'admin_videos.user_amount',
                    'admin_videos.unique_id',
                    'admin_videos.type_of_user',
                    'admin_videos.type_of_subscription',
                    'admin_videos.category_id as category_id',
                    'admin_videos.sub_category_id',
                    'admin_videos.genre_id',
                    'admin_videos.is_home_slider',
                    'admin_videos.watch_count',
                    'admin_videos.compress_status',
                    'admin_videos.trailer_compress_status',
                    'admin_videos.main_video_compress_status',
                    'admin_videos.status', 'admin_videos.uploaded_by',
                    'admin_videos.edited_by', 'admin_videos.is_approved',
                    'admin_videos.video_subtitle',
                    'admin_videos.trailer_subtitle',
                    'admin_videos.release_date',
                    'admin_videos.publish_time',
                    'categories.name as category_name', 'sub_categories.name as sub_category_name',
                    'genres.name as genre_name',
                    'admin_videos.is_banner',
                    'admin_videos.position')
                ->orderBy($column, $order_by);

            $title_search = '';
        }

        if ($request->banner == BANNER_VIDEO) {

            $query->where('is_banner', BANNER_VIDEO);

            $sub_page = 'view-banner-videos';

        } else {

            $sub_page = 'view-videos';

        }

        $category = $sub_category = $genre = $moderator_details = "";

        if ($request->category_id) {

            $query->where('admin_videos.category_id', $request->category_id);

            $category = Category::find($request->category_id);

        }

        if ($request->sub_category_id) {

            $query->where('admin_videos.sub_category_id', $request->sub_category_id);

            $sub_category = SubCategory::find($request->sub_category_id);

        }

        if ($request->genre_id) {

            $query->where('admin_videos.genre_id', $request->genre_id);

            $genre = Genre::find($request->genre_id);

        }

        if ($request->moderator_id) {

            $query->where('admin_videos.moderator_id', $request->moderator_id);

            $moderator_details = Moderator::find($request->moderator_id);

        }

        if (isset($_GET['moderator_id']) && !empty($_GET['moderator_id'])) {
            $moderator_id = $_GET['moderator_id'];
        } else {
            $moderator_id = '';
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        $videos = $query->paginate($per_page)->appends(request()->query());

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';
        return view('admin.videos.videos')->with('videos', $videos)
            ->withPage('videos')
            ->with('sub_page', $sub_page)
            ->with('category', $category)
            ->with('sub_category', $sub_category)
            ->with('genre', $genre)
            ->with('moderator_details', $moderator_details)
            ->with('moderator_id', $moderator_id)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('title_search', $title_search)
            ->with('category', $category)
            ->with('per_page', $per_page);
    }

    /**
     * Function Name : moderator_videos()
     *
     * Description: Display the moderator videos list
     *
     * @param Moderator id
     *
     * @return Moderator video list details
     */
    public function moderator_videos(Request $request, $id)
    {
        $title = $request->get('title');
        $columns = array('video_id', 'title', 'admin_amount', 'category_name', 'watch_count', 'publish_time', 'is_banner', 'uploaded_by', 'is_approved', 'created_at');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (isset($_GET['moderator_id']) && !empty($_GET['moderator_id'])) {
            $moderator_id = $_GET['moderator_id'];
        } else {
            $moderator_id = '';
        }

        $title_search = $request->session()->get('search');
        $videos = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->select('admin_videos.id as video_id', 'admin_videos.title',
                'admin_videos.description', 'admin_videos.ratings',
                'admin_videos.reviews', 'admin_videos.created_at as video_date',
                'admin_videos.default_image',
                'admin_videos.amount',
                'admin_videos.user_amount',
                'admin_videos.type_of_user',
                'admin_videos.type_of_subscription',
                'admin_videos.category_id as category_id',
                'admin_videos.sub_category_id',
                'admin_videos.genre_id',
                'admin_videos.compress_status',
                'admin_videos.trailer_compress_status',
                'admin_videos.main_video_compress_status',
                'admin_videos.redeem_amount',
                'admin_videos.watch_count',
                'admin_videos.unique_id',
                'admin_videos.status', 'admin_videos.uploaded_by',
                'admin_videos.edited_by', 'admin_videos.is_approved',
                'admin_videos.video_subtitle',
                'admin_videos.trailer_subtitle',
                'categories.name as category_name', 'sub_categories.name as sub_category_name',
                'genres.name as genre_name')
            //->orderBy('admin_videos.created_at', 'desc')
            ->orderBy($column, $order_by)
            ->where('moderator_id', $id)
            ->paginate($per_page);

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';
        return view('admin.videos.videos')
            ->with('videos', $videos)
            ->with('category', '')
            ->with('sub_category', '')
            ->with('genre', '')
            ->withPage('videos')
            ->with('sub_page', 'view-videos')
            ->with('moderator_id', $moderator_id)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('title_search', $title_search)
            ->with('per_page', $per_page);
    }

    public function view_video(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:admin_videos,id',
        ]);

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error_messages);
        } else {
            $videos = AdminVideo::where('admin_videos.id', $request->id)
                ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
                ->select('admin_videos.id as video_id', 'admin_videos.title',
                    'admin_videos.description', 'admin_videos.ratings',
                    'admin_videos.reviews', 'admin_videos.created_at as video_date',
                    'admin_videos.video', 'admin_videos.trailer_video',
                    'admin_videos.default_image', 'admin_videos.banner_image', 'admin_videos.is_banner', 'admin_videos.video_type',
                    'admin_videos.video_upload_type',
                    'admin_videos.amount',
                    'admin_videos.type_of_user',
                    'admin_videos.type_of_subscription',
                    'admin_videos.category_id as category_id',
                    'admin_videos.sub_category_id',
                    'admin_videos.genre_id',
                    'admin_videos.video_type',
                    'admin_videos.uploaded_by',
                    'admin_videos.ppv_created_by',
                    'admin_videos.details',
                    'admin_videos.watch_count',
                    'admin_videos.admin_amount',
                    'admin_videos.user_amount',
                    'admin_videos.video_upload_type',
                    'admin_videos.video_type_trailer',
                    'admin_videos.duration',
                    'admin_videos.redeem_amount',
                    'admin_videos.compress_status',
                    'admin_videos.trailer_compress_status',
                    'admin_videos.main_video_compress_status',
                    'admin_videos.video_resolutions',
                    'admin_videos.video_resize_path',
                    'admin_videos.trailer_resize_path',
                    'admin_videos.is_approved',
                    'admin_videos.unique_id',
                    'admin_videos.video_subtitle',
                    'admin_videos.trailer_subtitle',
                    'admin_videos.trailer_duration',
                    'admin_videos.trailer_video_resolutions',
                    'admin_videos.publish_time',
                    'categories.name as category_name', 'sub_categories.name as sub_category_name',
                    'genres.name as genre_name',
                    'admin_videos.video_gif_image',
                    'admin_videos.is_banner',
                    'admin_videos.is_pay_per_view',
                    'admin_videos.actors',
                    'admin_videos.directors',
                    'admin_videos.writers',
                    'admin_videos.moderator_id',
                    'admin_videos.age',
                    'admin_videos.release_date',
                    'admin_videos.alternative_titles',
                    'admin_videos.is_home_slider',
                    'admin_videos.is_banner',
                    'admin_videos.status'
                )
                ->orderBy('admin_videos.created_at', 'desc')
                ->first();

            $videoPath = $video_pixels = $trailer_video_path = $trailer_pixels = $trailerstreamUrl = $videoStreamUrl = '';

            $ios_trailer_video = $videos->trailer_video;

            $ios_video = $videos->video;

            if ($videos->video_type == VIDEO_TYPE_UPLOAD && $videos->video_upload_type == VIDEO_UPLOAD_TYPE_DIRECT) {

                if (check_valid_url($videos->trailer_video)) {

                    if (Setting::get('streaming_url')) {
                        $trailerstreamUrl = Setting::get('streaming_url') . get_video_end($videos->trailer_video);
                    }

                    if (Setting::get('HLS_STREAMING_URL')) {
                        $ios_trailer_video = Setting::get('HLS_STREAMING_URL') . get_video_end($videos->trailer_video);
                    }

                }

                if (check_valid_url($videos->video)) {

                    if (Setting::get('streaming_url')) {
                        $videoStreamUrl = Setting::get('streaming_url') . get_video_end($videos->video);
                    }

                    if (Setting::get('HLS_STREAMING_URL')) {
                        $ios_video = Setting::get('HLS_STREAMING_URL') . get_video_end($videos->video);
                    }

                }

                if (\Setting::get('streaming_url')) {
                    if ($videos->is_approved == 1) {
                        if ($videos->trailer_video_resolutions) {
                            $trailerstreamUrl = Helper::web_url() . '/uploads/smil/' . get_video_end_smil($videos->trailer_video) . '.smil';
                        }
                        if ($videos->video_resolutions) {
                            $videoStreamUrl = Helper::web_url() . '/uploads/smil/' . get_video_end_smil($videos->video) . '.smil';
                        }
                    }
                } else {

                    $videoPath = $videos->video_resize_path ? $videos->video . ',' . $videos->video_resize_path : $videos->video;
                    $video_pixels = $videos->video_resolutions ? 'original,' . $videos->video_resolutions : 'original';
                    $trailer_video_path = $videos->trailer_resize_path ? $videos->trailer_video . ',' . $videos->trailer_resize_path : $videos->trailer_video;
                    $trailer_pixels = $videos->trailer_video_resolutions ? 'original,' . $videos->trailer_video_resolutions : 'original';
                }

                $trailerstreamUrl = $trailerstreamUrl ? $trailerstreamUrl : "";
                $videoStreamUrl = $videoStreamUrl ? $videoStreamUrl : "";
            } else {

                $trailerstreamUrl = $videos->trailer_video;

                $videoStreamUrl = $videos->video;

                if ($videos->video_type == VIDEO_TYPE_YOUTUBE) {

                    $videoStreamUrl = $ios_video = get_youtube_embed_link($videos->video);

                }

                if ($videos->video_type_trailer == VIDEO_TYPE_YOUTUBE) {
                    $trailerstreamUrl = $ios_trailer_video = getYoutubeIdFromUrl($videos->trailer_video);
                }


            }

            $admin_video_images = AdminVideoImage::where('admin_video_id', $request->id)
                ->orderBy('is_default', 'desc')
                ->get();

            $page = 'videos';

            $sub_page = 'admin_videos_view';

            if ($videos->is_banner == 1) {

                $sub_page = 'view-banner-videos';
            }

            $video_cast_crews = VideoCastCrew::select('cast_crew_id', 'name')
                ->where('admin_video_id', $request->id)
                ->leftjoin('cast_crews', 'cast_crews.id', '=', 'video_cast_crews.cast_crew_id')
                ->get()->pluck('name')->toArray();

            // $videoStreamUrl = change_web_url_to_cdn($videoStreamUrl);
            // $trailerstreamUrl = change_web_url_to_cdn($trailerstreamUrl);
            // $videoPath = change_web_url_to_cdn($videoPath);
            // $trailer_video_path = change_web_url_to_cdn($trailer_video_path);

            return view('admin.videos.view-video')->with('video', $videos)
                ->with('video_images', $admin_video_images)
                ->withPage($page)
                ->with('sub_page', $sub_page)
                ->with('videoPath', $videoPath)
                ->with('video_pixels', $video_pixels)
                ->with('ios_trailer_video', $ios_trailer_video)
                ->with('ios_video', $ios_video)
                ->with('trailer_video_path', $trailer_video_path)
                ->with('trailer_pixels', $trailer_pixels)
                ->with('videoStreamUrl', $videoStreamUrl)
                ->with('trailerstreamUrl', $trailerstreamUrl)
                ->with('video_cast_crews', $video_cast_crews);
        }
    }

    public function approve_video($id)
    {

        try {

            DB::beginTransaction();

            $video = AdminVideo::find($id);

            $video->is_approved = DEFAULT_TRUE;

            if (empty($video->publish_time) || $video->publish_time == '0000-00-00 00:00:00') {

                $video->publish_time = date('Y-m-d H:i:s');

            }

            // Check the video has genre type or not

            if ($video->genre_id > 0) {

                /** Check is there any videos present in same genre,* if it is, assign the position with increment of 1*/

                $get_video_position = AdminVideo::where('genre_id', $video->genre_id)
                    ->orderBy('position', 'desc')
                    ->where('is_approved', DEFAULT_TRUE)
                    ->where('status', DEFAULT_TRUE)
                    ->first();

                if ($get_video_position) {

                    $video->position = $get_video_position->position + 1;

                }

            }

            // Uncommented by vidhya. with below code the response will error

            if ($video->is_approved == DEFAULT_TRUE) {

                // Notification::save_notification($video->id);

                $message = tr('admin_not_video_approve');
            } else {
                $message = tr('admin_not_video_decline');
            }

            $video->save();

            DB::commit();

            return back()->with('flash_success', $message);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }
    }

    public function seo_create(Request $request)
    {
        if ($request->get('id') == null) {
            return redirect('admin/videos');
        }
        $seo = DB::table('admin_video_seo')->where('admin_video_id', $request->get('id'))->first();

        $video = AdminVideo::find($request->get('id'));

        return view('admin.videos.video-seo-create')->with('page', 'videos')->with('sub_page', 'seo')->with('video', $video)->with('seo', $seo);

    }

    public function seo_store(Request $request)
    {
        $video = DB::table('admin_video_seo')->where('admin_video_id', $request->video_id)->first();

        if ($video == null) {
            DB::table('admin_video_seo')->insert([
                'admin_video_id' => $request->video_id,
                'title' => $request->title,
                'keywords' => $request->keywords,
                'description' => $request->description,
            ]);
        } else {
            DB::table('admin_video_seo')->where('admin_video_id', $request->video_id)
                ->update([
                    'admin_video_id' => $request->video_id,
                    'title' => $request->title,
                    'keywords' => $request->keywords,
                    'description' => $request->description,
                ]);
        }

        return back();
    }

    /**
     * Function Name : publish_video()
     * To Publish the video for user
     *
     * @param int $id : Video id
     *
     * @return Flash Message
     */
    public function publish_video($id)
    {
        // Load video based on Auto increment id
        $video = AdminVideo::find($id);
        // Check the video present or not
        if ($video) {
            $video->status = DEFAULT_TRUE;
            $video->publish_time = date('Y-m-d H:i:s');
            // Save the values in DB
            if ($video->save()) {
                return back()->with('flash_success', tr('admin_published_video_success'));
            }
        }
        return back()->with('flash_error', tr('admin_published_video_failure'));
    }

    public function decline_video($id)
    {

        try {

            $video = AdminVideo::find($id);

            $video->is_approved = DEFAULT_FALSE;

            // Check the video has genre type or not

            if ($video->genre_id > 0) {

                /** Check is there any videos present in same genre,* if it is, assign the position with decrement of 1.(for all videos) */

                $next_videos = AdminVideo::where('genre_id', $video->genre_id)
                    ->where('position', '>', $video->position)
                    ->orderBy('position', 'asc')
                    ->where('is_approved', DEFAULT_TRUE)
                    ->where('status', DEFAULT_TRUE)
                    ->get();

                if (count($next_videos) > 0) {

                    foreach ($next_videos as $key => $value) {

                        $value->position = $value->position - 1;

                        if ($value->save()) {

                        } else {

                            throw new Exception(tr('video_not_saved'));

                        }

                    }

                }

                $video->position = 0;

            }

            if ($video->is_approved == DEFAULT_TRUE) {

                $message = tr('admin_not_video_approve');

            } else {

                $message = tr('admin_not_video_decline');

            }

            DB::commit();

            $video->save();

            return back()->with('flash_success', $message);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());

        }
    }

    public function delete_video($id)
    {

        try {

            DB::beginTransaction();

            if ($video = AdminVideo::where('id', $id)->first()) {

                $main_video = $video->video;
                $trailer_video = $video->trailer_video;

                $subtitle = $video->subtitle;
                $trailer_subtitle = $video->trailer_subtitle;

                $banner_image = $video->banner_image;

                $default_image = $video->default_image;

                $video_resize_path = $video->video_resize_path;

                $trailer_resize_path = $video->trailer_resize_path;

                $position = $video->position;

                $genre_id = $video->genre_id;

                if ($video->delete()) {

                    if ($genre_id > 0) {

                        $next_videos = AdminVideo::where('genre_id', $genre_id)
                            ->where('position', '>', $position)
                            ->orderBy('position', 'asc')
                            ->where('is_approved', DEFAULT_TRUE)
                            ->where('status', DEFAULT_TRUE)
                            ->get();

                        if (count($next_videos) > 0) {

                            foreach ($next_videos as $key => $value) {

                                $value->position = $value->position - 1;

                                if ($value->save()) {

                                } else {

                                    throw new Exception(tr('video_not_saved'));

                                }

                            }

                        }

                    }
                    $images_others = DB::table('admin_video_images')
                        ->where('admin_video_id', $id)
                        ->get();
                    if (!empty($images_others)) {
                        foreach ($images_others as $key => $value) {
                            Helper::delete_picture($value->image, "/uploads/images/");
                            Helper::spaces_delete_picture($value->image, "images");
                        }
                    }

                    Helper::delete_picture($main_video, "/uploads/videos/original/");
                    Helper::delete_picture($trailer_video, "/uploads/videos/original/");

                    Helper::delete_picture($subtitle, "/uploads/subtitles/");
                    Helper::delete_picture($trailer_subtitle, "/uploads/subtitles/");

                    Helper::spaces_delete_picture($main_video, 'videos');
                    Helper::spaces_delete_picture($trailer_video, 'videos');

                    if ($banner_image) {

                        Helper::delete_picture($banner_image, "/uploads/images/");
                    }

                    Helper::delete_picture($default_image, "/uploads/images/");
                    Helper::spaces_delete_picture($default_image, "images");

                    if ($video_resize_path) {

                        $explode = explode(',', $video_resize_path);

                        if (count($explode) > 0) {

                            foreach ($explode as $key => $exp) {

                                Helper::delete_picture($exp, "/uploads/videos/original/");

                            }

                        }

                    }

                    if ($trailer_resize_path) {

                        $explode = explode(',', $trailer_resize_path);

                        if (count($explode) > 0) {

                            foreach ($explode as $key => $exp) {

                                Helper::delete_picture($exp, "/uploads/videos/original/");

                            }

                        }

                    }

                } else {

                    throw new Exception(tr('video_delete_failure'));

                }

            }

            DB::commit();

            return back()->with('flash_success', 'Video deleted successfully');

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }
    }

    public function slider_video($id)
    {

        $video = AdminVideo::where('is_home_slider', 1)->update(['is_home_slider' => 0]);

        $video = AdminVideo::where('id', $id)->update(['is_home_slider' => 1]);

        return back()->with('flash_success', tr('slider_success'));

    }

    /**
     * Function: banner_videos()
     *
     * @param -
     *
     * @return banner_videos management view page
     * @uses used to list the banner videos
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function banner_videos(Request $request)
    {

        $videos = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.is_banner', 1)
            ->select('admin_videos.id as video_id', 'admin_videos.title',
                'admin_videos.description', 'admin_videos.ratings',
                'admin_videos.reviews', 'admin_videos.created_at as video_date',
                'admin_videos.default_image',
                'admin_videos.banner_image',

                'admin_videos.category_id as category_id',
                'admin_videos.sub_category_id',
                'admin_videos.genre_id',
                'admin_videos.is_home_slider',

                'admin_videos.status', 'admin_videos.uploaded_by',
                'admin_videos.edited_by', 'admin_videos.is_approved',

                'categories.name as category_name', 'sub_categories.name as sub_category_name',
                'genres.name as genre_name')
            ->orderBy('admin_videos.created_at', 'desc')
            ->paginate(10);

        return view('admin.banner_videos.banner-videos')->with('videos', $videos)
            ->withPage('banner-videos')
            ->with('sub_page', 'view-banner-videos');

    }

    public function add_banner_video(Request $request)
    {

        $categories = Category::where('categories.is_approved', 1)
            ->select('categories.id as id', 'categories.name', 'categories.picture',
                'categories.is_series', 'categories.status', 'categories.is_approved')
            ->leftJoin('sub_categories', 'categories.id', '=', 'sub_categories.category_id')
            ->groupBy('sub_categories.category_id')
            ->havingRaw("COUNT(sub_categories.id) > 0")
            ->orderBy('categories.name', 'asc')
            ->get();

        return view('admin.banner_videos.banner-video-upload')
            ->with('categories', $categories)
            ->with('page', 'banner-videos')
            ->with('sub_page', 'add-banner-video');

    }

    public function change_banner_video($id)
    {

        $video = AdminVideo::find($id);

        $video->is_banner = 0;

        $video->save();

        $message = tr('change_banner_video_success');

        return back()->with('flash_success', $message);
    }

    /**
     * Function: user_ratings()
     *
     * @param -
     *
     * @return user_ratings management view page
     * @uses used to list the user_ratings
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function user_ratings()
    {

        $user_reviews = UserReview::leftJoin('users', 'user_ratings.user_id', '=', 'users.id')
            ->select('user_ratings.id as rating_id', 'user_ratings.rating',
                'user_ratings.comment',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.id as user_id', 'user_ratings.created_at')
            ->orderBy('user_ratings.id', 'ASC')
            ->paginate(10);
        return view('admin.reviews')->with('name', 'User')->with('reviews', $user_reviews);
    }

    public function delete_user_ratings(Request $request)
    {

        if ($user = UserReview::find($request->id)) {
            $user->delete();
        }

        return back()->with('flash_success', tr('admin_not_ur_del'));
    }

    /**
     * Function: user_payments()
     *
     * @param -
     *
     * @return user_payments management view page
     * @uses used to list the user_payments
     *
     * @created vidhya R
     *
     * @edited Vedgupt S
     *
     */

    public function user_payments(Request $request)
    {

        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('created_at', 'name');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('search')) {
            $search = $request->session()->get('search');
            $query = UserPayment::whereHas('user', function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');

            })->orderBy($column, $order_by);
        } else {
            $query = UserPayment::orderBy($column, $order_by);
            $search = '';
        }

        $payments = $query->paginate($per_page)->appends(request()->query());

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        $payment_count = UserPayment::count();

        return view('admin.payments.user-payments')->with('data', $payments)->with('page', 'payments')->with('sub_page', 'user-payments')->with('payment_count', $payment_count)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('search', $search)
            ->with('per_page', $per_page);
    }

    public function email_settings()
    {

        $admin_id = \Auth::guard('admin')->user()->id;

        $result = EnvEditorHelper::getEnvValues();

        \Auth::guard('admin')->loginUsingId($admin_id);

        return view('admin.email-settings')->with('result', $result)->withPage('email-settings')->with('sub_page', '');
    }

    public function email_settings_process(Request $request)
    {

        $email_settings = ['MAIL_DRIVER', 'MAIL_HOST', 'MAIL_PORT', 'MAIL_USERNAME', 'MAIL_PASSWORD', 'MAIL_ENCRYPTION', 'MAILGUN_DOMAIN', 'MAILGUN_SECRET'];

        $admin_id = \Auth::guard('admin')->user()->id;

        if ($email_settings) {
            foreach ($email_settings as $key => $data) {

                if ($request->$data) {

                    // \Enveditor::set($data,$request->$data);
                    EnvEditorHelper::setEnv($data, $data, $request->$data);

                } else {

                    //\Enveditor::set($data,$request->$data);
                    EnvEditorHelper::setEnv($data, $data, $request->$data);
                }
            }
        }

        $result = EnvEditorHelper::getEnvValues();

        return redirect(route('clear-cache'))->with('result', $result)->with('flash_success', tr('email_settings_success'));

    }

    /**
     * Function Name: mobile_settings_save()
     *
     * @param form data
     *
     * @return redirect to success page
     *
     * @uses used to update the mobile app related details
     *
     * @created Vidhya R
     *
     * @edited
     *
     */

    public function mobile_settings_save(Request $request)
    {

        $mobile_settings = ['FCM_SENDER_ID', 'FCM_SERVER_KEY', 'FCM_PROTOCOL'];

        $admin_id = \Auth::guard('admin')->user()->id;

        if ($mobile_settings) {

            foreach ($mobile_settings as $key => $data) {

                if ($request->$data) {

                    //\Enveditor::set($data,$request->$data);
                    EnvEditorHelper::setEnv($data, $data, $request->$data);

                } else {

                    //\Enveditor::set($data,$request->$data);
                    EnvEditorHelper::setEnv($data, $data, $request->$data);
                }
            }

        }

        if ($request->has('playstore')) {
            Settings::where('key', 'playstore')->update(['value' => $request->playstore]);
        }

        if ($request->has('appstore')) {
            Settings::where('key', 'appstore')->update(['value' => $request->appstore]);
        }

        $result = EnvEditorHelper::getEnvValues();

        return redirect(route('clear-cache'))->with('result', $result)->with('flash_success', tr('mobile_settings_success'));

    }

    public function other_settings(Request $request)
    {

        $settings = Settings::where('key', 'token_expiry_hour')->first();

        if ($settings) {

            $settings->value = $request->token_expiry_hour;

            $settings->save();

        }

        $settings = Settings::where('key', 'custom_users_count')->first();

        if ($settings) {

            $settings->value = $request->custom_users_count;

            $settings->save();

        }

        $settings = Settings::where('key', 'email_notification')->first();

        if ($settings) {

            $settings->value = $request->email_notification ? $request->email_notification : DEFAULT_FALSE;

            $settings->save();

        }

        $settings = Settings::where('key', 'google_analytics')->first();

        if ($settings) {

            $settings->value = $request->google_analytics ? $request->google_analytics : "";

            $settings->save();

        }

        $settings = Settings::where('key', 'header_scripts')->first();

        if ($settings) {

            $settings->value = $request->header_scripts ? $request->header_scripts : "";

            $settings->save();

        }

        $settings = Settings::where('key', 'body_scripts')->first();

        if ($settings) {

            $settings->value = $request->body_scripts ? $request->body_scripts : "";

            $settings->save();

        }

        $settings = Settings::where('key', 'copyright_content')->first();

        if ($settings) {

            $settings->value = $request->copyright_content ? $request->copyright_content : "";

            $settings->save();

        }

        return redirect(route('clear-cache'))->with('flash_success', tr('email_settings_success'));
    }

    public function vue_settings(Request $request)
    {
        if (file_exists(base_path() . $request->vue_site_path)) {

            $settings = Settings::where('key', 'vue_site_path')->first();
            if ($settings) {
                $settings->value = $request->vue_site_path;
                $settings->save();
            }

            $settings = Settings::where('key', 'vue_site_name')->first();
            if ($settings) {

                file_put_contents(base_path() . $request->vue_site_path, str_replace(
                    'u,"title","' . $settings->value . '"',
                    'u,"title","' . $request->vue_site_name . '"',
                    file_get_contents(base_path() . $request->vue_site_path)
                ));

                $settings->value = $request->vue_site_name;
                $settings->save();
            }

            $settings = Settings::where('key', 'vue_site_url')->first();
            if ($settings) {

                file_put_contents(base_path() . $request->vue_site_path, str_replace(
                    'u,"siteUrl","' . $settings->value . '"',
                    'u,"siteUrl","' . $request->vue_site_url . '"',
                    file_get_contents(base_path() . $request->vue_site_path)
                ));

                $settings->value = $request->vue_site_url;
                $settings->save();

            }

            $settings = Settings::where('key', 'vue_site_base_url')->first();
            if ($settings) {

                file_put_contents(base_path() . $request->vue_site_path, str_replace(
                    'u,"appBase","' . $settings->value . '"',
                    'u,"appBase","' . $request->vue_site_base_url . '"',
                    file_get_contents(base_path() . $request->vue_site_path)
                ));

                $settings->value = $request->vue_site_base_url;
                $settings->save();
            }

            $settings = Settings::where('key', 'vue_site_api_url')->first();
            if ($settings) {

                file_put_contents(base_path() . $request->vue_site_path, str_replace(
                    'u,"apiBase","' . $settings->value . '"',
                    'u,"apiBase","' . $request->vue_site_api_url . '"',
                    file_get_contents(base_path() . $request->vue_site_path)
                ));

                $settings->value = $request->vue_site_api_url;
                $settings->save();

            }

            return redirect(route('clear-cache'))->with('flash_success', 'Vue setting updated successfully');
        }
        return redirect(route('clear-cache'))->with('flash_error', 'Vue Setting page not found');

    }

    public function getContactUs(Request $request)
    {
        return view('admin.settings.contact-us') ->with('page', 'contact-us')
        ->with('sub_page', 'home_page_settings');
    }

    public function contact_settings(Request $request)
    {

        $settings = Settings::where('key', 'contact_email')->first();
        if ($settings) {
            $settings->value = $request->contact_email;
            $settings->save();
        }

        $settings = Settings::where('key', 'contact_mobile')->first();
        if ($settings) {
            $settings->value = $request->contact_mobile;
            $settings->save();
        }

        $settings = Settings::where('key', 'contact_address')->first();
        if ($settings) {
            $settings->value = $request->contact_address;
            $settings->save();
        }

        $settings = Settings::where('key', 'contact_description')->first();
        if ($settings) {
            $settings->value = $request->contact_description;
            $settings->save();
        }

        return redirect(route('clear-cache'))->with('flash_success', 'Vue setting updated successfully');

    }
    
    public function captcha_settings(Request $request)
    {

        $settings = Settings::where('key', 'GOOGLE_RECAPTCHA_SECRET')->first();
        if ($settings) {
            $settings->value = $request->GOOGLE_RECAPTCHA_SECRET;
            $settings->save();
        }

        $settings = Settings::where('key', 'GOOGLE_RECAPTCHA_SITE_KEY')->first();
        if ($settings) {
            $settings->value = $request->GOOGLE_RECAPTCHA_SITE_KEY;
            $settings->save();
        }
        return back()->with('flash_success', 'Catpcha setting updated successfully');
    }

    public function settings()
    {
        $settings = array();

        $result = EnvEditorHelper::getEnvValues();

        $languages = Language::where('status', DEFAULT_TRUE)->get();

        return view('admin.settings.settings')
            ->with('settings', $settings)
            ->with('result', $result)
            ->withPage('settings')
            ->with('sub_page', 'site_settings')
            ->with('languages', $languages);

    }

    /**
     * Functiont Name: home_page_settings()
     *
     * @param Get the route of home page setting option
     *
     * @return Html page
     * @uses: User home page content with image uploading page settings
     *
     * @created Maheswari
     *
     * @editd Maheswari
     *
     */

    public function home_page_settings()
    {

        return view('admin.settings.home_page')
            ->with('page', 'settings')
            ->with('sub_page', 'home_page_settings');

    }

    public function payment_settings()
    {

        $settings = array();

        return view('admin.payment-settings')->with('settings', $settings)->withPage('payment-settings')->with('sub_page', '');
    }

    public function theme_settings()
    {

        $settings = array();

        $settings[] = Setting::get('theme');

        if (Setting::get('theme') != 'default') {
            $settings[] = 'default';
        }

        if (Setting::get('theme') != 'teen') {
            $settings[] = 'teen';
        }

        return view('admin.theme.theme-settings')->with('settings', $settings)->withPage('theme-settings')->with('sub_page', '');
    }

    public function settings_process(Request $request)
    {

        $settings = Settings::all();

        $check_streaming_url = "";

        $refresh = "";

        if ($settings) {

            foreach ($settings as $setting) {

                $key = $setting->key;

                if ($setting->key == 'site_icon') {

                    if ($request->hasFile('site_icon')) {

                        if ($setting->value) {
                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('site_icon'), 'images');

                    }

                } else if ($setting->key == 'site_logo') {

                    if ($request->hasFile('site_logo')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('site_logo'), 'images');
                    }

                } else if ($setting->key == 'home_page_bg_image') {

                    if ($request->hasFile('home_page_bg_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('home_page_bg_image'), 'images');
                    }

                } else if ($setting->key == 'common_bg_image') {

                    if ($request->hasFile('common_bg_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('common_bg_image'), 'images');
                    }

                } else if ($setting->key == 'streaming_url') {

                    if ($request->has('streaming_url') && $request->streaming_url != $setting->value) {

                        if (check_nginx_configure()) {
                            $setting->value = $request->streaming_url;
                        } else {
                            $check_streaming_url = " !! ====> Please Configure the Nginx Streaming Server.";
                        }
                    }

                } else if ($setting->key == "theme") {

                    if ($request->has('theme')) {
                        change_theme($setting->value, $request->$key);
                        $setting->value = $request->theme;
                    }

                } else if ($setting->key == 'default_lang') {

                    if ($request->default_lang != $setting->value) {

                        $refresh = $request->default_lang;

                    }

                    $setting->value = $request->$key;

                } else if ($setting->key == "admin_commission") {

                    $setting->value = $request->has('admin_commission') ? ($request->admin_commission < 100 ? $request->admin_commission : 100) : $setting->value;

                    $user_commission = $setting->value < 100 ? 100 - $setting->value : 0;

                    $user_commission_details = Settings::where('key', 'user_commission')->first();

                    if ($user_commission_details) {

                        $user_commission_details->value = $user_commission;

                        $user_commission_details->save();
                    }

                } else if ($setting->key == 'site_name') {

                    if ($request->has('site_name')) {

                        $site_name = preg_replace("/[^A-Za-z0-9]/", "", $request->site_name);

                        //  \Enveditor::set("SITENAME", $site_name);
                        EnvEditorHelper::setEnv('SITENAME', 'SITENAME', $site_name);

                        $setting->value = $request->site_name;

                    }

                } else if ($setting->key == 'privacycookies') {
                    $setting->value = $request->privacycookies;
                } else if ($setting->key == 'termsconditions') {
                    $setting->value = $request->termsconditions;
                } else if ($setting->key == 'legaldisclaimer') {
                    $setting->value = $request->legaldisclaimer;
                } else if ($setting->key == 'community') {
                    $setting->value = $request->community;
                } elseif ($setting->key == 'home_browse_mobile_image') {

                    if ($request->hasFile('home_browse_mobile_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                        }

                        $setting->value = Helper::normal_upload_picture($request->file('home_browse_mobile_image'));
                    }

                } elseif ($setting->key == 'home_cancel_image') {

                    if ($request->hasFile('home_cancel_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                        }

                        $setting->value = Helper::normal_upload_picture($request->file('home_cancel_image'));
                    }

                } elseif ($setting->key == 'home_browse_desktop_image') {

                    if ($request->hasFile('home_browse_desktop_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                        }

                        $setting->value = Helper::normal_upload_picture($request->file('home_browse_desktop_image'));
                    }

                } elseif ($setting->key == 'home_browse_tv_image') {

                    if ($request->hasFile('home_browse_tv_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                        }

                        $setting->value = Helper::normal_upload_picture($request->file('home_browse_tv_image'));
                    }

                } else {

                    if (isset($_REQUEST[$key])) {

                        $setting->value = $request->$key;

                    }

                }

                $setting->save();

            }

        }

        if ($refresh) {
            $fp = fopen(base_path() . '/config/new_config.php', 'w');
            fwrite($fp, "<?php return array( 'locale' => '" . $refresh . "', 'fallback_locale' => '" . $refresh . "');?>");
            fclose($fp);
            \Log::info("Key : " . config('app.locale'));

        }

        foreach ($_REQUEST as $key => $value) {
            $data = Settings::where('key', $key)->first();
            if (!$data) {
                Settings::insert(['key' => $key, 'value' => $value]);
            }
        }
        $message = "Settings Updated Successfully" . " " . $check_streaming_url;

        return redirect(route('clear-cache'))->with('setting', $settings)->with('flash_success', $message);

    }

    public function help()
    {
        return view('admin.static.help')->withPage('help')->with('sub_page', "");
    }

    /**
     * Function: pages()
     *
     * @param -
     *
     * @return pages management view page
     * @uses used to list the pages
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function pages()
    {

        $data = Page::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.pages.index')->with('page', "viewpages")->with('sub_page', 'view_pages')->with('data', $data);
    }

    /*** function page_create()** Used to create page**/

    public function page_create()
    {
        $model = new Page;
        return view('admin.pages.create')->with('page', 'viewpages')->with('model', $model)->with('sub_page', "add_page");
    }

    public function page_edit($id)
    {
        $data = Page::find($id);

        if ($data) {
            return view('admin.pages.edit')->withPage('viewpage')->with('sub_page', "view_pages")
                ->with('data', $data);
        } else {
            return back()->with('flash_error', tr('something_error'));
        }
    }

    public function page_view(Request $request)
    {

        $data = Page::find($request->id);

        if ($data) {
            return view('admin.pages.view')->withPage('viewpage')->with('sub_page', "view_pages")
                ->with('data', $data);
        } else {
            return back()->with('flash_error', tr('something_error'));
        }
    }

    public function page_save(Request $request)
    {
        $taglessBody = strip_tags($request->description);

        if ($request->has('id')) {
            $validator = Validator::make($request->all(), array(
                // 'title' => '',
                'heading' => 'required',
                'description' => 'required',
            ));
        } else {
            $validator = Validator::make($request->all(), array(
                // 'title' => 'required|max:255|unique:pages,deleted_at,NULL',
                'heading' => 'required|max:255',
                'description' => 'required',
            ));
        }

        if ($validator->fails()) {
            $error = implode(',', $validator->messages()->all());
            return back()->with('flash_error', $error);
        } else {

            if ($request->has('id')) {
                $pages = Page::find($request->id);

                if ($pages->heading != $request->heading) {
                    $page_slug = seoUrl($request->heading);

                    // check to see if any other slugs exist that are the same & count them
                    $slug_count = Page::whereRaw("page_slug RLIKE '^{$page_slug}(-[0-9]+)?$'")->count();

                    // if other slugs exist that are the same, append the count to the slug
                    $slug = ($slug_count) ? $page_slug . '-' . $slug_count : $page_slug;

                    $pages->page_slug = $slug;
                }

            } else {
                $pages = new Page;

                $page_slug = seoUrl($request->heading);

                // check to see if any other slugs exist that are the same & count them
                $slug_count = Page::whereRaw("page_slug RLIKE '^{$page_slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $slug = ($slug_count) ? $page_slug . '-' . $slug_count : $page_slug;

                $pages->page_slug = $slug;
            }

            if ($pages) {

                //$pages->type = $request->type ? $request->type : $pages->type;
                //  $pages->title = $request->title ? $request->title : $pages->title;
                $pages->heading = $request->heading ? $request->heading : $pages->heading;
                // $pages->description = $taglessBody ? $taglessBody : $pages->description;
                $pages->description = $request->description ? $request->description : $pages->description;
                $pages->is_error_page = $request->is_error_page;
                $pages->created_at = $request->created_at;
                $pages->save();

                // save seo tags
                \SEO\Seo::save($pages, url($pages->page_slug), [
                    'title' => $pages->heading
                ]);
            }
            if ($pages) {
                return back()->with('flash_success', tr('page_create_success'));
            } else {
                return back()->with('flash_error', tr('something_error'));
            }
        }
    }

    public function page_delete($id)
    {

        $page = Page::where('id', $id)->delete();

        if ($page) {
            return back()->with('flash_success', tr('page_delete_success'));
        } else {
            return back()->with('flash_error', tr('something_error'));
        }
    }

    public function custom_push()
    {

        return view('admin.static.push')->with('title', "Custom Push")->with('page', "custom-push");

    }

    public function custom_push_process(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make(
            $request->all(),
            array('message' => 'required')
        );

        if ($validator->fails()) {

            $error = $validator->messages()->all();

            return back()->with('flash_error', $error);

        } else {

            $message = $request->message;

            $title = Setting::get('site_name');

            $message = $message;

            $id = 'all';

            $android_register_ids = User::where('is_activated', USER_APPROVED)->where('device_token', '!=', "")->where('device_type', DEVICE_ANDROID)->where('push_status', ON)->pluck('device_token')->toArray();

            PushRepo::push_notification_android($android_register_ids, $title, $message);

            $ios_register_ids = User::where('is_activated', USER_APPROVED)->where('device_type', 'DEVICE_IOS')->where('push_status', ON)->select('device_token', 'id as user_id')->get();

            PushRepo::push_notification_ios($ios_register_ids, $title, $message);
            return back()->with('flash_success', tr('push_send_success'));
        }
    }

    /**
     * Function Name : spam_videos()
     *
     * Description: Load all the videos from flag table
     *
     * @created Maheswari
     *
     * @edited vidhya R
     *
     * @param Get the flag details in groupby video_id
     *
     * @return all the spam videos
     */
    public function spam_videos()
    {

        // Load all the videos from flag table
        $model = Flag::groupBy('video_id')->paginate(10);

        // Return array of values
        return view('admin.spam_videos.spam_videos')->with('model', $model)
            ->with('page', 'videos')
            ->with('sub_page', 'spam_videos');
    }

    /**
     * Function Name : spam_videos_user_reports()
     *
     * Description: Load the flags based on the video id
     *
     * @create Maheswari
     *
     * @edited Maheswari
     *
     * @param integer $id Video id
     *
     * @return all the spam videos in user reports
     */
    public function spam_videos_user_reports($id)
    {

        if (!$id) {

            return redirect()->route('admin.spam-videos')->with('flash_error', tr('spam_video_id_error'));
        }

        $video_details = AdminVideo::find($id);

        if ($video_details) {

            // Load all the users based on the selected

            $model = Flag::where('video_id', $id)->paginate(10);

            // Return array of values

            return view('admin.spam_videos.user_report')
                ->with('model', $model)
                ->with('video_details', $video_details)
                ->with('page', 'videos')
                ->with('sub_page', 'spam_videos');
        } else {
            return redirect()->route('admin.spam-videos')->with('flash_error', tr('spam_video_id_error'));
        }

    }

    /**
     * Function:delete_spam()
     *
     * Description: Delete the spam details
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Flag id integer
     *
     * @return html page with delete success message
     */
    public function delete_spam($id)
    {

        if ($id) {

            $flag_detail = Flag::find($id);

            if ($flag_detail) {

                $flag_detail->delete();

                return back()->with('flash_success', tr('spam_deleted'));

            } else {

                return back()->with('flash_error', tr('spam_details_not_found'));
            }
        } else {
            return back()->with('flash_error', tr('spam_video_id_error'));
        }
    }

    public function revenue_system()
    {

        $total_sub_revenue = UserPayment::sum('amount');

        $total_revenue = $total_sub_revenue ? $total_sub_revenue : 0;

        // Video Payments

        $live_video_amount = PayPerView::sum('amount');

        $video_amount = $live_video_amount ? $live_video_amount : 0;

        $live_user_amount = PayPerView::sum('moderator_amount');

        $user_amount = $live_user_amount ? $live_user_amount : 0;

        $final = PayPerView::where('admin_amount', '=', 0)->where('moderator_amount', '=', 0)->sum('amount');

        $live_admin_amount = PayPerView::sum('admin_amount');

        $admin_amount = $live_admin_amount + $final;

        $video_amount = $live_video_amount;

        return view('admin.payments.revenue-dashboard')
            ->with('total_revenue', $total_revenue)
            ->with('video_amount', $video_amount)
            ->with('user_amount', $user_amount)
            ->with('admin_amount', $admin_amount ? $admin_amount : 0)
            ->with('page', 'payments')
            ->with('sub_page', 'revenue_system');
    }

    /**
     * Function Name : video_payments()
     *
     * To get payments based on the video subscription
     *
     * @created Shobana C
     *
     * @edited Vedgupt S
     *
     * @return array of payments
     */

    public function video_payments(Request $request)
    {
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $request->session()->forget('search');
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];

                $request->session()->put('search', $search);
            } else {
                $search = $request->session()->get('search');
            }
        }

        $columns = array('created_at', 'name');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('search')) {
            $search = $request->session()->get('search');
            $query = PayPerView::whereHas('adminVideo', function ($q) use ($search) {
                $q->where('title', 'like', '%' . $search . '%');

            })->orderBy($column, $order_by);
        } else {
            $query = PayPerView::orderBy($column, $order_by);
            $search = '';
        }

        $payments = $query->paginate($per_page)->appends(request()->query());

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        $payment_count = PayPerView::count();
        return view('admin.payments.video-payments')->with('data', $payments)->with('page', 'payments')->with('sub_page', 'video-payments')->with('payment_count', $payment_count)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('search', $search)
            ->with('per_page', $per_page);
    }

    /**
     * Function: save_video_payment
     *
     * @param integer $id Video Id
     *
     * @param object $request Object (Post Attributes)
     *
     * @return flash message
     * @uses : To save the payment details
     *
     */

    public function save_video_payment($id, Request $request)
    {

        if ($request->amount > 0) {

            $model = AdminVideo::find($id);
            if ($model) {

                $request->request->add([
                    'is_pay_per_view' => PPV_ENABLED,
                ]);

                if ($data = $request->all()) {
                    // Update the post
                    if (AdminVideo::where('id', $id)->update($data)) {
                        // Redirect into particular value
                        return back()->with('flash_success', tr('payment_added'));
                    }
                }
            }
            return back()->with('flash_error', tr('admin_published_video_failure'));

        } else {

            return back()->with('flash_error', tr('add_ppv_amount'));
        }
    }

    /**
     * Function : save_common_settings
     *
     * @descritpion: Save the values in env file
     *
     * @created ** @edited Vedgupt S
     *
     * @param object $request Post Attribute values
     *
     * @return settings values
     */

    public function save_common_settings(Request $request)
    {

        $admin_id = \Auth::guard('admin')->user()->id;

        foreach ($request->all() as $key => $data) {

            config([$key => $data]);

            // for TMDB API key
            if ($key == 'TMDB_API_KEY') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'OMDB_API_KEY') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            // for S3 space
            if ($key == 'S3_KEY') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'S3_SECRET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'S3_REGION') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'S3_BUCKET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'S3_SES_REGION') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            // for DO space
            if ($key == 'DO_SPACES_KEY') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'DO_SPACES_SECRET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'DO_SPACES_ENDPOINT') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'DO_SPACES_REGION') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'DO_SPACES_BUCKET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'DO_SPACES_FOLDER') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'CDN_SUB_DOMAIN_URL') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            // social setting
            if ($key == 'FB_CLIENT_ID') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'FB_CLIENT_SECRET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'FB_CALL_BACK') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'GOOGLE_CLIENT_ID') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'GOOGLE_CLIENT_SECRET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'GOOGLE_CALL_BACK') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            // paypal setting
            if ($key == 'PAYPAL_ID') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'PAYPAL_SECRET') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }

            if ($key == 'PAYPAL_MODE') {
                EnvEditorHelper::setEnv($key, $key, $data);
            }
        }

        $settings = Settings::all();

        $check_streaming_url = "";

        $refresh = "";

        if ($settings) {

            foreach ($settings as $setting) {

                $key = $setting->key;

                if ($setting->key == 'site_icon') {

                    if ($request->hasFile('site_icon')) {

                        if ($setting->value) {
                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('site_icon'), 'images');

                    }

                } else if ($setting->key == 'site_logo') {

                    if ($request->hasFile('site_logo')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('site_logo'), 'images');
                    }

                } else if ($setting->key == 'home_page_bg_image') {

                    if ($request->hasFile('home_page_bg_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('home_page_bg_image'), 'images');
                    }

                } else if ($setting->key == 'common_bg_image') {

                    if ($request->hasFile('common_bg_image')) {

                        if ($setting->value) {

                            Helper::delete_picture($setting->value, "/uploads/");
                            Helper::spaces_delete_picture($setting->value, "images");
                        }

                        $setting->value = Helper::upload_file_to_digitalocean_spaces_image($request->file('common_bg_image'), 'images');
                    }

                } else if ($setting->key == "theme") {

                    if ($request->has('theme')) {
                        change_theme($setting->value, $request->$key);
                        $setting->value = $request->theme;
                    }

                } else if ($setting->key == 'default_lang') {

                    if ($request->default_lang != $setting->value) {

                        $refresh = $request->default_lang;

                    }

                    $setting->value = $request->$key;

                } else if ($setting->key == "admin_commission") {

                    $setting->value = $request->has('admin_commission') ? ($request->admin_commission < 100 ? $request->admin_commission : 100) : $setting->value;

                    $user_commission = $setting->value < 100 ? 100 - $setting->value : 0;

                    $user_commission_details = Settings::where('key', 'user_commission')->first();

                    if (!empty($user_commission_details)) {

                        $user_commission_details->value = $user_commission;

                        $user_commission_details->save();
                    }

                } else {

                    if (isset($_REQUEST[$key])) {

                        $setting->value = $request->$key;

                    }

                }

                $setting->save();

            }

        }

        return redirect(route('clear-cache'))->with('setting', $settings);

    }

    /*** Function : remove_payper_view ** @descritpion: To remove pay per view ** @created ** @edited vidhya R ** @param object $request Post Attribute values ** @return falsh success */

    public function remove_payper_view($id)
    {
        $model = AdminVideo::find($id);
        if ($model) {
            $model->amount = DEFAULT_FALSE;
            $model->type_of_subscription = DEFAULT_FALSE;
            $model->type_of_user = DEFAULT_FALSE;
            $model->is_pay_per_view = PPV_DISABLED;
            $model->save();
            if ($model) {
                return back()->with('flash_success', tr('removed_pay_per_view'));
            }
        }
        return back()->with('flash_error', tr('admin_published_video_failure'));
    }

    public function subscriptions()
    {

        $data = Subscription::orderBy('created_at', 'desc')->whereNotIn('status', [DELETE_STATUS])->paginate(10);

        return view('admin.subscriptions.index')->withPage('subscriptions')
            ->with('data', $data)
            ->with('sub_page', 'view-subscription');

    }

    public function user_subscriptions($id)
    {
        $data = Subscription::orderBy('created_at', 'desc')->whereNotIn('status', [DELETE_STATUS])->get();
        $payments = [];
        if ($id) {
            $payments = UserPayment::orderBy('created_at', 'desc')
                ->where('user_id', $id)->get();
        }
        return view('admin.subscriptions.user_plans')
            ->withPage('subscriptions')
            ->withPage('payments')
            ->withPage('users')
            ->with('id', $id)
            ->with('subscriptions', $data)
            ->with('payments', $payments)
            ->with('page', 'users');

    }

    public function user_subscription_save($s_id, $u_id)
    {

        $load = UserPayment::where('user_id', $u_id)->where('status', DEFAULT_TRUE)->orderBy('id', 'desc')->first();

        $payment = new UserPayment();

        $payment->subscription_id = $s_id;

        $payment->user_id = $u_id;

        $payment->subscription_amount = ($payment->subscription) ? $payment->subscription->amount : 0;

        $payment->amount = ($payment->subscription) ? $payment->subscription->amount : 0;

        $payment->payment_id = ($payment->amount > 0) ? uniqid(str_replace(' ', '-', 'PAY')) : 'Free Plan';

        if ($load) {

            if (strtotime($load->expiry_date) >= strtotime(date('Y-m-d H:i:s'))) {

                $payment->expiry_date = date('Y-m-d H:i:s', strtotime("+{$payment->subscription->plan} months", strtotime($load->expiry_date)));

            } else {

                $payment->expiry_date = date('Y-m-d H:i:s', strtotime("+{$payment->subscription->plan} months"));

            }

        } else {

            $payment->expiry_date = date('Y-m-d H:i:s', strtotime("+{$payment->subscription->plan} months"));

        }

        $payment->status = DEFAULT_TRUE;

        if ($payment->save()) {

            $payment->user->user_type = DEFAULT_TRUE;

            if ($payment->user->save()) {

                return back()->with('flash_success', tr('subscription_applied_success'));

            }

        }

        return back()->with('flash_error', tr('went_wrong'));

    }

    public function subscription_create()
    {
        $model = new Subscription;
        return view('admin.subscriptions.create')->with('page', 'subscriptions')
            ->with('sub_page', 'subscriptions-add')
            ->with('model', 'model');
    }

    public function subscription_edit($unique_id)
    {

        $data = Subscription::where('unique_id', $unique_id)->first();

        return view('admin.subscriptions.edit')->withData($data)
            ->with('sub_page', 'subscriptions-view')
            ->with('page', 'subscriptions ');

    }

    public function subscription_save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'plan' => 'required|numeric|min:1|max:12',
            'amount' => 'required|numeric',
            'no_of_account' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);

        } else {

            if ($request->popular_status) {
                Subscription::where('popular_status', 1)->update(['popular_status' => 0]);
            }

            if ($request->id != '') {

                $model = Subscription::find($request->id);

                $model->update($request->all());

            } else {
                $model = Subscription::create($request->all());
                $model->status = 1;
                $model->popular_status = $request->popular_status ? 1 : 0;
                $model->unique_id = $model->title;
                $model->no_of_account = $request->no_of_account;
                $model->created_at = $request->created_at;
                $model->save();
            }

            if ($model) {
                return redirect(route('admin.subscriptions.view', $model->unique_id))->with('flash_success', $request->id ? tr('subscription_update_success') : tr('subscription_create_success'));

            } else {
                return back()->with('flash_error', tr('admin_not_error'));
            }
        }

    }

    /**** Subscription View **/

    public function subscription_view($unique_id)
    {

        if ($data = Subscription::where('unique_id', $unique_id)->first()) {

            $earnings = $data->userSubscription()->where('status', 1)->sum('amount');

            $total_subscribers = $data->userSubscription()->where('status', 1)->count();

            return view('admin.subscriptions.view')
                ->with('data', $data)
                ->withPage('subscriptions')
                ->with('total_subscribers', $total_subscribers)
                ->with('earnings', $earnings)
                ->with('sub_page', 'subscriptions-view');

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }

    }

    public function subscription_delete(Request $request)
    {

        if ($data = Subscription::where('id', $request->id)->first()) {

            $data->status = DELETE_STATUS;

            $data->save();

            return back()->with('flash_success', tr('subscription_delete_success'));

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }

    }

    /*** Subscription status change ***/

    public function subscription_status($unique_id)
    {

        if ($data = Subscription::where('unique_id', $unique_id)->first()) {

            $data->status = $data->status ? 0 : 1;

            $data->save();

            return back()->with('flash_success', $data->status ? tr('subscription_approve_success') : tr('subscription_decline_success'));
        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    /**
     * Subscription Popular status change
     *
     *
     */

    public function subscription_popular_status($unique_id)
    {

        if ($data = Subscription::where('unique_id', $unique_id)->first()) {

            Subscription::where('popular_status', 1)->update(['popular_status' => 0]);

            $data->popular_status = $data->popular_status ? 0 : 1;

            $data->save();

            return back()->with('flash_success', $data->popular_status ? tr('subscription_popular_success') : tr('subscription_popular_remove'));

        } else {
            return back()->with('flash_error', tr('admin_not_error'));
        }
    }

    /*** View list of users based on the selected Subscription **/

    public function subscription_users($id)
    {

        $user_ids = [];

        $users = UserPayment::where('subscription_id', $id)->select('user_id')->get();

        foreach ($users as $key => $value) {

            $user_ids[] = $value->user_id;
        }

        $subscription = Subscription::find($id);

        $data = User::whereIn('id', $user_ids)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.users.users')
            ->withPage('users')
            ->with('users', $data)
            ->with('sub_page', 'view-user')
            ->with('subscription', $subscription);
    }

    /**
     * Function : users_sub_profiles
     *
     * @descritpion: list the sub profiles based on the selected user
     *
     * @created Shobana C
     *
     * @edited vidhya R
     *
     * @param -
     *
     * @return list of sub profiles page
     */

    public function users_sub_profiles($id)
    {

        if (!$id) {
            return redirect()->route('admin.users')->with('flash_error', tr('user_id_not_found'));
        }

        $user_details = User::find($id);
        $sub_profiles = SubProfile::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.users.sub_profiles')
            ->withPage('users')
            ->with('sub_page', 'view-user')
            ->with('sub_profiles', $sub_profiles)
            ->with('user_details', $user_details);
    }

    public function users_password_reset($id)
    {
        $user_data = User::where('id', $id)->select()->first();

        return view('admin.users.password_reset')
            ->withPage('users')
            ->with('user_data', $user_data)
            ->with('sub_page', 'view-user');

    }

    public function users_password_update(Request $request)
    {
        if ($request->password == $request->password_confirmation) {
            $password = \Hash::make($request->password);

            $change_password = User::where('id', $request->user_id)
                ->update([
                    'password' => $password,
                ]);

            $user = User::find($request->user_id);

            if ($user->is_moderator == 1) {
                $change_password = Moderator::where('id', $user->moderator_id)
                    ->update([
                        'password' => $password,
                    ]);
            }

            return back()->with('flash_success', 'You have password change successfully');
        } else {
            return back()->with('flash_error', 'You have entered wrong confirmed password');
        }
    }

    public function subadmin_password_reset($id)
    {
        $user_data = Admin::where('id', $id)->select()->first();

        return view('admin.subadmins.password_reset')
            ->withPage('subadmins')
            ->with('user_data', $user_data)
            ->with('sub_page', 'view-user');

    }

    public function subadmin_password_update(Request $request)
    {
        if ($request->password == $request->password_confirmation) {
            $password = \Hash::make($request->password);

            $change_password = Admin::where('id', $request->user_id)->where('user_type', 2)
                ->update([
                    'password' => $password,
                ]);

            return back()->with('flash_success', 'You have password change successfully');
        } else {
            return back()->with('flash_error', 'You have entered wrong confirmed password');
        }
    }

    public function moderators_redeem_requests(Request $request)
    {

        $base_query = RedeemRequest::orderBy('updated_at', 'desc');

        $moderator = [];

        if ($request->id) {

            $base_query = $base_query->where('moderator_id', $request->id);

            $moderator = Moderator::find($request->id);
        }

        $data = $base_query->get();

        return view('admin.moderators.redeems')->withPage('redeems')->with('sub_page', 'redeems')->with('data', $data)->with('moderator', $moderator);

    }

    /**
     * Function: moderators_redeems_payout_direct()
     *
     * @param -
     *
     * @return redirect to view page with success/failure message
     * @uses used to payout for the selected redeem request with direct payment
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function moderators_redeems_payout_direct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'redeem_request_id' => 'required|exists:redeem_requests,id',
            'paid_amount' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->route('admin.moderators.redeems')->with('flash_error', $validator->messages()->all())->withInput();

        } else {

            $redeem_request_details = RedeemRequest::find($request->redeem_request_id);

            if ($redeem_request_details) {

                if ($redeem_request_details->status == REDEEM_REQUEST_PAID) {

                    return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_request_status_mismatch'));

                } else {

                    $redeem_request_details->paid_amount = $redeem_request_details->paid_amount + $request->paid_amount;

                    $redeem_request_details->status = REDEEM_REQUEST_PAID;

                    $redeem_request_details->payment_mode = "direct";

                    $redeem_request_details->save();

                    $redeem = Redeem::where('moderator_id', $redeem_request_details->moderator_id)->first();

                    $redeem->paid += $request->paid_amount;

                    $redeem->remaining = $redeem->total_moderator_amount - $redeem->paid;

                    $redeem->save();

                    if ($redeem_request_details->moderator) {

                        $redeem_request_details->moderator->paid_amount += $request->paid_amount;

                        $redeem_request_details->moderator->remaining_amount = $redeem->total_moderator_amount - $redeem->paid;

                        $redeem_request_details->moderator->save();

                    }

                    return redirect()->route('admin.moderators.redeems')->with('flash_success', tr('action_success'));

                }

            } else {
                return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('something_error'));
            }
        }

    }

    /**
     * Function: moderators_payout_invoice()
     *
     * @param -
     *
     * @return redirect to view page with success/failure message
     * @uses used to list the categories
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function moderators_redeems_payout_invoice(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'redeem_request_id' => 'required|exists:redeem_requests,id',
            'paid_amount' => 'required',
            'moderator_id' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->route('admin.moderators.redeems')
                ->with('flash_error', $validator->messages()->all())
                ->withInput();

        } else {

            $redeem_request_details = RedeemRequest::find($request->redeem_request_id);

            if ($redeem_request_details) {

                if ($redeem_request_details->status == REDEEM_REQUEST_PAID) {

                    return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_request_status_mismatch'));

                } else {

                    $invoice_data['moderator_details'] = $moderator_details = Moderator::find($request->moderator_id);

                    $invoice_data['redeem_request_id'] = $request->redeem_request_id;

                    $invoice_data['redeem_request_status'] = $redeem_request_details->status;

                    $invoice_data['moderator_id'] = $request->moderator_id;

                    $invoice_data['item_name'] = Setting::get('site_name') . " - Checkout to" . "$moderator_details ? $moderator_details->name : -";

                    $invoice_data['payout_amount'] = $request->paid_amount;

                    $data = json_decode(json_encode($invoice_data));

                    return view('admin.moderators.payout')->with('data', $data)->withPage('moderators')->with('sub_page', 'moderators');

                }

            } else {
                return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_not_found'));

            }
        }

    }

    /**
     * Function: moderators_redeems_payout_response()
     *
     * @param -
     *
     * @return redirect to view page with success/failure message
     * @uses used to get the response from paypal checkout
     *
     * @created vidhya R
     *
     * @edited Vidhya R
     *
     */

    public function moderators_redeems_payout_response(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'redeem_request_id' => 'required|exists:redeem_requests,id',
        ]);

        if ($validator->fails()) {

            return redirect()->route('admin.moderators.redeems')->with('flash_error', $validator->messages()->all())->withInput();

        } else {

            if ($request->success == false) {

                return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_paypal_cancelled'));

            }

            $redeem_request_details = RedeemRequest::find($request->redeem_request_id);

            if ($redeem_request_details) {

                if ($redeem_request_details->status == REDEEM_REQUEST_PAID) {

                    return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_request_status_mismatch'));

                } else {

                    $redeem_request_details->paid_amount = $redeem_request_details->paid_amount + $request->paid_amount;

                    $redeem_request_details->status = REDEEM_REQUEST_PAID;

                    $redeem_request_details->payment_mode = PAYPAL;

                    $redeem_request_details->save();

                    $redeem = Redeem::where('moderator_id', $redeem_request_details->moderator_id)->first();

                    $redeem->paid += $request->paid_amount;

                    $redeem->remaining = $redeem->total_moderator_amount - $redeem->paid;

                    $redeem->save();

                    if ($redeem_request_details->moderator) {

                        $redeem_request_details->moderator->paid_amount += $request->paid_amount;

                        $redeem_request_details->moderator->remaining_amount = $redeem->total_moderator_amount - $redeem->paid;

                        $redeem_request_details->moderator->save();

                    }

                    return redirect()->route('admin.moderators.redeems')->with('flash_success', tr('action_success'));

                }

            } else {
                return redirect()->route('admin.moderators.redeems')->with('flash_error', tr('redeem_not_found'));

            }
        }

    }

    /**
     * Function Name : genre_position()
     *
     * Change position of the genre
     *
     * @param object $request - Genre id & position of the genre
     *
     * @created_by - Shobana Chandrasekar
     *
     * @edited_by - Shobana Chandrasekar
     *
     * @return response of success/failure message
     */
    public function genre_position(Request $request)
    {

        try {

            DB::beginTransaction();

            $model = Genre::find($request->genre_id);

            if ($model) {

                $changing_row_position = $model->position;

                $change_genre = Genre::where('position', $request->position)->where('sub_category_id', $model->sub_category_id)->where('is_approved', DEFAULT_TRUE)->first();

                if ($change_genre) {

                    $new_row_position = $change_genre->position;

                    $model->position = $new_row_position;

                    if ($model->save()) {

                        $change_genre->position = $changing_row_position;

                        if ($change_genre->save()) {

                        } else {

                            throw new Exception(tr('genre_not_saved'));

                        }

                    } else {

                        throw new Exception(tr('genre_not_saved'));

                    }

                } else {

                    throw new Exception(tr('given_position_not_exits'));
                }

            } else {

                throw new Exception(tr('genre_not_found'));

            }

            DB::commit();

            return back()->with('flash_success', tr('genre_position_updated_success'));

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * Function Name : video_position()
     *
     * Change position of the video based on genres
     *
     * @param object $request - Genre id & position of the genre
     *
     * @return response of success/failure message
     */
    public function video_position(Request $request)
    {

        try {

            DB::beginTransaction();

            $model = AdminVideo::find($request->video_id);

            if ($model) {

                $changing_row_position = $model->position;

                $change_video = AdminVideo::where('position', $request->position)
                    ->where('genre_id', $model->genre_id)
                    ->where('is_approved', DEFAULT_TRUE)
                    ->where('status', DEFAULT_TRUE)
                    ->first();

                if ($change_video) {

                    $new_row_position = $change_video->position;

                    $model->position = $new_row_position;

                    if ($model->save()) {

                        $change_video->position = $changing_row_position;

                        if ($change_video->save()) {

                        } else {

                            throw new Exception(tr('video_not_saved'));

                        }

                    } else {

                        throw new Exception(tr('video_not_saved'));

                    }

                } else {

                    throw new Exception(tr('given_position_not_exits'));
                }

            } else {

                throw new Exception(tr('video_not_found'));

            }

            DB::commit();

            return back()->with('flash_success', tr('video_position_updated_success'));

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * Function Name : templates()
     *
     * To display a templates of the page
     *
     * @param object $request - -
     *
     * @return response of list page
     */
    public function templates(Request $request)
    {

        $templates = EmailTemplate::orderBy('created_at', 'desc')->get();

        return view('admin.email_templates.index')
            ->with('templates', $templates)
            ->with('page', 'email_templates')
            ->with('sub_page', 'email_templates');

    }

    /**
     * Function Name : edit_template()
     *
     * To display a edit template page
     *
     * @param object $request - id
     *
     * @return response of view page
     */
    public function edit_template(Request $request)
    {

        $template = EmailTemplate::find($request->id);

        $template_types = [USER_WELCOME => tr('user_welcome_email'),
            ADMIN_USER_WELCOME => tr('admin_created_user_welcome_mail'),
            FORGOT_PASSWORD => tr('forgot_password'),
            MODERATOR_WELCOME => tr('moderator_welcome'),
            PAYMENT_EXPIRED => tr('payment_expired'),
            PAYMENT_GOING_TO_EXPIRY => tr('payment_going_to_expiry'),
            NEW_VIDEO => tr('new_video'),
            EDIT_VIDEO => tr('edit_video')];

        if ($template) {

            return view('admin.email_templates.template')
                ->with('template', $template)
                ->with('template_types', $template_types)
                ->with('page', 'email_templates')
                ->with('sub_page', 'create_template');
        } else {

            return back()->with('flash_error', tr('template_not_found'));
        }
    }

    /**
     * Function Name : view_template()
     *
     * To display a view template page
     *
     * @param object $request - id
     *
     * @return response of view page
     */
    public function view_template(Request $request)
    {

        $template = EmailTemplate::find($request->id);

        if ($template) {

            return view('admin.email_templates.view')->with('model', $template)->with('page', 'email_templates')->with('sub_page', 'templates');
        } else {

            return back()->with('flash_error', tr('template_not_found'));
        }
    }

    /**
     * Function Name : save_template()
     *
     * To save the template details
     *
     * @param object $request - id
     *
     * @return response of view page
     */
    public function save_template(Request $request)
    {

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'template_type' => 'required|in:' . USER_WELCOME . ',' . ADMIN_USER_WELCOME . ',' . FORGOT_PASSWORD . ',' . MODERATOR_WELCOME . ',' . PAYMENT_EXPIRED . ',' . PAYMENT_GOING_TO_EXPIRY . ',' . NEW_VIDEO . ',' . EDIT_VIDEO,
                'subject' => 'required|max:255',
                'description' => 'required',
            ]);

            $template = $request->id ? EmailTemplate::find($request->id) : new EmailTemplate;

            if ($template) {

                $template->subject = $request->subject;

                $template->description = $request->description;

                $template->template_type = $request->template_type;

                $template->status = DEFAULT_TRUE;

                if ($template->save()) {

                } else {

                    throw new Exception(tr('template_not_saved'));

                }

            } else {

                throw new Exception(tr('template_not_found'));
            }

            DB::commit();

            return back()->with('flash_success', $request->id ? tr('template_update_success') : tr('template_create_success'));

        } catch (Exception $e) {

            DB::rollback();

            $message = $e->getMessage();

            return back()->with('flash_error', $message);

        }
    }

    // Coupons

    /**
     * Function Name: coupon_create()
     *
     * Description: Get the coupon add form fields
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Get the route of add coupon form
     *
     * @return Html form page
     */
    public function coupon_create()
    {
        $model = new Coupon;
        return view('admin.coupons.create')
            ->with('page', 'coupons')
            ->with('sub_page', 'create')
            ->with('model', 'model');
    }

    /**
     * Function Name: coupon_save()
     *
     * Description: Save/Update the coupon details in database
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Request to all the coupon details
     *
     * @return add details for success message
     */
    public function coupon_save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'exists:coupons,id',
            'title' => 'required',
            'coupon_code' => $request->id ? 'required|max:10|min:1|unique:coupons,coupon_code,' . $request->id : 'required|unique:coupons,coupon_code|min:1|max:10',
            'amount' => 'required|numeric|min:1|max:5000',
            'amount_type' => 'required',
            'expiry_date' => 'required|date_format:d-m-Y|after:today',
            'no_of_users_limit' => 'required|numeric|min:1|max:1000',
            'per_users_limit' => 'required|numeric|min:1|max:100',
        ]);

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);
        }
        if ($request->id != '') {

            $coupon_detail = Coupon::find($request->id);

            $message = tr('coupon_update_success');

        } else {

            $coupon_detail = new Coupon;

            $coupon_detail->status = DEFAULT_TRUE;

            $message = tr('coupon_add_success');
        }

        // Check the condition amount type equal zero mean percentage
        if ($request->amount_type == PERCENTAGE) {

            // Amount type zero must should be amount less than or equal 100 only
            if ($request->amount <= 100) {

                $coupon_detail->amount_type = $request->has('amount_type') ? $request->amount_type : DEFAULT_FALSE;

                $coupon_detail->amount = $request->has('amount') ? $request->amount : '';

            } else {

                return back()->with('flash_error', tr('coupon_amount_lessthan_100'));
            }

        } else {

            // This else condition is absoulte amount

            // Amount type one must should be amount less than or equal 5000 only
            if ($request->amount <= 5000) {

                $coupon_detail->amount_type = $request->has('amount_type') ? $request->amount_type : DEFAULT_TRUE;

                $coupon_detail->amount = $request->has('amount') ? $request->amount : '';

            } else {

                return back()->with('flash_error', tr('coupon_amount_lessthan_5000'));
            }
        }
        $coupon_detail->title = ucfirst($request->title);

        // Remove the string space and special characters
        $coupon_code_format = preg_replace("/[^A-Za-z0-9\-]+/", "", $request->coupon_code);

        // Replace the string uppercase format
        $coupon_detail->coupon_code = strtoupper($coupon_code_format);

        // Convert date format year,month,date purpose of database storing
        $coupon_detail->expiry_date = date('Y-m-d', strtotime($request->expiry_date));

        $coupon_detail->description = $request->has('description') ? $request->description : '';
        // Based no users limit need to apply coupons
        $coupon_detail->no_of_users_limit = $request->no_of_users_limit;

        $coupon_detail->per_users_limit = $request->per_users_limit;

        $coupon_detail->created_at = $request->created_at;

        if ($coupon_detail) {

            $coupon_detail->save();

            return back()->with('flash_success', $message);

        } else {

            return back()->with('flash_error', tr('coupon_not_found_error'));
        }

    }

    /**
     * Function Name: coupon_index()
     *
     * Description: Get the coupon details for all
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Get the coupon list in table
     *
     * @return Html table from coupon list page
     */
    public function coupon_index()
    {

        $coupons = Coupon::orderBy('updated_at', 'desc')->paginate(10);

        if ($coupons) {

            return view('admin.coupons.index')
                ->with('coupons', $coupons)
                ->with('page', 'coupons')
                ->with('sub_page', 'view_coupons');
        } else {

            return back()->with('flash_error', tr('coupon_not_found_error'));
        }
    }

    /**
     * Function Name: coupon_edit()
     *
     * Description: Edit the coupon details and get the coupon edit form for
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Coupon id
     *
     * @return Get the html form
     */
    public function coupon_edit($id)
    {

        if ($id) {

            $edit_coupon = Coupon::find($id);

            if ($edit_coupon) {

                return view('admin.coupons.edit')
                    ->with('edit_coupon', $edit_coupon)
                    ->with('page', 'coupons')
                    ->with('sub_page', 'edit_coupons');

            } else {
                return back()->with('flash_error', tr('coupon_not_found_error'));
            }
        } else {

            return back()->with('flash_error', tr('coupon_id_not_found_error'));
        }
    }

    /**
     * Function Name: coupon_delete()
     *
     * Description: Delete the particular coupon detail
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Coupon id
     *
     * @return Deleted Success message
     */
    public function coupon_delete($id)
    {

        if ($id) {

            $delete_coupon = Coupon::find($id);

            if ($delete_coupon) {

                $delete_coupon->delete();

                return back()->with('flash_success', tr('coupon_delete_success'));
            } else {

                return back()->with('flash_error', tr('coupon_not_found_error'));
            }

        } else {

            return back()->with('flash_error', tr('coupon_id_not_found_error'));
        }
    }

     /**
     * Function Name: subject_create()
     *
     * Description: Get the coupon add form fields
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Get the route of add coupon form
     *
     * @return Html form page
     */
    public function subject_create()
    {
        $model = new Subject;
        return view('admin.subjects.create')
            ->with('page', 'subjects')
            ->with('sub_page', 'create')
            ->with('model', 'model');
    }

    /**
     * Function Name: subject_save()
     *
     * Description: Save/Update the coupon details in database
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Request to all the coupon details
     *
     * @return add details for success message
     */
    public function subject_save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
        ]);

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);
        }
        else {
            if(!empty($request->id))
            {
                $subject =  Subject::find($request->id);
                $message = 'Subject updated successfully';
            }else{
                $subject =  new Subject();
                $message = 'Subject created successfully';
            }
            $subject->subject = $request->subject;
            $subject->save();
            return redirect('/admin/subject/list')->with('flash_success', $message);
        } 

    }

    /**
     * Function Name: subject_index()
     *
     * Description: Get the coupon details for all
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Get the coupon list in table
     *
     * @return Html table from coupon list page
     */
    public function subject_index()
    {

        $subjects = Subject::orderBy('updated_at', 'desc')->paginate(10);

        if ($subjects) {
            return view('admin.subjects.index')
                ->with('subjects', $subjects)
                ->with('page', 'subjects')
                ->with('sub_page', 'view_subject');
        } else {
            return back()->with('flash_error', tr('subject_result_not_found_error'));
        }
    }

    /**
     * Function Name: subject_edit()
     *
     * Description: Edit the subject details and get the subject edit form for
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param subject id
     *
     * @return Get the html form
     */
    public function subject_edit($id)
    {

        if ($id) {

            $edit_subject = Subject::find($id);

            if ($edit_subject) {

                return view('admin.subjects.edit')
                    ->with('edit_subject', $edit_subject)
                    ->with('page', 'subjects')
                    ->with('sub_page', 'edit_subject');

            } else {
                return back()->with('flash_error', tr('coupon_not_found_error'));
            }
        } else {

            return back()->with('flash_error', tr('coupon_id_not_found_error'));
        }
    }

    /**
     * Function Name: subject_delete()
     *
     * Description: Delete the particular subject detail
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param subject id
     *
     * @return Deleted Success message
     */
    public function subject_delete($id)
    {

        if ($id) {

            $delete_subject = Subject::find($id);

            if ($delete_subject) {

                $delete_subject->delete();

                return back()->with('flash_success', tr('subject_delete_success'));
            } else {

                return back()->with('flash_error', tr('coupon_not_found_error'));
            }

        } else {

            return back()->with('flash_error', tr('coupon_id_not_found_error'));
        }
    }

    public function subject_update(Request $request)
    {
       
        $id = $request->id;
        $status = $request->status;
        if ($id) {
            $update_subject = Subject::find($id);
            if ($update_subject) {
                $update_subject->status = $status;
                $update_subject->save();
                return back()->with('flash_success', tr('subject_update_success'));
            } else {
                return back()->with('flash_error', tr('subject_not_found'));
            }
        } else {
            return back()->with('flash_error', tr('subject_not_found'));
        }
    }
    /**
     * Function Name: coupon_status_change()
     *
     * Description: Coupon status for active and inactive update the status function
     *
     * @created Maheswari
     *
     * @edited Maheswari
     *
     * @param Request the coupon id
     *
     * @return Success message for active/inactive
     */
    public function coupon_status_change(Request $request)
    {

        $coupon_status = Coupon::find($request->id);

        if ($coupon_status) {

            $coupon_status->status = $request->status;

            $coupon_status->save();

        } else {

            return back()->with('flash_error', tr('coupon_not_found_error'));
        }

        if ($request->status == DEFAULT_FALSE) {

            $message = tr('coupon_decline_success');

        } else {

            $message = tr('coupon_approve_success');
        }
        return back()->with('flash_success', $message);
    }

    /**
     * Function Name: coupon_view()
     *
     * Description: Get the particular coupon details for view page content
     *
     * @created Maheswari
     *
     * @edited Maheswaari
     *
     * @param Coupon id
     *
     * @return Html view page with coupon detail
     */
    public function coupon_view($id)
    {

        if ($id) {

            $view_coupon = Coupon::find($id);

            if ($view_coupon) {

                $user_coupon = UserCoupon::where('coupon_code', $view_coupon->coupon_code)->sum('no_of_times_used');

                return view('admin.coupons.view')
                    ->with('view_coupon', $view_coupon)
                    ->with('page', 'coupons')
                    ->with('user_coupon', $user_coupon)
                    ->with('sub_page', 'view_coupons');
            }

        } else {

            return back()->with('flash_error', tr('coupon_id_not_found_error'));
        }
    }

    // Mail Camp

    /**
     * Function Name: create_mailcamp
     *
     * Description: Get the mail camp form in this list
     *
     * @edited Maheswari
     *
     * @created Maheswari
     *
     * @return Html form
     */
    public function create_mailcamp()
    {

        $users_list = User::select('users.id', 'users.name', 'users.email', 'users.is_activated', 'users.is_verified', 'users.amount_paid')->where('is_activated', 1)->where('email_notification', 1)->where('is_verified', 1)->get();

        $moderator_list = Moderator::select('moderators.id', 'moderators.name', 'moderators.email', 'moderators.is_activated')->where('is_activated', 1)->get();

        return view('admin.mail_camp')
            ->with('users_list', $users_list)
            ->with('moderator_list', $moderator_list)
            ->with('page', 'mail_camp');
    }

    /**
     * Function Name : email_send_process()
     *
     * Description : Get user list from based on to address
     *
     * @edited Maheswari
     *
     * @created Maheswari
     *
     * @param request the mail form fields
     *
     * @return Response is mail send successfull message
     */
    public function email_send_process(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [

            'to' => 'required|in:' . USERS . ',' . MODERATORS . ',' . CUSTOM_USERS,
            'users_type' => 'in:' . ALL_USER . ',' . NORMAL_USERS . ',' . PAID_USERS . ',' . SELECT_USERS . ',' . ALL_MODERATOR . ',' . SELECT_MODERATOR,
            'subject' => 'required|min:5|max:255',
            'content' => 'required|min:5',
        ]);
        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);
        }

        if ($request->to == USERS) {

            if ($request->users_type == ALL_USER) {
                $user_email = User::select('users.id')->where('is_activated', 1)->where('is_verified', 1)->pluck('users.id')->toArray();
            } else if ($request->users_type == NORMAL_USERS) {
                $user_email = User::select('users.id')->where('is_activated', 1)->where('is_verified', 1)->where('user_type', 0)->pluck('users.id')->toArray();
            } else if ($request->users_type == PAID_USERS) {
                $user_email = User::select('users.id')->where('is_activated', 1)->where('user_type', 1)->where('is_verified', 1)->pluck('users.id')->toArray();
            } elseif ($request->users_type == SELECT_USERS) {
                $user_email = $request->select_user;
            } else {
                return back()->with('flash_error', tr('user_type_not_found'));
            }
        } else if ($request->to == MODERATORS) {
            if ($request->users_type == ALL_MODERATOR) {
                $user_email = Moderator::select('moderators.id')->where('is_activated', 1)->pluck('moderators.id')->toArray();
            } else if ($request->users_type == SELECT_MODERATOR) {
                $user_email = $request->select_moderator;
            } else {
                return back()->with('flash_error', tr('moderators_not_found_error'));
            }
        } else if ($request->to == CUSTOM_USERS) {
            $custom_user = $request->custom_user;
            if ($custom_user != '') {
                $user_email = explode(',', $custom_user);
                if (Setting::get('custom_users_count') >= count($user_email)) {
                    foreach ($user_email as $key => $value) {
                        Log::info('Custom Mail list : ' . $value);
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {

                            //This variable is only for email validate messsage purpose only
                            $validate_email = 0;
                            $invalid_email[] = $value;
                            $message = tr('custom_email_invalid');
                            $invalid_email_address = implode(' , ', $invalid_email);
                        } else {
                            //This variable is only for email validate messsage purpose only  using
                            $validate_email = 1;
                            $subject = $request->subject;
                            $content = $request->content;
                            $page = "emails.send_mail";
                            $email = $value;

                            // Get the custom user name before @ symbol
                            $name = substr($email, 0, strrpos($email, "@"));
                            $email_data['name'] = $name;
                            $email_data['content'] = $content;
                            $email_data['email'] = $value;
                            Helper::send_email($page, $subject, $email, $email_data);
                        }
                    }

                    if ($validate_email == 0) {

                        return back()->with('flash_success', tr('mail_send_successfully'))->with('flash_error', $invalid_email_address . $message);

                    } else {

                        return back()->with('flash_success', tr('mail_send_successfully'));
                    }

                } else {

                    return back()->with('flash_error', tr('custom_user_count'));

                }

            } else {
                return back()->with('flash_error', tr('custom_user_field_required'));
            }

        } else {

            return back()->with('flash_error', tr('user_not_found'));
        }

        if (count($user_email) > 0) {

            $users_moderator_type = $request->to;

            $subject = $request->subject;

            $content = $request->content;

            dispatch(new SendMailCamp($user_email, $subject, $content, $users_moderator_type));

            return back()->with('flash_success', tr('mail_send_successfully'));

        } else {

            return back()->with('flash_error', tr('details_not_found'));
        }
    }

    /**
     * Function Name : user_subscription_pause
     *
     * To prevent automatic subscriptioon, user have option to cancel subscription
     *
     * @param object $request - USer details & payment details
     *
     * @return boolean response with message
     */
    public function user_subscription_pause(Request $request)
    {

        $user_payment = UserPayment::where('user_id', $request->id)->where('status', PAID_STATUS)->orderBy('created_at', 'desc')->first();

        if ($user_payment) {

            $user_payment->is_cancelled = AUTORENEWAL_CANCELLED;

            $user_payment->cancel_reason = $request->cancel_reason;

            $user_payment->save();

            return back()->with('flash_success', tr('cancel_subscription_success'));

        } else {

            return back()->with('flash_error', Helper::get_error_message(163));

        }

    }

    /**
     * Function Name : user_subscription_enable
     *
     * To prevent automatic subscriptioon, user have option to cancel subscription
     *
     * @created shobana Chandrasekar
     *
     * @edited
     *
     * @param object $request - USer details & payment details
     *
     * @return boolean response with message
     */
    public function user_subscription_enable(Request $request)
    {

        $user_payment = UserPayment::where('user_id', $request->id)->where('status', PAID_STATUS)->orderBy('created_at', 'desc')
            ->where('is_cancelled', AUTORENEWAL_CANCELLED)
            ->first();

        if ($user_payment) {

            $user_payment->is_cancelled = AUTORENEWAL_ENABLED;

            $user_payment->save();

            return back()->with('flash_success', tr('autorenewal_enable_success'));

        } else {

            return back()->with('flash_error', Helper::error_message(163));

        }

    }

    /**
     * Function Name : admin_videos_create()
     *
     * To display a upload video form
     *
     * @created_by - Shobana Chandrasekar
     *
     * @updated_by - -
     *
     * @param object $request - -
     *
     * @return response of html page with details
     */
    public function admin_videos_create(Request $request)
    {

        $categories = Category::where('categories.is_approved', DEFAULT_TRUE)
            ->select('categories.id as id', 'categories.name', 'categories.category_slug', 'categories.picture',
                'categories.is_series', 'categories.status', 'categories.is_approved')
            ->leftJoin('sub_categories', 'categories.id', '=', 'sub_categories.category_id')
            ->groupBy('sub_categories.category_id')
            ->where("sub_categories.is_approved", SUB_CATEGORY_APPROVED)
            ->havingRaw("COUNT(sub_categories.id) > 0")
            ->orderBy('categories.name', 'asc')
            ->get();

        //print_r($categories);die;

        $model = new AdminVideo;

        $model->trailer_video_resolutions = [];

        $model->video_resolutions = [];

        $videoimages = [];

        $video_cast_crews = [];

        $cast_crews = [];
        //$cast_crews = CastCrew::select('id', 'name')->get();
        $actors = CastCrew::select('id', 'name')->where('position', 1)->get();
        $directors = CastCrew::select('id', 'name')->where('position', 2)->get();
        $writers = CastCrew::select('id', 'name')->where('position', 3)->get();
        $genre = CastCrew::select('id', 'name')->where('position', 4)->get();
        $moderators = Moderator::select('id', 'name')->get();
        $tags = Tag::select('id', 'name', 'slug')->get();

        return view('admin.videos.upload')->with('page', 'videos')
            ->with('actors', $actors)
            ->with('directors', $directors)
            ->with('writers', $writers)
            ->with('genre', $genre)
            ->with('categories', $categories)
            ->with('sub_page', 'admin_videos_create')
            ->with('model', $model)
            ->with('videoimages', $videoimages)
            ->with('cast_crews', $cast_crews)
            ->with('video_cast_crews', $video_cast_crews)
            ->with('moderators', $moderators)
            ->with('tags', $tags);
    }

    public function search(Request $request)
    {

        $title = urlencode(trim($request->title));
        $api_key = getenv('TMDB_API_KEY');
        $url = "https://api.themoviedb.org/3";
        $category = $request->category;
        if ($category == 'movies') {
            $category = 'movie';
        } else if ($category == 'tv-shows') {
            $category = 'tv';
        } else {
            $category = 'movie';
        }
        $category = urlencode(trim($category));

        try {
            $movie = file_get_contents("$url/search/$category?query=$title&api_key=$api_key&page=$request->page"); // imdb
            $data = json_decode($movie, true);
            $data['category'] = $category;

        } catch (Exception $e) {

            return ['status' => 0, 'data' => []];

        }
        return $data;

    }

    public function get_movie(Request $request)
    {

        $id = $request->id;
        $api_key = getenv('TMDB_API_KEY');
        $url = "https://api.themoviedb.org/3";
        $category = $request->category;
        if ($category == 'movies') {
            $category = 'movie';
        } else if ($category == 'tv-shows') {
            $category = 'tv';
        } else {
            $category = 'movie';
        }
        $category = urlencode(trim($category));

        try {
            if ($category == 'tv') {
                $movie = file_get_contents("$url/tv/$id?api_key=$api_key&append_to_response=videos,credits,keywords");
            } else {
                $movie = file_get_contents("$url/movie/$id?api_key=$api_key&append_to_response=videos,credits,release_dates,alternative_titles,keywords");
            }

            $data = json_decode($movie, true);
            $data['category'] = $category;
            if (isset($data['release_dates']) && $data['category'] == 'movie') {
                $key = $this->searchArrayKeyVal("iso_3166_1", 'US', $data['release_dates']['results']);
                if ($key !== false) {
                    $match = $data['release_dates']['results'][$key];
                    $dates = $match['release_dates'];
                    $data['rated'] = !empty($dates) ? $dates[0]['certification'] : '';
                } else {
                    $data['rated'] = '';
                }
            }

        } catch (Exception $e) {

            return ['status' => 0, 'data' => [], 'msg' => $e->getMessage()];

        }

        //print_r($data);die;
        return $data;

    }

    // Array Search Function
    public function searchArrayKeyVal($sKey, $id, $array)
    {
        foreach ($array as $key => $val) {
            if ($val[$sKey] == $id) {
                return $key;
            }
        }
        return false;
    }

    /**
     * Function Name : admin_videos_save()
     *
     * @param object $request - -
     *
     * @return response of success/failure page
     * @uses To save a new video as well as updated video details
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function admin_videos_save(Request $request)
    {
        // Call video save method of common function video repo
        // echo '<pre>';
        // print_r($request->all());die;
        $response = VideoRepo::video_save($request)->getData();
        return ['response' => $response];
    }

    // public function admin_videos_save(Request $request)
    // {
    //     // Call video save method of common function video repo
    //     // echo '<pre>';
    //     // print_r($request->all());die;
    //     try{
    //     $video_model = AdminVideo::find($request->admin_video_id);
    //     $video_model->video_subtitle = Helper::subtitle_upload($request->file('video_subtitle'));
    //     // $video_model->video_subtitle = Helper::upload_file_to_digitalocean_spaces_image($request->file('video_subtitle'), 'images');
       
    //     echo "<pre>";
    //     print_r($video_model);die;

    //      }catch(\Exception $ex){
    //         echo "<pre>";
    //         print_r($ex->getMessage());die;
    //     }
    // }

    /**
     * Function Name : admin_videos_edit()
     *
     * @param object $request - -
     *
     * @return response of html page with details
     * @uses To display a upload video form
     *
     * @created: Shobana Chandrasekar
     *
     * @updated: -
     *
     */
    public function admin_videos_edit(Request $request)
    {
        $model = AdminVideo::where('admin_videos.id', $request->id)->first();

        if ($model) {

            $categories = Category::where('categories.is_approved', DEFAULT_TRUE)
                ->select('categories.id as id', 'categories.name', 'categories.picture',
                    'categories.is_series', 'categories.status', 'categories.is_approved')
                ->leftJoin('sub_categories', 'categories.id', '=', 'sub_categories.category_id')
                ->groupBy('sub_categories.category_id')
                ->where('sub_categories.is_approved', SUB_CATEGORY_APPROVED)
                ->havingRaw("COUNT(sub_categories.id) > 0")
                ->orderBy('categories.name', 'asc')
                ->get();

            $sub_categories = SubCategory::where('category_id', '=', $model->category_id)
                ->leftJoin('sub_category_images', 'sub_categories.id', '=', 'sub_category_images.sub_category_id')
                ->select('sub_category_images.picture', 'sub_categories.*')
                ->where('sub_category_images.position', 1)
                ->where('is_approved', SUB_CATEGORY_APPROVED)
                ->orderBy('name', 'asc')
                ->get();

            $model->publish_time = $model->publish_time ? date('d-m-Y H:i:s', strtotime($model->publish_time)) : $model->publish_time;

            //     $model->trailer_video_resolutions = [];

            // $model->video_resolutions = [];

            // $videoimages = [];

            // $video_cast_crews = [];

            $videoimages = get_video_image($model->id);

            $model->video_resolutions = $model->video_resolutions ? explode(',', $model->video_resolutions) : [];

            $model->trailer_video_resolutions = $model->trailer_video_resolutions ? explode(',', $model->trailer_video_resolutions) : [];

            $video_cast_crews = VideoCastCrew::select('cast_crew_id')
                ->where('admin_video_id', $request->id)
                ->get()->pluck('cast_crew_id')->toArray();

            //print_r(json_decode($model->actors, true));die;
            $cast_crews = [];
            if (!empty($model->actors)) {
                foreach (json_decode($model->actors, true) as $key => $value) {
                    $query = DB::table('cast_crews')
                        ->whereRaw('FIND_IN_SET(?,id)', [$value])
                        ->first();
                    $cast_crews[] = $query;
                }
            }

            //print_r($cast_crews);die;
            //$cast_crews = CastCrew::select('id', 'name')->get();
            $moderators = Moderator::select('id', 'name')->get();
            $actors = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(1, position)")->get();
            $directors = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(2, position)")->get();
            $writers = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(3, position)")->get();
            $genre = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(4, position)")->get();

            $model->sub_category_id = $model->sub_category_id != '0' ? $model->sub_category_id : '';

            return view('admin.videos.upload')->with('page', 'videos')
                ->with('categories', $categories)
                ->with('actors', $actors)
                ->with('directors', $directors)
                ->with('writers', $writers)
                ->with('genre', $genre)
                ->with('model', $model)
                ->with('sub_categories', $sub_categories)
                ->with('sub_page', 'admin_videos_create')
                ->with('videoimages', $videoimages)
                ->with('cast_crews', $cast_crews)
                ->with('video_cast_crews', $video_cast_crews)
                ->with('moderators', $moderators);

        } else {

            return back()->with('flash_error', tr('something_error'));

        }

    }

    public function admin_actor_list(Request $request)
    {
        $search = $request->search;
        if($request->type == 'actor'){
            $position = 1;
        }elseif($request->type == 'writer'){
            $position = 3;
        }elseif($request->type == 'director'){
            $position = 2;
        }

        if ($search == '') {
            $actors = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(".$position.", position)")->paginate(100);
        } else {
            $actors = CastCrew::select('id', 'name')->whereRaw("FIND_IN_SET(".$position.", position)")->where('name', 'like', '%' . $search . '%')->paginate(100);
        }

        //print_r($actors->total());die;

        $response = array();
        foreach ($actors as $actor) {
            $response[] = array(
                "id" => $actor->id,
                "text" => $actor->name,
            );
        }

        $result['data'] = $response;

        $result['total_count'] = $actors->total();

        echo json_encode($result);
        die;
        exit;
    }

   
    /**
     * Function Name : cast_crews_add()
     *
     * @param  - -
     *
     * @return response of html page with details
     * @uses To display a form to add a new cast
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function cast_crews_add(Request $request)
    {
        $model = new CastCrew;
        $model1 = new CastProfile;
        return view('admin.cast_crews.create')->with('page', 'cast-crews')
            ->with('sub_page', 'cast-crew-add')->with('model', $model)->with('model1',$model1);

    }

    /**
     * Function Name : cast_crews_edit()
     *
     * @param string $request - Unique id of the cast and crew
     *
     * @return response of html page with details
     * @uses Display a form to edit a cast with existing details
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function cast_crews_edit(Request $request)
    {

        $model = CastCrew::with('castProfile')->where('unique_id', $request->id)->first();
        $model1 = $model->castProfile;
        // echo "<pre>";
        // print_r($model1);die;
        if ($model) {

            return view('admin.cast_crews.edit')->with('page', 'cast-crews')
                ->with('sub_page', 'cast-crew-add')->with('model', $model)->with('model1',$model1);

        } else {

            return back()->with('flash_error', tr('cast_crew_not_found'));
        }

    }

    /**
     * Function Name : cast_crews_view()
     *
     * @param string $request - Unique id of the cast and crew
     *
     * @return response of html page with details
     * @uses To view the detaisl of cast and crew
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function cast_crews_view(Request $request)
    {

        $model = CastCrew::with('castProfile')->where('unique_id', $request->id)->first();

        if ($model) {

            return view('admin.cast_crews.view')->with('page', 'cast-crews')
                ->with('sub_page', 'cast-crew-index')->with('model', $model);

        } else {

            return back()->with('flash_error', tr('cast_crew_not_found'));
        }

    }

    /**
     * Function Name : cast_crews_index()
     *
     * @param  - -
     *
     * @return response of html page with details
     * @uses To list out details of cast and crews
     *
     * @created: Shobana Chandrasekar
     *
     * @updated: Vedgupt S
     *
     */
    public function cast_crews_index(Request $request)
    {
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $cast_crew_search = $_GET['search'];
                $request->session()->put('cast_crew_search', $cast_crew_search);
            } else {
                $request->session()->forget('cast_crew_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $cast_crew_search = $_GET['search'];
                $request->session()->put('cast_crew_search', $cast_crew_search);
            } else {
                $cast_crew_search = $request->session()->get('cast_crew_search');
            }
        }

        $columns = array('id', 'name', 'position', 'status');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if ($request->session()->get('cast_crew_search')) {
            $model = CastCrew::with('castProfile')->where('name', $cast_crew_search)
                ->orWhere('name', 'LIKE', '%' . $cast_crew_search . '%')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $cast_crew_name = $cast_crew_search;
        } else {
            $model = CastCrew::with('castProfile')->orderBy($column, $order_by)->paginate($per_page)->appends(request()->query());

            $cast_crew_name = '';
        }
        //echo '<pre>';
        //print_r($model);die;

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.cast_crews.index')->with('page', 'cast-crews')
            ->with('sub_page', 'cast-crews-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('cast_crew_name', $cast_crew_name)
            ->with('per_page', $per_page);
    }

    /**
     * Function Name : cast_crews_delete()
     *
     * @param string $request - Unique id of the cast and crew
     *
     * @return response of success/failure message
     * @uses To delete a cast and crew based on the unique id
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function cast_crews_delete(Request $request)
    {

        $model = CastCrew::where('unique_id', $request->id)->first();

        if ($model) {

            $image = $model->image;

            if ($model->delete()) {

                if ($image) {

                    Helper::delete_picture($image, '/uploads/cast_crews/');
                    Helper::spaces_delete_picture($image, 'cast_crews');
                }

                return redirect(route('admin.cast_crews.index'))->with('flash_success', tr('cast_crew_delete_success'));

            }

        } else {

            return back()->with('flash_error', tr('cast_crew_not_found'));
        }

    }

    /**
     * Function Name : cast_crews_status()
     *
     * @param string $request - Unique id of the cast and crew
     *
     * @return response of success/failure message
     * @uses To change the status of cast details based on cast id (Approve/Decline)
     *
     * @created: Shobana Chandrasekar
     *
     * @updated:
     *
     */
    public function cast_crews_status(Request $request)
    {

        $model = CastCrew::where('unique_id', $request->id)->first();

        if ($model) {

            $model->status = $model->status == CAST_APPROVED ? CAST_DECLINED : CAST_APPROVED;

            if ($model->save()) {

                if ($model->status == CAST_DECLINED) {

                    if (count($model->videoCastCrews) > 0) {

                        foreach ($model->videoCastCrews as $value) {

                            $value->delete();

                        }

                    }

                }

                return redirect(route('admin.cast_crews.index'))->with('flash_success', $model->status ? tr('cast_crew_approve_success') : tr('cast_crew_decline_success'));

            }

        } else {

            return back()->with('flash_error', tr('cast_crew_not_found'));
        }

    }

    public function cast_crews_position()
    {
        $actors_count = CastCrew::whereRaw("FIND_IN_SET(1, position)")
            ->count();
        
        $director_count = CastCrew::whereRaw("FIND_IN_SET(2, position)")
        ->count();

        $writer_count = CastCrew::whereRaw("FIND_IN_SET(3, position)")
        ->count();
        
        return view('admin.cast_crews.position')
            ->with('page', 'cast-crews')
            ->with('sub_page', 'cast-crews-position')
            ->with('actor_count', $actors_count)
            ->with('director_count', $director_count)
            ->with('writer_count', $writer_count);
    }
    
    public function cast_crews_position_name(Request $request, $position_name)
    {
        if ($position_name == 'actors') {
            $position = 1;
        } elseif ($position_name == 'directors') {
            $position = 2;
        }  elseif ($position_name == 'writers') {
            $position = 3;
        }

        if (!isset($_GET['page'])) {
            if (isset($_GET['search'])) {
                $cast_crew_search = $_GET['search'];
                $request->session()->put('cast_crew_search', $cast_crew_search);
            } else {
                $request->session()->forget('cast_crew_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $cast_crew_search = $_GET['search'];
                $request->session()->put('cast_crew_search', $cast_crew_search);
            } else {
                $cast_crew_search = $request->session()->get('cast_crew_search');
            }
        }

        $columns = array('id', 'name', 'position', 'status');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }
        
        if ($request->session()->get('cast_crew_search')) {
            $query = CastCrew::with('castProfile')->whereRaw('FIND_IN_SET('.$position.' ,position)');
            $query = $query->where(function ($query) use ($cast_crew_search) {
                $query->where('name', $cast_crew_search)
                      ->orWhere('name', 'LIKE', '%' . $cast_crew_search . '%');
            });
            $model = $query->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $cast_crew_name = $cast_crew_search;
        } else {
            $model = CastCrew::with('castProfile')->whereRaw('FIND_IN_SET('.$position.' ,position)')->orderBy($column, $order_by)->paginate($per_page)->appends(request()->query());

            $cast_crew_name = '';
        }
        //echo '<pre>';
        //print_r($model);die;

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.cast_crews.position-list')->with('page', 'cast-crews')
            ->with('sub_page', 'cast-crews-position-list')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('cast_crew_name', $cast_crew_name)
            ->with('position_name', $position_name)
            ->with('per_page', $per_page);
    }

    /**
     * Function Name : cast_crews_save()
     *
     * @param string $request - Unique id of the cast and crew
     *
     * @return response of success/failure message
     * @uses To save the details of the cast and crews
     *
     * @created: Shobana Chandrasekar
     *
     * @updated: Vidhya R
     *
     */
    public function cast_crews_save(Request $request)
    {

        // echo "<pre>";
        // print_r($request->all());die;
        $data = $request->all();

        $cast_credits = [];
        $crew_credits = [];
        if (!empty($data['cast_credits'])) {
            $cast_credits_array = json_decode($data['cast_credits'], true);
            foreach ($cast_credits_array as $key => $value) {
                $cast_credits[] = $value;
            }
        }

        if (!empty($data['crew_credits'])) {
            $crew_credits_array = json_decode($data['crew_credits'], true);
            foreach ($crew_credits_array as $key => $value) {
                $crew_credits[] = $value;
            }
        }


        try {

            $is_tmdb = ($request->tmdb == 'custom') ? 0 : 1;

            if ($is_tmdb) {
                $validator = Validator::make($data, [
                    'person_id' => 'unique:cast_crews,name,' . $request->name,
                    'name' => 'required',
                    'image' => $request->id ? 'mimes:jpeg,jpg,png' : 'mimes:jpeg,png,jpg',
                    'position' => 'required',
                ], ['person_id.unique' => 'The Cast has been already taken']);
            } else {
                $validator = Validator::make($data, [
                    'name' => 'required',
                    'image' => $request->id ? 'mimes:jpeg,jpg,png' : 'mimes:jpeg,png,jpg',
                    'position' => 'required',
                ]);
            }

            if ($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error);

            } else {

                $model = $request->id ? CastCrew::where('id', $request->id)->first() : new CastCrew;

                $model->name = $request->name;

                $model->unique_id = $request->id ? $model->unique_id : uniqid(str_replace(' ', '-', $model->name));

                $model->cast_id = $request->person_id;
                $model->position = implode(',', $request->position);

                $model->status = DEFAULT_TRUE; // By default it will be 1, future it may vary
                $model->tmdb = $is_tmdb;
                $model->created_at = $request->created_at;

                // echo "<pre>";
                // print_r($model);die;
                if ($model->save()) {
                    if ($request->id) {
                        $person = CastProfile::where('cast_crew_id', $model->id)->first();
                    } else {
                        $person = new CastProfile;
                    }

                    $person->cast_crew_id = $model->id;
                    $person->cast_id = $model->cast_id;
                    $person->name = (!empty($request->name)) ? $request->name : $person->name;
                    $person->biography = (!empty($request->biography)) ? $request->biography : '';
                    $person->birthday = (!empty($request->celeb_birthday)) ? $request->celeb_birthday : '';
                    $person->deathday = (!empty($request->deathday)) ? $request->deathday : '';
                    $person->homepage = (!empty($request->homepage)) ? $request->homepage : '';
                    $person->place_of_birth = (!empty($request->celeb_place_of_birth)) ? $request->celeb_place_of_birth : '';
                    $person->gender = (!empty($request->gender)) ? $request->gender : '';
                    $person->known_for_department = (!empty($request->known_for_department)) ? $request->known_for_department : '';
                    $person->imdb_id = $request->imdb_id;
                    $person->also_known_as = (!empty($request->celeb_known_as)) ? $request->celeb_known_as : '';
                    $person->popularity = (!empty($request->popularity)) ? $request->popularity : '';
                    $person->cast_credits = (!empty($cast_credits)) ? json_encode($cast_credits) : json_encode([]);
                    $person->crew_credits = (!empty($crew_credits)) ? json_encode($crew_credits) : json_encode([]);

                    //print_r($person);die;
                 
                    if ($request->hasFile('default_image') || $request->poster) {

                        if ($request->hasFile('default_image') && $request->id) {

                            Helper::delete_picture($person->default_image, '/uploads/images/');
                            Helper::spaces_delete_picture($person->default_image, 'images');

                        }
                        if ($request->poster) {
                            $person->profile_path = Helper::copy_img_to_digitalocean($request->poster, 'images');
                        } else {

                            $person->profile_path = Helper::upload_file_to_digitalocean_spaces_image($request->file('default_image'), 'images');
                        }
                    }

                    $person->profile_path = (!empty($person->profile_path)) ? $person->profile_path : '';

                    $person->save();
                    
                   
                    Seo::save($person, url('person/'.$person->cast_crew->unique_id), [
                        'title' => $person->name,
                        'images' => [
                            $person->profile_path
                        ],
                        
                    ]);
                } else {

                    throw new Exception(tr('cast_crew_not_saving'));

                }

            }

            return redirect(route('admin.cast_crews.view', ['id' => $model->unique_id]))->with('flash_success', $request->id ? tr('cast_crew_update_success') : tr('cast_crew_create_success'));

        } catch (Exception $e) {

            return back()->with('flash_error', $e->getMessage());

        }
    }

    /**
     * Function Name : gif_generator()
     *
     * @param integer $request - video id
     *
     * @return response of json details
     * @uses Future, Not now - To create a gif based on 3 images
     *
     * @created: Shobana Chandrasekar
     *
     * @edited: Vidhya R
     *
     */
    public function gif_generator(Request $request)
    {

        $video = AdminVideo::find($request->id);

        if ($video) {

            // Gif Generation Based on three images

            $FFmpeg = new \FFmpeg;

            $FFmpeg
                ->setImage('image2')
                ->setFrameRate(1)
                ->input(base_path() . "/uploads/images/video_{$request->video_id}_%03d.png")
                ->setAspectRatio("4:2")
                ->frameRate(30)
                ->output(base_path() . "/uploads/gifs/video_{$request->video_id}.gif")
                ->ready();

            $video->video_gif_image = Helper::web_url() . "/uploads/gifs/video_{$request->video_id}.gif";

            $video->save();

            return back()->with('flash_success', tr('gif_generate_success'));

        } else {

            return back()->with('flash_error', tr('gif_generate_failure'));

        }

    }

    /*** Function Name : clear_login** @uses To clear all the logins from all devices** @created: Shobana Chandrasekar** @updated: Vidhya R** @param object $request - User details** @return response of success/failure message */
    public function clear_login(Request $request)
    {

        $user = User::find($request->id);

        if ($user) {

            // Delete all the records which is stored before

            UserLoggedDevice::where('user_id', $request->id)->delete();

            $user->logged_in_account = 0;

            $user->save();

            return back()->with('flash_success', tr('user_clear'));

        } else {

            return back()->with('flash_error', tr('user_not_found'));
        }

    }

    /**
     * Function Name : videos_compression_complete()
     *
     * @param integer video id - Video id
     *
     * @created: shobana chandrasekar
     *
     * @updated: Vidhya R
     *
     * @return response of success/failure message
     * @uses To complete the compressing videos
     *
     */
    public function videos_compression_complete(Request $request)
    {

        $video = AdminVideo::find($request->id);

        if ($video) {

            // Check the video has compress state or not

            if ($video->compress_status <= OVERALL_COMPRESS_COMPLETED) {

                $video->compress_status = COMPRESSION_NOT_HAPPEN;

                $video->trailer_compress_status = COMPRESS_COMPLETED;

                $video->main_video_compress_status = COMPRESS_COMPLETED;

                if ($video->save()) {

                    return back()->with('flash_success', tr('video_compress_success'));

                } else {

                    return back()->with('flash_error', tr('video_not_saved'));

                }

            } else {

                return back()->with('flash_error', tr('already_video_compressed'));
            }

        } else {

            return back()->with('flash_error', tr('video_not_found'));

        }
    }

    /**
     * Function Name : banner_image()
     *
     * @param object $request - Banner image video details
     *
     * @created: Shobana Chandrasekar
     *
     * @updated: Vidhya R
     *
     * @return response of success/failure message details
     * @uses Set banner image for video
     *
     */
    public function videos_set_banner(Request $request)
    {

        $validator = Validator::make($request->all(), array(
            'admin_video_id' => 'required|exists:admin_videos,id,is_approved,' . VIDEO_APPROVED . ',status,' . VIDEO_PUBLISHED,
            'banner_image' => 'required|mimes:jpeg,jpg,bmp,png',
        ), [

                'admin_video_id.exists' => tr('video_not_exists'),

            ]
        );

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);

        } else {

            $video = AdminVideo::find($request->admin_video_id);

            if ($request->hasFile('banner_image')) {

                if ($video->is_banner == BANNER_VIDEO) {

                    //Helper::delete_picture($video->banner_image, "/uploads/images/video/385x225/");
                    Helper::spaces_delete_picture($video->banner_image, 'images');

                }

                $video->banner_image = Helper::upload_file_to_digitalocean_spaces_image($request->file('banner_image'), 'images');

            }

            $video->is_banner = BANNER_VIDEO;
            $video->is_home_slider = 0;
            // $video->slider_image = '';

            $video->save();

            return back()->with('flash_success', tr('video_set_banner_success'));

        }

    }

    public function change_slider($id)
    {

        $video = AdminVideo::find($id);

        $video->is_home_slider = 1;
        $video->is_banner = 0;

        $video->save();

        return back()->with('flash_success', "Successfully changed");

    }

    public function videos_remove_slider($id)
    {

        $video = AdminVideo::find($id);

        Helper::delete_picture($video->slider_image, "/uploads/images/");

        $video->is_home_slider = 0;
        $video->slider_image = '';

        $video->save();

        return back()->with('flash_success', "Successfully removed");
    }

    /**
     * Function Name : videos_remove_banner()
     *
     * @param object $request - Banner image video details
     *
     * @created: Shobana Chandrasekar
     *
     * @updated: Vidhya R
     *
     * @return response of success/failure message details
     * @uses Remove banner image for video
     *
     */
    public function videos_remove_banner(Request $request)
    {

        $validator = Validator::make($request->all(), array(
            'admin_video_id' => 'required|exists:admin_videos,id',
        ), [

                'admin_video_id.exists' => tr('video_not_exists'),

            ]
        );

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return back()->with('flash_error', $error_messages);

        } else {

            $video = AdminVideo::find($request->admin_video_id);

            //Helper::delete_picture($video->banner_image, "/uploads/images/");
            Helper::spaces_delete_picture($video->banner_image, 'images');

            $video->is_banner = BANNER_VIDEO_REMOVED;

            $video->save();

            return back()->with('flash_success', tr('video_remove_banner'));

        }

    }

    public function load_seo()
    {
        $videos = AdminVideo::all();
        //echo '<pre>';
        //print_r($videos);die;
        if (!empty($videos)) {
            foreach ($videos as $key => $value) {
                $seo_pages = DB::table('seo_pages')->where('object_id', '=', $value->id)->count();
                if ($seo_pages <= 0) {
                    $value->details = $this->strip_tags_with_whitespace($value->details);
                    // save seo tags
                    Seo::save($value, url('movies/' . $value->unique_id), [
                        'title' => $value->title,
                        'images' => [
                            $value->default_image
                        ]
                    ],
                        [
                            'title' => $value->title,
                            'robot_index' => 'index',
                            'canonical_url' => url('movies/' . $value->unique_id),
                            'description' => $value->details
                        ],
                        [
                            '4' => 'video.movie',
                            '5' => $value->title,
                            '6' => $value->details,
                            '7' => url('movies/' . $value->unique_id),
                            '8' => Setting::get('site_name'),
                            '9' => $value->default_image,
                            '10' => $value->title,
                            '22' => 'summary',
                            '25' => $value->details,
                            '26' => $value->title,
                            '27' => $value->default_image,
                            '1' => Setting::get('site_name')
                        ]);
                }
            }
            echo '<h1>Load SEO script run successfully !!<br> The data has been inserted successfully from admin_seo table to seo tables.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        } else {
            echo '<h1>Load SEO script run successfully !!<br> You have no data found in admin_video table.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        }

    }

    public function strip_tags_with_whitespace($string, $allowable_tags = null)
    {
        $string = str_replace('<', ' <', $string);
        $string = strip_tags($string, $allowable_tags);
        $string = str_replace('  ', ' ', $string);
        $string = trim($string);
        $string = preg_replace("/\s+/", " ", $string);
        return $string;
    }

    public function ratings_index(Request $request)
    {
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $request->session()->forget('rated_by_user_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $rated_by_user_search = $request->session()->get('rated_by_user_search');
            }
        }

        $columns = array('user_reviews.id', 'user_reviews.admin_video_id', 'review_count', 'admin_videos.vote_count', 'admin_videos.vote_rating_total');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (!empty($request->get('search'))) {
            $model = UserReview::leftJoin('admin_videos', 'user_reviews.admin_video_id', '=', 'admin_videos.id')
                ->selectRaw('user_reviews.admin_video_id, admin_videos.title, admin_videos.vote_rating_total, admin_videos.vote_count, count(user_reviews.id) as review_count')
                ->Where('admin_videos.title', 'Like', '%' . $rated_by_user_search . '%')
                ->groupBy('user_reviews.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = $rated_by_user_search;
        } else {
            $model = UserReview::leftJoin('admin_videos', 'user_reviews.admin_video_id', '=', 'admin_videos.id')
                ->selectRaw('user_reviews.admin_video_id, admin_videos.title, admin_videos.vote_rating_total, admin_videos.vote_count, count(user_reviews.id) as review_count')
                ->groupBy('user_reviews.admin_video_id')
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = '';
        }

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.ratings.index')->with('page', 'ratings')
            ->with('sub_page', 'rating-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('rated_by_user_search', $rated_by_user_search)
            ->with('per_page', $per_page);
    }

    public function ratings_list(Request $request, $video_id)
    {
        if (!isset($_GET['page'])) {

            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $request->session()->forget('rated_by_user_search');
            }
        } else {
            if (isset($_GET['search'])) {
                $rated_by_user_search = $_GET['search'];
                $request->session()->put('rated_by_user_search', $rated_by_user_search);
            } else {
                $rated_by_user_search = $request->session()->get('rated_by_user_search');
            }
        }

        $columns = array('id', 'user_id', 'admin_video_id', 'rating', 'spoilers', 'status');

        if ((!empty($_GET['order_by'])) && (!empty($_GET['column']) && in_array($_GET['column'], $columns))) {
            $order_by = ($_GET['order_by'] == 'asc') ? 'asc' : 'desc';
            $column = $_GET['column'];
        } else {
            $order_by = 'desc';
            $column = $columns[0];
        }

        if (isset($_GET['per_page']) && !empty($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        } else {
            $per_page = 10;
        }

        if (!empty($request->get('search'))) {

            $video_title = AdminVideo::select('id', 'title')->where('id', $video_id)->first();

            $model = UserReview::where(function ($model) use ($rated_by_user_search) {
                $model->where('comment', 'Like', '%' . $rated_by_user_search . '%')
                    ->orWhere('subject', 'Like', '%' . $rated_by_user_search . '%');
            });

            $model = $model->where('admin_video_id', '=', $video_id)
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());

            $rated_by_user_search = $rated_by_user_search;
        } else {
            $video_title = AdminVideo::select('id', 'title')->where('id', $video_id)->first();

            $model = UserReview::where('admin_video_id', $video_id)
                ->orderBy($column, $order_by)
                ->paginate($per_page)->appends(request()->query());
            $rated_by_user_search = '';

            $rated_by_user_search = '';
        }

        $up_or_down = $order_by;
        $asc_or_desc = ($order_by == 'desc') ? 'asc' : 'desc';

        return view('admin.ratings.list')->with('page', 'reviews')
            ->with('sub_page', 'rating-index')->with('model', $model)
            ->with('asc_or_desc', $asc_or_desc)
            ->with('up_or_down', $up_or_down)
            ->with('column', $column)
            ->with('rated_by_user_search', $rated_by_user_search)
            ->with('video_title', $video_title->title)
            ->with('video_id', $video_id)
            ->with('per_page', $per_page);
    }

    public function ratings_view(Request $request)
    {

        $model = UserReview::where('id', $request->id)->first();

        if ($model) {

            return view('admin.cast_crews.view')->with('page', 'ratings')
                ->with('sub_page', 'rating-index')->with('model', $model);

        } else {

            return back()->with('flash_error', tr('rating_not_found'));
        }

    }

    public function ratings_add()
    {
        $model = new UserReview;
        $users = User::where('status', '=', 1)->where('is_activated', '=', 1)->get();
        $videos = AdminVideo::select('title', 'id')->where('status', '=', 1)->where('is_approved', '=', 1)->get();
        return view('admin.ratings.create')->with('page', 'ratings')
            ->with('sub_page', 'rating-add')
            ->with('model', $model)
            ->with('users', $users)
            ->with('videos', $videos);
    }

    public function ratings_edit(Request $request)
    {

        $model = UserReview::where('id', $request->id)->first();

        $users = User::where('status', '=', 1)->where('is_activated', '=', 1)->get();
        $videos = AdminVideo::select('title', 'id')->where('status', '=', 1)->where('is_approved', '=', 1)->get();

        if ($model) {

            return view('admin.ratings.edit')->with('page', 'ratings')
                ->with('sub_page', 'rating-add')->with('model', $model)
                ->with('users', $users)
                ->with('videos', $videos);

        } else {

            return back()->with('flash_error', tr('rating_not_found'));
        }

    }

    public function ratings_save(Request $request)
    {

        $data = $request->all();

        foreach ($data as $key => $value) {
            if ($value == null) {
                $data[$key] = "";
            }
        }

        try {

            $validator = Validator::make($data, [
                'review' => 'required',
                'subject' => 'required',
                'rating' => 'required',
            ]);

            if ($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error);

            } else {

                $model = $request->id ? UserReview::where('id', $request->id)->first() : new UserReview;


                //$model->user_id = $request->user_id;

                //$model->admin_video_id = $request->admin_video_id;
                $model->spoilers = ($request->spoilers == '1') ? 'YES' : 'NO';
                $model->subject = $request->subject;
                $model->comment = (!empty($request->review)) ? $request->review : '';

                $model->status = $request->id ? $model->status : DEFAULT_FALSE; // By default it will be 1, future it may vary

                // get and update calculate vote rating data
                $admin_video = AdminVideo::find($request->admin_video_id);

                $ratings = $request->rating;
                $vote_count = $admin_video->vote_count;
                $vote_total = ($admin_video->vote_rating_total - $model->rating) + $request->rating;
                //$vote_total = round($vote_count * $ratings);


                $vote_average = number_format($vote_total / $vote_count, 2);
                $ratings = round($vote_total / $vote_count);

                $model->rating = $request->rating;

                if ($model->save()) {
                    //update admin video table by vote_average, vote_count, vote_total, and ratings
                    AdminVideo::where('id', $request->admin_video_id)
                        ->update([
                            'vote_average' => $vote_average,
                            'vote_count' => $vote_count,
                            'vote_rating_total' => $vote_total,
                            'ratings' => $ratings
                        ]);
                } else {

                    throw new Exception(tr('rating_not_saving'));

                }

            }

            return redirect(route('admin.ratings.list', $model->admin_video_id))->with('flash_success', $request->id ? tr('rating_update_success') : tr('rating_create_success'));

        } catch (Exception $e) {

            return back()->with('flash_error', $e->getMessage());

        }
    }

    public function add_comment(Request $request)
    {
        $subject = trim($request->subject);
        $comment = trim($request->comment);
        $rating = trim($request->rating);
        $video_id = trim($request->video_id);
        $spoilers = $request->spoilers;
        $error = [];
        if (empty($subject)) {
            $error[] = 'Please type your subject or headline.';
        }

        if (empty($rating)) {
            $error[] = 'Please select your rating.';
        }

        if (empty($comment)) {
            $error[] = 'Please type your comment.';
        }

        if (!$spoilers) {
            $error[] = 'Please mark for spoiler content.';
        }

        if (!empty($error)) {
            return Response::json(['errors' => $error, 'status' => false]);
        } else {

            $model = new UserReview;
            $model->user_id = (!empty(Auth::guard('admin')->user()->id)) ? Auth::guard('admin')->user()->id : '1';
            $model->admin_video_id = $video_id;
            $model->rating = $rating;
            $model->subject = $request->subject;

            $model->spoilers = ($request->spoilers == '1') ? 'YES' : 'NO';
            $model->comment = $request->comment;

            $model->status = DEFAULT_TRUE; // By default it will be 1, future it may vary

            if ($model->save()) {
                $comments = get_comments($video_id);
                $html = '';
                $html = View::make('admin.videos._comments', compact('comments'))->render();

            }

            return Response::json(['status' => true, 'view' => $html]);
        }
    }

    public function ratings_delete(Request $request)
    {

        $model = UserReview::where('id', $request->id)->first();

        if ($model) {

            if ($model->delete()) {
                $comment_count = comment_count($model->admin_video_id);

                return Response::json(['status' => true, 'msg' => 'success', 'data' => $request->id, 'comment_count' => $comment_count, 'video_id' => $model->admin_video_id]);
            }

        } else {
            return Response::json(['status' => false, 'msg' => 'error', 'data' => '']);
        }

    }

    public function ratings_status(Request $request)
    {

        $model = UserReview::where('id', $request->id)->first();

        if ($model) {

            $model->status = $model->status == 1 ? 0 : 1;

            if ($model->save()) {

                $decline_msg = tr('decline_ratings');
                $html = '';
                if ($model->status == 0) {
                    $html = '<a class="menuitem comment-status" tabindex="-1" href="javascript:void(0)" url="' . route('admin.ratings.status', ['id' => $model->id]) . '" onclick="return confirm(&quot;' . tr('ratings_approve_confirmation') . '&quot)">' . tr('approve') . ' </a>';
                } else {
                    $html = '<a class="menuitem comment-status" href="javascript:void(0)" tabindex="-1" url="' . route('admin.ratings.status', ['id' => $model->id]) . '" onclick="return confirm(&quot;' . tr('decline_ratings') . '&quot)">' . tr('decline') . '</a>';
                }

                return Response::json(['status' => true, 'msg' => 'success', 'data' => $request->id, 'html' => $html, 'video_id' => $model->admin_video_id]);

            }

        } else {

            return Response::json(['status' => false, 'msg' => 'error', 'data' => '']);
        }

    }

    public function seo_canonical(Request $request)
    {
        if (!empty($request->video_id)) {
            $video_model = AdminVideo::select('unique_id', 'title')->where('id', $request->video_id)->first();
            $video_slug = $video_model->unique_id;
        } else {
            $video_slug = '';
        }

        if (!empty($video_slug)) {
            if ($video_model->title != $request->canonical_url) {
                $title_slug = seoUrl($request->canonical_url);
                // check to see if any other slugs exist that are the same & count them
                $slug_count = AdminVideo::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;

                echo $canonical_slug = url('movies/' . $unique_slug);
                die;
            } else {
                echo url('movies/' . $video_slug);
                die;
            }
        } else {
            $title_slug = seoUrl($request->canonical_url);
            // check to see if any other slugs exist that are the same & count them
            $slug_count = AdminVideo::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

            // if other slugs exist that are the same, append the count to the slug
            $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;

            echo $canonical_slug = url('movies/' . $unique_slug);
            die;
        }
    }

    public function person_seo_canonical(Request $request)
    {
        
        if (!empty($request->video_id)) {
            $video_model = CastCrew::select('unique_id', 'name')->where('id', $request->video_id)->first();
            $video_slug = $video_model->unique_id;
        } else {
            $video_slug = '';
        }

        if (!empty($video_slug)) {
            if ($video_model->name != $request->canonical_url) {
                $title_slug = seoUrl($request->canonical_url);
                // check to see if any other slugs exist that are the same & count them
                $slug_count = CastCrew::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;

                echo $canonical_slug = url('person/' . $unique_slug);
                die;
            } else {
                echo url('person/' . $video_slug);
                die;
            }
        } else {
            $title_slug = seoUrl($request->canonical_url);
            // check to see if any other slugs exist that are the same & count them
            $slug_count = CastCrew::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

            // if other slugs exist that are the same, append the count to the slug
            $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;

            echo $canonical_slug = url('person/' . $unique_slug);
            die;
        }
    }

    public function load_aka(Request $request)
    {
        $videos = AdminVideo::all();
        echo ' <link rel="stylesheet" href="'.url('/').'/admin-css/dist/css/custom.css">';
        // echo '<pre>';
        // print_r($videos);die;
        if (!empty($videos)) {
            echo '<h2>Here are the report listing for which movies who has not tmdb movie ID or release date correct in our database.</h2><table class="load-aka-report" border="1px solid #000" cellpadding="10px" style="border-collapse: collapse;"><tr><td>ID</td><td>Movie name</td></tr>';
            foreach ($videos as $key => $value) {
                $old_tmdb_id = DB::table('admin_videos')->select('tmdb_movie_id')->where('id', '=', $value->id)->first();
                if (empty($old_tmdb_id->tmdb_movie_id) || is_null($old_tmdb_id->tmdb_movie_id)) {
                    if ($value->tmdb == 1) {
                        $title = urlencode(trim($value->title));
                        $release_date = date('Y', strtotime($value->release_date));
                        $api_key = getenv('TMDB_API_KEY');
                        $url = "https://api.themoviedb.org/3";
                        try {
                            if ($value->release_date != '' || $value->release_date != NULL) {
                                $movie = @file_get_contents("$url/search/movie?query=$title&api_key=$api_key&year=$release_date"); // imdb
                                if ($movie === FALSE) {
                                    echo 'Movie_id => ' . $value->id . '<br>' . 'Movie Title => ' . $value->title;
                                } else {
                                    $data = json_decode($movie, true);
                                    $results = $data['results'];
                                    if (!empty($results)) {
                                        $arr = [];
                                        foreach ($results as $index => $row) {
                                            if ($value->title == $row['title'] && ($value->release_date == $row['release_date'])) {
                                                $tmdb_id = $row['id'];
                                                //$get_alternative_title = file_get_contents("$url/movie/$tmdb_id/alternative_titles?api_key=$api_key"); // imdb
                                                $get_alternative_title = file_get_contents("$url/movie/$tmdb_id?api_key=$api_key&append_to_response=videos,credits,release_dates,alternative_titles"); // imdb
                                                $data_2 = json_decode($get_alternative_title, true);

                                                if ($data_2['alternative_titles']['titles']) {
                                                    $alternative_titles = json_encode($data_2['alternative_titles']['titles']);
                                                } else {
                                                    $alternative_titles = '';
                                                }

                                                // update admin video table by tmdb_id and alternative titles
                                                AdminVideo::where('tmdb', 1)
                                                    ->where('id', $value->id)
                                                    ->update(['tmdb_movie_id' => $tmdb_id, 'alternative_titles' => $alternative_titles,
                                                        'tmdb_details' => $get_alternative_title]);
                                            } else {
                                                // report file
                                                if (!in_array($value->id, $arr)) {
                                                    $arr[] = $value->id; 
                                                    echo '<tr><td>'. $value->id .'</td><td>'. $value->title .'</td></tr>';
                                                    
                                                    file_put_contents('load_aka.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->title.PHP_EOL, FILE_APPEND);
                                                }
                                            }
                                        }
                                    }  else {
                                        echo '<tr><td>'. $value->id .'</td><td>'. $value->title .'</td></tr>';
                                                    
                                        file_put_contents('load_aka.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->title.PHP_EOL, FILE_APPEND);
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            return ['status' => 0, 'data' => []];
                        }
                    } else {
                        echo '<tr><td>'. $value->id .'</td><td>'. $value->title .'</td></tr>';
                                                    
                        file_put_contents('load_aka.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->title.PHP_EOL, FILE_APPEND);
                    }
                }
            }
            echo '</table>';
            echo '<h1>Script run successfully !!<br> The TMDB movie id and alternative titles has been inserted and updated successfully into admin_videos table those have correct release date.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        } else {
            if (file_exists('load_aka.txt')) 
            {
                unlink('load_aka.txt');
            }
            echo '<h1>Script run successfully !!<br> You have no data found in admin_videos table.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        }

    }

    public function load_unique(Request $request)
    {
        $videos = AdminVideo::where('slug_updated', 0)->get();
        echo ' <link rel="stylesheet" href="'.url('/').'/admin-css/dist/css/custom.css">';
        //echo '<pre>';
        //print_r($videos);die;
        if (!empty($videos)) {
            foreach ($videos as $key => $value) {
                $title_slug = seoUrl($value->title);

                // check to see if any other slugs exist that are the same & count them
                $slug_count = AdminVideo::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

                $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;
                
                $value->unique_id = $unique_slug;
                // update admin video table by tmdb_id and alternative titles
                AdminVideo::where('id', $value->id)->update(['unique_id' => $value->unique_id, 'slug_updated' => 1]);
            }
            echo '<h1>Script run successfully !!<br> All unique id has been updated successfully.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        } else {
            echo '<h1>Script run successfully !!<br> No data found for update unique id.</h1><br><h4>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h4>';
            die;
        }
    }

    public function change_slug(Request $request)
    {
        $slug = $request->slug;
        $video_id = $request->video_id;

        if (!empty($slug)) {

            // check to see if any other slugs exist that are the same & count them
            $slug_count = AdminVideo::whereRaw("unique_id RLIKE '^{$slug}(-[0-9]+)?$'")->where('id', '!=', $video_id)->count();

            // if other slugs exist that are the same, append the count to the slug
            $unique_slug = ($slug_count) ? $slug . '-' . $slug_count : $slug;
            AdminVideo::where('id', '=', $video_id)->update(['unique_id' => $unique_slug]);
            echo $unique_slug;
            die;
        } else {
            echo $slug;
            die;
        }
    }

    public function change_banner_status(Request $request)
    {
        $video_id = $request->video_id;
        $is_banner = ($request->status == 0) ? 1 : 0;

        $banner_status = AdminVideo::where('id', '=', $video_id)->update(['is_banner' => $is_banner]);

        $html = '';
        if ($is_banner == 1) {
            $html = '<span onclick="change_banner_status(' . $video_id .
                ', 1)" class="label label-success btn">' . tr('yes') . '</span>';
        } else {
            $html = '<span onclick="change_banner_status(' . $video_id .
                ', 0)" class="label label-danger btn">' . tr('no') . '</span>';
        }

        return Response::json(['status' => true, 'data' => $html, 'video_id' => $video_id]);
    }

    public function get_votes(Request $request)
    {
        if ($request->ajax()) {
            $videos = AdminVideo::select('id', 'title', 'tmdb_movie_id', 'vote_count')->paginate(50);
            $reported = '';
            $last_page = $videos->lastPage();
            $current_page = $videos->currentPage();
            //echo '<pre>';
            //print_r($videos);die;
            if (!empty($videos)) {
                foreach ($videos as $key => $value) {
                    $tmdb_movie_id = $value->tmdb_movie_id;
                    if (!empty($tmdb_movie_id)) {
                        if ($value->vote_count <= 0) {
                            $api_key = getenv('TMDB_API_KEY');
                            $url = "https://api.themoviedb.org/3";
                            try {
                                $movie = file_get_contents("$url/movie/$tmdb_movie_id?api_key=$api_key&language=en-US"); // imdb
                                $data = json_decode($movie, true);

                                if (!empty($data)) {
                                    $vote_average = ($data['vote_average']) ? $data['vote_average'] : 0;
                                    $vote_count = ($data['vote_count']) ? $data['vote_count'] : 0;
                                    $vote_total = round($vote_average * $vote_count);

                                    if (($vote_total > 0) && ($vote_count > 0)) {
                                        $ratings = round($vote_total / $vote_count);
                                    } else {
                                        $ratings = 0;
                                    }

                                    //update admin video table by vote_average, vote_count, vote_total, and ratings
                                    AdminVideo::where('id', $value->id)
                                        ->where('tmdb_movie_id', $tmdb_movie_id)
                                        ->update([
                                            'vote_average' => $vote_average,
                                            'vote_count' => $vote_count,
                                            'vote_rating_total' => $vote_total,
                                            'ratings' => $ratings
                                        ]);

                                    if ($vote_count <= 0) {
                                        $reported .= '<tr><td>'. $value->id .'</td><td>'. $value->title .'</td></tr>';
                                        file_put_contents('get_votes.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->title.PHP_EOL, FILE_APPEND);
                                    }
                                }
                            } catch (Exception $e) {

                                return ['status' => 0, 'data' => []];
                            }
                        } else {
                            
                        }
                    } else {
                        AdminVideo::where('id', $value->id)
                            ->update([
                                'vote_average' => 0,
                                'vote_count' => 0,
                                'vote_rating_total' => 0,
                                'ratings' => 0
                            ]);
                    }
                }
                return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br>The listing of only tmdb movies which has not votes or 0 vote count.</h1><br><h6>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h6></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
            } else {
                return response()->json(['success' => false, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br> You have no data found in admin_videos table which has tmdb movie.</h4><br><h6>Please go back to the dashboard <a href="' . url('admin') . '">here..</a></h6></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
            }
        } else {
            if (file_exists('get_votes.txt')) 
            {
                unlink('get_votes.txt');
            }
            return view('admin.get_votes')->with('page', 'Get Votes');
        }

    }

    public function tag_search(Request $request)
    {
        //echo $request->term;die;
        $term = $request->term;

        $query = Tag::where('name', 'LIKE', '%' . $term . '%')->get();

        $tags = [];
        if (!empty($query)) {
            foreach ($query as $key => $value) {
                $tags[] = array("value" => $value->name, "label" => $value->name, "id" => $value->id);
            }
        }

        $response = json_encode($tags);
        echo $response;
        die;
        //print_r($query);die;
    }

    public function load_Cast(Request $request)
    {
        if ($request->ajax()) {
            $videos = AdminVideo::paginate(100);
            $reported = '';
            $last_page = $videos->lastPage();
            $current_page = $videos->currentPage();

            if (!empty($videos)) {
                foreach ($videos as $key => $value) {
                    
                    $tmdb_movie_id = $value->tmdb_movie_id;

                    if (!empty($tmdb_movie_id)) {

                        $api_key = getenv('TMDB_API_KEY');
                        $url = "https://api.themoviedb.org/3";

                        $movie = file_get_contents("$url/movie/$tmdb_movie_id?api_key=$api_key&append_to_response=videos,credits");


                        $data = json_decode($movie, true);
                        $casts = array_merge($data['credits']['cast'], $data['credits']['crew']);
                        $actors = json_decode($value->actors);
                        
                        if (!empty($casts)) {
                            foreach ($casts as $index => $row) {
                                $cast_name = $row['name'];
                                $cast_id = $row['id'];

                                $get_cast = DB::table('cast_crews')->where('name', $cast_name)->where('cast_id', 0)->count();
                                if ($get_cast > 0) {
                                    DB::table('cast_crews')->where('name', $cast_name)->where('cast_id', '=', 0)->update(['cast_id' => $cast_id]);
                                }
                                    
                            }
                        }
                        //echo '<pre>';
                        //print_r($casts);die;
                    }  else {
                        $reported .= '<tr><td>'. $value->id .'</td><td>'. $value->title .'</td></tr>';
            
                        file_put_contents('load_cast.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->title.PHP_EOL, FILE_APPEND);
                    }
                }
                    return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br>The cast id has been inserted and updated successfully into cast_crews table those videos have tmdb_movie_id.</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
                } else {
                    return response()->json(['success' => false, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br> You have no data found in admin_videos table which has tmdb_movie_id.</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
                }
        } else {
            if (file_exists('load_cast.txt')) 
            {
                unlink('load_cast.txt');
            }
            return view('admin.load_cast')->with('page', 'Load Cast');
        } 
    }

    public function load_cast_profile(Request $request)
    {
        if ($request->ajax()) {
            $casts = DB::table('cast_crews')->paginate(100);

            $reported = '';
            $last_page = $casts->lastPage();
            $current_page = $casts->currentPage();
            
            if (!empty($casts)) {
                foreach ($casts as $key => $value) {
                    if ($value->cast_id != 0) {
                        $person_id = $value->cast_id;
                        $cast_crew_id = $value->id;
                        $api_key = getenv('TMDB_API_KEY');
                        $url = "https://api.themoviedb.org/3";

                        $get_cast_profile = DB::table('cast_profiles')->where('cast_id', $person_id)->count();

                        if ($get_cast_profile <= 0) {
                            //$person = file_get_contents("$url/person/$person_id?api_key=$api_key&append_to_response=combined_credits");

                            $curl_handle=curl_init();
                            curl_setopt($curl_handle, CURLOPT_URL, "$url/person/$person_id?api_key=$api_key&append_to_response=combined_credits");
                            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
                            $person = curl_exec($curl_handle);
                            $httpcode = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
                            curl_close($curl_handle);

                            if ($httpcode != 200) {
                                $reported .= '<tr><td>'. $value->id .'</td><td>'. $value->name .'</td></tr>';
            
                                file_put_contents('load_cast_profile.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->name.PHP_EOL, FILE_APPEND);
                            } else {
                                $data = json_decode($person, true);

                                $profile_picture = $data['profile_path'];
                                if (!empty($profile_picture) || $profile_picture != NULL) {
                                    $profile_picture = 'https://image.tmdb.org/t/p/w300' . $profile_picture;
                                    $profile_img = Helper::copy_img_to_digitalocean($profile_picture, 'images', '');
                                } else {
                                    $profile_img = '';
                                }

                                if (!empty($data['combined_credits']['cast'])) {
                                    $cast_credits = json_encode($data['combined_credits']['cast']);
                                } else {
                                    $cast_credits = '';
                                }

                                if (!empty($data['combined_credits']['crew'])) {
                                    $crew_credits = json_encode($data['combined_credits']['crew']);
                                } else {
                                    $crew_credits = '';
                                }

                                $input = [
                                    'cast_id' => $person_id,
                                    'cast_crew_id' => $cast_crew_id,
                                    'name' => $data['name'],
                                    'biography' => nl2br($data['biography']),
                                    'birthday' => $data['birthday'],
                                    'deathday' => $data['deathday'],
                                    'gender' => $data['gender'],
                                    'known_for_department' => $data['known_for_department'],
                                    'imdb_id' => $data['imdb_id'],
                                    'place_of_birth' => $data['place_of_birth'],
                                    'profile_path' => $profile_img,
                                    'also_known_as' => json_encode($data['also_known_as']),
                                    'popularity' => $data['popularity'],
                                    'homepage' => $data['homepage'],
                                    'cast_credits' => $cast_credits,
                                    'crew_credits' => $crew_credits
                                ];

                                $cast_profile_id = DB::table('cast_profiles')->insertGetId($input);
                            }
                        }
                    } else {
                        $reported .= '<tr><td>'. $value->id .'</td><td>'. $value->name .'</td></tr>';
            
                        file_put_contents('load_cast_profile.txt', 'ID ==> '. $value->id .'  ||  Movie Name ==> '. $value->name.PHP_EOL, FILE_APPEND);
                    }
                }
                return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br>The cast profile data has been inserted successfully into cast_profiles table those have cast_id.</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
            } else {
                return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br> You have no data found in cast_crews table which has cast_id.</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page, 'reported' => $reported]);
            }
        } else {
            if (file_exists('load_cast_profile.txt')) 
            {
                unlink('load_cast_profile.txt');
            }
            return view('admin.load_cast_profile')->with('page', 'Load Cast Profile');
        }
    }

    public function save_people(Request $request)
    {
        //print_r($request->all());die;
        // Basic validations of video save form
        $rules = [
            'people' => 'required',
            'biography' => 'required',
            'gender' => 'required',
            'known_for_department' => 'required',

        ];

        if ($request->poster) {
            $rules['poster'] = 'required';
        }
        if ($request->default_image) {

            $rules['default_image'] = $request->id ? 'mimes:png,jpeg,jpg' : 'required|mimes:png,jpeg,jpg';
        }

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {

            $errors = implode(',', $validator->messages()->all());

            throw new Exception($errors);

        } else {
            try {
                $post = $request->all();

                if ($request->id) {
                    $person = People::find($request->id);


                } else {
                    $person = new People;
                }

                $person->cast_id = $request->person_id;
                $person->name = (!empty($request->people)) ? $request->people : '';
                $person->biography = (!empty($request->biography)) ? $request->biography : '';
                $person->birthday = (!empty($request->celeb_birthday)) ? $request->celeb_birthday : '';
                $person->deathday = (!empty($request->deathday)) ? $request->deathday : '';
                $person->homepage = (!empty($request->homepage)) ? $request->homepage : '';
                $person->place_of_birth = (!empty($request->celeb_place_of_birth)) ? $request->celeb_place_of_birth : '';
                $person->gender = (!empty($request->gender)) ? $request->gender : 0;
                $person->known_for_department = (!empty($request->known_for_department)) ? $request->known_for_department : '';
                $person->imdb_id = $request->imdb_id;
                $person->also_known_as = (!empty($request->celeb_known_as)) ? $request->celeb_known_as : '';
                $person->popularity = (!empty($request->popularity)) ? $request->popularity : '';
                $person->cast_credits = (!empty($request->cast_credits)) ? json_encode($request->cast_credits) : json_encode([]);
                $person->crew_credits = (!empty($request->crew_credits)) ? json_encode($request->crew_credits) : json_encode([]);

                //print_r($person);die;

                if ($request->hasFile('default_image') || $request->poster) {

                    if ($request->hasFile('default_image') && $request->id) {

                        Helper::delete_picture($person->default_image, "/uploads/images/");

                        Helper::delete_picture($person->default_image, "/uploads/images/385x225/");

                        Helper::spaces_delete_picture($person->default_image, "images");

                    }
                    if ($request->poster) {
                        $person->profile_path = Helper::copy_img_to_digitalocean($request->poster, 'images');
                    } else {

                        $person->profile_path = Helper::upload_file_to_digitalocean_spaces_image($request->file('default_image'), 'images');
                    }
                }

                $person->profile_path = (!empty($person->profile_path)) ? $person->profile_path : '';

                $person->save();
                return Response::json(['success' => true, 'message' => 'Person Added Successfully.']);
            } catch (\Exception $ex) {
                return Response::json(['success' => false, 'message' => $ex->getMessage()]);
            }
        }
    }

    public function load_cast_unique(Request $request)
    {
        if ($request->ajax()) {
            $videos = DB::table('cast_crews')->paginate(100);
            $reported = '';
            $last_page = $videos->lastPage();
            $current_page = $videos->currentPage();

            if (!empty($videos)) {
                foreach ($videos as $key => $value) {
                    if ($value->slug_updated == 0) {
                        // load unique slug for all cast
                        $title_slug = seoUrl($value->name);

                        // check to see if any other slugs exist that are the same & count them
                        $slug_count = CastCrew::whereRaw("unique_id RLIKE '^{$title_slug}(-[0-9]+)?$'")->count();

                        $unique_slug = ($slug_count) ? $title_slug . '-' . $slug_count : $title_slug;
                        
                        $value->name = $unique_slug;
                        // update cast_crews table by unique id
                        CastCrew::where('id', $value->id)->update(['unique_id' => $value->name, 'slug_updated' => 1]);
                        // load unique slug for all cast
                    }
                }
            
                return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br> All unique id has been updated successfully. !!</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page]);
            } else {
                return response()->json(['success' => true, 'msg' => '<div class="col-md-12"><h4>Script run successfully !!<br> No data found for update unique id.</h4></div>', 'current_page' => $current_page, 'last_page' => $last_page]);
            }
        }
        return view('admin.load-cast-unique')->with('page', 'Load Unique Cast');
    }
}
