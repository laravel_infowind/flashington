@extends('layouts.moderator.focused')

@section('title', tr('login'))

<style type="text/css">
body {
    background: #000 !important; 
}
</style>


@section('content')

    <div class="login-box-body" style="height:300px">

    @include('notification.notify')

        <form class="form-layout" role="form" method="POST" action="{{ url('/moderator/login') }}">
            {{ csrf_field() }}

            <div class="login-logo">
            </div>

            <p class="text-center mb30"></p>

            <div class="form-inputs">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control input-lg" required name="email" value="{{ old('email') }}" placeholder="{{tr('email')}}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <input type="password" class="form-control input-lg" required name="password" placeholder="{{tr('password')}}">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group">
                    <div class="form-checkbox">
                        <input type="checkbox" class="form-control-input" id="formCheck" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-control-label" for="formCheck">Remember me</label>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-md-4">
                    <button class="btn btn-success btn-block" type="submit">
                        <span><i class="fa fa-btn fa-sign-in"></i> {{tr('login')}}</span>
                    </button>
                </div>
                <div class="col-md-8">
                    @if (Route::has('moderator.password.request'))
                        <a class="btn btn-warning btn-block text-white" href="{{ route('moderator.password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>

            <input type="hidden" name="timezone" value="" id="userTimezone">
            <!-- <div class="form-group">
                    <a style="margin-left:100px" class="btn btn-link" href="{{ url('/moderator/password/reset') }}">Reset Password</a>
            </div> -->

        </form>

    </div>

@endsection

@section('scripts')

<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        // alert(dtz);
        $("#userTimezone").val(jstz.determine().name());
    });

</script>

@endsection