@extends('layouts.admin')

@section('title', tr('edit_user'))

@section('content-header', tr('edit_user'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.users')}}"><i class="fa fa-user"></i> {{tr('users')}}</a></li>
    <li class="active">{{tr('edit_user')}}</li> -->
@endsection

@section('content')

@include('notification.notify')


<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.user')}}" method="POST" enctype="multipart/form-data" role="form">
    <div class="row">
        <div class="col-md-8">
            <div class="boxx">
                <div class="box-bodyy">
                    <div class="card mb-20">
                        <div class="card-header">
                            <label for="title">{{tr('edit_user')}}</label>
                        </div>
                        <div class="card-body">
                            

                                <div class="form-body">
                                
                                    <input type="hidden" name="id" value="{{$user->id}}">

                                    <div class="form-group">
                                        <label for="username">*{{tr('username')}}</label>
                                        <input type="text" required pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" name="name" value="{{$user->name}}" class="form-control" id="username" placeholder="{{tr('name')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">*{{tr('email')}}</label>
                                        <input type="email" required  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$" class="form-control" value="{{$user->email}}" id="email" name="email" placeholder="{{tr('email')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="mobile">*{{tr('mobile')}}</label>
                                        <input type="text" required  minlength="4" maxlength="16" pattern="[0-9]{4,16}" name="mobile" value="{{$user->mobile}}" class="form-control" id="mobile" placeholder="{{tr('mobile')}}">
                                    </div>

                                </div>

                                <input type="hidden" name="timezone" value="" id="userTimezone">

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <div class="card mb-20">
                <div class="card-header">
                    <label for="">Publish</label>
                </div>
                <div class="card-body">
                    <div class="publish_card">
                        
                            {{-- <label>Modified on: <span class="published_on">immediately</span></label>
                            <a href="javascript:void(0)" class="edit-timestamp hide-if-no-js btn" role="button" style="display: inline-block;">
                                <span aria-hidden="true">Edit</span>
                            </a> --}}
                            {!! App\Helpers\Helper::published_block($user) !!}

                            @if($user->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($user->updated_at)). ' at '. date('H:i', strtotime($user->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        
                    </div>
                </div>
                <div class="card-footer">
                    <div class="publish_item">
                        <div class="box-tools pull-left" style="width: 30%">
                            <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                            @if(Setting::get('admin_delete_control'))
                                <a href="#" class="btn btn-primary" disabled>{{tr('submit')}}</a>
                            @else
                                <button type="submit" class="btn btn-primary">Update</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>
</div>


@endsection

@section('scripts')

<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });

</script>

@endsection