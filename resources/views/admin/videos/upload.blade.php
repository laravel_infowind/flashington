@extends('layouts.admin')
@if(!$model->id)
@section('title', tr('add_video'))
@else
@section('title', tr('edit_video'))
@endif
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/wizard.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.jbswizard.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet"
    href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/imgareaselect-default.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.awesome-cropper.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    
    .container-narrow {
        margin: 150px auto 50px auto;
        max-width: 728px;
    }

    canvas {
        width: 100%;
        height: auto;
    }

    span.select2-container {
        width: 100% !important;
    }

    .example-wizard.panel {
        border: 0px solid white;
        padding: 20px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        margin-bottom: 10px;
        width: 30%;
    }

    .select2-container .select2-search--inline {
        border: 1px solid #d2d6df !important;
        width: 30%;
    }
</style>
@endsection
@section('content')
@include('notification.notify')
@if(envfile('QUEUE_DRIVER') != 'redis')
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('warning_error_queue')}}
</div>
@endif
@if(checkSize())
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('max_upload_size')}} <b>{{ini_get('upload_max_filesize')}}</b>&nbsp;&amp;&nbsp;{{tr('post_max_size')}}
    <b>{{ini_get('post_max_size')}}</b>
</div>
@endif
@if(Setting::get('ffmpeg_installed') == FFMPEG_NOT_INSTALLED)
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('ffmpeg_warning_notes')}}
</div>
@endif
<div class="main-content">
    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display: none"
        id="error_popup">popup</button>
    <!-- popup -->
    <div class="modal fade error-popup" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="media">
                        <div class="media-left">
                            <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Information</h4>
                            <p id="error_messages_text"></p>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="example">
        <div class="example-wizard panel-primary">
            <div class="">
                <!-- Example Wizard START -->
                <div id="j-bs-wizard-example">
                    <form method="post" enctype="multipart/form-data" id="upload_video_form"
                        action="{{route('admin.videos.save')}}">
                        <input type="hidden" name="admin_video_id" id="admin_video_id" value="{{$model->id}}">
                        <input type="hidden" name="tmdb_details" id="tmdb_details" value=""/>
                        <!-- tab1 -->
                        <div class="row">
                            <div class="col-md-8">

                                <div class="boxx">
                                    <div class="box-bodyy">
                                        <div class="card mb-20">
                                            <div class="card-header">
                                                {{-- <label for="reviews">{{tr('msg')}} <span class="asterisk"><i
                                                        class="fa fa-asterisk"></i></span></label> --}}
                                                <label for="title">{{tr('title')}} <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span> </label>
                                            </div>
                                            <div class="card-body">
                                                @if(!$model->id)
                                                <div class="publish_card">
                                                    <div class="publish">
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" class="search-type" checked id="tmdb"
                                                                value="1" name="tmdb">
                                                            <label for="tmdb"> {{tr('tmdb')}} </label>
                                                        </div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" class="search-type form-control"
                                                                id="custom" value="0" name="tmdb">
                                                            <label for="custom"> {{tr('custom')}} </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif

                                                <div class="publish_card">
                                                    <input class="form-control" onkeyup = "canonical(this.value)"
                                                        style="background-color: #fff !important;" required type="text"
                                                        name="title" maxlength="100" maxlength="255"
                                                        value="{{$model->title}}" id='title' autocomplete="off">
                                                    @if(!$model->id)
                                                    <button
                                                        style="margin-top:5px; height: 34px; border-radius:0"
                                                        title="Click to search on {{tr('tmdb')}}" type="button"
                                                        class="btn btn-primary btn-sm" id="tmdb_btn">Search
                                                        {{tr('tmdb')}}</button>
                                                    @endif
                                                </div>

                                                @if($model->id)
                                                <input type="hidden" name="tmdb" value="{{ $model->tmdb }}"/>
                                                <input type="hidden" name="tmdb_movie_id" value="{{ $model->tmdb_movie_id }}"/>
                                                <div class="publish_card" style="margin-top: 10px;">
                                                    <div class="url-edit">
                                                        <strong>Permalink:</strong>
                                                        <span id="sample-permalink">
                                                            <a
                                                                href="{{ url('movies') }}/{{ $model->unique_id }}">{{ url('movies') }}/<span id="editable-post-name">{{ $model->unique_id }}</span></a>
                                                        </span>
                                                        <span id="edit-slug-buttons">
                                                            <button type="button" class="edit-slug btn">Edit</button>
                                                        </span>
                                                        <span class="editable-post-name-full">{{ $model->unique_id }}</span>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="description">{{tr('description')}} <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span></label>
                                            </div>
                                            <div class="card-body">
                                                <div class="detail_description_editor">
                                                    <textarea class="overview" id="description" name="description"
                                                        rows="4"
                                                        required="">{!! html_entity_decode($model->description) !!}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="details">{{tr('details')}} <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span></label>
                                            </div>
                                            <div class="card-body">
                                                <div class="detail_description_editor">
                                                    @if($model->details == "")
                                                    <textarea type="text" name="details" rows="4"
                                                        id='details'></textarea>
                                                    @else
                                                    <textarea type="text" name="details" rows="4"
                                                        id='details'>{!! html_entity_decode($model->details) !!}</textarea>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
                                        <script>
                                            CKEDITOR.replace( 'description' , {
                                                extraPlugins: 'colorbutton,colordialog',
                                                allowedContent: true
                                            });
                                            
                                            var details_editor = CKEDITOR.replace( 'details' , {
                                                extraPlugins: 'colorbutton,colordialog',
                                                allowedContent: true
                                            });

                                            details_editor.on( "pluginsLoaded", function( event ){
                                                details_editor.on( 'contentDom', function( evt ) {
                                                    var editable = details_editor.editable();                   
                                                    editable.attachListener( editable, 'keyup', function( e ) { 
                                                        // do something
                                                        var seo_description = editable.getData(evt).replace(/<[^>]*>|&nbsp;/g, '');
                                                        $('#page_description').val(seo_description);
                                                        $('input[name="meta[6]"]').val(seo_description);
                                                        $('input[name="meta[25]"]').val(seo_description);
                                                    });
                                                }); 
                                            });
                                        </script>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="alternative_titles">{{tr('alternative_titles')}}</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="detail_description_editor">
                                                    <textarea class="form-control alternative_titles"
                                                        name="alternative_titles" rows="4"><?php 
                                                        if (json_validate($model->alternative_titles)) {
                                                            $aka = '';
                                                            foreach(json_decode($model->alternative_titles) as $title) {
                                                                $aka .= $title->title. ', ';
                                                            }
                                                            echo trim($aka, ', ');
                                                        } else {
                                                            echo $model->alternative_titles;
                                                        }
                                                    ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display:none;" class="card mb-20 cast_crew_block">
                                            <div class="card-header">
                                                <label for="description">Cast and Crew</label>
                                            </div>
                                            <div class="card-body">
                                                <div style="display:none;" class="directors-list">
                                                    <label for="directors">Directors </label>
                                                    <select id="directors" name="directors[]"
                                                        class="select2 form-control" multiple>
                                                        @foreach($directors as $direc)
                                                        <option value="{{$direc->id}}" @if( $model->directors !="0" &&
                                                            is_array(json_decode($model->directors, true)) &&
                                                            in_array($direc->id, json_decode($model->directors, true)))
                                                            selected @endif>{{$direc->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div style="display:none;" class="writers-list">
                                                    <label for="writers">Writers </label>
                                                    <select id="writers" name="writers[]" class="select2 form-control"
                                                        multiple>
                                                        @foreach($writers as $writer)
                                                        <option value="{{$writer->id}}" @if( $model->writers !="0" &&
                                                            is_array(json_decode($model->writers, true)) &&
                                                            in_array($writer->id, json_decode($model->writers, true)))
                                                            selected @endif>{{$writer->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div style="display:none;" class="actors-list">
                                                    <label for="actors">Actors </label>
                                                    <select id="actors" name="actors[]" class="select2 form-control"
                                                        multiple>
                                                        @if(!empty($cast_crews))
                                                        @foreach($cast_crews as $act)
                                                        <option value="{{$act->id}}" @if( $model->actors !="") selected
                                                            @endif>{{$act->name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- tab4 -->
                                        @include('tab4')

                                        <!-- tab4 -->
                                        @seoForm($model)

                                        @if($model->id)
                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="comment">{{tr('comment')}}</label>
                                            </div>
                                            <div class="card-body">
                                                <div>
                                                    <label for="subject">Subject</label>
                                                    <div class="form-group"> 

                                                        <input class="form-control" id="subject" type="text" name="subject" value=""/>
                                                        
                                                    </div>
                                                </div>
                                                <div>
                                                    <label for="rating">{{tr('rating')}}</label>
                                                    <div class="form-group"> 

                                                        {{-- <select id="rating_val" name="rating" class="form-control">
                                                            <option value="">Select Rating</option>
                                                            @for($i=1; $i<=5; $i++)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endfor
                                                        </select> --}}
                                                        
                                                        
                                                        <div class="starRating">
                                                            <input id="comment_rating10" type="radio" name="rating" value="10"
                                                                @if($model->rating == 10) checked @endif>
                                                            <label for="comment_rating10">10</label>
                                                            <input id="comment_rating9" type="radio" name="rating" value="9"
                                                                @if($model->rating == 9) checked @endif>
                                                            <label for="comment_rating9">9</label>
                                                            <input id="comment_rating8" type="radio" name="rating" value="8"
                                                                @if($model->rating == 8) checked @endif>
                                                            <label for="comment_rating8">8</label>
                                                            <input id="comment_rating7" type="radio" name="rating" value="7"
                                                                @if($model->rating == 7) checked @endif>
                                                            <label for="comment_rating7">7</label>
                                                            <input id="comment_rating6" type="radio" name="rating" value="6"
                                                                @if($model->rating == 6) checked @endif>
                                                            <label for="comment_rating6">6</label>

                                                            <input id="comment_rating5" type="radio" name="rating" value="5"
                                                                @if($model->rating == 5) checked @endif>
                                                            <label for="comment_rating5">5</label>
                                                            <input id="comment_rating4" type="radio" name="rating" value="4"
                                                                @if($model->rating == 4) checked @endif>
                                                            <label for="comment_rating4">4</label>
                                                            <input id="comment_rating3" type="radio" name="rating" value="3"
                                                                @if($model->rating == 3) checked @endif>
                                                            <label for="comment_rating3">3</label>
                                                            <input id="comment_rating2" type="radio" name="rating" value="2"
                                                                @if($model->rating == 2) checked @endif>
                                                            <label for="comment_rating2">2</label>
                                                            <input id="comment_rating1" type="radio" name="rating" value="1"
                                                                @if($model->rating == 1) checked @endif>
                                                            <label for="comment_rating1">1</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mb-20">
                                                    <label for="directors">Add Comment </label>
                                                    <div class="detail_description_editor">
                                                        <textarea id="comment_val" class="form-control comment"
                                                            name="comment" rows="3">
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="mb-20">
                                                    <label for="spoilers">Does this review contain spoilers?
                                                    </label>
                                                    <div class="form-group"> 
                            
                                                        <input type="radio" value="1" name="spoilers"/> Yes
                                                        <input type="radio" value="0" name="spoilers"/> No
                                                    </div>
                                                </div>
                                                <div>
                                                    <button type="button" class="btn  btn-primary comment-btn">Add Comment </button>
                                                    {{-- <button type="button" type="reset" class="btn btn-danger">Cancel</button> --}}
                                                    
                                                </div>
                                                <div class="notice notice-error notice-alt inline hidden">
                                                    <div class="error-comment"></div>
                                                </div>
                                                <?php $comments = get_comments($model->id); ?>
                                                <div class="show_comments">
                                                    <table class="table table-bordered table-striped show_comments_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Authour</th>
                                                                <th>Subject</th>
                                                                <th>{{tr('rating')}}</th>
                                                                <th>{{tr('review')}}</th>
                                                        </thead>
                                                        <tbody class="load_comments">
                                                            @if(!empty($comments))
                                                                @foreach($comments as $value)
                                                                <tr class="comment-tr-{{ $value->id }}">
                                                                <td class="comment-first">{{ $value->user->name }}</td>
                                                                <td class="comment-subject">{{ $value->subject }}
                                                                <div class="row-actions">
                                                                    <span class="status-{{ $value->id }}">
                                                                        @if($value->status == 0)
                                                                        <a class="menuitem comment-status" tabindex="-1" href="javascript:void(0)" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('ratings_approve_confirmation')}}&quot)">{{tr('approve')}} </a>
                                                                        @else
                                                                        <a class="menuitem comment-status" href="javascript:void(0)" tabindex="-1" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('decline_ratings')}}&quot)">{{tr('decline')}}</a>
                                                                        @endif
                                                                    </span>
                                                                    |
                                                                    <span>
                                                                        @if(Setting::get('admin_delete_control'))
                                                                        <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                                                        @else
                                                                        <a role="menuitem" tabindex="-1" href="{{route('admin.ratings.edit' , array('id' => $value->id))}}">{{tr('edit')}}</a>
                                                                        @endif
                                                                    </span>
                                                                    |
                                                                    <span class="trash">
                                                                        @if(Setting::get('admin_delete_control'))
                                                                        <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                                                                        @else
                                                                        <a class="comment-del" role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('rating_delete_confirmation')}}&quot;);" href="javascript:void(0)" url="{{route('admin.ratings.delete' , array('id' => $value->id))}}">{{tr('delete')}}</a>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                                </td>
                                                                <td class="comment-rating">{{ $value->rating }}/10</td>
                                                                    <td class="comment-second">
                                                                    @php
                                                                        $review = (strlen($value->comment) > 50) ? substr($value->comment, 0, 50).'...' : $value->comment;
                                                                    @endphp
                                                                    {!! ($review) !!}
                                                                        
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                    
                                                    @if ($comments->count() > 10)
                                                        <p class="hide-if-no-js" id="show-comments"><a href="{{ url('admin/ratings/list') }}/{{ $value->admin_video_id }}">Show more comments</a> <span class="spinner"></span></p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            var comment_editor = CKEDITOR.replace( 'comment' , {
                                                extraPlugins: 'colorbutton,colordialog',
                                                allowedContent: true
                                            });

                                            var comment = '';
                                            
                                            // comment_editor.on( "pluginsLoaded", function( event ){
                                            //     comment_editor.on( 'contentDom', function( evt ) {
                                            //         var editable = comment_editor.editable();                   
                                            //         editable.attachListener( editable, 'keyup', function( e ) { 
                                            //             // do something
                                            //             comment = editable.getData(evt);
                                            //         });
                                            //     }); 
                                            // });
                                        </script>
                                        @endif

                                    </div>
                                </div>


                            </div>
                            <div class="col-md-4">

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label for="">Publish</label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_item">
                                                
                                            <div style="padding-left:0px; padding-right:0px; width: 100%; margin-left: 3px; margin-top: 9px;"
                                                class="progress col-sm-12 col-md-4 col-lg-4"
                                                style="margin-top: 8px;">
                                                <div class="bar"></div>
                                                <div class="percent">0%</div>
                                            </div>
                                            
                                        </div>
                                        <div class="publish_card">
                                            <div class="publish_item mb-20">
                                                <label>Moderator <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span></label>
                                                <select class="form-control" required name="moderator_id">
                                                    <option value=""></option>
                                                    @foreach($moderators as $mod)
                                                    <option {{ ($mod->id == $model->moderator_id ) ? 'selected' : '' }}
                                                        value="{{ $mod->id }}">{{ $mod->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                                @include('admin.videos._published')
                                            
                                            @if($model->id)
                                                <div class="publish_item mb-20">
                                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($model->updated_at)). ' at '. date('H:i', strtotime($model->updated_at)) }}</span></label>
                                                </div>
                                                <div class="publish_item mb-20">
                                                    <label>Created on: <span class="published_on">{{ date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) }}</span></label> 
                                                </div>
                                            
                                            @endif
                                            
                                            {{-- <div class="publish_item mb-20">
                                                <label for="reviews">{{tr('publish_type')}} <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span></label>
                                                <div class="publish">
                                                    <div class="radio radio-primary radio-inline">
                                                        <input type="radio" id="now" value="{{PUBLISH_NOW}}"
                                                            name="publish_type" onchange="checkPublishType(this.value)"
                                                            {{($model->id) ?  (($model->status) ? "checked" : '' ) : 'checked' }}>
                                                        <label for="now"> {{tr('now')}} </label>
                                                    </div>
                                                    <div class="radio radio-primary radio-inline">
                                                        <input type="radio" id="later" value="{{PUBLISH_LATER}}"
                                                            name="publish_type" onchange="checkPublishType(this.value)"
                                                            {{($model->id) ?  ((!$model->status) ? "checked" : '' ) : '' }}>
                                                        <label for="later"> {{tr('later')}} </label>
                                                    </div>
                                                </div>
                                                <div id="time_li" style="margin-top: 10px; display: none;">
                                                    
                                                    <input type="text" name="publish_time" id="datepicker"
                                                        placeholder="{{tr('publish_time')}}" class="form-control"
                                                        value="{{$model->publish_time}}" readonly>
                                                </div>
                                            </div> --}}
                                            {{-- <div class="publish_item mb-20">
                                                <div class="checkbox checkbox-inline checkbox-primary">
                                                    <input type="checkbox" value="1" name="send_notification"
                                                        id="send_notification">
                                                    <label for="send_notification">{{tr('send_notification')}}</label>
                                                </div>
                                            </div> --}}

                                            @if($model->id)
                                            <div class="publish_item mb-20">
                                                <label>Pay Per View: 
                                                <a role="menuitem" tabindex="-1" class="ppv-btn">  
                                                    @if($model->amount > 0)
                                                          <span class="label label-success">{{tr('yes')}}</span>
                                                    @else
                                                          <span class="label label-danger">{{tr('no')}}</span>
                                                    @endif
                                                </a>
                                                </label>
                                                <!--PPV block content-->
                                                <div class="modal-content ppv-block" style="display: none; border:1px solid rgba(0, 0, 0, .125)">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <input type="hidden" name="ppv_created_by" id="ppv_created_by" value="{{Auth::guard('admin')->user()->name}}">
                                                            <div class="col-lg-12">
                                                                <label class="text-uppercase">{{tr('type_of_user')}} *</label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="input-group">
                                                                    <input type="radio" name="type_of_user" value="{{NORMAL_USER}}" {{($model->type_of_user == 0 || $model->type_of_user == '') ? 'checked' : (($model->type_of_user == NORMAL_USER) ? 'checked' : '')}}>&nbsp;<label class="text-normal">{{tr('normal_user')}}</label>&nbsp;
                                                                    <input type="radio" name="type_of_user" value="{{PAID_USER}}" {{($model->type_of_user == PAID_USER) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('paid_user')}}</label>&nbsp;
                                                                    <input type="radio" name="type_of_user" value="{{BOTH_USERS}}" {{($model->type_of_user == BOTH_USERS) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('both_user')}}</label>
                                                                </div>
                                                                <!-- /input-group -->
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label class="text-uppercase">{{tr('type_of_subscription')}} *</label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="input-group">
                                                                    <input type="radio" name="type_of_subscription" value="{{ONE_TIME_PAYMENT}}" {{($model->type_of_subscription == 0 || $model->type_of_subscription == '') ? 'checked' : (($model->type_of_subscription == ONE_TIME_PAYMENT) ? 'checked' : '')}}>&nbsp;<label class="text-normal">{{tr('one_time_payment')}}</label>&nbsp;
                                                                    <input type="radio" name="type_of_subscription" value="{{RECURRING_PAYMENT}}" {{($model->type_of_subscription == RECURRING_PAYMENT) ? 'checked' : ''}}>&nbsp;<label class="text-normal">{{tr('recurring_payment')}}</label>
                                                                </div>
                                                                <!-- /input-group -->
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label class="text-uppercase">{{tr('amount')}} *</label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="number" required value="{{$model->amount}}" name="amount" class="form-control" id="amount" placeholder="{{tr('amount')}}" step="any">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="pull-left">
                                                            @if($model->amount > 0)
                                                            <a class="btn btn-danger" href="{{route('admin.remove_pay_per_view', $model->id)}}" onclick="return confirm(&quot;{{tr('remove_ppv_confirmation')}}&quot;);">
                                                            {{tr('remove_pay_per_view')}}
                                                            </a>
                                                            @endif
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <!--PPV block content-->
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="publish_item">
                                            <div class="box-tools pull-left" style="width: 30%">
                                                @if(Setting::get('admin_delete_control') == 1)
                                                <button style="width: 100%" disabled class="btn  btn-primary finish-btn"
                                                    type="submit" id="finish_video">

                                                    {{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                                @else
                                                <button class="btn  btn-primary finish-btn" type="submit"
                                                    id="finish_video">{{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label>Info <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_card mb-20">
                                            <label for="age">{{tr('age')}} <span class="asterisk1"><i
                                                        class="fa fa-asterisk1"></i></span></label>
                                            <input class="form-control" type="text" name="age" maxlength="3"
                                                value="{{$model->age}}" id='age' required="">
                                        </div>


                                        <input type="hidden" id="vote_average" name="vote_average" value="{{($model->vote_average) ? $model->vote_average : 0}}"/>
                                        <input type="hidden" id="vote_count" name="vote_count" value="{{($model->vote_count) ? $model->vote_count : 0}}"/>
                                        <input type="hidden" id="vote_total" name="vote_total" value="{{($model->vote_rating_total) ? $model->vote_rating_total : 0}}"/>

                                        
                                            <div class="publish_card mb-20">
                                                <label for="vote_average_input">Vote Average: <span class="vavg span-vote-value">{{($model->vote_average) ? $model->vote_average : 0}}</span></label>

                                                <span id="edit-vavg-buttons">
                                                    <button type="button" class="edit-vavg btn edit_v_btn">Edit</button>
                                                </span>
                                                <div class="wrap_vavg" style="display:none">
                                                    
                                                    <input class="form-control" type="text" name="vote_average_input"
                                                    value="{{($model->vote_average) ? $model->vote_average : 0}}" id='vote_average_input' required="">

                                                    <p>
                                                        <a href="javascript:void(0)" class="save-vavg hide-if-no-js button btn">OK</a>
                                                        <a href="javascript:void(0)" class="cancel-vavg hide-if-no-js button-cancel">Cancel</a>
                                                    </p>

                                                </div>
                                            </div>

                                            <div class="publish_card mb-20">
                                                <label for="vote_count_input">Vote Count: <span class="vote_count span-vote-value">{{($model->vote_count) ? $model->vote_count : 0}}</span></label>

                                                <span id="edit-vcount-buttons">
                                                    <button type="button" class="edit-vcount btn edit_v_btn">Edit</button>
                                                </span>
                                                <div class="wrap_vote_count" style="display:none">
                                                    
                                                    <input class="form-control" type="text" name="vote_count_input"
                                                    value="{{($model->vote_count) ? $model->vote_count : 0}}" id='vote_count_input' required="">

                                                    <p>
                                                        <a href="javascript:void(0)" class="save-vcount hide-if-no-js button btn">OK</a>
                                                        <a href="javascript:void(0)" class="cancel-vcount hide-if-no-js button-cancel">Cancel</a>
                                                    </p>

                                                </div>

                                            </div>

                                        
                                            <div class="publish_card mb-20">
                                                <label for="vote_rating_total">Vote Total:  <span class="vote_rating_total span-vote-value">{{($model->vote_rating_total) ? $model->vote_rating_total : 0}}</span></label>

                                                {{-- <span id="edit-vtotal-buttons">
                                                    <button type="button" class="edit-vtotal btn edit_v_btn">Edit</button>
                                                </span>
                                                <div class="wrap_vote_rating_total" style="display:none">
                                                    
                                                    <input class="form-control" type="text" name="vote_rating_total" value="{{($model->vote_rating_total) ? $model->vote_rating_total : 0}}" id='vote_rating_total' required="">

                                                    <p>
                                                        <a href="javascript:void(0)" class="save-vtotal hide-if-no-js button btn">OK</a>
                                                        <a href="javascript:void(0)" class="cancel-vtotal hide-if-no-js button-cancel">Cancel</a>
                                                    </p>

                                                </div> --}}
                                            </div>
                                        
                                    
                                        {{-- <div class="publish_card mb-20">
                                            <label for="reviews">{{tr('ratings')}}</label>
                                            <div class="starRating">
                                                <input id="rating10" type="radio" name="ratings" value="10"
                                                @if($model->ratings == 10) checked @endif>
                                                <label for="rating10">10</label>
                                                <input id="rating9" type="radio" name="ratings" value="9"
                                                @if($model->ratings == 9) checked @endif>
                                                <label for="rating9">9</label>
                                                <input id="rating8" type="radio" name="ratings" value="8"
                                                @if($model->ratings == 8) checked @endif>
                                                <label for="rating8">8</label>
                                                <input id="rating7" type="radio" name="ratings" value="7"
                                                @if($model->ratings == 7) checked @endif>
                                                <label for="rating7">7</label>
                                                <input id="rating6" type="radio" name="ratings" value="6"
                                                @if($model->ratings == 6) checked @endif>
                                                <label for="rating6">6</label>
                                                
                                                <input id="rating5" type="radio" name="ratings" value="5"
                                                    @if($model->ratings == 5) checked @endif>
                                                <label for="rating5">5</label>
                                                <input id="rating4" type="radio" name="ratings" value="4"
                                                    @if($model->ratings == 4) checked @endif>
                                                <label for="rating4">4</label>
                                                <input id="rating3" type="radio" name="ratings" value="3"
                                                    @if($model->ratings == 3) checked @endif>
                                                <label for="rating3">3</label>
                                                <input id="rating2" type="radio" name="ratings" value="2"
                                                    @if($model->ratings == 2) checked @endif>
                                                <label for="rating2">2</label>
                                                <input id="rating1" type="radio" name="ratings" value="1"
                                                    @if($model->ratings == 1) checked @endif>
                                                <label for="rating1">1</label>
                                            </div>
                                        </div> --}}
                                        <div class="publish_item mb-20">
                                            <label for="reviews">Release Date</label>
                                            <div class="publish">
                                                <input required="" type="date" class="form-control" id="release_date"
                                                    name="release_date"
                                                    value="{{ ( $model->release_date !='0000-00-00' && $model->release_date !='') ? $model->release_date:'' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label>{{tr('category')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_card">
                                            <select class="form-control" onchange="displaySubCategory(this.value);"
                                                name="category_id" id="category_id" required="">
                                                @foreach($categories as $category)
                                        <option cat_slug="{{ $category->category_slug }}" {{ ($category->id == $model->category_id) ? 'selected' : '' }}
                                                    value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label>Sub-Categories <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_card">
                                            <select multiple class="form-control select2" name="sub_category_id[]"
                                                id="sub_category">
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label>Tags</label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_card">
                                            <div class="tags_wrap">
                                                <?php $selected_tags = App\Helpers\Helper::getSelectedTags($model->id); ?>
                                            <textarea name="tags_input" rows="3" cols="20" class="the-tags hide form-control" id="tax-input-post_tag" aria-describedby="new-tag-post_tag-desc" spellcheck="false">{{ $selected_tags }}</textarea>

                                                <input class="form-control" type="text" name="tags[]" value="" id="tag_id"/> 
                                                <a href="javascript:void(0)" class="add-tag-btn btn" role="button"><span aria-hidden="true">Add</span></a>
                                            </div>
                                            <ul class="tagchecklist" role="list">
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label for="">Upload Image <span class="asterisk"><i
                                            class="fa fa-asterisk"></i></span></label>
                                    </div>
                                    <div class="card-body">
                                        <div class="image-upload">
                                            <div class="image-upload-item">
                                                <input type="hidden" name="poster" id="poster">
                                                <input type="file" id="default_image"
                                                    accept="image/png, image/jpeg, image/jpg" name="default_image"
                                                    placeholder="{{tr('default_image')}}" style="display:none"
                                                    onchange="loadFile(this,'default_img')">
                                                <input type="hidden" name="old_default_image" id="old_default_image"
                                                    value="{{ $model->default_image }}">
                                                <img src="{{$model->default_image ? $model->default_image : asset('images/default.png')}}"
                                                    onclick="$('#default_image').click();return false;" id="default_img"
                                                    style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">
                                                <p style="margin-bottom: 20px;" class="img-note">
                                                    {{tr('rectangle_image')}}</p>
                                            </div>
                                            
                                           @if($model->id)
                                            <div class="image-upload-item">
                                                <label style="margin-bottom: 5px;">Banner Image: </label>
                                                {{-- <select style="width: 100%; margin-left: 0;margin-bottom: 10px;"
                                                    name="image_type" class="form-control">
                                                    <option value="">Select Image Type</option>
                                                    <option @if($model->is_home_slider==1) selected @endif
                                                        value="is_home_slider">Home Slider</option>
                                                    <option @if($model->is_banner==1) selected @endif
                                                        value="is_banner"> Banner</option>
                                                </select> --}}
                                                
                                                {{-- <a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#banner_{{$model->id}}">

                                                    <span class="label label-success btn">{{tr('banner_videos')}}</span>

                                                    @if($model->is_banner == BANNER_VIDEO)

                                                    <span class="text-green"><i class="fa fa-check-circle"></i></span>

                                                    @endif

                                                </a> --}}
                                                <span class="banner_status_{{ $model->id }}">  
                                                    @if($model->is_banner == BANNER_VIDEO)
                                                          <span onclick="change_banner_status({{$model->id}}, 1)" class="label label-success btn">{{tr('yes')}}</span>
                                                      @else
                                                          <span onclick="change_banner_status({{$model->id}}, 0)" class="label label-danger btn">{{tr('no')}}</span>
                                                    @endif
                                                </span>
                                            </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="tmdb_movie_id" id="tmd-movie-id">
                        <input type="hidden" name="tmd_actors" id="tmd-actors">
                        <input type="hidden" name="tmd_directors" id="tmd-directors">
                        <input type="hidden" name="tmd_writers" id="tmd-writers">
                        <input type="hidden" name="tmd_genre" id="tmd-genre">
                        <input type="hidden" name="timezone" value="{{ Auth::guard('admin')->user()->timezone }}">


                    </form>
                </div>
                <!-- Example Wizard END -->
            </div>
        </div>
    </div>
</div>

<div class="overlay">
    <div id="loading-img"></div>
</div>

@if($model->id)
    @if ($model->compress_status >= OVERALL_COMPRESS_COMPLETED && $model->is_approved && $model->status)
    <!-- Modal -->
    <div id="banner_{{$model->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form action="{{route('admin.banner.set', ['admin_video_id'=>$model->id])}}" method="POST" enctype="multipart/form-data">
                <!-- Modal content-->
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{tr('set_banner_image')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-blue text-uppercase">
                                    {{-- {{ tr('banner_video_notes') }} --}}
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>{{$model->title}}</p>
                            </div>
                            <div class="col-lg-12">
                                <label>{{tr('banner_image_website')}} *</label>
                                <p class="help-block">{{tr('image_validate')}} {{tr('rectangle_image')}}</p>
                            </div>
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <input type="file" id="banner_image_file_{{$model->id}}" accept="image/png,image/jpeg" name="banner_image" placeholder="{{tr('banner_image')}}" style="display:none" onchange="loadFile(this,'banner_image_{{$model->id}}')" />
                                    <div>
                                        <img src="{{($model->is_banner) ? $model->banner_image : asset('images/320x150.png')}}" style="width:300px;height:150px;" 
                                            onclick="$('#banner_image_file_{{$model->id}}').click();return false;" id="banner_image_{{$model->id}}" style="cursor: pointer;" />
                                    </div>
                                </div>
                                <!-- /input-group -->
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        @if($model->is_banner == BANNER_VIDEO)
                        <div class="pull-left">
                            <?php $remove_banner_image_notes = tr('remove_banner_image_notes');?>
                            <a onclick="return confirm('{{$remove_banner_image_notes}}')" role="menuitem" tabindex="-1" href="{{route('admin.banner.remove',['admin_video_id'=>$model->id])}}" class="btn btn-danger">{{tr('remove_banner_image')}}</a>
                        </div>
                        @endif
                        <div class="pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{tr('close')}}</button>
                            <button type="submit" class="btn btn-primary" onclick="return confirm(&quot;{{tr('set_banner_image_confirmation')}}&quot;);">{{tr('submit')}}</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif
@endif

<!-- Modal -->
<div id="data-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Movies</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Movie Title</th>
                            <th>Release Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="movie-data">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="tmdb_btn_prev btn btn-primary" prev_page="0">Prev</button>
                <button class="tmdb_btn_next btn btn-primary" next_page="2">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script>
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('admin-css/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.jbswizard.min.js')}}"></script>
<script src="{{asset('assets/js/jbswizard.js')}}"></script>
<script src="{{asset('admin-css/plugins/jquery.form.js')}}"></script>
<script src="{{asset('assets/js/jquery.awesome-cropper.js')}}"></script>
<script src="{{asset('assets/js/jquery.imgareaselect.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tag_id" )
      // don't navigate away from the field on tab when selecting an item
        .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
            }
        })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( '{{ url("admin/tag/search")}}', {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });
} );

var current_tags = [];
var tag_text = '';
var tagchecklist = $('.tagchecklist');
var newTags = [];
var currentTags = [];
var tags, newtag, newtags, text;
var tagDelimiter = ',';
function array_unique_noempty( array ) {
    var out = [];

    // Trim the values and ensure they are unique.
    $.each( array, function( key, val ) {
        val = $.trim( val );

        if ( val && $.inArray( val, out ) === -1 ) {
            out.push( val );
        }
    } );

    return out;
};

$(document).ready(function(){
    current_tags = $('.the-tags').val().split( tagDelimiter );
    createTags(current_tags);
    
    // add tag
    $(document).on('click', '.add-tag-btn', function(){
        tags = $('.the-tags'),
		newtag = $('#tag_id');
        text = newtag.val();
        tagsval = tags.val();
        newtags = tagsval ? tagsval + tagDelimiter + text : text;

        newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
        tags.val( newtags );
        newtag.val('');

        current_tags = tags.val().split( tagDelimiter );
        createTags(current_tags);
    });

    $( '#tag_id').keypress( function( event ) {
        if ( 13 == event.which ) {
            event.preventDefault();
            event.stopPropagation();

            tags = $('.the-tags'),
            newtag = $('#tag_id');
            text = newtag.val();
            tagsval = tags.val();
            newtags = tagsval ? tagsval + tagDelimiter + text : text;

            newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
            tags.val( newtags );
            newtag.val('');

            current_tags = tags.val().split( tagDelimiter );

            createTags(current_tags);
        }
    })

    function createTags(currentTags)
    {
        tagchecklist.empty();
        $.each( currentTags, function( key, val ) {
            var listItem, xbutton;
            val = $.trim( val );

            if (val != '') {
                
                listItem = $( '<li />' ).text( val );
                var xbutton = $( '<button type="button" id="post_tag-check-num-' + key + '" class="ntdelbutton">' + '<span>×</span></button>'+ listItem.html() );
        
                listItem.prepend( '&nbsp;' ).prepend( xbutton );
                tagchecklist.append(listItem);

                //$('#tag_id').val('');
            }
        });
    }

    // remove tags
    $(document).on('click', '.ntdelbutton', function(){
        parseTags(this);
    });

    function parseTags(el) {
        var id = el.id,
            num = id.split('post_tag-check-num-')[1],
            current_tags = $('.the-tags').val().split( tagDelimiter ),
            new_tags = [];
            console.log(id);
        delete current_tags[num];

        // Sanitize the current tags and push them as if they're new tags.
        $.each( current_tags, function( key, val ) {
            val = $.trim( val );
            if ( val ) {
                new_tags.push( val );
            }
        });

        $('.the-tags').val( new_tags.join( tagDelimiter ));

        createTags(current_tags);
        return false;
    }

});
</script>
<script type="text/javascript">
var ratingc_cal = false;
function calculate_rating(value1, value2, key)
{
    var error = 0;
    var msg = '';
    var vote_count, vote_avg, vote_rating_total;
    
    var validate = validate_votes(value1, value2, key);
    
    if (key == 'vote_count') {
        vote_count = parseInt(value1);
        vote_avg = parseInt(value2);
    }

    if (key == 'vote_average') {
        vote_avg = value1;
        vote_count = value2;
    }

    if (key == 'vote_rating_total') {
        vote_rating_total = value1;
        vote_count = value2;
    }

    vote_rating_total = Math.round(vote_avg * vote_count);
    if (vote_rating_total != 'undefiend') {
        $('#vote_total').val(vote_rating_total); 
        $('.vote_rating_total').text(vote_rating_total); 
    }

    if (validate) {
        return true;
    } else {
        return false;
    }

}

function validate_votes(value, value2, key)
{
    var error = 0;
    var msg = '';
    var float= /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/;
    var vote_count, vote_avg, vote_rating_total;
    if(!$.isNumeric(value)) {
        error++;
        msg = 'Please enter numeric value';
        //$('#'+key).val(0);
        $('#'+key+'_input').val(0);
    } else if(value < 0 && value != '') {
        error++;
        msg = 'Please enter value greater or equal to 0';
        //$('#'+key).val(0);
        $('#'+key+'_input').val(0);
    } else if(key == 'vote_average' && (parseFloat(value) < 0 || parseFloat(value) > 10)) {
        error++;
        msg = 'The vote average value should be between 0 and 10.';
    }

    if (error > 0) {
        alert(msg);
        return false;
    } else {
        return true;
    }
}

$(document).ready(function(){

    // edit for vote average
    $(document).on('click', '.edit-vavg', function(){
        $(this).hide();
        $('.wrap_vavg').slideDown('fast');
    });

    $(document).on('click', '.cancel-vavg', function(){
        $('.wrap_vavg').slideUp('fast');
        $('.edit-vavg').show();
    });

    $(document).on('click', '.save-vavg', function(){
        var vote_avg = $('#vote_average_input').val();
        var vote_count = $('#vote_count_input').val();
        ratingc_cal = calculate_rating(vote_avg, vote_count, 'vote_average');
        if (ratingc_cal) {
            $('.vavg').text($('#vote_average_input').val());
            $('#vote_average').val($('#vote_average_input').val());
            $('.wrap_vavg').slideUp('fast');
            $('.edit-vavg').show();
        }
    });

    // edit for vote count
    $(document).on('click', '.edit-vcount', function(){
        $(this).hide();
        $('.wrap_vote_count').slideDown('fast');
    });

    $(document).on('click', '.cancel-vcount', function(){
        $('.wrap_vote_count').slideUp('fast');
        $('.edit-vcount').show();
    });

    $(document).on('click', '.save-vcount', function() {
        var vote_avg = $('#vote_average_input').val();
        var vote_count = $('#vote_count_input').val();
        ratingc_cal = calculate_rating(vote_count, vote_avg, 'vote_count');
        if (ratingc_cal) {
            $('.vote_count').text($('#vote_count_input').val());
            $('#vote_count').val($('#vote_count_input').val());
            $('.wrap_vote_count').slideUp('fast');
            $('.edit-vcount').show();
        }
    });

    // edit for vote rating total
    $(document).on('click', '.edit-vtotal', function(){
        $(this).hide();
        $('.wrap_vote_rating_total').slideDown('fast');
    });

    $(document).on('click', '.cancel-vtotal', function(){
        $('.wrap_vote_rating_total').slideUp('fast');
        $('.edit-vtotal').show();
    });

    $(document).on('click', '.save-vtotal', function(){
        $('.vote_rating_total').text($('#vote_rating_total').val());
        $('.wrap_vote_rating_total').slideUp('fast');
        $('.edit-vtotal').show();
    });

    $('.ppv-btn').click(function(){
        $('.ppv-block').slideToggle();
    });

    // actor list in ajax pagination
    var actorUrl = "{{url('admin/videos/actor-list')}}";
    $(".select2").select2();
    var CSRF_TOKEN = '<?php echo csrf_token(); ?>';
    $('#actors').select2({
    ajax: {
        url: actorUrl,
        type: "post",
        dataType: 'json',
        data: function (params) {
            return {
                _token: CSRF_TOKEN,
                search: params.term, // search term
                page: params.page || 1,
                type:'actor'
            };
        },
        processResults: function (response, params) {
            params.page = params.page || 1;
            return {
                results: response.data,
                pagination: {
                    more: parseInt((params.page * 100)) < parseInt(response.total_count)
                }
            };
        },
        cache: true
    }
});

var CSRF_TOKEN = '<?php echo csrf_token(); ?>';
$('#writers').select2({
ajax: {
    url: actorUrl,
    type: "post",
    dataType: 'json',
    data: function (params) {
        return {
            _token: CSRF_TOKEN,
            search: params.term, // search term
            page: params.page || 1,
            type:'writer'
        };
    },
    processResults: function (response, params) {
        params.page = params.page || 1;
        return {
            results: response.data,
            pagination: {
                more: parseInt((params.page * 100)) < parseInt(response.total_count)
            }
        };
    },
    cache: true
}
});

var CSRF_TOKEN = '<?php echo csrf_token(); ?>';
$('#directors').select2({
ajax: {
    url: actorUrl,
    type: "post",
    dataType: 'json',
    data: function (params) {
        return {
            _token: CSRF_TOKEN,
            search: params.term, // search term
            page: params.page || 1,
            type:'director'
        };
    },
    processResults: function (response, params) {
        params.page = params.page || 1;
        return {
            results: response.data,
            pagination: {
                more: parseInt((params.page * 100)) < parseInt(response.total_count)
            }
        };
    },
    cache: true
}
});

// edit permalink
    $(document).on('click', '.edit-slug', function(){
        var get_slug = $('#editable-post-name').text();
        var slug_link = '<?php echo url("movies"); ?>/';
        $('#edit-slug-buttons').html('<button type="button" class="save btn">OK</button> <button type="button" class="cancel-btn button-link">Cancel</button>');
        $('#sample-permalink').html(slug_link + '<span id="editable-post-name"><input type="text" id="new-post-slug" value="'+ get_slug +'" autocomplete="off"></span>');
    });

    $(document).on('click', '.cancel-btn', function(){
        $('#edit-slug-buttons').html('<button type="button" class="edit-slug btn">Edit</button>');
        var changed_slug = $('.editable-post-name-full').text();
        var slug_link = '<?php echo url("movies"); ?>/' + changed_slug;
        
        $('#sample-permalink').html('<a href="'+ slug_link +'"><?php echo url("movies");?>/<span id="editable-post-name">'+ changed_slug + '</span></a>');

    });

    $(document).on('click', '.save.btn', function(){
        var slug = $('#new-post-slug').val();
        $.ajax({
            type:'POST',
            url:'{{ url("admin/videos/change-slug")}}',
            data: {
                slug:slug,
                _token: '<?php echo csrf_token(); ?>',
                video_id: '<?php echo $model->id; ?>'
            },
            success: function(data){
                var slug_link = '<?php echo url("movies"); ?>';
                $('#edit-slug-buttons').html('<button type="button" class="edit-slug btn">Edit</button>');
                $('#sample-permalink').html('<a href="'+ slug_link +'"><?php echo url("movies");?>/<span id="editable-post-name">'+ data + '</span></a>');
                $('.editable-post-name-full').text(data);
            },
            error: function(){
                alert('Something wrong with request');
            }
        });
    });
    
    // search tags by autocomplete
    // $(document).on('keyup', '#tag_id', function(){
    //     var query = $('#tag_id').val();
    //     $.ajax({
    //         type:'POST',
    //         url:'{{ url("admin/tag/search")}}',
    //         data: {
    //             query:query,
    //             _token: '<?php echo csrf_token(); ?>'
    //         },
    //         success: function(data){
    //             var slug_link = '<?php echo url("movies"); ?>';
    //             $('#edit-slug-buttons').html('<button type="button" class="edit-slug btn">Edit</button>');
    //             $('#sample-permalink').html('<a href="'+ slug_link +'"><?php echo url("movies");?>/<span id="editable-post-name">'+ data + '</span></a>');
    //             $('.editable-post-name-full').text(data);
    //         },
    //         error: function(){
    //             alert('Something wrong with request');
    //         }
    //     });
    // });

    // add rating comment
    
    $(document).on('click', '.comment-btn', function(){
        var rating = $('input[name="rating"]:checked').val();
        var subject = $('#subject').val();
        var spoilers = 0;
        if($('input[name="spoilers"]').is(':checked')) {
            spoilers = 1;
        }
        console.log(spoilers);
        comment = comment_editor.getData();
        $.ajax({
            type:'POST',
            url:'{{ url("admin/videos/add-comment")}}',
            data: {
                spoilers: spoilers,
                subject: subject,
                rating: rating,
                comment: comment,
                _token: '<?php echo csrf_token(); ?>',
                video_id: '<?php echo $model->id; ?>'
            },
            success: function(data){
                if (!data.status) {
                    $('.error-comment').html('');
                    $('.error-comment').show();
                    $.each(data.errors, function(i, val){
                        console.log(val);
                        $('.error-comment').append('<p>'+ val +'</p>');
                        $('.notice-error').removeClass('hidden');
                    });
                } else {
                    $('.error-comment').hide();
                    $('.error-comment').html('');
                    $('.notice-error').addClass('hidden');
                    $('.load_comments').html(data.view);
                    $('#subject').val('');
                    $('input[name="spoilers"]').prop('checked', false);
                    $('input[name="rating"]').prop('checked', false);
                    comment_editor.setData('');
                }
            },
            error: function(){
                $('.error-comment').hide();
                alert('Something wrong with request');
            }
        });
    });
});
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        
        // onchange default image poster will be blank
        $('#default_image').on('change', function(){
            $('#poster').val('');
        });

        // get and set seo title
        $(document).on('keyup', '#title', function(){
            var heading = $(this).val();
            $('#page_title').val(heading);
            $('input[name="meta[5]"]').val(heading);
            $('input[name="meta[26]"]').val(heading);
        });
    });
    
    
    function makeSelected(){ // function is working in js/video-upload.js
        @if($model->sub_category_id )
        var e='{{ $model->sub_category_id}}';
        $('#sub_category').val(e.split(','));
        @endif
    }
</script>
<script type="text/javascript">
    var banner_image = "{{$model->is_banner}}";
    var cat_url = "{{ url('select/sub_category')}}";
    var step3 = "{{REQUEST_STEP_3}}";
    var sub_cat_url = "{{ url('select/genre')}}";
    var final = "{{REQUEST_STEP_FINAL}}";
    var video_id = "{{$model->id}}";
    var genreId = "{{$model->genre_id}}";
    var video_type = "{{$model->video_type}}";
    var view_video_url = "{{url('admin/view/video')}}?id=";
</script>
<script src="{{asset('assets/js/upload-video.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup', 'input[name="title"]', function(){
       
        });
    });

    function canonical(name){
        var canonical_url = name;
            $.ajax({
            type:'POST',
            url:'{{ url("admin/videos/seo-canonical")}}',
            data: {
                canonical_url:canonical_url,
                _token: '<?php echo csrf_token(); ?>',
                video_id: '<?php echo $model->id; ?>'
            },
            success: function(data){
                    $('#page_canonical_url').val(data);
                    $('input[name="meta[7]"]').val(data);
                },
                error: function(){
                    alert('Something wrong with request');
                }
                
        });
    }
        
        $('#datepicker').datetimepicker({
              minTime: "00:00:00",
              minDate: moment(),
              autoclose:true,
              format:'dd-mm-yyyy hh:ii',
          });
        $('.manual_video_upload').show();
        $('.others').hide();
        $("#video_upload").change(function()
        {
          $(".manual_video_upload").show();
          $(".others").hide();
        });
        $("#youtube").change(function()
        {
          $(".others").show();
          $(".manual_video_upload").hide();
        });
        $("#other_link").change(function()
        {
          $(".others").show();
          $(".manual_video_upload").hide();
        });
          function trailerVideoUpload(value, autoload_status) 
          {
          // On initialization, show others videos section
          if (value == 1) 
          {
            $("#upload_trailer").show();
            $("#other_trailer_video").val("");
            $(".other_trailer").hide();
          }
          if ((value == 2 || value == 3)) 
          {
            $("#other_trailer_video").val("");
            if(("{{$model->video_type_trailer}}" == value) || ("{{$model->video_type_trailer}}" == value)){
                $("#other_trailer_video").val("{{ change_web_url_to_cdn($model->trailer_video) }}");
              }
              $("#upload_trailer").hide();
              $(".other_trailer").show();
          }
          show_upload_location(); 
          }
          function show_upload_location()
          {
              // if($('#video_upload_link').is(':checked') || $('#upload_t_option').is(':checked')){
              //    $('#upload-location').show(); 
              // }else{
              //    $('#upload-location').hide(); 
              // }
              
          }
        
          function videoUploadType(value, autoload_status) 
          {
        
              // On initialization, show others videos section
        
              $(".others").show();
        
              $("#other_video").attr('required', true);
        
        
              if (autoload_status == 0) {
        
                  $("#video").attr('required', true);
        
        
              }
        
              $(".manual_video_upload").hide();
        
              $("#other_video").val("{{ change_web_url_to_cdn($model->video) }}");
        
              
              if (value == "{{VIDEO_TYPE_UPLOAD}}") {
        
                  $("#other_video").val("");
        
                  $(".manual_video_upload").show();
        
                  $(".others").hide();
        
                  $("#other_video").attr('required', false);
        
                  // If admin editing the video means remove the required fields for video & trailer video (If already in VIDEO_TYPE_UPLOAD)
        
                  @if($model->video_type == VIDEO_TYPE_UPLOAD)
        
                      $("#video").attr('required', false);
        
        
                  @endif
        
              }
        
              if ((value == "{{VIDEO_TYPE_OTHER}}" || value == "{{VIDEO_TYPE_YOUTUBE}}") && autoload_status == 0) {
        
                  $("#other_video").val("");
        
        
                  if(("{{$model->video_type}}" == value) || ("{{$model->video_type}}" == value)) {
        
                      $("#other_video").val("{{ change_web_url_to_cdn($model->video) }}");
        
                  }
        
                  $("#video").attr('required', false);
        
              }
        
            show_upload_location();
          
          }
        
            
        
        $(document).on('click','#tmdb_btn',function(){
        
        var title=$('#title').val();
        var category=$('#category_id option:selected').attr('cat_slug');
        if($('#tmdb').is(':checked') && title !=''){
        $('#tmdb_btn').text('Searching...');
        $.ajax({
        type:'POST',
        url:'{{ url("admin/videos/search")}}',
        data: {title:title, category:category, page:1},
        success: function(data){
            $('#tmdb_btn').text('Search TMDB');
            var results=data.results;
            var html='';
            if( results.length > 0){
                $.each(results, function(i, val){
                    console.log(title);
                    if (data.category == 'tv') {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        var first_air_date = val.first_air_date != null ? val.first_air_date : '';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.name+"</td>\
                            <td>"+first_air_date+"</td>\
                            <td><button type='button' id='"+val.id+"' onclick='canonical("+val.name+")' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    } else {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.title+"</td>\
                            <td>"+val.release_date+"</td>\
                            <td><button type='button' id='"+val.id+"' onclick='canonical(`"+val.title+"`)' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    }
                });
                $('#movie-data').html(html);
          }else{
             $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
          }
        
          
          $('#data-modal').modal('show');
        },
        error: function(){
          alert('Something wrong with request');
           $('#tmdb_btn').text('Search TMDB');
        },
        
        });
        }
        });
        
        $(document).on('click','.tmdb_btn_next',function(){
        var next_page = parseInt(jQuery(this).attr('next_page'));
        var category=$('#category_id option:selected').attr('cat_slug');
        var prev_page = parseInt(jQuery('.tmdb_btn_prev').attr('prev_page'));
        var isThis = this;
        console.log('step 1')
        var title=$('#title').val();
        if($('#tmdb').is(':checked') && title !=''){
        $('#tmdb_btn').text('Searching...');
        $.ajax({
        type:'POST',
        url:'{{ url("admin/videos/search")}}',
        data: {title:title, category:category, page:next_page},
        success: function(data){
           $('#tmdb_btn').text('Search TMDB');
          var results=data.results;
          var html='';
          if( results.length > 0){
                $.each(results, function(i, val){
                    if (data.category == 'tv') {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        var first_air_date = val.first_air_date != null ? val.first_air_date : '';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.name+"</td>\
                            <td>"+first_air_date+"</td>\
                            <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    } else {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.title+"</td>\
                            <td>"+val.release_date+"</td>\
                            <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    }
                });
                $('#movie-data').html(html);
          }else{
             $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
          }
          console.log('step 2')
          next_page = next_page + 1;
          prev_page = prev_page + 1;
          jQuery('.tmdb_btn_prev').attr('prev_page', prev_page)
          jQuery(isThis).attr('next_page', next_page)
          $('#data-modal').modal('show');
        },
        error: function(){
          alert('Something wrong with request');
           $('#tmdb_btn').text('Search TMDB');
        },
        
        });
        }
        });
        $(document).on('click','.tmdb_btn_prev',function(){
        var prev_page = parseInt(jQuery(this).attr('prev_page'));
        var next_page = parseInt(jQuery('.tmdb_btn_next').attr('next_page'));
        var isThis = this;
        var title=$('#title').val();
        var category=$('#category_id option:selected').attr('cat_slug');
        if($('#tmdb').is(':checked') && title !=''){
        $('#tmdb_btn').text('Searching...');
        $.ajax({
        type:'POST',
        url:'{{ url("admin/videos/search")}}',
        data: {title:title, category:category, page:prev_page},
        success: function(data){
           $('#tmdb_btn').text('Search TMDB');
          var results=data.results;
          var html='';
          if( results.length > 0){
              $.each(results, function(i, val){
                if (data.category == 'tv') {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        var first_air_date = val.first_air_date != null ? val.first_air_date : '';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.name+"</td>\
                            <td>"+first_air_date+"</td>\
                            <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    } else {
                        var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
                        html+=" <tr>\
                            <td>"+(i+1)+"</td>\
                            <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                            <td>"+val.title+"</td>\
                            <td>"+val.release_date+"</td>\
                            <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                            </tr>\
                            ";
                    }
              });
              $('#movie-data').html(html);
          }else{
             $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
          }
          console.log('step 2')
          if(prev_page == 0){
              prev_page = prev_page;
          }else{
              prev_page = prev_page - 1;
          }   
          if(next_page == 2){
              next_page = next_page;
          }else{
              next_page = next_page - 1;
          }        
          jQuery(isThis).attr('prev_page', prev_page)
          jQuery('.tmdb_btn_next').attr('next_page', next_page)
          $('#data-modal').modal('show');
        },
        error: function(){
          alert('Something wrong with request');
           $('#tmdb_btn').text('Search TMDB');
        },
        
        });
        }
        });
        
        
        $(document).on('change','.search-type',function(){
        if(parseInt(this.value)==1){
          $('#tmdb_btn').show();
           $('input#default_img').val('');
            $('img#default_img').attr('src','{{url("images/default.png")}}');
             @if(!$model->id)
            $('.cast_crew_block,.actors-list, .directors-list, .writers-list, .genre-list').hide();
               @endif
        }else{
          $('#duration,#description,#poster,input#default_img').val('');  
          $('#description').text('');
          $('.starRating input').prop('checked',false);
          $('#tmdb_btn').hide();
           $('img#default_img').attr('src','{{url("images/default.png")}}');
          $('.cast_crew_block, .actors-list, .directors-list, .writers-list, .genre-list').show();
          $('#youtube_t_option').prop('checked',false);
          // $('#other_trailer_video').val('').removeAttr('required');
          $('#upload_t_option').prop('checked',true).change();
        
        }
        
        });
        @if($model->id)
        $('.cast_crew_block, .actors-list, .directors-list, .writers-list, .genre-list').show();
        @endif
        
        $(document).on('click','.get-movie-data',function(){
        $(this).text('Wait...');
        var id=$(this).attr('id');
        var category=$('#category_id option:selected').attr('cat_slug');
        $.ajax({
        type:'POST',
        url:'{{ url("admin/videos/get_movie")}}',
        data: {id:id, category:category},
        context: this,
        success: function(data){
            console.log(data);
            $(this).text('Select');
            $('#tmdb_details').val(JSON.stringify(data));
         
            var rating=Math.round(data.vote_average);
            rating=parseInt(rating> 0 ? (rating):0);
            
            $('#vote_average, #vote_average_input').val(data.vote_average);
            $('.vavg').text(data.vote_average);
            $('#vote_count, #vote_count_input').val(data.vote_count);
            $('.vote_count').text(data.vote_count);

            var vote_total = Math.round(data.vote_average * data.vote_count);
            $('#vote_total').val(vote_total);
            $('.vote_rating_total').text(vote_total);
            
            if (data.category == 'tv') {
                $('#title').val(data.name);
                // get and set seo title
                $('#page_title').val(data.name);
                $('input[name="meta[5]"]').val(data.name);
                $('input[name="meta[26]"]').val(data.name);
            } else {
                $('#title').val(data.title);
                // get and set seo title
                $('#page_title').val(data.title);
                $('input[name="meta[5]"]').val(data.title);
                $('input[name="meta[26]"]').val(data.title);
            }
            
            $('#tmd-movie-id').val(data.id);


              var time=parseInt(data.runtime);
              if(time>0){
                $('#duration').val(convertMinsToHrsMins(time));  
              }else{
                  $('#duration').val('');  
              }
              if(data.rated){
              $('#age').val(data.rated);
              }else{
               $('#age').val('');   
              }  
        
            if (data.category == 'tv') {
                if(data.first_air_date) {
                    $('#release_date').val(data.first_air_date);
                }else {
                    $('#release_date').val('');   
                }
            } else {
                if(data.release_date) {
                    $('#release_date').val(data.release_date);
                }else {
                    $('#release_date').val('');   
                }
            }

            if (data.category == 'movie') {
                if (data.alternative_titles.titles.length) {
                    //$('#alternative_titles').val(JSON.stringify(data.alternative_titles.titles));
                    var aka = '';
                    $.each(data.alternative_titles.titles, function(i, titles) {
                        aka += titles.title + ', ';
                    });
                    aka = aka.replace(/,\s*$/, "");
                    $('.alternative_titles').val(aka);
                } else{
                //$('#alternative_titles').val('');   
                $('.alternative_titles').val('');   
                }
            }

            if (data.category == 'tv') {
                if (data.keywords.results.length){
                    var keywords = '';
                    $.each(data.keywords.results, function(i, keyword) {
                        keywords += keyword.name + ', ';
                    });
                    keywords = keywords.replace(/,\s*$/, "");
                    $('textarea[name="meta[30]"]').val(keywords);
                } else{
                $('textarea[name="meta[30]"]').val('');   
                }
            } else {
                if (data.keywords.keywords.length){
                    var keywords = '';
                    $.each(data.keywords.keywords, function(i, keyword) {
                        keywords += keyword.name + ', ';
                    });
                    keywords = keywords.replace(/,\s*$/, "");
                    $('textarea[name="meta[30]"]').val(keywords);
                } else{
                $('textarea[name="meta[30]"]').val('');   
                }
            }
        
               if(data.credits.cast.length > 0){
                  var actors='';
                  $.each(data.credits.cast,function(i,actor){
                      if(i==0) 
                      actors+=actor.id + ':' + actor.name;
                       else 
                      actors+='|'+ actor.id + ':' + actor.name;
                  });
              $('#tmd-actors').val(actors);
              }else{
               $('#tmd-actors').val('');   
              }
              if(data.credits.crew.length > 0){
                  var directors='',writers='';
                    $.each(data.credits.crew,function(i,crew){
                      if(crew.department=='Directing'){
                      if(directors=='')
                       directors+=crew.id + ':' + crew.name;
                       else
                       directors+='|'+ crew.id + ':' + crew.name;
                      }
                      
                    
                    if(crew.department=='Writing'){
                      if(writers=='')
                       writers+=crew.id + ':' + crew.name;
                     else
                       writers+='|'+ crew.id + ':' + crew.name;
                    }
                  });
                    $('#tmd-directors').val(directors);
                    $('#tmd-writers').val(writers);
              }else{
                    $('#tmd-directors,#tmd-writers').val('');
              }
                  if(data.genres.length > 0){
                  var genres='';
                    $.each(data.genres,function(i,genre){
                        $("#sub_category > option").each(function() { 
                            if (this.text == genre.name) {
                                $(this).attr('selected', 'selected');
                            }
                        });
                        if(genres=='')
                       genres+=genre.name;
                       else
                       genres+=','+genre.name;
                 
                  });
                    $('#tmd-genre').val(genres);

                    $("#sub_category").select2("destroy");
                    $("#sub_category").select2();
              }else{
                    $('#tmd-genre').val('');
              }
        
              $('#youtube_t_option').prop('checked',true).change();
              // $('#other_trailer_video').val('').attr('required','required');
              if(data.videos.results.length > 0){
                  var trailer_url = 'https://youtu.be/'+data.videos.results[0].key;
                  $('#other_trailer_video').val(trailer_url);
        
              } else {
                $('#upload_t_option').prop('checked',true).change();
              }
            
               $('textarea#description').val(data.overview);
              
               CKEDITOR.instances.description.setData(data.overview);

               $('textarea#details').val(data.overview);
              
               CKEDITOR.instances.details.setData(data.overview);

                // get and set seo description
                $('#page_description').val(data.overview);
                $('input[name="meta[6]"]').val(data.overview);
                $('input[name="meta[25]"]').val(data.overview);
              
              $('.starRating input[value="'+rating+'"]').attr('checked','checked');
              if(rating===0){
                  $('.starRating input').prop('checked',false);
              }
              var poster_url="https://image.tmdb.org/t/p/w300"+data.poster_path;
              if(data.poster_path){
                 $('#poster').val(poster_url);  
                 $('img#default_img').attr('src',poster_url);  
              }else{
                   $('img#default_img').attr('src','{{url("images/default.png")}}');
                   $('#poster').val('');  
              }
               
        
              $('#data-modal').modal('hide');
              
          
          
          
        },
        error: function(){
          alert('Something wrong with request');
            $(this).text('Select');
        },
        
        });
        
        });
        function  convertMinsToHrsMins(minutes) {
        var h = Math.floor(minutes / 60);
        var m = minutes % 60;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        return h + ':' + m+':00';
        }
         
          @if($model->id && !$model->status)
        
              checkPublishType("{{PUBLISH_LATER}}");
        
              $("#datepicker").val("{{$model->publish_time}}");
        
          @endif
        
          @if($model->id)
        
          videoUploadType("{{$model->video_type}}", 1);
          trailerVideoUpload("{{$model->video_type_trailer}}", 1);
          @endif
        
          function checkPublishType(val){
              $("#time_li").hide();
              //$("#datepicker").prop('required',false);
              $("#datepicker").val("");
              if(val == 2) {
                  $("#time_li").show();
              // $("#datepicker").prop('required',true);
              }
          }
        
</script>
<script>
    $('form#upload_video_form').submit(function () {
       window.onbeforeunload = null;
    });
    
    
    window.onbeforeunload = function() {
         return "Data will be lost if you leave the page, are you sure?";
    };
    
    loadGenre(genreId);
    
    window.setTimeout(function() {
    
        @if($model->genre_id) 
    
            $("#genre select").val("{{$model->genre_id}}");
    
        @endif
    
    }, 2000);
</script>

<script type="text/javascript">
$(document).ready(function(){
    // delete comment
    $(document).on('click', '.comment-del', function(){
        var url = $(this).attr('url');
        $.ajax({
            type:'GET',
            url:url,
            success: function(data){
                if (data.status) {
                    $('.comment-tr-' + data.data).remove();
                    $('.comment-count-'+ data.video_id).text(data.comment_count);
                } else {
                    alert('Something wrong with request');
                }
            },
            error: function(){
                alert('Something wrong with request');
            }
        });
    });

    // change status comment
    $(document).on('click', '.comment-status', function(){
        var url = $(this).attr('url');
        $.ajax({
            type:'GET',
            url:url,
            success: function(data){
                if (data.status) {
                    $('.status-' + data.data).html(data.html);
                } else {
                    alert('Something wrong with request');
                }
            },
            error: function(){
                alert('Something wrong with request');
            }
        });
    });
});
</script>

<script>
// published on timestamp
$(document).ready(function(){
    $(document).on('click', '.edit-timestamp', function(){
        $(this).hide();
        $('#timestampdiv').slideDown('fast');
    });

    $(document).on('click', '.cancel-timestamp', function(){
        $('#timestampdiv').slideUp('fast');
        $('.edit-timestamp').show();
    });

    $(document).on('click', '.save-timestamp', function(){
        var today_date = new Date();
        var aa = $('#aa').val(),
            mm = $('#mm').val(),
            jj = $('#jj').val(),
            hh = $('#hh').val(),
            mn = $('#mn').val(),
            newD = new Date(aa, mm - 1, jj, hh, mn);

        event.preventDefault();

        if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
            $('.timestamp-wrap').addClass('form-invalid');
            return;
        } else {
            $('.timestamp-wrap').removeClass('form-invalid');
        }

        var month = $('#mm option[value="' + mm + '"]').attr('data-text');
        var date = parseInt(jj, 10);
        var year = aa;
        var hour = ('00' + hh).slice(-2);
        var min = ('00' + mn).slice(-2);
        
        var date_string = month + ' '+ date + ',' + ' ' + year + ' at ' + hour + ':' + min; 
        var publish_time = year + '-'+ (1+ newD.getMonth()) + '-' + date +' ' + hour + ':' + min + ':00';
        if (today_date >= newD) {
            $('#publish_time').val(publish_time);
            $('#publish_type').val(1);
            $('#timestamp').html('<label>Published on:'+ ' <span class="published_on">' + date_string + '</span></label>');
        } else {
            $('#publish_time').val(publish_time);
            $('#publish_type').val(2);
            $('#timestamp').html('<label>Scheduled for:'+ ' <span class="published_on">' + date_string + '</span></label>');
        }
        
        // Move focus back to the Edit link.
        $('#timestampdiv').slideUp('fast');
        $('.edit-timestamp').show();
    });
});

function change_banner_status(video_id, status)
{
	$.ajax({
		type:'POST',
		url:'{{ url("admin/videos/change-banner-status")}}',
		data: {
			video_id: video_id,
			status: status,
			_token: '<?php echo csrf_token(); ?>'
		},
		success: function(res){
			if(res.data != '') {
				$('.banner_status_'+res.video_id).html(res.data);
			} else {
				alert('Something wrong with request');
			}
		},
		error: function(){
			alert('Something wrong with request');
		}
	});
}
</script>
@endsection
