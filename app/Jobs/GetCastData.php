<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\AdminVideo;
use App\User;
use App\Helpers\Helper;
use DB;
use CastCrew;

use Setting;
use SEO\Seo;
use App\CastProfile;

class GetCastData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $videoId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $video_details = AdminVideo::where('id', $this->videoId)->first();
        if (!empty($video_details)) {
            $actors = json_decode($video_details->actors, true);
            $directors = json_decode($video_details->directors, true);
            $writers = json_decode($video_details->writers, true);

            $actors = ($actors == 0) ? [] : $actors;
            $directors = ($directors == 0) ? [] : $directors;
            $writers = ($writers == 0) ? [] : $writers;

            $casts = array_merge($actors, $directors, $writers);

            if (!empty($casts)) {
                foreach ($casts as $key => $value) {
                    $cast_data = DB::table('cast_crews')->where('id', '=', $value)->first();
                    //file_put_contents(__DIR__.'/get_cast.txt', print_r($cast_data, true));die;
                    $person_id = $cast_data->cast_id;
                    $cast_crew_id = $cast_data->id;
                    $api_key = getenv('TMDB_API_KEY');
                    $url = "https://api.themoviedb.org/3";
                    
                    $get_cast_profile = DB::table('cast_profiles')->where('cast_crew_id', $cast_crew_id)->count();
                    
                    if ($get_cast_profile <= 0) {
                        try {
                            $person = file_get_contents("$url/person/$person_id?api_key=$api_key&append_to_response=combined_credits");
                            
                            $data = json_decode($person, true);
                            
                            $profile_picture = $data['profile_path'];
                            if (!empty($profile_picture) || $profile_picture != NULL) {
                                $profile_picture = 'https://image.tmdb.org/t/p/w300'.$profile_picture;
                                $profile_img = Helper::copy_img_to_digitalocean($profile_picture, 'images', '');
                            } else {
                                $profile_img = '';
                            }
                            $person1 =  new CastProfile();
                            // $input = [
                                $person1->cast_id =  $person_id;
                                $person1->cast_crew_id = $cast_crew_id;
                                $person1->name = $data['name'];
                                $person1->biography = nl2br($data['biography']);
                                $person1->birthday = $data['birthday'];
                                $person1->deathday = $data['deathday'];
                                $person1->gender = $data['gender'];
                                $person1->known_for_department = $data['known_for_department'];
                                $person1->imdb_id = $data['imdb_id'];
                                $person1->place_of_birth = $data['place_of_birth'];
                                $person1->profile_path = $profile_img;
                                $person1->also_known_as = json_encode($data['also_known_as']);
                                $person1->popularity = $data['popularity'];
                                $person1->homepage = $data['homepage'];
                                $person1->cast_credits = json_encode($data['combined_credits']['cast']);
                                $person1->crew_credits = json_encode($data['combined_credits']['crew']);
                            // ];
                            $person1->save();
                            
                            $person1->title = $person1->name;
                            $person1->description = $person1->biography;

                            // file_put_contents(__DIR__.'/get_cast_1234.txt', print_r($person1, true));die;

                            Seo::save($person1, url('person/'.$person1->cast_crew->unique_id), [
                                'title' => $person1->name,
                                'images' => [
                                    $person1->profile_path
                                ],
                            ],
                            [
                                'title'=>$person1->name,
                                'robot_index' => 'index',
                                'robot_follow' => 'follow',
                                'canonical_url' => url('person/'.$person1->cast_crew->unique_id),
                                'description' => $person1->biography
                            ]
                            );
                       
                        } catch (\Throwable $th) {
                            Log::info("Failed cast profile for cast_id : ". $person_id);
                        }
                    }
                    
                    // $person1 = CastProfile::where('cast_crew_id', $cast_crew_id)->first();
                 
                    // print_r($person1);die;
                    // file_put_contents(__DIR__.'/get_cast_1234.txt', print_r($person1, true));die;
                    
                }
            }
        }

        return true;
    }
}
