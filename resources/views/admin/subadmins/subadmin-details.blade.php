@extends('layouts.admin')

@section('title', tr('view_subadmin'))

@section('content-header', tr('view_subadmin'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.subadmins')}}"><i class="fa fa-users"></i> {{tr('subadmins')}}</a></li>
    <li class="active"><i class="fa fa-user"></i> {{tr('view_subadmins')}}</li>
@endsection

@section('content')

	<style type="text/css">
		.timeline::before {
		    content: '';
		    position: absolute;
		    top: 0;
		    bottom: 0;
		    width: 0;
		    background: #fff;
		    left: 0px;
		    margin: 0;
		    border-radius: 0px;
		}
	</style>

	<div class="row mond">

		<div class="col-md-6">

    		<div class="box box-widget widget-user-2">

            	<div class="widget-user-header bg-gray">
            		<div class="pull-left">
	              		<div class="widget-user-image">
	                		<img class="img-circle" src="@if($subadmin->picture) {{$subadmin->picture}} @else {{asset('admin-css/dist/img/avatar.png')}} @endif" alt="User Avatar">
	              		</div>

	              		<h3 class="widget-user-username">{{$subadmin->name}} </h3>
	      				<h5 class="widget-user-desc">{{tr('subadmin')}}</h5>
	      			</div>
	      			<div class='pull-right subadmin-detail'>
						<a href="{{route('admin.edit.subadmin',$subadmin->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{tr('edit')}}</a>
						<a class="btn btn-sm btn-danger"
							onclick="return confirm(&quot;{{tr('admin_subadmin_delete_confirmation' , $subadmin->name)}}&quot;);"
							href="{{route('admin.delete.subadmin', $subadmin->id)}}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>
						@if($subadmin->is_activated) 
							<a class="btn btn-sm btn-warning" role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('subadmin_decline_confirmation' , $subadmin->name)}}&quot;);" href="{{route('admin.subadmin.decline',$subadmin->id)}}"><i class="fa fa-close"></i> {{tr('decline')}}</a>
						@else
							<a class="btn btn-sm btn-success" role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('subadmin_approve_confirmation' , $subadmin->name)}}&quot;);" href="{{route('admin.subadmin.approve',$subadmin->id)}}"><i class="fa fa-check"></i> {{tr('approve')}}</a>
						@endif
	      			</div>
	      			<div class="clearfix"></div>
            	</div>
            	
            	<div class="box-footer no-padding">
              		<ul class="nav nav-stacked">
		                <li><a href="#">{{tr('username')}} <span class="pull-right">{{$subadmin->name}}</span></a></li>
		                <li><a href="#">{{tr('email')}} <span class="pull-right">{{$subadmin->email}}</span></a></li>
		                <li><a href="#">{{tr('mobile')}} <span class="pull-right">{{$subadmin->mobile}}</span></a></li>

		                <li>
		                	<a href="#">{{tr('status')}} 
		                		<span class="pull-right">
		                			@if($subadmin->is_activated) 
						      			<span class="label label-success">{{tr('approved')}}</span>
						       		@else 
						       			<span class="label label-warning">{{tr('pending')}}</span>
						       		@endif
		                		</span>
		                	</a>
		                </li>

		                
		                <li><a href="#">{{tr('timezone')}} <span class="pull-right">{{$subadmin->timezone}}</span></a></li>
		                <li><a href="#">{{tr('created_at')}} <span class="pull-right">{{convertTimeToUSERzone($subadmin->created_at, Auth::guard('admin')->user()->timezone, 'd-m-Y H:i a')}}</span></a></li>
		                <li><a href="#">{{tr('updated_at')}} <span class="pull-right">{{convertTimeToUSERzone($subadmin->updated_at, Auth::guard('admin')->user()->timezone, 'd-m-Y H:i a')}}</span></a></li>
		                	@if($subadmin->description)
		                <li><a href="#"><b>{{tr('description')}}</b> <br>{!! $subadmin->description !!}</a></li>
		                @endif
              		</ul>
            	</div>
          	
          	</div>

		</div>

    </div>

@endsection


