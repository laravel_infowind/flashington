(function($) {
    "use strict";
    $(window).on('load', function() {
        var loading = $('.loading');
        loading.delay(1000).fadeOut(1000);
    });

    $(document).ready(function() {
        if ($('header').hasClass('sticky')) {
            $("header.sticky").clone(true).addClass('cloned').insertAfter("header.sticky").removeClass('header-transparent text-white');
            var stickyHeader = document.querySelector(".sticky.cloned");
            var stickyHeaderHeight = $("header.sticky").height();
            var headroom = new Headroom(stickyHeader, {
                "offset": stickyHeaderHeight + 100,
                "tolerance": 0
            });
            $(window).bind("load resize", function(e) {
                var winWidth = $(window).width();
                if (winWidth > 1200) {
                    headroom.init();
                } else if (winWidth < 1200) {
                    headroom.destroy();
                }
            });
        }
        if ($("nav#main-mobile-nav").length > 0) {
            function mmenuInit() {
                if ($(window).width() <= '1024') {
                    $("#main-menu").clone().addClass("mmenu-init").prependTo("#main-mobile-nav").removeAttr('id').removeClass('navbar-nav mx-auto').find('li').removeAttr('class').find('a').removeAttr('class data-toggle aria-haspopup aria-expanded').siblings('ul.dropdown-menu').removeAttr('class');
                    var main_menu = $('nav#main-mobile-nav');
                    console.log('main_menu');
                    main_menu.mmenu({
                        extensions: ['fx-menu-zoom', 'position-right'],
                        counters: true
                    }, {
                        offCanvas: {
                            pageSelector: ".wrapper"
                        }
                    });
                    var menu_toggler = $("#mobile-nav-toggler");
                    var menu_API = main_menu.data("mmenu");
                    menu_toggler.on("click", function() {
                        menu_API.open();
                    });
                    menu_API.bind("open:finish", function() {
                        setTimeout(function() {
                            menu_toggler.addClass("is-active");
                        }, 100);
                    });
                    menu_API.bind("close:finish", function() {
                        setTimeout(function() {
                            menu_toggler.removeClass("is-active");
                        }, 100);
                    });
                }
            }
            mmenuInit();
            $(window).resize(function() {
                mmenuInit();
            });
        }
        var button = $('.btn-effect');
        $(button).on('click', function(e) {
            $('.ripple').remove();
            var posX = $(this).offset().left,
                posY = $(this).offset().top,
                buttonWidth = $(this).width(),
                buttonHeight = $(this).height();
            $(this).prepend("<span class='ripple'></span>");
            if (buttonWidth >= buttonHeight) {
                buttonHeight = buttonWidth;
            } else {
                buttonWidth = buttonHeight;
            }
            var x = e.pageX - posX - buttonWidth / 2;
            var y = e.pageY - posY - buttonHeight / 2;
            $('.ripple').css({
                width: buttonWidth,
                height: buttonHeight,
                top: y + 'px',
                left: x + 'px'
            }).addClass("rippleEffect");
        });
        var pxShow = 100;
        var scrollSpeed = 500;
        $(window).scroll(function() {
            if ($(window).scrollTop() >= pxShow) {
                $("#backtotop").addClass('visible');
            } else {
                $("#backtotop").removeClass('visible');
            }
        });
        $('#backtotop a').on('click', function() {
            $('html, body').animate({
                scrollTop: 0
            }, scrollSpeed);
            return false;
        });
        var search_btn = $('.extra-nav .toggle-search');
        var general_searchform = $('.general-search-wrapper');
        var search_close = $('.general-search-wrapper .toggle-search');
        search_btn.on('click', function() {
            general_searchform.addClass('open');
        });
        search_close.on('click', function() {
            general_searchform.removeClass('open');
        });
        if ($("#fullscreen-slider").length > 0) {
            var tpj = jQuery;
            var revapi24;
            tpj(document).ready(function() {
                if (tpj("#fullscreen-slider").revolution == undefined) {
                    revslider_showDoubleJqueryError("#fullscreen-slider");
                } else {
                    revapi24 = tpj("#fullscreen-slider").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "assets/revolution/js/",
                        sliderLayout: "fullscreen",
                        dottedOverlay: "none",
                        delay: 9000,
                        spinner: 'spinner2',
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "off",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "vertical",
                                drag_block_vertical: false
                            },
                            bullets: {
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 1024,
                                style: "uranus",
                                hide_onleave: false,
                                direction: "vertical",
                                h_align: "right",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0,
                                space: 20,
                                tmp: '<span class="tp-bullet-inner"></span>'
                            }
                        },
                        viewPort: {
                            enable: true,
                            outof: "wait",
                            visible_area: "80%"
                        },
                        responsiveLevels: [1200, 992, 768, 480],
                        visibilityLevels: [1200, 992, 768, 480],
                        gridwidth: [1200, 992, 768, 480],
                        lazyType: "single",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                        },
                        disableProgressBar: "on",
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 1,
                        stopAtSlide: 1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        };
        if ($("#fullwidth-slider").length > 0) {
            var tpj = jQuery;
            var revapi24;
            tpj(document).ready(function() {
                if (tpj("#fullwidth-slider").revolution == undefined) {
                    revslider_showDoubleJqueryError("#fullwidth-slider");
                } else {
                    revapi24 = tpj("#fullwidth-slider").show().revolution({
                        sliderType: "hero",
                        jsFileLocation: "assets/revolution/js/",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        spinner: 'off',
                        navigation: {},
                        viewPort: {
                            enable: true,
                            outof: "wait",
                            visible_area: "80%"
                        },
                        responsiveLevels: [1200, 992, 768, 480],
                        visibilityLevels: [1200, 992, 768, 480],
                        gridwidth: [1200, 992, 768, 480],
                        gridheight: [600, 600, 500, 400],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                        },
                        disableProgressBar: "on",
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 1,
                        stopAtSlide: 1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        };
        if ($("#hero-slider").length > 0) {
            var tpj = jQuery;
            var revapi24;
            tpj(document).ready(function() {
                if (tpj("#hero-slider").revolution == undefined) {
                    revslider_showDoubleJqueryError("#hero-slider");
                } else {
                    revapi24 = tpj("#hero-slider").show().revolution({
                        sliderType: "hero",
                        jsFileLocation: "assets/revolution/js/",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        spinner: 'off',
                        navigation: {},
                        viewPort: {
                            enable: true,
                            outof: "wait",
                            visible_area: "80%"
                        },
                        responsiveLevels: [1200, 992, 768, 480],
                        visibilityLevels: [1200, 992, 768, 480],
                        gridwidth: [1200, 992, 768, 480],
                        gridheight: [700, 700, 600, 500],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                        },
                        disableProgressBar: "on",
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 1,
                        stopAtSlide: 1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        };
        if ($("#hero-slider2").length > 0) {
            var tpj = jQuery;
            var revapi24;
            tpj(document).ready(function() {
                if (tpj("#hero-slider2").revolution == undefined) {
                    revslider_showDoubleJqueryError("#hero-slider2");
                } else {
                    revapi24 = tpj("#hero-slider2").show().revolution({
                        sliderType: "hero",
                        jsFileLocation: "assets/revolution/js/",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        spinner: 'off',
                        navigation: {},
                        viewPort: {
                            enable: true,
                            outof: "wait",
                            visible_area: "80%"
                        },
                        responsiveLevels: [1200, 992, 768, 480],
                        visibilityLevels: [1200, 992, 768, 480],
                        gridwidth: [1200, 992, 768, 480],
                        gridheight: [700, 700, 600, 500],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                        },
                        disableProgressBar: "on",
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 1,
                        stopAtSlide: 1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        };
        $(".signUpClick").on('click', function() {
            $('.signin-wrapper, .forgetpassword-wrapper').fadeOut(300);
            $('.signup-wrapper').delay(300).fadeIn();
        });
        $(".signInClick").on('click', function() {
            $('.forgetpassword-wrapper, .signup-wrapper').fadeOut(300);
            $('.signin-wrapper').delay(300).fadeIn();
        });
        $(".forgetPasswordClick").on('click', function() {
            $('.signup-wrapper, .signin-wrapper').fadeOut(300);
            $('.forgetpassword-wrapper').delay(300).fadeIn();
        });
        $(".cancelClick").on('click', function() {
            $('.forgetpassword-wrapper, .signup-wrapper').fadeOut(300);
            $('.signin-wrapper').delay(300).fadeIn();
        });
        $('body').magnificPopup({
            type: 'image',
            delegate: 'a.mfp-gallery',
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: true,
            removalDelay: 0,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true
            },
            callbacks: {
                buildControls: function() {
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
        });
        $('.mfp-image').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            image: {
                verticalFit: true
            }
        });
        $('.image-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            fixedContentPos: true,
            gallery: {
                enabled: true
            },
            removalDelay: 300,
            mainClass: 'mfp-fade',
            retina: {
                ratio: 1,
                replaceSrc: function(item, ratio) {
                    return item.src.replace(/\.\w+$/, function(m) {
                        return '@2x' + m;
                    });
                }
            }
        });
        $('.play-video, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });

        function owl_latest_movies() {
            var latest_movies = $('section.latest-movies .latest-movies-slider');
            latest_movies.owlCarousel({
                loop: true,
                margin: 15,
                autoplay: false,
                nav: false,
                dots: true,
                responsive: {
                    0: {
                        items: 1,
                        stagePadding: 10
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        }

        // var latest_tvshows = $('section.latest-tvshows .latest-tvshows-slider');
        // latest_tvshows.owlCarousel({
        //     loop: true,
        //     margin: 15,
        //     autoplay: false,
        //     nav: false,
        //     dots: true,
        //     responsive: {
        //         0: {
        //             items: 1,
        //             stagePadding: 10
        //         },
        //         600: {
        //             items: 3
        //         },
        //         1000: {
        //             items: 4
        //         }
        //     }
        // });
        // var latest_releases = $('.latest-releases-slider');
        // latest_releases.owlCarousel({
        //     loop: true,
        //     margin: 30,
        //     stagePadding: 20,
        //     autoplay: false,
        //     nav: false,
        //     dots: false,
        //     responsive: {
        //         0: {
        //             items: 1,
        //         },
        //         600: {
        //             items: 2,
        //         },
        //         1000: {
        //             items: 3,
        //         },
        //         1200: {
        //             items: 4,
        //         },
        //         1500: {
        //             items: 5,
        //         }
        //     }
        // });
        // var recommended = $('section.recommended-movies .recommended-slider');
        // recommended.owlCarousel({
        //     loop: true,
        //     margin: 15,
        //     autoplay: false,
        //     nav: false,
        //     dots: true,
        //     responsive: {
        //         0: {
        //             items: 1,
        //             stagePadding: 10
        //         },
        //         600: {
        //             items: 3
        //         },
        //         1000: {
        //             items: 4
        //         }
        //     }
        // });
        // var testimonials = $('.testimonial-slider');
        // testimonials.owlCarousel({
        //     loop: true,
        //     margin: 15,
        //     autoplay: true,
        //     nav: false,
        //     dots: true,
        //     items: 1,
        // });
        var counter_up = $('section.counter');
        counter_up.on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $(this).find('.counter-item').each(function() {
                    var $this = $(this);
                    $('.counter-item').countTo({
                        speed: 3000,
                        refreshInterval: 50
                    });
                });
                $(this).unbind('inview');
            }
        });
        if ("ontouchstart" in window) {
            document.documentElement.className = document.documentElement.className + " touch";
        }

        function parallaxBG() {
            $('.parallax').prepend('<div class="parallax-overlay"></div>');
            $(".parallax").each(function() {
                var attrImage = $(this).attr('data-background');
                var attrColor = $(this).attr('data-color');
                var attrOpacity = $(this).attr('data-color-opacity');
                if (attrImage !== undefined) {
                    $(this).css('background-image', 'url(' + attrImage + ')');
                }
                if (attrColor !== undefined) {
                    $(this).find(".parallax-overlay").css('background-color', '' + attrColor + '');
                }
                if (attrOpacity !== undefined) {
                    $(this).find(".parallax-overlay").css('opacity', '' + attrOpacity + '');
                }
            });
        }
        parallaxBG();
        if (!$("html").hasClass("touch")) {
            $(".parallax").css("background-attachment", "fixed");
        }

        function backgroundResize() {
            var windowH = $(window).height();
            $(".parallax").each(function(i) {
                var path = $(this);
                var contW = path.width();
                var contH = path.height();
                var imgW = path.attr("data-img-width");
                var imgH = path.attr("data-img-height");
                var ratio = imgW / imgH;
                var diff = 100;
                diff = diff ? diff : 0;
                var remainingH = 0;
                if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
                    remainingH = windowH - contH;
                }
                imgH = contH + remainingH + diff;
                imgW = imgH * ratio;
                if (contW > imgW) {
                    imgW = contW;
                    imgH = imgW / ratio;
                }
                path.data("resized-imgW", imgW);
                path.data("resized-imgH", imgH);
                path.css("background-size", imgW + "px " + imgH + "px");
            });
        }
        $(window).resize(backgroundResize);
        $(window).focus(backgroundResize);
        backgroundResize();

        function parallaxPosition(e) {
            var heightWindow = $(window).height();
            var topWindow = $(window).scrollTop();
            var bottomWindow = topWindow + heightWindow;
            var currentWindow = (topWindow + bottomWindow) / 2;
            $(".parallax").each(function(i) {
                var path = $(this);
                var height = path.height();
                var top = path.offset().top;
                var bottom = top + height;
                if (bottomWindow > top && topWindow < bottom) {
                    var imgH = path.data("resized-imgH");
                    var min = 0;
                    var max = -imgH + heightWindow;
                    var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow;
                    top = top - overflowH;
                    bottom = bottom + overflowH;
                    var value = 0;
                    if ($('.parallax').is(".titlebar")) {
                        value = min + (max - min) * (currentWindow - top) / (bottom - top) * 2;
                    } else {
                        value = min + (max - min) * (currentWindow - top) / (bottom - top);
                    }
                    var orizontalPosition = path.attr("data-oriz-pos");
                    orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
                    $(this).css("background-position", orizontalPosition + " " + value + "px");
                }
            });
        }
        if (!$("html").hasClass("touch")) {
            $(window).resize(parallaxPosition);
            $(window).scroll(parallaxPosition);
            parallaxPosition();
        }
        var mailChimp = $('.mailchimp');
        mailChimp.ajaxChimp({
            callback: mailchimpFunction,
            url: "your-mailchimp-url-here"
        });

        function mailchimpFunction(resp) {
            if (resp.result === 'success') {
                setTimeout(function() {
                    $("form.mailchimp label").removeClass();
                }, 5000);
            } else if (resp.result === 'error') {
                setTimeout(function() {
                    $("form.mailchimp label").removeClass();
                }, 5000);
            }
        }
        $('[data-toggle="tooltip"]').tooltip({
            animated: 'fade',
            container: 'body'
        });
        $("#countdown").countdown('2020/12/12', function(event) {
            var $this = $(this).html(event.strftime('' +
                '<div><span>%D</span> <i>Days</i></div>' +
                '<div><span>%H</span> <i>Hours</i></div> ' +
                '<div><span>%M</span> <i>Minutes</i></div> ' +
                '<div><span>%S</span> <i>Seconds</i></div>'));
        });
        $("#contact-form").on('submit', function(e) {
            e.preventDefault();
            var user_name = $("input[name=name]").val();
            var user_email = $("input[name=email]").val();
            var user_subject = $("input[name=subject]").val();
            var user_message = $("textarea[name=message]").val();
            var proceed = true;
            if (user_name === "") {
                $("input[name=name]").css('border-color', 'red');
                proceed = false;
            }
            if (user_email === "") {
                $("input[name=email]").css('border-color', 'red');
                proceed = false;
            }
            if (user_message === "") {
                $("textarea[name=message]").css('border-color', 'red');
                proceed = false;
            }
            if (user_subject === "") {
                $("input[name=subject]").css('border-color', 'red');
                proceed = false;
            }
            if (proceed) {
                var post_data;
                var output;
                post_data = {
                    'user_name': user_name,
                    'user_email': user_email,
                    'user_subject': user_subject,
                    'user_message': user_message
                };
                $.post('assets/php/email.php', post_data, function(response) {
                    if (response.type === 'error') {
                        $("#contact-result").addClass('error');
                        output = response.text;
                        setTimeout(function() {
                            $("#contact-result").removeClass();
                        }, 5000);
                    } else {
                        $("#contact-result").removeClass().addClass('valid');
                        output = response.text;
                        setTimeout(function() {
                            $("#contact-result").removeClass();
                        }, 5000);
                        $("input").val('');
                        $("textarea").val('');
                    }
                    $("#contact-result").html(output);
                }, 'json');
            }
        });
        $("input, textarea").on("change keyup", function(event) {
            $("input, textarea").css('border-color', '');
        });
        var isotope = $('.isotope');
        isotope.imagesLoaded(function() {
            isotope.isotope({
                itemSelector: '.element',
                transitionDuration: '0.8s',
            });
        });

        function loadjscssfile(filename, filetype) {
            if (filetype == "js") {
                var fileref = document.createElement('script')
                fileref.setAttribute("type", "text/javascript")
                fileref.setAttribute("src", filename)
            } else if (filetype == "css") {
                var fileref = document.createElement("link")
                fileref.setAttribute("rel", "stylesheet")
                fileref.setAttribute("type", "text/css")
                fileref.setAttribute("href", filename)
            }
            if (typeof fileref != "undefined")
                document.getElementsByTagName("head")[0].appendChild(fileref)
        }
        loadjscssfile("https://www.googletagmanager.com/gtag/js?id=UA-60264400-7", "js");
        loadjscssfile("assets/js/google-analytics.js", "js");

        var categories = [];
        var token = $('meta[name="csrf-token"]').attr('content')
        var site_url = $('meta[name="site-url"]').attr('content')
        var scroll_load_height = 0;

        function get_categories() {
            $("#video_categories li").each(function(i) {
                categories.push($(this).text());
            });
            $('#video_categories').remove();
        }
        get_categories();

        //       $(window).scroll(function(){  

        //           if ($(this).scrollTop() > scroll_load_height && categories.length != 0)
        //            {
        //     	load_category_video(categories[0])
        //     	categories.shift();
        //     	$('.ajax-loading').show();
        //     	scroll_load_height += 500;
        //     	// console.log(categories);
        //     }
        // });


        function load_category_video(id) {
            jQuery.ajax({
                type: 'post',
                url: site_url + '/render-movies',
                data: {
                    _token: token,
                    id: id
                },
                success: function(data) {
                    // console.log(data);
                    $('.ajax-loading').hide();
                    if (data == 'null') {
                        scroll_load_height -= 700;
                    } else {
                        $('section').last().after(data);
                        //owl_latest_movies();
                    }


                }
            });

        }

        // for search page loade more data
        function paginate_search(page) {
            var url = window.location.href;
            url = url.split("?")[1];
            var urls = url.split("&");
            var qry = '';
            for (var i = 0; i < urls.length; i++) {
                if (urls[i].split("=")[0] != 'page') {
                    qry += urls[i] + '&';
                }
            }

            jQuery.ajax({
                type: 'get',
                url: site_url + '/search?' + qry + 'page=' + page,
                data: {
                    _token: token
                },
                success: function(data) {
                    $('.ajax-loading').hide();
                    $('.search-result').append(data);

                }
            });
        }

        var last_page = jQuery('.all-pagination .page-item:nth-last-child(2) .page-link').text();
        last_page = parseInt(last_page);
        jQuery('.all-pagination').remove();
        var current_page = 2;

        if (current_page <= last_page) {
            $('#loadmore_data').show();
            //$("#loadmore_data").click(function()
            //{
            $(window).scroll(function() { //detect page scroll
                if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.97) { //if user scrolled from top to bottom of the page
                    //alert(current_page);  
                    if (current_page <= last_page) {
                        //console.log(current_page);
                        paginate_search(current_page);
                        $('.ajax-loading').show();
                        // scroll_load_height += 1500;
                        scroll_load_height += 500;
                        current_page += 1;
                    }
                    if (current_page > last_page) {
                        $('#loadmore_data').hide();
                    }
                }
            });
            //});
        }
        // for search page loade more data

        // for subcategory page loade more data
        function paginate_subcategory(page) {
            var url = window.location.href;
            url = url.split("/").pop();
            //url = url.split("?")[1];

            jQuery.ajax({
                type: 'get',
                url: site_url + '/sub-category/' + url + '?page=' + page,
                data: {
                    _token: token
                },
                success: function(data) {
                    $('.ajax-loading').hide();
                    //$('section').last().after(data);
                    $('.search-result').append(data);
                }
            });
        }

        if (window.location.href.indexOf('sub-category') > -1) {
            var last_page = jQuery('.all-pagination-subcategory .page-item:nth-last-child(2) .page-link').text();
            last_page = parseInt(last_page);
            jQuery('.all-pagination-subcategory').remove();
            var current_page = 2;

            if (current_page <= last_page) {
                $('#loadmore_data').show();
                //$("#loadmore_data").click(function()
                //{
                $(window).scroll(function() { //detect page scroll
                    //if ($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled from top to bottom of the page
                    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.97) {
                        if (current_page <= last_page) {
                            //console.log(current_page);
                            paginate_subcategory(current_page);
                            $('.ajax-loading').show();
                            // scroll_load_height += 1500;

                            scroll_load_height += 450;
                            current_page += 1;
                        }
                        if (current_page > last_page) {
                            $('#loadmore_data').hide();
                        }
                    }
                });
                //});
            }
        }
        // for subcategory page loade more data

        // for recently added page loade more data
        function paginate_recently(page) {

            var url = window.location.href;
            url = url.split("?")[1];

            jQuery.ajax({
                type: 'get',
                url: site_url + '/recently-added?page=' + page,
                data: {
                    _token: token
                },
                success: function(data) {
                    $('.ajax-loading').hide();
                    //$('section').last().after(data);
                    $('.recently-wrap').append(data);
                }
            });
        }

        if (window.location.href.indexOf('recently-added') > -1) {
            var last_page = jQuery('.recently-all-pagination .page-item:nth-last-child(2) .page-link').text();
            last_page = parseInt(last_page);
            jQuery('.recently-all-pagination').remove();
            var current_page = 2;

            if (current_page <= 8) {
                $('#loadmore_data').show();
                //$("#loadmore_data").click(function()
                //{
                $(window).scroll(function() { //detect page scroll
                    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.97) { //if user scrolled from top to bottom of the page
                        //alert(current_page);  
                        if (current_page <= 8) {
                            //console.log(current_page);
                            paginate_recently(current_page);
                            $('.ajax-loading').show();
                            // scroll_load_height += 1500;
                            scroll_load_height += 500;
                            current_page += 1;
                        }
                        if (current_page > 8) {
                            $('#loadmore_data').hide();
                        }
                    }
                });
                //});
            }
        }
        // for recently added page loade more data

        // for channel detail page loade more data
        function paginate_channel_detail(page) {
            var url = window.location.href;
            var moderator_slug = url.split("/").pop();

            jQuery.ajax({
                type: 'get',
                url: site_url + '/channels/' + moderator_slug + '?page=' + page,
                success: function(data) {
                    $('.ajax-loading').hide();
                    $('.channel_detail').append(data);
                }
            });
        }

        if (window.location.href.indexOf('channels/') > -1) {
            var last_page = jQuery('.channel-detail-all-pagination .page-item:nth-last-child(2) .page-link').text();
            last_page = parseInt(last_page);
            jQuery('.channel-detail-all-pagination').remove();
            var current_page = 2;

            if (current_page <= last_page) {
                $('#loadmore_data').show();
                $(window).scroll(function() { //detect page scroll
                    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.97) { //if user scrolled from top to bottom of the page
                        if (current_page <= last_page) {
                            //console.log(current_page);
                            paginate_channel_detail(current_page);
                            $('.ajax-loading').show();
                            // scroll_load_height += 1500;
                            scroll_load_height += 500;
                            current_page += 1;
                        }
                        if (current_page > last_page) {
                            $('#loadmore_data').hide();
                        }
                    }
                });
            }
        }
        // for channel detail page loade more data

        // for reviews page loade more data
        function paginate_reviews(page) {
            var url = window.location.href;
            if (window.location.href.indexOf('?') > -1) {
                url = url + '&page=' + page;
            } else {
                url = url + '?page=' + page;
            }

            jQuery.ajax({
                type: 'get',
                url: url,
                data: {
                    _token: token
                },
                success: function(data) {
                    $('.ajax-loading').hide();
                    //$('section').last().after(data);
                    $('.append-reviews').append(data);
                }
            });
        }

        if (window.location.href.indexOf('reviews') > -1) {
            var last_page = jQuery('.all-pagination-reviews .page-item:nth-last-child(2) .page-link').text();
            last_page = parseInt(last_page);
            jQuery('.all-pagination-reviews').remove();
            var current_page = 2;

            if (current_page <= last_page) {
                $('#loadmore_data').show();
                //$("#loadmore_data").click(function()
                //{
                $(window).scroll(function() { //detect page scroll
                    //if user scrolled from top to bottom of the page
                    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.97) {
                        if (current_page <= last_page) {
                            //console.log(current_page);
                            paginate_reviews(current_page);
                            $('.ajax-loading').show();
                            // scroll_load_height += 1500;
                            scroll_load_height += 500;
                            current_page += 1;
                        }
                        if (current_page > last_page) {
                            $('#loadmore_data').hide();
                        }
                    }
                });
            }
        }
        // for all reviews page loade more data

        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('header.header.header-fixed.header-transparent.text-white').addClass('scrolled');
            } else {
                $('header.header.header-fixed.header-transparent.text-white').removeClass('scrolled');
            }
        });

    });

    //  Model Jquery
    $("#idetify").click(function() {
        console.log('test');
    });
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

    $("button[data-modal-id]").click(function(e) {
        e.preventDefault();
        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $("#" + modalBox).fadeIn($(this).data());
    });

    $(".js-modal-close, .modal-overlay").click(function() {
        $(".modal-box, .modal-overlay").fadeOut(500, function() {
            $(".modal-overlay").remove();
        });
    });

    $(window).resize(function() {
        $(".modal-box").css({
            top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });

    $(window).resize();

    $(".for_signup").click(function() {
        $("#sign_in").hide();
        $("#forget_password").hide();
        $("#sign_up").show();
        $('body').addClass('popup-open');
    });
    $(".for_password").click(function() {
        $("#sign_in").hide();
        $("#sign_up").hide();
        $("#forget_password").show();
        $('body').addClass('popup-open');
    });
    $(".for_signin").click(function() {
        $("#forget_password").hide();
        $("#sign_up").hide();
        $("#sign_in").show();
        $('body').addClass('popup-open');
    });

    $('#myModal .close').click(function() {
        $('body').removeClass('popup-open');
    });

    $('#gotobottom').on("mouseover", function() {
        $('html, body').animate({
            scrollTop: 625
        }, 'slow');
        return false;
    });

    jQuery('#slider .episode-img').on("mouseover", function(e) {
        e.preventDefault();
        $('#slider .epi-play-icon').css({
            "opacity": "0.6"
        });
        $('#slider #trailersmore .episode-list-box .epi-play-icon').css({
            "display": "inline-block"
        });
        return true;
    });
    jQuery('#slider .episode-img').on("mouseleave", function(e) {
        e.preventDefault();
        $('#slider .epi-play-icon').css({
            "opacity": "1"
        });
        $('#slider #trailersmore .episode-list-box .epi-play-icon').css({
            "display": "none"
        });
        return false;
    });
    jQuery('#slider #morelike .episode-list-box .episode-img').on("mouseover", function(e) {
        e.preventDefault();
        var imgid = $(this).attr("id");
        console.log(imgid);
        if (imgid) {
            $('#slider #morelike .episode-list-box #' + imgid + '.video_morelikethis').show();
            $('#slider #morelike .episode-list-box #' + imgid + '.video_morelikethis').css({
                "display": "inline-block"
            });
            return true;
        }

    });
    jQuery('#slider #morelike .episode-list-box .episode-img').on("mouseleave", function(e) {
        var imgid = $(this).attr("id");
        var video_id = $("#slider .rev-slider-wrapper #morelike .episode-list-box #video_morelikethis").attr("href");
        e.preventDefault();
        if (imgid) {
            $('#slider #morelike .episode-list-box #' + imgid + '.video_morelikethis').hide();
            return true;
        }
    });

    // Home Page Hover on image
    //      if($(window).width() < 767)
    //     {
    //     jQuery('.latest-movies .poster_div').on("mouseover",function(e) 
    //     {
    //         console.log('render');
    //        e.preventDefault();
    //        var imgid = $(this).attr("hover-id");
    //        console.log(imgid);
    //        if(imgid){

    //     $('.latest-movies .poster_div .movie-box-1 #'+imgid).css({"height":"300px","padding":"0px","transition":"width 2s"});
    //     $('.latest-movies .poster_div .movie-box-1 .poster #'+imgid).css({"position": "static","height":"300px","transition":"width 2s"});
    //         return true;
    //        }

    //     });
    //     jQuery('.latest-movies .poster_div').on("mouseleave",function(e) 
    //     {
    //         var imgid = $(this).attr("hover-id");
    //          e.preventDefault();
    //         if (imgid)
    //         {
    //     $('.latest-movies .poster_div .movie-box-1 #'+imgid).css({"position": "relative","padding":"28.125% 0","overflow":"hidden","width":"100%"});
    //     $('.latest-movies .poster_div .movie-box-1 .poster #'+imgid).css({"position":"absolute","top":"0","bottom":"0","left":"0","right":"0","width":"100%","padding":"0"});
    //             return true;
    //         }
    //     });
    // }

    // Hover On image

    jQuery('#slider_playiconid').on("mouseover", function(e) {
        e.preventDefault();

        $(this).css({
            "color": "red",
            "width": "105px",
            "height": "105px"
        });
        $('#slider .rev-slider-wrapper #morelike .episode-list-box #' + imgid + '.video_morelikethis').css({
            "display": "inline-block"
        });
        return true;

    });
    jQuery('#slider_playiconid').on("mouseleave", function(e) {
        $(this).css({
            "color": "white",
            "width": "95px",
            "height": "95px"
        });
        return true;
    });
    $('#slider #morelike .episode-list-box .video_morelikethis').on("click", function() {
        var video_id = $(this).attr("id");
        $("#collapsibleNavbar .navbar-nav .nav-item .signin_click").click();
        $("#myModal #btn_submit #get_videoid").val(video_id);
        return false;
    });
    $('#slider .otmd ul li .overview_tab').on("click", function() {
        var video_id = $(this).attr("id").replace('overview_tab_', '');
        $('#slider .otmd ul li #overview_tab_' + video_id).css({
            "border-bottom": "3px solid #E50914"
        });
        $('#slider .otmd ul li #morelike_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #details_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #trailersmore_tab_' + video_id).css({
            "border-bottom": "none"
        });
    });
    $('#slider .otmd ul li .morelike_tab').on("click", function() {
        var video_id = $(this).attr("id").replace('morelike_tab_', '');
        $('#slider .otmd ul li #overview_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #morelike_tab_' + video_id).css({
            "border-bottom": "3px solid #E50914"
        });
        $('#slider .otmd ul li #details_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #trailersmore_tab_' + video_id).css({
            "border-bottom": "none"
        });
    });
    $('#slider .otmd ul li .details_tab').on("click", function() {
        var video_id = $(this).attr("id").replace('details_tab_', '');
        $('#slider .otmd ul li #overview_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #details_tab_' + video_id).css({
            "border-bottom": "3px solid #E50914"
        });
        $('#slider .otmd ul li #morelike_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #trailersmore_tab_' + video_id).css({
            "border-bottom": "none"
        });
    });
    $('#slider .otmd ul li .trailersmore_tab').on("click", function() {
        var video_id = $(this).attr("id").replace('trailersmore_tab_', '');
        $('#slider .otmd ul li #overview_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #trailersmore_tab_' + video_id).css({
            "border-bottom": "3px solid #E50914"
        });
        $('#slider .otmd ul li #morelike_tab_' + video_id).css({
            "border-bottom": "none"
        });
        $('#slider .otmd ul li #details_tab_' + video_id).css({
            "border-bottom": "none"
        });
    });
    $("#btn_search").on("click", function() {

        $("#div_typesearch").animate({
            width: 250
        });


        $(".header-fixed .navbar #div_typesearch input").css({
            "width": "100%",
            "padding": "8px",
            "display": "block"
        });
        $(".header-fixed .navbar  #btn_search").css({
            "display": "none"
        });
        $(".header-fixed .navbar #div_typesearch #btn_searchclick").css({
            "display": "block"
        });
    })

    $('#movie-posterset').on("click", function() {

        var video_id = $("#auto_play").attr("href");
        $("#collapsibleNavbar .navbar-nav .nav-item .signin_click").click();
        $("#myModal #btn_submit #get_videoid").val(video_id);
        return false;
    });

    $('#play_videobutton').on("click", function() {
        $('#slider_playiconid').click();
        return false;
    });

    $('#slider_playiconid').on("click", function() {

        var video_id = $("#slider_playiconid").attr("href");
        $("#collapsibleNavbar .navbar-nav .nav-item .signin_click").click();
        $("#myModal #btn_submit #get_videoid").val(video_id);
        return false;
    });
    $('#slider .epi-play-icon').on("click", function() {

        var video_id = $("#slider .epi-play-icon").attr("href");
        var video_trailer = 'video_trailer';
        $("#collapsibleNavbar .navbar-nav .nav-item .signin_click").click();
        $("#myModal #btn_submit #get_trailerid").val(video_id);
        $("#myModal #btn_submit #video_trailer").val(video_trailer);
        return false;
    });

    if ($(window).width() <= 768) {
        $("#auto_play").show();
    }
    if ($(window).width() <= 767) {
        $("#btn_search").on("click", function() {
            // $("#div_typesearch").animate({
            //     width: 250
            // });


            $(".header-fixed .navbar .navbar-collapse .navbar-nav .nav-item input").css({
                "width": "400px;",
                "padding": "8px",
                "display": "block"
            });
            $(".header-fixed .navbar .navbar-collapse .navbar-nav .nav-item #btn_search").css({
                "display": "none"
            });
            $(".header-fixed .navbar .navbar-collapse .navbar-nav .nav-item #btn_searchclick").css({
                "display": "block"
            });
        })
        $(".signin_click").on('click', function(e) {
            e.preventDefault();
            $("#collapsibleNavbar").toggle();
            $('body').addClass('popup-open');
        });
        $("#movie-posterset").on('click', function(e) {
            e.preventDefault();
            $("#collapsibleNavbar").toggle();
        });
        $("#navbar_btn").on('click', function(e) {
            e.preventDefault();
            $("#collapsibleNavbar").toggle();
            console.log('clicksignin');
        });
    }
    $("#signup_submit").on('submit', function(e) {
        e.preventDefault();
        var token = $('meta[name="csrf-token"]').attr('content');
        var site_url = $('meta[name="site-url"]').attr('content');
        var name = $("#name").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var confirm_password = $("#confirm_password").val();
        var login_by = $("#login_by").val();
        var device_type = $("#device_type").val();
        var device_token = $("#device_token").val();
        var step_remove_url = $("#signup_submit").attr('action');
        jQuery.ajax({
            type: 'POST',
            url: APP_URL + '/user/signup/create',
            data: {
                _token: token,
                name: name,
                email: email,
                password: password,
                confirm_password: confirm_password,
                login_by: login_by,
                device_type: device_type,
                device_token: device_token
            },
            success: function(data) {
                $("#sign_up").hide();
                $("#sign_in").show();
                if (data.success) {
                    $("#message_signup").css({
                        "background-color": "#28a745"

                    });
                } else {
                    $("#message_signup").css({
                        "background-color": "#E50914"

                    });
                }
                $('#message_signup').fadeIn().html("<p>Your account has been successfully Registered,Please Verify your email and Sign In</p>");
                setTimeout(function() {
                    $('#message_signup').fadeOut("slow");
                }, 5000);
            }
        });
    });
    $("#password_forget").on('submit', function(e) {
        e.preventDefault();
        var token = $('meta[name="csrf-token"]').attr('content');
        var site_url = $('meta[name="site-url"]').attr('content');
        var email = $("#useremail").val();
        console.log(email);

        jQuery.ajax({
            type: 'POST',
            url: APP_URL + '/user/forget/password',
            data: {
                _token: token,
                email: email
            },
            success: function(data) {
                $("#forget_password").hide();
                $("#sign_in").show();
                $("#message_forgetpass").css({
                    "background-color": "#46d369"
                });
                $('#message_forgetpass').fadeIn().html("<p>Mail Sent Successfully</p>").addClass('message_mail');
                setTimeout(function() {
                    $('#message_forgetpass').fadeOut("slow");
                }, 5000);
            }
        });
    });



    // Get the modal
    var signmodal = document.getElementById("myModal");

    // Get the button that opens the modal
    var signbtn = document.getElementsByClassName("signin_click");

    // Get the <span> element that closes the modal
    var signspan = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    $(".signin_click").on('click', function(e) {
        $("#sign_up").hide();
        signmodal.style.display = "block";
        $("#sign_in").show();
        $('body').addClass('popup-open');
    });

    $(".register_click").on('click', function(e) {

        signmodal.style.display = "block";
        $(".for_signup").click();
        $('body').addClass('popup-open');

    });

    // When the user clicks on <span> (x), close the modal
    signspan.onclick = function() {
        signmodal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
            if (event.target == signmodal) {
                signmodal.style.display = "none";
            }
        }
        // Movie owl carousel slider

    // $('.carousel-main').owlCarousel({
    //     items: 1,
    //     loop: true,
    //     margin: 10,
    //     nav: true,
    //     dots: false,
    //     navText: ['<span class="fa fa-chevron-left fa-2x"></span>', '<span class="fa fa-chevron-right fa-2x"></span>'],
    //     responsive: {
    //         1440: {
    //             items: 6,
    //             slideBy: 4
    //         },
    //         1020: {
    //             items: 6,
    //             slideBy: 3
    //         },
    //         740: {
    //             items: 2.2,
    //             margin: 20
    //         },
    //         340: {
    //             items: 1,
    //             margin: 10
    //         }
    //     }
    // });

    $("#Genres").on('click', function(e) {
        e.preventDefault();
        console.log("genres");

        $("#Genresdata").toggle();

    });

    $(".form-control-placeholder").on('click', function(e) {
        e.preventDefault();
        $(".form-control-placeholder").css({
            "font-size": "65%",
            "transform": 'translate3d(0, -25%, 0)',
            "margin-left": "7px",
            "color": "#fff",
            "opacity": "1",
            "padding-bottom": '5px'
        });
    });
    $(".form-control-input").on('focus', function(e) {
        console.log("label");
        $(".form-control-placeholder").css({
            "font-size": "65%",
            "transform": 'translate3d(0, -25%, 0)',
            "margin-left": "7px",
            "color": "#fff",
            "opacity": "1",
            "padding-bottom": '5px'
        });
    });

    //  jw Player

    // jw player


    // popover
    var width = $(window).width();

    if (width > 768) {
        $(".movie_item:not(.mc_item) > a").hover(function() {
            $(this).css("opacity", 0.4);
        }, function() {
            return;
        });
        $('.movie_item > a').on('hidden.bs.popover', function() {
            if (!$(this).is(":hover")) {
                $(".movie_item > a").css("opacity", 1);
            }
        });
    }


    if (width > 768) {
        $('[data-toggle="popover"]').popover("enable");
        $(".movie_item:not(.mc_item) > a").unbind("mouseenter mouseleave").hover(function() {
            $(this).css("opacity", 0.4);
        }, function() {
            return;
        });
        $('.movie_item > a').on('hidden.bs.popover', function() {
            if (!$(this).is(":hover")) {
                $(".movie_item > a").css("opacity", 1);
            }
        });
    } else {
        $('[data-toggle="popover"]').popover("disable");
        $(".movie_item > a").unbind("mouseenter mouseleave");
        $('.movie_item > a').on('hidden.bs.popover');
    }

})(jQuery);

$(document).ajaxComplete(function() {
    $('[data-toggle="popover"]').popover("enable");
});

$(document).ready(function() {
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function() {
        $(this).parent().children('li.star').each(function(e) {
            $(this).removeClass('hover');
        });
    });

    $('#stars_rate_this li').on('mouseover', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
        //$('.rate_count').text(onStar);

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function() {
        $(this).parent().children('li.star').each(function(e) {
            $(this).removeClass('hover');
        });
    });

    // $('.rate_this_content').on('mouseleave', function() {
    //     //if ($('.rate_count').text() == 'Rate This') {
    //     $('.star-rating-delete').click();
    //     //}
    // });


    /* 2. Action to perform on click */
    $('#stars_rate_this li').on('click', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

        // JUST RESPONSE (Not needed)
        var admin_video_id = $('#admin_video_id').val();
        var ratingValue = parseInt($('#stars_rate_this li.selected').last().data('value'), 10);
        var msg = "";
        if (ratingValue >= 1) {
            $('.rate_count').text(ratingValue);
            msg = $('#rating').val(ratingValue);
            var token = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
                type: 'POST',
                url: APP_URL + '/rated',
                data: {
                    _token: token,
                    rating: ratingValue,
                    review_id: $('#review_id').val(),
                    admin_video_id: admin_video_id
                },
                success: function(data) {
                    $('#review_id').val(data);
                    $('.rate_this_content').hide();
                    $('#fa_star_change').addClass('fa-star fa-fw').removeClass('fa-star-o');
                    //$('.rate_count').text(parseInt($('#stars_rate_this li.selected').last().data('value'), 10));
                    var stars = $('#stars li').parent().children('li.star');
                    var onStar = parseInt($('#stars_rate_this li.selected').last().data('value'), 10);
                    for (i = 0; i < stars.length; i++) {
                        $(stars[i]).removeClass('selected');
                    }
                    for (i = 0; i < onStar; i++) {
                        $(stars[i]).addClass('selected');
                    }
                }
            });
        } else {
            msg = $('#rating').val(ratingValue);
        }

    });

    $('#stars li').on('click', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

        // JUST RESPONSE (Not needed)
        var admin_video_id = $('#admin_video_id').val();
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        console.log(ratingValue);
        var msg = "";
        if (ratingValue >= 1) {
            msg = $('#rating').val(ratingValue);
            var token = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
                type: 'POST',
                url: APP_URL + '/rated',
                data: {
                    _token: token,
                    rating: ratingValue,
                    review_id: $('#review_id').val(),
                    admin_video_id: admin_video_id
                },
                success: function(data) {
                    $('#review_id').val(data)
                }
            });
        } else {
            msg = $('#rating').val(ratingValue);
        }

    });

    $(document).on('click', '.star-rating-delete', function() {
        var token = $('meta[name="csrf-token"]').attr('content');
        var admin_video_id = $('#admin_video_id').val();
        var ratingValue = 0;
        jQuery.ajax({
            type: 'POST',
            url: APP_URL + '/rated',
            data: {
                _token: token,
                rating: ratingValue,
                review_id: $('#review_id').val(),
                admin_video_id: admin_video_id
            },
            success: function(data) {
                $('#review_id').val(data);
                $('.rate_count').text('Rate This');
                $('#fa_star_change').addClass('fa-star-o').removeClass('fa-star fa-fw');
            }
        });
    });


    $(document).on('click', '.rate_this', function() {
        $('.rate_this_content').show();
    });

    $(document).on('click', '.star-rating-delete', function() {
        $('.rate_this_content').hide();
    });
});

$(document).ready(function() {
    $(".click-full-casts").click(function() {
        $('.nav-tabs a[href="#casts"]').tab('show');
    });




    $(".sr-sec").click(function() {
        $('.header-right').slideToggle();
    });

    $(".sr-sec").click(function() {
        $('.header-right').slideToggle();
        $("#btn_search").click();
    });
});

//discussion module
$(document).ready(function() {
    $('.message-area-wrap').hide();
    $('#reply_to_discussion').click(function() {
        $(this).hide();
        $('#cke_message_area').show();
        $('.message-area-wrap').slideDown('slow');
    });

    $(document).on('click', '.cancel', function() {
        $('.message-area-wrap').slideUp('slow', '', function() {
            $('#cke_message_area').hide();
        });
        $('#reply_to_discussion').show();
    });

    $(document).on('click', '.submit_reply', function() {
        var fd = new FormData();
        var token = $('meta[name="csrf-token"]').attr('content');
        var desc = CKEDITOR.instances['message_area'].getData();

        var other_data = $('form#submit_reply').serializeArray();
        $.each(other_data, function(key, input) {
            fd.append(input.name, input.value);
        });
        fd.append('message', desc);
        fd.append('_token', token);
        console.log(fd);
        jQuery.ajax({
            type: 'POST',
            url: APP_URL + '/save-reply',
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                if (data.errors) {
                    $('.errors').show();
                    $.each(data.errors, function(index, value) {
                        $('.errors ul').html('<li>' + value + '</li>')
                    });
                } else {
                    $('.errors').hide();
                    location.reload();
                }
            }
        });
    });

    // get and set quote
    $(document).on('click', '.quote', function() {
        var post_id = $(this).attr('data-id');
        var by_user = $(this).attr('data-user');
        var parent_post = $('.post_id_' + post_id).html();
        console.log(parent_post);
        var quote = '<blockquote><p><a class="said_by" href="#' + post_id + '">@' + by_user + '</a> said:</p>' + parent_post + '</blockquote>';
        CKEDITOR.instances['message_area'].setData(quote);
        $('#is_quote').val(1);
        $('#reply_to_discussion').click();
        $('html, body').animate({
            scrollTop: $($('#submit_reply')).offset().top
        }, 1000);
    });

    // get and set quote
    $(document).on('click', '.like', function() {
        var token = $('meta[name="csrf-token"]').attr('content');
        var post_id = $(this).attr('id').replace('like_', '');
        var user_id = $(this).attr('data-user');
        var like_value = $(this).attr('data-value');
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: APP_URL + '/like-forum-post',
            data: { post_id: post_id, user_id: user_id, like_value: like_value, _token: token },
            success: function(data) {
                $('#like_' + post_id).attr('data-value', data.like_value);
                $('.like_count_' + post_id).text(data.like_count);
                //if (data == 1)
                //$('#' + post_id).text('Like');
                //if (data == 0)
                //$('#' + post_id).text('Dislike');
                // if (data.errors) {
                //     $('.errors').show();
                //     $.each(data.errors, function(index, value) {
                //         $('.errors ul').html('<li>' + value + '</li>')
                //     });
                // } else {
                //     $('.errors').hide();
                //     location.reload();
                // }
            }
        });
    });

});