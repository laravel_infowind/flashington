<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class ForumTopic extends Model
{
    public $table = "forum_topics";

	public function posts()
    {
        return $this->hasMany('App\ForumPost')->with('user');
    }

    public function category()
    {
        return $this->belongsTo('App\ForumCategory', 'forum_category_id');
    }

    public function video()
    {
        return $this->belongsTo('App\AdminVideo', 'admin_video_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function adminVideo() {
        return $this->belongsTo('App\AdminVideo');
    }
}
