@extends('layouts.admin')

@section('title', tr('edit_tag'))

@section('content-header', tr('edit_tag'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.categories')}}"><i class="fa fa-suitcase"></i> {{tr('categories')}}</a></li>
    <li class="active">{{tr('edit_tag')}}</li> -->
@endsection


@section('styles')

    <link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">

@endsection

@section('content')

@include('notification.notify')

<div class="main-content">
    @if(Setting::get('admin_delete_control'))
    <form class="form-horizontal" role="form">
    @else
    <form class="form-horizontal" action="{{route('admin.save.tag')}}" method="POST" enctype="multipart/form-data" role="form">
    @endif
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('edit_tag')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <input type="hidden" name="id" value="{{$tag->id}}">
                                    <div class="form-group">
                                        <label for="name">{{tr('name')}}*</label>
                                        <input type="text" required class="form-control"  pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" value="{{$tag->name}}" id="name" name="name" placeholder="{{tr('tag_name')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="slug">{{tr('tag_slug')}}</label>
                                        <input type="text" class="form-control" value="{{$tag->slug}}" id="slug" name="slug" placeholder="{{tr('tag_slug')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($tag) !!}

                            @if($tag->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($tag->updated_at)). ' at '. date('H:i', strtotime($tag->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Update</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>

</div>

@endsection