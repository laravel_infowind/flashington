<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'admin_video_id', 'subject','rating','comment','spoilers',
        'status'
    ];

    public function adminVideo() {
        return $this->belongsTo('App\AdminVideo');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
