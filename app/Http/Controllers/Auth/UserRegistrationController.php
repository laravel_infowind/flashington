<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\SubProfile;
use App\Moderator;
use App\Settings;
use Log;

use File;

use DB;

use Auth;

use Setting;


use Exception;

class UserRegistrationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function signup(Request $request)
    {
        try {

            DB::beginTransaction();

            $basicValidator = Validator::make(
                $request->all(),
                array(
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'device_token' => 'required',
                    'login_by' => 'required|in:manual,facebook,google',
                )
            );

            if($basicValidator->fails()) {

                $error_messages = implode(',', $basicValidator->messages()->all());

                $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);

                throw new Exception($error_messages);

            } else {

                $allowedSocialLogin = array('facebook','google');

                if (in_array($request->login_by,$allowedSocialLogin)) {

                    // validate social registration fields

                    $socialValidator = Validator::make(
                                $request->all(),
                                array(
                                    'social_unique_id' => 'required',
                                    'name' => 'required|min:2|max:100',
                                    'email' => 'required|email|max:255',
                                    'mobile' => 'digits_between:4,16',
                                    'picture' => '',
                                    'gender' => 'in:male,female,others',
                                )
                            );

                    if ($socialValidator->fails()) {

                        $error_messages = implode(',', $socialValidator->messages()->all());

                        $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);

                        throw new Exception($error_messages);

                    }

                } else {

                    // Validate manual registration fields

                    $manualValidator = Validator::make(
                        $request->all(),
                        array(
                            'name' => 'required|regex:/^[a-z\d\-.\s]+$/i|min:2|max:255',
                            'email' => 'required|email|max:255',
                            'password' => 'required|min:6',
                            'picture' => 'mimes:jpeg,jpg,bmp,png',
                        )
                    );

                    // validate email existence

                    $emailValidator = Validator::make(
                        $request->all(),
                        array(
                            'email' => 'unique:users,email',
                        )
                    );

                    if($manualValidator->fails()) {

                        $error_messages = implode(',', $manualValidator->messages()->all());

                        $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);

                        throw new Exception($error_messages);
                        
                    } else if($emailValidator->fails()) {

                        $error_messages = implode(',', $emailValidator->messages()->all());

                        $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);
                        
                        throw new Exception($error_messages);

                    } 

                }

                $user = User::where('email' , $request->email)->first();

                $new_user_send_email = NO;

                // Creating the user

                if(!$user) {

                    $user = new User;

                    register_mobile($request->device_type);

                    $new_user_send_email = YES;

                } else {

                    if ($user->is_activated == USER_DECLINED) {

                        throw new Exception(tr('user_login_decline'));
                    
                    }

                    $sub_profile = SubProfile::where('user_id', $user->id)->first();

                    if (!$sub_profile) {

                        $new_user_send_email = YES;

                    }

                }

                if($request->has('name')) {

                    $user->name = $request->name;

                }

                if($request->has('email')) {

                    $user->email = $request->email;

                }

                if($request->has('mobile')) {

                    $user->mobile = $request->mobile;

                }

                if($request->has('password')) {

                    $user->password = Hash::make($request->password);

                }

                $user->gender = $request->has('gender') ? $request->gender : "male";

                $user->token = Helper::generate_token();

                $user->token_expiry = Helper::generate_token_expiry();

                $check_device_exist = User::where('device_token', $request->device_token)->first();

                if($check_device_exist){

                    $check_device_exist->device_token = "";

                    $check_device_exist->save();
                }

                $user->device_token = $request->has('device_token') ? $request->device_token : "";

                $user->device_type = $request->has('device_type') ? $request->device_type : "";

                $user->login_by = $request->has('login_by') ? $request->login_by : "";

                $user->social_unique_id = $request->has('social_unique_id') ? $request->social_unique_id : '';

                $user->picture = asset('placeholder.png');

                // Upload Picture

                if($request->login_by == "manual") {

                    if($request->hasFile('picture')) {

                        $user->picture = Helper::normal_upload_picture($request->file('picture'));

                    }

                } else {

                    if($request->has('picture')) {

                        $user->picture = $request->picture;

                    }

                    $user->is_verified = USER_EMAIL_VERIFIED;

                }

                $user->is_activated = $user->no_of_account = $user->is_verified = 1;
                
                if(Setting::get('email_verify_control')) {

                    $user->status = DEFAULT_FALSE;

                    $user->is_verified = USER_EMAIL_VERIFIED;

                    if ($request->login_by == 'manual') {

                        $user->is_verified = USER_EMAIL_NOT_VERIFIED;

                    }

                } 

                if ($user->is_verified) {

                    $user->status = 1;   

                    $user->logged_in_account = 1;
                }
                $user->user_ip_address = $request->getClientIp(true);
                if ($user->save()) {

                    // Send welcome email to the new user:
                    
                    if($new_user_send_email == YES) {

                        // Check the default subscription and save the user type 

                        user_type_check($user->id);

                        if ($user->login_by == 'manual') {

                            $user->password = $request->password;

                            // $subject = tr('user_welcome_title').' '.Setting::get('site_name');

                            $email_data = [];

                            $email_data['user_id'] = $user->id;

                            $email_data['verification_code'] = $user->verification_code;

                            $email_data['template_type'] = USER_WELCOME;

                            $email = $user->email;
                            $page = "emails.welcome";

                            Helper::send_email($page,$subject = null,$email,$email_data);

                            // Mail::to($email)->send(new RegisterUser($email_data));


                        }

                        $sub_profile = new SubProfile;

                        $sub_profile->user_id = $user->id;

                        $sub_profile->name = $user->name;

                        $sub_profile->picture = $user->picture;

                        $sub_profile->status = DEFAULT_TRUE;

                        if ($sub_profile->save()) {

                            // Response with registered user details:

                            if (!Setting::get('email_verify_control')) {

                                $logged_device = new UserLoggedDevice();

                                $logged_device->user_id = $user->id;

                                $logged_device->token_expiry = Helper::generate_token_expiry();

                                $logged_device->status = DEFAULT_TRUE;

                                $logged_device->save();

                            }
                            

                        } else {

                            throw new Exception(tr('sub_profile_not_save'));
                            
                        }


                    }

                    $moderator = Moderator::where('email', $user->email)->first();

                    // If the user already registered as moderator, automatically the status will update.

                    if($moderator && $user) {

                        $user->is_moderator = DEFAULT_TRUE;

                        $user->moderator_id = $moderator->id;

                        $user->save();

                        $moderator->is_activated = DEFAULT_TRUE;

                        $moderator->is_user = DEFAULT_TRUE;

                        $moderator->save();

                    }

                    if ($user->is_verified) {

                        $response_array = array(
                            'success' => true,
                            'id' => $user->id,
                            'name' => $user->name,
                            'mobile' => $user->mobile,
                            'gender' => $user->gender,
                            'email' => $user->email,
                            'picture' => $user->picture,
                            'token' => $user->token,
                            'token_expiry' => $user->token_expiry,
                            'login_by' => $user->login_by,
                            'social_unique_id' => $user->social_unique_id,
                            'verification_control'=> Setting::get('email_verify_control'),
                            'sub_profile_id'=>$sub_profile->id,
                            'email_notification'=>$user->email_notification,
                            'payment_subscription' => Setting::get('ios_payment_subscription_status'),
                            'message'=> Setting::get('email_verify_control') ? tr('register_verify_success') : tr('register_success')
                        );

                        $response_array = Helper::null_safe($response_array);

                        $response_array['user_type'] = $user->user_type ? 1 : 0;
                        
                        $response_array['push_status'] = $user->push_status ? 1 : 0;

                    } 
                    else {

                       // throw new Exception(Helper::get_error_message(3001), 3001);

                        $response_array = ['success'=>true, 'error_messages'=>Helper::get_error_message(3001), 'error_code'=>3001];

                        DB::commit();

                        return response()->json($response_array, 200);

                    }
                }

            }

            DB::commit();

            $response = response()->json($response_array, 200);

            return $response;

        } 
        catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            $code = $e->getCode();

            $response_array = ['success'=>false, 'error_messages'=>$error, 'error_code'=>$code];

            return response()->json($response_array);

        }
    }
 
    public function forget_password(Request $request)
    {

         try {

            DB::beginTransaction();

            $email =$request->email;
            
            $validator = Validator::make(

                $request->all(),

                array(
                    'email' => 'required|email|exists:users,email',
                ),
                 array(
                    'exists' => 'The :attribute doesn\'t exists',
                )
            );

            if ($validator->fails()) {
                
                $error_messages = implode(',',$validator->messages()->all());
                
                $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);

                throw new Exception($error_messages);
            
            } else {

                $user = User::where('email' , $email)->first();

                if($user) {

                    // If the user social login users, should not send mail to the users.

                    $allowedSocialLogin = array('facebook','google');

                    if (in_array($user->login_by,$allowedSocialLogin)) {

                        throw new Exception(tr('you_registered_as_social_user'));

                    } else {

                        $new_password = Helper::generate_password();

                        $user->password = Hash::make($new_password);

                        $user->save();

                        $email_data = array();

                       // $email = $user->email;

                        $email_data['email']  = $user->email;

                        $email_data['password'] = $new_password;

                        $email_data['template_type'] = FORGOT_PASSWORD;

                        $page = "emails.forgot-password";

                        $email_send = Helper::send_email($page,$subject = null,$user->email,$email_data);
                        // Mail::to($user->email)->send(new ForgotPassword($email_data));
                                        
                        $response_array['success'] = true;

                        $response_array['message'] = Helper::get_message(106);

                    } 

                } else {

                    throw new Exception(tr('no_user_detail_found'));
                    
                }

            }

            DB::commit();

            $response = response()->json($response_array, 200);

            return $response;

        } catch(Exception $e) {

            DB::rollback();

            $e = $e->getMessage();

            $response_array = ['success'=>false, 'error_messages'=>$e];

            return response()->json($response_array);
        }

    }
}
