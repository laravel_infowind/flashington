@extends('layouts.admin')

@section('title', tr('edit_page'))

@section('content-header', tr('edit_page'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.pages.index')}}"><i class="fa fa-book"></i> {{tr('pages')}}</a></li>
    <li class="active"> {{tr('edit_page')}}</li> -->
@endsection

@section('content')

@include('notification.notify')

<div class="main-content">
    <form  action="{{route('admin.pages.save')}}" method="POST" enctype="multipart/form-data" role="form">
      <div class="row">
         <div class="col-md-8">
            <div class="boxx">
               <div class="box-bodyy">
                  <div class="card mb-20">
                     <div class="card-header">
                        <label for="title">{{tr('edit_page')}}</label>
                     </div>
                     <div class="card-body">
                        <div class="form-body">
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="form-group">
                                <label for="heading">{{tr('heading')}}*</label>
                                <input type="text" class="form-control" name="heading" required value="{{ $data->heading  }}" id="heading" placeholder="{{tr('enter_heading')}}" {{ ($data->page_slug == '404-page') ? 'readonly' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="description">{{tr('description')}}*</label>
                                <textarea id="ckeditor" name="description" class="form-control" required placeholder="{{tr('enter_text')}}">{{$data->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="heading">Is Error Page</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="is_error_page" value="1" {{ ($data->is_error_page == 1) ? 'checked' : '' }}>YES
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" {{ ($data->is_error_page == 0) ? 'checked' : '' }} name="is_error_page" value="0">NO
                                    </label>
                                </div>
                            </div>
                        </div>
                     </div>
				  </div>
				  @seoForm($data)
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card mb-20">
               <div class="card-header">
                  <label for="">Publish</label>
               </div>
               <div class="card-body">
                  <div class="publish_card">
                     {!! App\Helpers\Helper::published_block($data) !!}

                     @if($data->id)
                        <div class="publish_item mb-20">
                           <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($data->updated_at)). ' at '. date('H:i', strtotime($data->updated_at)) }}</span></label>
                        </div>
                     
                     @endif
                  </div>
               </div>
               <div class="card-footer">
                  <div class="publish_item">
                     <div class="box-tools pull-left" style="width: 30%">                        
                        <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                        <button type="submit" class="btn btn-primary">{{tr('submit')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
   
@endsection
