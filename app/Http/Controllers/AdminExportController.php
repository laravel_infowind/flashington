<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Moderator;

use App\AdminVideo;

use App\Subscription;

use App\UserPayment;

use App\PayPerView;

use Exception;

use Setting;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Exports\ModeratorsExport;
use App\Exports\AdminVideosExport;
use App\Exports\UserPaymentsExport;
use App\Exports\PayPerViewsExport;

class AdminExportController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');  
    }
    
    /**
	 * Function Name: users_export()
	 *
	 * @usage used export the users details into the selected format
	 *
	 * @created Vidhya R
	 *
	 * @edited Vidhya R
	 *
	 * @param string format (xls, csv or pdf)
	 *
	 * @return redirect users page with success or error message 
	 */
    public function users_export(Request $request) {

    	try {


	    	$result = User::orderBy('created_at' , 'desc')->get();

	    	if(count($result) == 0) {            	
            	return redirect()->route('admin.users')->with('flash_error' , tr('no_user_found'));
	    	}


    		$filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid();

    		if ($request->get('format') == 'xls') {
    			return Excel::download(new UsersExport, $filename.'-users.xlsx');
    		}
    		else if ($request->get('format') == 'csv') {
    			return Excel::download(new UsersExport, $filename.'-users.csv');
    		}

            return redirect()->route('admin.users')->with('flash_success' , tr('export_success'));

		} catch(\Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.users')->with('flash_error' , $error);

        }

    }

    /**
	 * Function Name: moderators_export()
	 *
	 * @usage used export the moderators details into the selected format
	 *
	 * @created Maheswari
	 *
	 * @edited Maheswari
	 *
	 * @param string format (xls, csv or pdf)
	 *
	 * @return redirect users page with success or error message 
	 */
    public function moderators_export(Request $request) {

    	try {



	    	$filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid();

	    	$result = Moderator::orderBy('created_at' , 'desc')->get();

	    	// Check the result is not empty

	    	if(count($result) == 0) {
            	
            	return redirect()->route('admin.moderators')->with('flash_error' , tr('no_user_found'));

	    	}

	    	if ($request->get('format') == 'xls') {
    			return Excel::download(new ModeratorsExport, $filename.'-moderators.xlsx');
    		}
    		else if ($request->get('format') == 'csv') {
    			return Excel::download(new ModeratorsExport, $filename.'-moderators.csv');
    		}

            return redirect()->route('admin.moderators')->with('flash_success' ,tr('export_success'));

		} catch(\Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.moderators')->with('flash_error' , $error);

        }

    }

    /**
	 * Function Name: videos_export()
	 *
	 * @usage used export the videos details into the selected format
	 *
	 * @created Maheswari
	 *
	 * @edited Maheswari
	 *
	 * @param string format (xls, csv or pdf)
	 *
	 * @return redirect users page with success or error message 
	 */
    public function videos_export(Request $request) {

    	try {


	    	$filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid();

	    	$result = AdminVideo::orderBy('created_at' , 'desc')->get();

	    	// Check the result is not empty

	    	if(count($result) == 0) {
            	
            	return redirect()->route('admin.videos')->with('flash_error' , tr('no_user_found'));

	    	}

	    	if ($request->get('format') == 'xls') {
    			return Excel::download(new AdminVideosExport, $filename.'-videos.xlsx');
    		}
    		else if ($request->get('format') == 'csv') {
    			return Excel::download(new AdminVideosExport, $filename.'-videos.csv');
    		}


            return redirect()->route('admin.videos')->with('flash_success' ,tr('export_success'));

		}
		 catch(\Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.videos')->with('flash_error' , $error);

        }

    }

    /**
	 * Function Name: subscription_export()
	 *
	 * @usage used export the subscription details into the selected format
	 *
	 * @created Maheswari
	 *
	 * @edited Maheswari
	 *
	 * @param string format (xls, csv or pdf)
	 *
	 * @return redirect users page with success or error message 
	 */
    public function subscription_export(Request $request) {

    	try {

    		// Get the admin selected format for download

	    	$filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid();

	    	$result = UserPayment::orderBy('created_at' , 'desc')->get();

	    	// Check the result is not empty

	    	if(count($result) == 0) {
            	
            	return redirect()->route('admin.user.payments')->with('flash_error' , tr('no_user_found'));

	    	}

			if ($request->get('format') == 'xls') {
    			return Excel::download(new UserPaymentsExport, $filename.'-userpayments.xlsx');
    		}
    		else if ($request->get('format') == 'csv') {
    			return Excel::download(new UserPaymentsExport, $filename.'-userpayments.csv');
    		}

            return redirect()->route('admin.user.payments')->with('flash_success' ,tr('export_success'));

		} catch(\Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.user.payments')->with('flash_error' , $error);

        }

    }

    /**
	 * Function Name: payperview_export()
	 *
	 * @usage used export the video payperview details into the selected format
	 *
	 * @created Maheswari
	 *
	 * @edited Maheswari
	 *
	 * @param string format (xls, csv or pdf)
	 *
	 * @return redirect users page with success or error message 
	 */
    public function payperview_export(Request $request) {

    	try {


	    	$filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid();

	    	$result = PayPerView::orderBy('created_at' , 'desc')->get();

	    	// Check the result is not empty

	    	if(count($result) == 0) {
            	
            	return redirect()->route('admin.user.video-payments')->with('flash_error' , tr('no_user_found'));

	    	}

			if ($request->get('format') == 'xls') {
    			return Excel::download(new PayPerViewsExport, $filename.'-payperview.xlsx');
    		}
    		else if ($request->get('format') == 'csv') {
    			return Excel::download(new PayPerViewsExport, $filename.'-payperview.csv');
    		}

            return redirect()->route('admin.user.video-payments')->with('flash_success',tr('export_success'));

		} catch(\Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.user.video-payments')->with('flash_error' , $error);

        }

    }
}
