@if($user_reviews->count() > 0)
@foreach($user_reviews as $review)
    <div class="user_reviews">
        {{-- <div class="author_img">
            <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$review->user->name}}">
        </div> --}}
        <div class="user-comments">
            <div class="tinystarbar" title="{{ $review->rating }}/10">
                <div class="rating text-left">
                    @if($review->rating >= 1)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 2)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 3)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 4)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 5)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 6)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 7)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 8)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 9)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                    @if($review->rating >= 10)
                    <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                    @else
                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                    @endif
                </div>
            </div>
            <div class="comment-meta-div">
                <div class="comment-meta">
                    <b><span>{{ $review->user->name }}</span></b> - {{ date('d F Y', strtotime($review->created_at)) }}
                </div>
                <div class="comment-subject">{{ $review->subject }}</div>
                <div class="author-comment">
                    {!! $review->comment !!}
                </div>
                @if($review->spoilers == 'YES')
                <span class="spoiler-warning"><strong>Warning: Spoilers</strong></span>
                @endif
            </div>
        </div>
    </div>
    @endforeach
@endif