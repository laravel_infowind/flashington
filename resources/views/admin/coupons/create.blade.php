@extends('layouts.admin')

@section('title',tr('add_coupon'))

@section('content-header',tr('add_coupon'))

@section('breadcrumb')

	<!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

	<li><a href="{{route('admin.coupon.list')}}"><i class="fa fa-gift"></i>{{tr('coupons')}}</a></li>

	<li class="active">{{tr('add_coupon')}}</li> -->

@endsection

@section('content')

	@include('notification.notify')



<div class="main-content">
    <form action="{{route('admin.save.coupon')}}" method="POST" class="form-horizontal" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_coupon')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    
									<div class="form-group">
										<label for = "title">{{tr('title')}}*</label>
										<input type="text" name="title" role="title" min="5" max="20" class="form-control" value="{{ old('title') }}" required placeholder="{{tr('enter_coupon_title')}}">
									</div>
									<div class="form-group">
										<label for = "coupon_code">{{tr('coupon_code')}}*</label>
										<input type="text" name="coupon_code" min="1" max="10" class="form-control" value="{{old('coupon_code')}}"  required pattern="[A-Z0-9]{1,10}"  placeholder="{{tr('enter_coupon_code')}}" title="{{tr('validation')}}">
										<p class="help-block">{{tr('note')}} : {{tr('coupon_code_note')}}</p>
									</div>
									<div class="form-group floating-label">
										<label for = "amount_type">{{tr('amount_type')}}*</label>
										<select id ="amount_type" name="amount_type" class="form-control select2">
											<option value="" selected="">{{tr('select_option')}}</option>
											<option value="{{PERCENTAGE}}">{{tr('percentage_amount')}}</option>
											<option value="{{ABSOULTE}}">{{tr('absoulte_amount')}}</option>
										</select>
									</div>
									<div class="form-group">
										<label for="amount">{{tr('amount')}}*</label>
										<input type="number" name="amount" min="1" max="5000" step="any" class="form-control" placeholder="{{tr('amount')}}" value="{{old('amount')}}" required title="{{tr('only_number')}}">
									</div>
									<div class="form-group">
										<label for="expiry_date">{{tr('expiry_date')}}*</label>
										<input type="text" id="expiry_date" name="expiry_date" class="form-control" placeholder="{{tr('expiry_date_coupon')}}" value="{{old('expiry_date')}}" required readonly>
									</div>
									<div class="form-group">
										<label for="no_of_users_limit">{{tr('no_of_users_limit')}}*</label>
										<input type="text" pattern="[0-9]{1,4}" name="no_of_users_limit" class="form-control" placeholder="{{tr('no_of_users_limit')}}" value="{{old('no_of_users_limit')}}" required title="{{tr('no_of_users_limit_notes')}}">
									</div>
									<div class="form-group">
										<label for="amount">{{tr('per_users_limit')}}*</label>
										<input type="text" pattern="[0-9]{1,2}" name="per_users_limit" class="form-control" placeholder="{{tr('per_users_limit')}}" value="{{old('per_users_limit')}}" required title="{{tr('per_users_limit_notes')}}">
									</div>
									<div class="form-group">
										<label for="description">{{tr('description')}}</label>
										<textarea name="description" class="form-control" max="255" style="resize: none;"></textarea>
									</div>
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($model) !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
								<!-- <a class="btn btn-danger">{{tr('reset')}}</a> -->
								<button type="submit" class="btn btn-primary">Publish</button>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>


	
@endsection

