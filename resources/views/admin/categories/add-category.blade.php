@extends('layouts.admin')

@section('title', tr('add_category'))

@section('content-header', tr('add_category'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.categories')}}"><i class="fa fa-suitcase"></i> {{tr('categories')}}</a></li>
    <li class="active">{{tr('add_category')}}</li> -->
@endsection

@section('styles')

    <link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">

@endsection

@section('content')

@include('notification.notify')


<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.category')}}" method="POST" enctype="multipart/form-data" role="form">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_category')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">

                                    <div class="form-group">
                                        <label for="name">{{tr('name')}}*</label>
                                        <input type="text" required class="form-control" pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" id="name" name="name" placeholder="{{tr('category_name')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{tr('description')}}*</label>
                                        <textarea required class="form-control" id="ckeditor" name="description" placeholder="{{tr('description')}}" value="{{old('description')}}"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox checklab">
                                            <label></label>
                                            <label>
                                                <input type="checkbox" name="is_series" value="1" class="minimal-red"> {{tr('is_series')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @seoForm($category)
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                        {!! App\Helpers\Helper::published_block($category) !!}

                        
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Publish</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Publish</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Upload Image</label>
                    </div>
                    <div class="card-body">
                        <div class="image-upload">
                            <div class="image-upload-item">
                                <label for="picture">{{tr('picture')}}</label> 
                                <input style="display:none" type="file" required accept="image/jpeg,image/png" id="picture" name="picture" placeholder="{{tr('picture')}}" onchange="loadFile(this,'image_preview')">
                                @if($category->picture)
                                    <img src="{{$category->picture}}" onclick="$('#picture').click();return false;" id="image_preview" style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">
                                @else
                                    <img src="{{url('images/default.png')}}" onclick="$('#picture').click();return false;" id="image_preview" style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">
                                @endif
                                <p class="help-block">{{tr('image_validate')}} {{tr('image_square')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>

@endsection

@section('scripts')

   <script src="{{asset('admin-css/plugins/iCheck/icheck.min.js')}}"></script>

    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

        $(document).ready(function() {
            $(document).on('keyup', '#name', function(){
                var heading = $(this).val();
                $('#page_title').val(heading);
                $('input[name="meta[5]"]').val(heading);
                $('input[name="meta[26]"]').val(heading);
            });
        });
    </script>

@endsection