@extends('layouts.admin')

@section('title', tr('edit_subscription'))

@section('content-header', tr('edit_subscription'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.subscriptions.index')}}"><i class="fa fa-key"></i> {{tr('subscriptions')}}</a></li>
    <li class="active">{{tr('edit_subscription')}}</li> -->
@endsection

@section('content')

@include('notification.notify')


<div class="main-content">

 <form class="form-horizontal" action="{{route('admin.subscriptions.save')}}" method="POST" enctype="multipart/form-data" role="form">
  <input type="hidden" name="id" value="{{$data->id}}">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('edit_subscription')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label for="title">*{{tr('title')}}</label>
                                            <input type="text" required name="title" class="form-control" id="title" value="{{isset($data) ? $data->title : old('title')}}" placeholder="{{tr('title')}}" pattern = "[a-zA-Z,0-9\s\-\.]{2,100}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">*{{tr('description')}}</label>
                                            <textarea class="form-control" name="description" required placeholder="{{tr('description')}}">{{isset($data) ? $data->description : old('description')}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="plan">*{{tr('no_of_months')}}</label>
                                            <input type="number" min="1" max="12" pattern="[0-9][0-2]{2}"  required name="plan" class="form-control" id="plan" value="{{isset($data) ? $data->plan : old('plan')}}" title="{{tr('please_enter_plan_month')}}" placeholder="{{tr('no_of_months')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount">*{{tr('no_of_account')}}</label>
                                            <input type="text" required name="no_of_account" class="form-control" id="manage_account_count" placeholder="{{tr('manage_account_count')}}" pattern="[0-9]{1,}" value="{{$data->no_of_account}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount">*{{tr('amount')}}</label>
                                            <input type="number" required value="{{isset($data) ? $data->amount : old('amount')}}" name="amount" class="form-control" id="amount" placeholder="{{tr('amount')}}" step="any">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($data) !!}

                            @if($data->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($data->updated_at)). ' at '. date('H:i', strtotime($data->updated_at)) }}</span></label>
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Update</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>



@endsection