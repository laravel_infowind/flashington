@if(!Request::is('discuss/movies'))
    @if(Auth::check())
    <div class="new-discussion-btn">
        <a class="btn btn-primary" href="{{ url('movies/'.$movie->unique_id.'/discuss/new') }}">New Discussion</a>
    </div>
@endif
@endif
<h5>Categories</h5>
<ul class="discussion-category-list">
    @if(!empty($forum_category))
        @foreach($forum_category as $value)
        <li><a href="{{ url('movie/'.$movie->id.'/discuss/'.$value->id) }}">{{ $value->name }}</a></li>
        @endforeach
    @endif
</ul>