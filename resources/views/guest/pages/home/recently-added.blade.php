@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')

<section id="slider" class="home-pg-movie ptb80">
 <div class="latest-movies-div">
    <div class="container container-main">
        <div class="row sub-category">
              <div class="col col-md-9 col-sm-6">
                <h3 class="mmt_subtitle title">Recently Added</h3>
              </div>
              <div class="col col-md-3 col-sm-6 align-self-center text-right">
                <a href="{{ url('movies') }}" class="btn btn-icon btn-main btn-effect">View All</a>
              </div>
            </div>
            <div class="related-movies row recently-wrap">
            @foreach($videos_data as $data)
            <?php 
            //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
            $movie_description = App\Helpers\Helper::truncate($data['details'], 200, '...<a href="'.url('movies/'.$data['unique_id']).'">More</a>', true, true);
            //$movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
            ?>
            <div hover-id="{{ $data->unique_id }}" class="col-lg-2 col-md-3 col-sm-4 col-4 related-movies-column">
                <a href="{{ url('movies/'.$data['unique_id'])  }}">
                    <div class="movie-box-1">
                        <div id="{{ $data->unique_id }}"  class="poster">
                            <div class="movie_item">
                                <img id="{{ $data->unique_id }}" class="poster_image" src="{{ $data['default_image'] }}" alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button" data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right" data-title="" data-content="" data-container="body" data-html="true" data-original-title="" title=""/>
                                <div class="movie_item_hover">
                                    <div class='popover-movie-title'>{{ $data['title'] }}</div>
                                    <div class='movie-info-row'>
                                        <div class="movie-info-item">
                                            <span class='movie-year'>{{ $data['release_date'] }}</span>
                                            <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                        </div>
                                        <div class="movie-info-item">
                                            <span class='movie-stars'>
                                                <i class='fa fa-star' style='color: orange; font-weight: 900;'></i> {{ $data['ratings'] }}/<span class='rating_out_of'>10</span>
                                            </span>
                                            <span class='movie-vote'>
                                                <span class='vote_total'>{{ (!empty($data['vote_count'])) ? $data['vote_count'] : 0 }} votes</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class='movie-description'>
                                        {!! $movie_description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <div><div class="recently-all-pagination">{{ $videos_data->links() }}</div></div>
 </div>
      <!-- End Recently Added -->
</section>
<div class="ajax-loading" style="display: none;">  
   <span><i  class="fa fa-circle clr_red"></i></span>
   <span><i  class="fa fa-circle clr_white"></i></span>
   <span><i  class="fa fa-circle clr_red"></i></span>
</div>
@endsection
