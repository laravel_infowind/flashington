<section class="home-pg-movie">
    <div id="category_set" class="">

        <div class="trending_rowdiv">
            <div class="container container-main">
                <div class="row">
                    <div class="col col-md-9 col-sm-12">
                        <h3 class="trending_title title">Trending</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="latest-movies">
            <div class="container container-main">
                <div class="row">
                    @foreach($videos_trending as $data)
                    <?php 
            //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
            $movie_description = App\Helpers\Helper::truncate($data['details'], 400, '...', true, true);
            $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
        ?>
                    <div hover-id="{{ $data->unique_id }}" class="col-lg-2 col-md-3 col-sm-4 col-6 poster_div">
                        <a href="{{ url('movies/'.$data['unique_id'])  }}">
                            <div class="movie-box-1">
                                <div id="{{ $data->unique_id }}" class="poster">
                                    <div class="movie_item">
                                        <img id="{{ $data->unique_id }}" src="{{ $data['default_image'] }}"
                                            alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button"
                                            data-toggle="popover" data-trigger="hover" tabindex="0"
                                            data-placement="right" data-title="
                            <span class='popover-movie-title'>{!! $data['title'] !!}</span>
                            <div class='movie-info-row'>
                                <span class='movie-year'>{{ $data['release_date'] }}</span>
                                <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                <span class='movie-stars'>
                                  <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        {{ $data['ratings'] }}/<span class='rating_out_of'>10</span> <span class='vote_total'>{{ (!empty($data['vote_count'])) ? $data['vote_count'] : 0 }} votes</span>
                                    </span>
                            </div>" data-content="<div class='movie-description'>
                                <div>{!! $movie_description !!}</div>
                            </div>" data-container="body" data-html="true" data-original-title="" title="" />
                                    </div>
                                </div>


                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>