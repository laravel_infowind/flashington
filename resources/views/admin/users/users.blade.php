@extends('layouts.admin')

@section('title', tr('users'))

@section('content-header')

	{{tr('users')}}
	

	<a href="#" id="help-popover" class="btn btn-danger" style="font-size: 14px;font-weight: 600" title="{{tr('any_help')}}">{{tr('help_ques_mark')}}</a>

	<div id="help-content" style="display: none">

	    <ul class="popover-list">
	        <li><span class="text-green"><i class="fa fa-check-circle"></i></span> - {{tr('paid_subscribed_users')}}</li>
	        <li><span class="text-red"><i class="fa fa-times"></i></span> -{{tr('unpaid_unsubscribed_user')}}</li>
	        <li><b>{{tr('validity_days')}} - </b>{{tr('expiry_days_subscription_user')}}</li>
	        <li><b>{{tr('upgrade')}} - </b>{{tr('admin_moderator_upgrade_option')}}</li>
	    </ul>

	</div>

@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-user"></i> {{tr('users')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
        <div class="col-lg-12">

          	<div class="box box-primary">

	          	<div class="box-header label-primary">

	          		<b style="font-size:18px;">
	          			{{tr('users')}} @if(isset($subscription))-
	          			<a style="color: white;text-decoration: underline;" href="{{route('admin.subscriptions.view' , $subscription->unique_id)}}">
	          				{{ $subscription->title }}
	          			</a>@endif
	          		</b>
	          		<a href="{{route('admin.users.create')}}" class="btn btn-default pull-right">{{tr('add_user')}}</a>
	            </div>


	            <div class="box-body">

	            	<div>

                        <div class="search-table-top"> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length">
                                        <label>Show
                                            <form method="get" action="{{ url('admin/users') }}" class="videos-change-pagination">
                                                <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'desc')) ? 'desc' : 'asc'; ?>">
                                                <input type="hidden" name="search" value="{{ $search }}">
                                                
                                                
                                                <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
                                             </select>
                                         </form>
                                        entries</label>
                                 </div>
                             </div>
                                                          
                             <div class="col-sm-6">
                                <form action="{{route('admin.users')}}" class="form" method="get">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search by Name" class="form-control input-video-search" name="search" value="{{session('search')}}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
	            		
		            	@if(count($users) > 0)

		            		<div class="table table-responsive">

				              	<table id="datatable-withoutpagination" class="table table-bordered table-striped ">

									<thead>
									    <tr>											
											<th>
                                                <a href="{{ url('admin/users?column=name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('username')}} <i class="fa fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                            </th>
											<th>
                                                <a href="{{ url('admin/users?column=email&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('email')}} <i class="fa fa-sort<?php echo $column == 'email' ? '-' . $up_or_down : ''; ?>"></i></a>
                                            </th> 
											<th>{{tr('upgrade')}}</th>
											<th>{{tr('validity_days')}}</th>
											<th>{{tr('active_plan')}}</th>
											<th>{{tr('sub_profiles')}}</th>
											<th>{{tr('clear_login')}}</th>
											<th>
                                                <a href="{{ url('admin/users?column=is_activated&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$search) }}">{{tr('status')}} <i class="fa fa-sort<?php echo $column == 'is_activated' ? '-' . $up_or_down : ''; ?>"></i></a>
                                            </th>											
									    </tr>

									</thead>

									<tbody>
										@foreach($users as $i => $user)

										    <tr>
										      	
										      	<td>
										      		<a href="{{route('admin.users.view' , $user->id)}}">
										      			{{$user->name}}

										      			@if($user->user_type)

										      				<span class="text-green"><i class="fa fa-check-circle"></i></span>

										      			@else

										      				<span class="text-red"><i class="fa fa-times"></i></span> 

										      			@endif
										      		</a>

										      		<ul class="table-row-actions">
								                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.users.edit' , array('id' => $user->id))}}">{{tr('edit')}}</a></li>

								                  	<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('admin.users.view' , $user->id)}}">{{tr('view')}}</a></li>

								                  	@if($user->is_activated)
								                  		<li role="presentation"><a role="menuitem" onclick="return confirm(&quot;{{$user->name}} - {{tr('user_decline_confirmation')}}&quot;);" tabindex="-1" href="{{route('admin.users.status.change' , array('id'=>$user->id))}}"> {{tr('decline')}}</a></li>
								                  	 @else
								                  	 	<li role="presentation"><a role="menuitem" onclick="return confirm(&quot;{{$user->name}} - {{tr('user_approve_confirmation')}}&quot;);" tabindex="-1" href="{{route('admin.users.status.change' , array('id'=>$user->id))}}">
								                  		{{tr('approve')}} </a></li>
								                  	@endif

								                  	@if(!$user->is_verified)
									                  	<li role="presentation" class="divider"></li>
											      		<li role="presentation">
									                  		<a role="menuitem" tabindex="-1" href="{{route('admin.users.verify' , $user->id)}}">{{tr('verify')}}</a>
								                  		</li>
										      		@endif								                  	

								                  	<li role="presentation">
								                  		<a role="menuitem" tabindex="-1" href="{{route('admin.users.subprofiles',  $user->id)}}">{{tr('sub_profiles')}}</a>
								                  	</li>
													<li role="presentation">
														<a role="menuitem" tabindex="-1" href="{{route('admin.users.password_reset',  $user->id)}}">Password Reset</a>
													</li>
								                  	<li role="presentation">
								                  	 	@if(Setting::get('admin_delete_control'))
								                  	 		<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
								                  	 	@elseif(get_expiry_days($user->id) > 0)

								                  	 		<a role="menuitem" tabindex="-1"  href="{{route('admin.users.delete', array('id' => $user->id))}}">{{tr('delete')}}
								                  			</a>
								                  		@else
								                  			<a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('admin_user_delete_confirmation' , $user->name)}}&quot;);" href="{{route('admin.users.delete', array('id' => $user->id))}}">{{tr('delete')}}
								                  			</a>
								                  	 	@endif

								                  	</li>								                  
								                  	<li>
														<a href="{{route('admin.subscriptions.plans' , $user->id)}}">
															<span class="text-green"><b><i class="fa fa-eye"></i>&nbsp;{{tr('subscription_plans')}}</b></span>
														</a>

													</li>
			            							</ul>
										      	</td>

										      	<td>{{$user->email}}</td>

										      	<td>
										      		@if($user->is_moderator)
										      			<a onclick="return confirm(&quot;{{tr('disable_user_to_moderator',$user->name)}}&quot;);" href="{{route('admin.users.upgrade.disable' , array('id' => $user->id, 'moderator_id' => $user->moderator_id))}}" class="label label-warning" title="Do you want to remove the user from Moderator Role?">{{tr('disable')}}</a>
										      		@else
										      			<a onclick="return confirm(&quot;{{tr('upgrade_user_to_moderator',$user->name)}}&quot;);" href="{{route('admin.users.upgrade' , array('id' => $user->id ))}}" class="label label-danger" title="Do you want to change the user as Moderator ?">{{tr('upgrade')}}</a>
										      		@endif

										      	</td>

										      	<td>
											      	@if($user->user_type)
				                                        {{get_expiry_days($user->id)}}
				                                    @endif
										      	</td>

										      	<td>
										      		<?php echo active_plan($user->id);?>
										      	</td>

										      	<td>
										      		<a role="menuitem" tabindex="-1" href="{{route('admin.users.subprofiles',  $user->id)}}"><span class="label label-primary">
										      			{{count($user->subProfile)}} {{tr('sub_profiles')}}</span>
										      		</a>
										      	</td>

										      	<td class="text-center">

										      		<a href="{{route('admin.users.clear-login', ['id'=>$user->id])}}"><span class="label label-warning">{{tr('clear')}}</span></a>

										      	</td>

										      	<td>
											      	@if($user->is_activated)

											      		<span class="label label-success">{{tr('approve')}}</span>

											      	@else

											      		<span class="label label-warning">{{tr('pending')}}</span>

											      	@endif

										     	</td>

										      	<td>		            							

										      	</td>
										    </tr>

										@endforeach

									</tbody>

								</table>
                                <div class="pagination-video-list">
                        
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">Showing {{ ($users->currentpage()-1)*$users->perpage()+1 }} to {{ ($users->currentpage()*$users->perpage() < $users->total() ) ? $users->currentpage()*$users->perpage() : $users->total() }} of {{ $users->total() }} entries
                                            </div>
                                        </div>
                                        <div class="col-sm-7 text-right">
                                            <div align="right" id="paglink"><?php echo $users->links(); ?></div>
                                        </div>
                                    </div>

                                </div>
								

							</div>

						@else
							<h3 class="no-result">{{tr('no_user_found')}}</h3>
						@endif
					</div>
	            </div>
          	</div>
        </div>

    </div>

@endsection
