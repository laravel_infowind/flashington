@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/wizard.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.jbswizard.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/imgareaselect-default.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.awesome-cropper.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<style type="text/css">
    .container-narrow {
    margin: 150px auto 50px auto;
    max-width: 728px;
    }
    canvas{
    width: 100%;
    height: auto;
    }
    span.select2-container{
    width:100% !important;
    }
    .example-wizard.panel {
    border: 0px solid white;
    padding: 20px;
    display: inline-block;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
    margin-bottom: 10px;
    width: 30%;
    }
    .select2-container .select2-search--inline {
    border: 1px solid #d2d6df !important;
    width: 30%;
    }
</style>
@endsection
@section('content')  
@include('notification.notify')
@if(envfile('QUEUE_DRIVER') != 'redis') 
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('warning_error_queue')}}
</div>
@endif
@if(checkSize())
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('max_upload_size')}} <b>{{ini_get('upload_max_filesize')}}</b>&nbsp;&amp;&nbsp;{{tr('post_max_size')}} <b>{{ini_get('post_max_size')}}</b>
</div>
@endif
@if(Setting::get('ffmpeg_installed') == FFMPEG_NOT_INSTALLED) 
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{tr('ffmpeg_warning_notes')}}
</div>
@endif
<div>
    <div class="main-content">
        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display: none" id="error_popup">popup</button>
        <!-- popup -->
        <div class="modal fade error-popup" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="media">
                            <div class="media-left">
                                <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Information</h4>
                                <p id="error_messages_text"></p>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="example">
            <div class="example-wizard panel-primary">
                <div class="">
                    <!-- Example Wizard START -->
                    <div id="j-bs-wizard-example">
                        <form method="post" enctype="multipart/form-data" id="upload_video_form" action="{{route('admin.videos.save')}}">
                            <!-- tab1 -->
                            <div class="row">
                                <div class="col-md-8">
                                    <!-- USERS LIST -->
                                    <div class="box">
                                        <div class="box-header">
                                            <h4 class="bg bg-primary" style="width:150px; padding: 10px;" >{{tr('video_details')}}</h4>
                                            <p class="note-sec">{{tr('note')}}: <span class="asterisk"><i class="fa fa-asterisk"></i></span> {{tr('mandatory_field_notes')}}
                                                <input type="hidden" name="admin_video_id" id="admin_video_id" value="{{$model->id}}">
                                            </p>
                                        </div>
                                        <ul class="form-style-7">
                                            <li class="height-54">
                                                <label for="reviews">{{tr('msg')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                <div class="publish">
                                                    <div class="radio radio-primary radio-inline">
                                                        <input type="radio" class="search-type"  checked id="tmdb" value="1" name="tmdb"   >
                                                        <label for="tmdb"> {{tr('tmdb')}} </label>
                                                    </div>
                                                    <div class="radio radio-primary radio-inline">
                                                        <input type="radio" class="search-type" id="custom" value="0" name="tmdb"   >
                                                        <label for="custom"> {{tr('custom')}} </label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <label for="title">{{tr('title')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                                                @if(!$model->id)
                                                <button style="float: right; position: absolute; right: 3%;" title="Click to search on {{tr('tmdb')}}" type="button" class="btn btn-primary btn-sm" id="tmdb_btn">Search {{tr('tmdb')}}</button>
                                                @endif
                                                <input style="background-color: #fff !important;" required type="text" name="title" maxlength="100" maxlength="255" value="{{$model->title}}" id='title'>
                                            </li>
                                            <li style="clear: both;">
                                                <label for="age">{{tr('age')}} <span class="asterisk1"><i class="fa fa-asterisk1"></i></span></label>
                                                <input type="text" name="age" maxlength="3" value="{{$model->age}}" id='age' required="">
                                            </li>
                                            <li class="height-54">
                                                <label for="reviews">{{tr('ratings')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                <div class="starRating">
                                                    <input id="rating5" type="radio" name="ratings" value="5" @if($model->ratings == 5) checked @endif>
                                                    <label for="rating5">5</label>
                                                    <input id="rating4" type="radio" name="ratings" value="4" @if($model->ratings == 4) checked @endif>
                                                    <label for="rating4">4</label>
                                                    <input id="rating3" type="radio" name="ratings" value="3" @if($model->ratings == 3) checked @endif>
                                                    <label for="rating3">3</label>
                                                    <input id="rating2" type="radio" name="ratings" value="2" @if($model->ratings == 2) checked @endif>
                                                    <label for="rating2">2</label>
                                                    <input id="rating1" type="radio" name="ratings" value="1" @if($model->ratings == 1) checked @endif>
                                                    <label for="rating1">1</label>
                                                </div>
                                            </li>
                                            <li id="time_li" style="display: none;width: 98%;">
                                                <label for="time">{{tr('publish_time')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                <input type="text" name="publish_time" id="datepicker" value="{{$model->publish_time}}" readonly>
                                            </li>
                                            <li class="detail_description_editor">
                                                <label for="description">{{tr('description')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                <textarea class="overview" id="description" name="description" rows="4" required="">{!! html_entity_decode($model->description) !!}</textarea>
                                            </li>
                                            <li class="detail_description_editor">
                                                <label for="details">{{tr('details')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                @if($model->details == "")
                                                <textarea type="text" name="details" rows="4" id='details'></textarea> 
                                                @else
                                                <textarea type="text" name="details" rows="4" id='details'>{!! html_entity_decode($model->details) !!}</textarea>
                                                @endif
                                            </li>
                                            <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
                                            <script>
                                                CKEDITOR.replace( 'description' );
                                                CKEDITOR.replace( 'details' );
                                            </script>
                                            <li style="display:none;"  class="actors-list">
                                                <label for="actors">Actors </label>
                                                <select id="actors" name="actors[]" class="select2" multiple>
                                                @foreach($actors as $act)
                                                <option value="{{$act->id}}" @if( $model->actors !="0" && is_array(json_decode($model->actors, true)) && in_array($act->id, json_decode($model->actors, true))) selected @endif>{{$act->name}}</option>
                                                @endforeach
                                                </select>
                                            </li>
                                            <li style="display:none;"  class="directors-list">
                                                <label for="directors">Directors </label>
                                                <select id="directors" name="directors[]" class="select2" multiple>
                                                @foreach($directors as $direc)
                                                <option value="{{$direc->id}}" @if( $model->directors !="0" &&  is_array(json_decode($model->directors, true)) && in_array($direc->id, json_decode($model->directors, true))) selected @endif>{{$direc->name}}</option>
                                                @endforeach
                                                </select>
                                            </li>
                                            <li style="display:none;"  class="writers-list">
                                                <label for="writers">Writers </label>
                                                <select id="writers" name="writers[]" class="select2" multiple>
                                                @foreach($writers as $writer)
                                                <option value="{{$writer->id}}" @if( $model->writers !="0" &&  is_array(json_decode($model->writers, true)) && in_array($writer->id, json_decode($model->writers, true))) selected @endif>{{$writer->name}}</option>
                                                @endforeach
                                                </select>
                                            </li>
                                            <li>
                                                <label>{{tr('category')}}</label>
                                                <select class="form-control" onchange="displaySubCategory(this.value);" name="category_id" id="category_id" required="">
                                                @foreach($categories as $category)
                                                <option {{ ($category->id == $model->category_id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                                </select>
                                            </li>
                                            <li>
                                                <!-- onchange="saveSubCategory(this.value, {{ REQUEST_STEP_3 }} )" -->
                                                <label>{{tr('sub_category')}}</label>
                                                <select multiple   class="form-control select2" name="sub_category_id[]" id="sub_category" required="">
                                                </select>
                                            </li>
                                            <li>
                                                <label>Moderator  <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                <select class="form-control" required name="moderator_id" >
                                                    <option value=""></option>
                                                    @foreach($moderators as $mod)
                                                    <option {{ ($mod->id == $model->moderator_id ) ? 'selected' : '' }} value="{{ $mod->id }}">{{ $mod->name }}</option>
                                                    @endforeach
                                                </select>
                                            </li>
                                            <li class="height-54" style="padding-bottom: 54px;">
                                                <label for="reviews">Release Date</label>
                                                <div class="publish">
                                                    <input required="" type="date" class="form-control" id="release_date" name="release_date" value="{{ ( $model->release_date !='0000-00-00' && $model->release_date !='') ? $model->release_date:'' }}">
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <br>
                                    </div>

                                    <!-- tab4 -->
                                    @include('tab4')
                                    <!-- tab4 -->
                                </div>
                                <div class="col-md-4">
                                    <div class="box">
                                        <div class="box-header">
                                            <div style="border-bottom: 1px solid #DDDDDD; height: 42px;">
                                                <div style="padding-left:0px; padding-right:0px; width: 66%; margin-left: 3px; margin-top: 9px;" class="progress col-xs-12 col-sm-4 col-md-4 col-lg-4" style="margin-top: 8px;">
                                                    <div class="bar"></div >
                                                    <div class="percent">0%</div >
                                                </div>
                                                <div class="box-tools pull-right" style="width: 30%">
                                                    @if(Setting::get('admin_delete_control') == 1) 
                                                    <button style="width: 100%" disabled class="btn  btn-primary finish-btn" type="submit" id="finish_video">
                                                    <i class="fa fa-arrow-right" aria-hidden="true"></i> &nbsp; Finish</button>
                                                    @else
                                                    <button class="btn  btn-primary finish-btn" type="submit" id="finish_video"><i class="fa fa-arrow-right" aria-hidden="true"></i> &nbsp; Finish</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <ul  class="form-style-7">
                                                <li style="width: 95% !important;margin-bottom: 25px;">
                                                    <label for="reviews">{{tr('publish_type')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                                    <div class="publish">
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" id="now" value="{{PUBLISH_NOW}}" name="publish_type" onchange="checkPublishType(this.value)" {{($model->id) ?  (($model->status) ? "checked" : '' ) : 'checked' }} >
                                                            <label for="now"> {{tr('now')}} </label>
                                                        </div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" id="later" value="{{PUBLISH_LATER}}" name="publish_type" onchange="checkPublishType(this.value)" {{($model->id) ?  ((!$model->status) ? "checked" : '' ) : '' }} >
                                                            <label for="later"> {{tr('later')}} </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div style="margin-left: 10px;" class="checkbox checkbox-inline checkbox-primary">
                                                <input type="checkbox" value="1" name="send_notification" id="send_notification">
                                                <label for="send_notification">{{tr('send_notification')}}</label>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 image-upload">
                                                    <div class="box-header">
                                                        <h4 class="bg bg-primary" style="width:162px; padding: 10px;" >Upload Image</h4>
                                                    </div>
                                                    <label style="margin-left: 10px;">{{tr('default_image')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                                                    <input type="hidden" name="poster" id="poster">
                                                    <input type="file" id="default_image" accept="image/png, image/jpeg, image/jpg" name="default_image" placeholder="{{tr('default_image')}}" style="display:none" onchange="loadFile(this,'default_img')">

                                                    <input type="hidden" name="old_default_image" id="old_default_image" value="{{ $model->default_image }}">
                                                    <img src="{{$model->default_image ? $model->default_image : asset('images/default.png')}}" onclick="$('#default_image').click();return false;" id="default_img" style="width: 75%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">
                                                    <p style="margin-left: 5px;margin-bottom: 0px;" class="img-note">{{tr('video_image_validate')}} {{tr('rectangle_image')}}</p>
                                                    <label style="margin-left: 10px !important;margin-bottom: 5px;">Image Type</label>
                                                    <select style="width: 90%; margin-left: 20px;margin-bottom: 10px;" name="image_type" class="form-control">
                                                        <option value="">Select Image Type</option>
                                                        <option @if($model->is_home_slider==1) selected @endif value="is_home_slider">Home Slider</option>
                                                        <option @if($model->is_banner==1) selected @endif value="is_banner"> Banner</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="tmd_actors" id="tmd-actors">
                            <input type="hidden" name="tmd_directors" id="tmd-directors">
                            <input type="hidden" name="tmd_writers" id="tmd-writers">
                            <input type="hidden" name="tmd_genre" id="tmd-genre">
                            <input type="hidden" name="alternative_titles" id="alternative_titles">
                            
                            <input type="hidden" name="timezone" value="{{ Auth::guard('admin')->user()->timezone }}">
                        </form>
                    </div>
                    <!-- Example Wizard END -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="overlay">
    <div id="loading-img"></div>
</div>
<!-- Modal -->
<div id="data-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Movies</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Movie Title</th>
                            <th>Release Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="movie-data">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="tmdb_btn_prev btn btn-primary" prev_page="0">Prev</button>
                <button class="tmdb_btn_next btn btn-primary" next_page="2">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script> 
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script> 
<script src="{{asset('admin-css/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.jbswizard.min.js')}}"></script>
<script src="{{asset('assets/js/jbswizard.js')}}"></script>
<script src="{{asset('admin-css/plugins/jquery.form.js')}}"></script>
<script src="{{asset('assets/js/jquery.awesome-cropper.js')}}"></script>
<script src="{{asset('assets/js/jquery.imgareaselect.js')}}"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
    
    
    function makeSelected(){ // function is working in js/video-upload.js
        @if($model->sub_category_id )
        var e='{{ $model->sub_category_id}}';
        $('#sub_category').val(e.split(','));
        @endif
    }
</script>
<script type="text/javascript">
    var banner_image = "{{$model->is_banner}}";
    var cat_url = "{{ url('select/sub_category')}}";
    var step3 = "{{REQUEST_STEP_3}}";
    var sub_cat_url = "{{ url('select/genre')}}";
    var final = "{{REQUEST_STEP_FINAL}}";
    var video_id = "{{$model->id}}";
    var genreId = "{{$model->genre_id}}";
    var video_type = "{{$model->video_type}}";
    var view_video_url = "{{url('admin/view/video')}}?id=";
</script>
<script src="{{asset('assets/js/upload-video.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
});
    $('#datepicker').datetimepicker({
          minTime: "00:00:00",
          minDate: moment(),
          autoclose:true,
          format:'dd-mm-yyyy hh:ii',
      });
    $('.manual_video_upload').show();
    $('.others').hide();
    $("#video_upload").change(function()
    {
      $(".manual_video_upload").show();
      $(".others").hide();
    });
    $("#youtube").change(function()
    {
      $(".others").show();
      $(".manual_video_upload").hide();
    });
    $("#other_link").change(function()
    {
      $(".others").show();
      $(".manual_video_upload").hide();
    });
      function trailerVideoUpload(value, autoload_status) 
      {
      // On initialization, show others videos section
      if (value == 1) 
      {
        $("#upload_trailer").show();
        $("#other_trailer_video").val("");
        $(".other_trailer").hide();
      }
      if ((value == 2 || value == 3)) 
      {
        $("#other_trailer_video").val("");
        if(("{{$model->video_type_trailer}}" == value) || ("{{$model->video_type_trailer}}" == value)){
            $("#other_trailer_video").val("{{ change_web_url_to_cdn($model->trailer_video) }}");
          }
          $("#upload_trailer").hide();
          $(".other_trailer").show();
      }
      show_upload_location(); 
      }
      function show_upload_location()
      {
          // if($('#video_upload_link').is(':checked') || $('#upload_t_option').is(':checked')){
          //    $('#upload-location').show(); 
          // }else{
          //    $('#upload-location').hide(); 
          // }
          
      }
    
      function videoUploadType(value, autoload_status) 
      {
    
          // On initialization, show others videos section
    
          $(".others").show();
    
          $("#other_video").attr('required', true);
    
    
          if (autoload_status == 0) {
    
              $("#video").attr('required', true);
    
    
          }
    
          $(".manual_video_upload").hide();
    
          $("#other_video").val("{{ change_web_url_to_cdn($model->video) }}");
    
          
          if (value == "{{VIDEO_TYPE_UPLOAD}}") {
    
              $("#other_video").val("");
    
              $(".manual_video_upload").show();
    
              $(".others").hide();
    
              $("#other_video").attr('required', false);
    
              // If admin editing the video means remove the required fields for video & trailer video (If already in VIDEO_TYPE_UPLOAD)
    
              @if($model->video_type == VIDEO_TYPE_UPLOAD)
    
                  $("#video").attr('required', false);
    
    
              @endif
    
          }
    
          if ((value == "{{VIDEO_TYPE_OTHER}}" || value == "{{VIDEO_TYPE_YOUTUBE}}") && autoload_status == 0) {
    
              $("#other_video").val("");
    
    
              if(("{{$model->video_type}}" == value) || ("{{$model->video_type}}" == value)) {
    
                  $("#other_video").val("{{ change_web_url_to_cdn($model->video) }}");
    
              }
    
              $("#video").attr('required', false);
    
          }
    
        show_upload_location();
      
      }
    
        
    
    $(document).on('click','#tmdb_btn',function(){
    
    var title=$('#title').val();
    if($('#tmdb').is(':checked') && title !=''){
    $('#tmdb_btn').text('Searching...');
    $.ajax({
    type:'POST',
    url:'{{ url("admin/videos/search")}}',
    data: {title:title, page:1},
    success: function(data){
       $('#tmdb_btn').text('Search TMDB');
      var results=data.results;
      var html='';
      if( results.length > 0){
          $.each(results, function(i, val){
              var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
              html+=" <tr>\
                  <td>"+(i+1)+"</td>\
                  <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                  <td>"+val.title+"</td>\
                  <td>"+val.release_date+"</td>\
                  <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                </tr>\
                ";
          });
          $('#movie-data').html(html);
      }else{
         $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
      }
    
      
      $('#data-modal').modal('show');
    },
    error: function(){
      alert('Something wrong with request');
       $('#tmdb_btn').text('Search TMDB');
    },
    
    });
    }
    });
    
    $(document).on('click','.tmdb_btn_next',function(){
    var next_page = parseInt(jQuery(this).attr('next_page'));
    var prev_page = parseInt(jQuery('.tmdb_btn_prev').attr('prev_page'));
    var isThis = this;
    console.log('step 1')
    var title=$('#title').val();
    if($('#tmdb').is(':checked') && title !=''){
    $('#tmdb_btn').text('Searching...');
    $.ajax({
    type:'POST',
    url:'{{ url("admin/videos/search")}}',
    data: {title:title, page:next_page},
    success: function(data){
       $('#tmdb_btn').text('Search TMDB');
      var results=data.results;
      var html='';
      if( results.length > 0){
          $.each(results, function(i, val){
              var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
              html+=" <tr>\
                  <td>"+(i+1)+"</td>\
                  <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                  <td>"+val.title+"</td>\
                  <td>"+val.release_date+"</td>\
                  <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                </tr>\
                ";
          });
          $('#movie-data').html(html);
      }else{
         $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
      }
      console.log('step 2')
      next_page = next_page + 1;
      prev_page = prev_page + 1;
      jQuery('.tmdb_btn_prev').attr('prev_page', prev_page)
      jQuery(isThis).attr('next_page', next_page)
      $('#data-modal').modal('show');
    },
    error: function(){
      alert('Something wrong with request');
       $('#tmdb_btn').text('Search TMDB');
    },
    
    });
    }
    });
    $(document).on('click','.tmdb_btn_prev',function(){
    var prev_page = parseInt(jQuery(this).attr('prev_page'));
    var next_page = parseInt(jQuery('.tmdb_btn_next').attr('next_page'));
    var isThis = this;
    var title=$('#title').val();
    if($('#tmdb').is(':checked') && title !=''){
    $('#tmdb_btn').text('Searching...');
    $.ajax({
    type:'POST',
    url:'{{ url("admin/videos/search")}}',
    data: {title:title, page:prev_page},
    success: function(data){
       $('#tmdb_btn').text('Search TMDB');
      var results=data.results;
      var html='';
      if( results.length > 0){
          $.each(results, function(i, val){
              var poster= val.poster_path !=null ? 'https://image.tmdb.org/t/p/w200'+val.poster_path :'{{ url("images/default.png") }}';
              html+=" <tr>\
                  <td>"+(i+1)+"</td>\
                  <td><img class='img-thumbnail' src='" + poster +"' width=100 height=100></td>\
                  <td>"+val.title+"</td>\
                  <td>"+val.release_date+"</td>\
                  <td><button type='button' id='"+val.id+"' class='btn get-movie-data btn-sm btn-primary'>Select</button></td>\
                </tr>\
                ";
          });
          $('#movie-data').html(html);
      }else{
         $('#movie-data').html("<tr><td class='text-center' colspan='5'>No data found</td></tr>"); 
      }
      console.log('step 2')
      if(prev_page == 0){
          prev_page = prev_page;
      }else{
          prev_page = prev_page - 1;
      }   
      if(next_page == 2){
          next_page = next_page;
      }else{
          next_page = next_page - 1;
      }        
      jQuery(isThis).attr('prev_page', prev_page)
      jQuery('.tmdb_btn_next').attr('next_page', next_page)
      $('#data-modal').modal('show');
    },
    error: function(){
      alert('Something wrong with request');
       $('#tmdb_btn').text('Search TMDB');
    },
    
    });
    }
    });
    
    
    $(document).on('change','.search-type',function(){
    if(parseInt(this.value)==1){
      $('#tmdb_btn').show();
       $('input#default_img').val('');
        $('img#default_img').attr('src','{{url("images/default.png")}}');
         @if(!$model->id)
        $('.actors-list, .directors-list, .writers-list, .genre-list').hide();
           @endif
    }else{
      $('#duration,#description,#poster,input#default_img').val('');  
      $('#description').text('');
      $('.starRating input').prop('checked',false);
      $('#tmdb_btn').hide();
       $('img#default_img').attr('src','{{url("images/default.png")}}');
      $('.actors-list, .directors-list, .writers-list, .genre-list').show();
      $('#youtube_t_option').prop('checked',false);
      // $('#other_trailer_video').val('').removeAttr('required');
      $('#upload_t_option').prop('checked',true).change();
    
    }
    
    });
    @if($model->id)
    $('.actors-list, .directors-list, .writers-list, .genre-list').show();
    @endif
    
    $(document).on('click','.get-movie-data',function(){
    $(this).text('Wait...');
    var id=$(this).attr('id');
    
    $.ajax({
    type:'POST',
    url:'{{ url("admin/videos/get_movie")}}',
    data: {id:id},
    context: this,
    success: function(data){
       $(this).text('Select');
     
          var rating=parseInt(data.vote_average);
              rating=parseInt(rating> 0 ? (rating/2):0);
          $('#title').val(data.title);
          var time=parseInt(data.runtime);
          if(time>0){
           $('#duration').val(convertMinsToHrsMins(time));  
          }else{
              $('#duration').val('');  
          }
          if(data.rated){
          $('#age').val(data.rated);
          }else{
           $('#age').val('');   
          }  
    
          if(data.release_date){
          $('#release_date').val(data.release_date);
          }else{
           $('#release_date').val('');   
          }
     if(data.alternative_titles.titles.length){
          $('#alternative_titles').val(JSON.stringify(data.alternative_titles.titles));
          }else{
           $('#alternative_titles').val('');   
          }
    
           if(data.credits.cast.length > 0){
              var actors='';
              $.each(data.credits.cast,function(i,actor){
                  if(i==0) 
                  actors+=actor.name;
                   else 
                  actors+=','+actor.name;
              });
          $('#tmd-actors').val(actors);
          }else{
           $('#tmd-actors').val('');   
          }
          if(data.credits.crew.length > 0){
              var directors='',writers='';
                $.each(data.credits.crew,function(i,crew){
                  if(crew.department=='Directing'){
                  if(directors=='')
                   directors+=crew.name;
                   else
                   directors+=','+crew.name;
                  }
                  
                
                if(crew.department=='Writing'){
                  if(writers=='')
                   writers+=crew.name;
                 else
                   writers+=','+crew.name;
                }
              });
                $('#tmd-directors').val(directors);
                $('#tmd-writers').val(writers);
          }else{
                $('#tmd-directors,#tmd-writers').val('');
          }
              if(data.genres.length > 0){
              var genres='';
                $.each(data.genres,function(i,genre){
                  if(genres=='')
                   genres+=genre.name;
                   else
                   genres+=','+genre.name;
             
              });
                $('#tmd-genre').val(genres);
          }else{
                $('#tmd-genre').val('');
          }
    
          $('#youtube_t_option').prop('checked',true).change();
          // $('#other_trailer_video').val('').attr('required','required');
          if(data.videos.results.length > 0){
              var trailer_url = 'https://youtu.be/'+data.videos.results[0].key;
              $('#other_trailer_video').val(trailer_url);
    
          } else {
            $('#upload_t_option').prop('checked',true).change();
          }
        
          $('textarea#description').val(data.overview);
          
           CKEDITOR.instances.description.setData(data.overview);
          
          $('.starRating input[value="'+rating+'"]').attr('checked','checked');
          if(rating===0){
              $('.starRating input').prop('checked',false);
          }
          var poster_url="https://image.tmdb.org/t/p/w300"+data.poster_path;
          if(data.poster_path){
             $('#poster').val(poster_url);  
             $('img#default_img').attr('src',poster_url);  
          }else{
               $('img#default_img').attr('src','{{url("images/default.png")}}');
               $('#poster').val('');  
          }
           
    
          $('#data-modal').modal('hide');
          
      
      
      
    },
    error: function(){
      alert('Something wrong with request');
        $(this).text('Select');
    },
    
    });
    
    });
    function  convertMinsToHrsMins(minutes) {
    var h = Math.floor(minutes / 60);
    var m = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m+':00';
    }
     
      @if($model->id && !$model->status)
    
          checkPublishType("{{PUBLISH_LATER}}");
    
          $("#datepicker").val("{{$model->publish_time}}");
    
      @endif
    
      @if($model->id)
    
      videoUploadType("{{$model->video_type}}", 1);
      trailerVideoUpload("{{$model->video_type_trailer}}", 1);
      @endif
    
      function checkPublishType(val){
          $("#time_li").hide();
          //$("#datepicker").prop('required',false);
          $("#datepicker").val("");
          if(val == 2) {
              $("#time_li").show();
          // $("#datepicker").prop('required',true);
          }
      }
    
</script>
<script>
    $('form').submit(function () {
       window.onbeforeunload = null;
    });
    
    
    window.onbeforeunload = function() {
         return "Data will be lost if you leave the page, are you sure?";
    };
    
    loadGenre(genreId);
    
    window.setTimeout(function() {
    
        @if($model->genre_id) 
    
            $("#genre select").val("{{$model->genre_id}}");
    
        @endif
    
    }, 2000);
    
    
</script>
@endsection