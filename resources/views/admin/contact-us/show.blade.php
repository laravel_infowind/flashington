@extends('layouts.admin')

@section('title', tr('view_videos'))

@section('content-header')

Contact

@endsection

@section('breadcrumb')
<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
<li class="active"><i class="fa fa-video-camera"></i> Contacts</li>
@endsection

@section('content')
@include('notification.notify')

<div class="row">

	<div class="col-lg-12">

		<div class="panel">

			<div class="panel-body">

				<div class="post">

					<div class="user-block">

						<div class="pull-left">
							<span class="username" style="margin-left: 0px;">
								<a href="{{ url('/admin/contacts/'.$contact->id) }}">{{$contact->name}}</a>

							</span>
						</div>

						<div class="pull-right">
							{{ date('Y-m-d', strtotime($contact->created_at)) }}
						</div>
					</div>

					<hr>

					<div class="row margin-bottom">

						<div class="col-sm-12">

							<div class="row">

								<div class="col-sm-12">

									<div class="header">

										<h4><b>Name</b></h4>

										<p>{{$contact->name}}</p>

									</div>

								</div>

								<div class="col-sm-12">

									<div class="header">

										<h4><b>Email</b></h4>

										<p>{{$contact->email}}</p>

									</div>

								</div>

								<div class="col-sm-12">

									<div class="header">

										<h4><b>Subject</b></h4>

										<p>{{$contact->subject}}</p>

									</div>

								</div>

								<div class="col-sm-12">

									<div class="header">

										<h4><b>Message</b></h4>

										<p>{!! $contact->message !!}</p>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

@endsection