@extends('layouts.admin')

@section('title', tr('subject_list'))

@section('content-header')

<span style="color:#1d880c !important">{{$video}} </span> - {{tr('subject_list') }}
@endsection
@section('breadcrumb')

<li>
<a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a>
</li>

<li class="active"><i class="fa fa-star"></i> {{tr('subject_list')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">

            <div class="box-header label-primary">

                <b style="font-size:18px;">{{tr('subject_list')}}</b>
            </div>

            <div class="box-body">

                <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                    <div class="search-table-top">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>Show
                                        <form method="get" action="{{ url('admin/discussion/subjects') }}"
                                            class="videos-change-pagination">
                                            <input type="hidden" name="column" value="{{ $column }}">
                                            <input type="hidden" name="video_id" value="{{ $_GET['video_id'] }}">
                                            <input type="hidden" name="order_by"
                                                value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="{{ $rated_by_user_search }}">


                                            <select name="per_page" aria-controls="datatable"
                                                class="form-control input-sm" onchange="this.form.submit()">
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
                                                    value="10">10</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
                                                    value="25">25</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
                                                    value="50">50</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
                                                    value="100">100</option>
                                            </select>
                                        </form>
                                        entries</label>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <form action="{{ url('admin/discussion/subjects') }}" class="form">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search by Subject"
                                            class="form-control input-video-search" name="search"
                                            value="{{session('rated_by_user_search')}}">
                                            <input type="hidden" name="video_id" value="{{$_GET['video_id']}}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <thead>
                        <tr>
                            <th>
                                <a href="{{ url('admin/discussion/subjects?video_id='.$_GET['video_id'].'&column=forum_topics.id&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">Subject
                                    <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th><a href="{{ url('admin/discussion/subjects?video_id='.$_GET['video_id'].'&column=forum_category.name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">Subject Message
                                <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th><a href="{{ url('admin/discussion/subjects?video_id='.$_GET['video_id'].'&column=forum_category.name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">Category
                                <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th>
                                <a href="{{ url('admin/discussion/subjects?video_id='.$_GET['video_id'].'&column=forum_topics.	is_closed&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">Status
                                    <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th>Reply
                            </th>
                            
                        </tr>
                    </thead>

                    <tbody class="comment-table response-links">
                        @foreach ($model as $value)
                        <tr>    
                            <td><a href="{{ url('admin/discussion/subject/posts?forum_topic_id='.$value->id) }}">{{ $value->subject }}</a>
                           
                                            <ul class="table-row-actions">     								 
                                                    <li role="presentation">

                                                    @if(Setting::get('admin_delete_control'))
                                                        <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                                    @else
                                                        <a role="menuitem" tabindex="-1" href="{{url('admin/edit/discussion/subject/'.$value->id)}}">{{tr('edit')}}</a>
                                                    @endif
                                                    </li>												
							                  	
                                                    @if(!$value->is_closed)
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('topic_close_confirmation')}}&quot;);" href="{{route('admin.discussion.subject.open_close' , array('id' => $value->id , 'status' =>1))}}">{{tr('close')}}</a></li>
                                                    @else
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('topic_open_confirmation')}}&quot;);" href="{{route('admin.discussion.subject.open_close' , array('id' => $value->id , 'status' =>0))}}">{{tr('open')}}</a></li>
                                                    @endif

							                  	<li role="presentation">

							                  		@if(Setting::get('admin_delete_control'))

								                  	 	<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>

								                  	 @else
                                                       <a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('delete_topic_confirm')}}&quot;);" href="{{url('admin/discussion/subject/delete/'.$value->id)}}">{{tr('delete')}}</a>
							                  		@endif

							                  	</li>
                                                  <li role="presentation">
                                                  @if(Setting::get('admin_delete_control'))
                                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                                                @else
                                                    <a role="menuitem" tabindex="-1"  href="{{url('admin/discussion/subject/add?video_id='.$_GET['video_id'])}}">Add Subject</a>
                                                @endif
                                                </li>
	            							</ul>

                            
                            </td>
                            
                            <td>
                             {{ strip_tags(App\Helpers\Helper::getTopicMessage($value->id)) }}
                            </td>
                            <td>
                             {{ $value->category }}
                            </td>
                            <td>
                            
                            @if($value->is_closed==0)
                            <sapn>{{'Open'}}</span>
                            @else
                            <sapn class="cross_mark">{{'Closed'}} </span>
                            @endif
                            </td>
                            <td>
                              {{ App\Helpers\Helper::getReplyCount($value->id) }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="pagination-video-list">

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">
                                Showing {{ ($model->currentpage()-1)*$model->perpage()+1 }} to
                                {{ ($model->currentpage()*$model->perpage() < $model->total() ) ? $model->currentpage()*$model->perpage() : $model->total() }}
                                of {{ $model->total() }} entries
                            </div>
                        </div>
                        <div class="col-sm-7 text-right">
                            <div>{{ $model->links() }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection