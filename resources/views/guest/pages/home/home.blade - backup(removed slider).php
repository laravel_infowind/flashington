@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')

<section id="slider" class="full-slider">
    <!-- Main Slider Code -->
    <div class="owl-carousel owl-theme">
        @foreach($videos as $video)
        <div class="item">
            <div class="tab-content">
                <!-- Start  OverView -->
                <div id="overview_{{ $video->video_id }}" class="overview_tab_content tab-pane active">
                    <div class="overview_backimage"
                        style="background-image: url({{isset($video->default_image) ? $video->default_image : ''}});">
                    </div>
                    <div class="linear"></div>
                    <a id="slider_playiconid" href="{{ $video->video_id }}"
                        class="play-icon epi-play-icon overview_playicon"><i class="fa fa-play"></i>
                    </a>
                    <div class="overview_datadiv">
                        <h1 class="overview_title">{{ $video->title }}</h1>
                        <div class="pull-left">
                            <div class="stars">
                                <div class="rating">
                                    @if($video->ratings >= 1)
                                    <i class="fa fa-star" style="color: #e1cb2b; font-weight: 700;font-size: 15px;"></i>
                                    @else
                                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;font-size: 15px;"></i>
                                    @endif
                                    @if($video->ratings >= 2)
                                    <i class="fa fa-star" style="color: #e1cb2b; font-weight: 700;font-size: 15px;"></i>
                                    @else
                                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;font-size: 15px;"></i>
                                    @endif
                                    @if($video->ratings >= 3)
                                    <i class="fa fa-star" style="color: #e1cb2b; font-weight: 700;font-size: 15px;"></i>
                                    @else
                                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;font-size: 15px;"></i>
                                    @endif
                                    @if($video->ratings >= 4)
                                    <i class="fa fa-star" style="color: #e1cb2b; font-weight: 700;font-size: 15px;"></i>
                                    @else
                                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;font-size: 15px;"></i>
                                    @endif
                                    @if($video->ratings >= 5)
                                    <i class="fa fa-star" style="color: #e1cb2b; font-weight: 700;font-size: 15px;"></i>
                                    @else
                                    <i class="fa fa-star" style="color: #dddddd; font-weight: 100;font-size: 15px;"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <p style="width: 500px;">
                            <span class="release_datespan">{{$video->release_date}}</span>
                            <span class="epi-count">18+ / <i class="fa fa-eye"></i>{{ $video->watch_count }}</span>
                            <span class="video_duration">{{$video->duration}}</span>
                        </p>
                        <div class="pull-left">
                            <a class="white-icon">
                                <i class="fa fa-thumbs-up"></i>
                                <span style="color: white;" id="like_count_354_989">1</span>
                            </a>
                            <a class="white-icon">
                                <i class="fa fa-thumbs-down"></i>
                                <span style="color: white;" id="dis_like_count_354_989">0</span></a>
                        </div><br>
                        <div>
                            <?php 
            //$video_description = (strlen($video_description) > 380) ? substr(strip_tags($video_description), 0, 380). '......' . '</p><a href="'. url('movies/'.$video->unique_id) .'">More</a>' :  $video_description;

            $video_description = App\Helpers\Helper::truncate($video->description, 380, '...<a href="'. url('movies/'.$video->unique_id) .'">More</a>', true, true);
            ?>
                            <div class="overview_description">{!! $video_description !!}</div>
                        </div>

                        <div id="my-list-txt354_989" class="pull-left" style="width: 500px;">
                            <a id="play_videobutton" class="bold" style="cursor: pointer;">
                                <button class="my-list">
                                    <i style="margin-right: 10px;" class="fa fa-play my-list-icon"></i>
                                    <span class="my-list-txt">Play</span>
                                </button>
                                <button class="my-list">
                                    <i style="margin-right: 10px;" class="fa fa-plus my-list-icon"></i>
                                    <span class="my-list-txt">My List</span>
                                </button>
                            </a>
                            <a id="like_354_989" style="cursor: pointer;">
                                <button class="banner-icon">
                                    <i class="fa fa-thumbs-up" style="color: #fff;"></i>
                                </button>
                            </a>
                            <a data-toggle="modal" data-target="#myModal" title="Report as Spam"
                                style="cursor: pointer;">
                                <button class="banner-icon"><i style="color: #fff;" class="fa fa-exclamation"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End  OverView -->
                <!-- Start Trailers And More -->
                <div id="trailersmore_{{ $video->video_id }}" class="trailersmore_tab_content tab-pane fade">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-12 trailersmore_div"
                            style="background-image: url({{isset($video->default_image) ? $video->default_image : ''}});">
                            <div class="trailersmore_insidediv">
                                <div style="padding: 75px 65px;">
                                    <h3 class="trailer_subcagory">{{ $video->sub_category_name }}</h3>
                                    <div class="trailers-list row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 top"
                                            style="margin-top: 15px !important;">
                                            <div class="episode-list-box">
                                                <div class="episode-img bg-img"
                                                    style="background-image: url({{isset($video->default_image) ? $video->default_image : ''}});">
                                                    <div class="episode-img-overlay">
                                                        <div class="epi-play-icon-outer">
                                                            <a href="{{ $video->video_id }}" class="epi-play-icon">
                                                                <i class="fa fa-play"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="episode-list-content">
                                                    <div data-v-fae5bece="" class="row no-margin" style="margin: 0;">
                                                        <div>
                                                            <h4 class="epi-tit gray-color1">{{ $video->title }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Trailers And More -->
                <!-- Start MoreLike -->
                <div id="morelike_{{ $video->video_id }}" class="morelike_tab_content tab-pane fade">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 morelike_tabdiv"
                            style="background-image: url({{isset($video->default_image) ? $video->default_image : ''}});">
                            <div class="morelike_insidediv">
                                <div style="padding: 75px 65px;">
                                    <h3 class="morelike_subcategory">{{ $video->sub_category_name }}</h3>
                                    <div class="morelike-list row">
                                        @foreach($admin_video_more as $admin_video_morelike)
                                        <div class="col-12 col-lg-3 col-md-3 col-sm-3 top"
                                            style="margin-top: 15px !important;">
                                            <div class="episode-list-box">
                                                <div id="{{$admin_video_morelike->id}}" class="episode-img bg-img"
                                                    style="background-image: url({{isset($admin_video_morelike->default_image) ? $admin_video_morelike->default_image : ''}});">
                                                    <div class="episode-img-overlay">
                                                        <div class="episode-img-inner">
                                                            <p class="new-epi">{{ $admin_video_morelike->title }}</p>
                                                        </div>
                                                        <div class="epi-play-icon-outer">
                                                            <span>
                                                                <a id="{{$admin_video_morelike->id}}" href="#/video/152"
                                                                    class="epi-play-icon video_morelikethis">
                                                                    <i class="fa fa-play"></i></a>
                                                            </span>
                                                        </div>
                                                        <div class="duration_more">
                                                            <span
                                                                style="color: #fff;">{{ $admin_video_morelike->duration }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row no-margin" style="margin: 0;">
                                                        <div class="pull-left">
                                                            <div class="stars">
                                                                <div class="rating">
                                                                    @if($admin_video_morelike->ratings >= 1)
                                                                    <i class="fa fa-star"
                                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                                    @else
                                                                    <i class="fa fa-star"
                                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                                    @endif
                                                                    @if($admin_video_morelike->ratings >= 2)
                                                                    <i class="fa fa-star"
                                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                                    @else
                                                                    <i class="fa fa-star"
                                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                                    @endif
                                                                    @if($admin_video_morelike->ratings >= 3)
                                                                    <i class="fa fa-star"
                                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                                    @else
                                                                    <i class="fa fa-star"
                                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                                    @endif
                                                                    @if($admin_video_morelike->ratings >= 4)
                                                                    <i class="fa fa-star"
                                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                                    @else
                                                                    <i class="fa fa-star"
                                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                                    @endif
                                                                    @if($admin_video_morelike->ratings >= 5)
                                                                    <i class="fa fa-star"
                                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                                    @else
                                                                    <i class="fa fa-star"
                                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p class="content-txt bold seasons">
                                                            <span
                                                                class="green-clr hidden-sm">{{ $admin_video_morelike->release_date }}</span>
                                                            <span class="epi-count hidden-sm hidden-md">18+ / <i
                                                                    class="fa fa-eye"></i>{{ $admin_video_morelike->watch_count }}</span>
                                                        </p>
                                                    </div>
                                                    <div class="morelike_para">
                                                        <p class="content-txt overview-des more-link">
                                                            {{ $admin_video_morelike->description }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End MoreLike -->
                <!-- Start Details -->
                <div id="details_{{ $video->video_id }}" class="details_tab_content tab-pane fade">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 details_tabdiv"
                            style="background-image: url({{isset($video->default_image) ? $video->default_image : ''}});">
                            <div class="details_insidetabdiv">
                                <div class="details_tabset">
                                    <h1 class="details_title">{{ $video->title }}</h1>
                                    <div class="row col-sm-12 col-sm-12 col-lg-12 col-md-12">
                                        <div class="detailsItem detailsMedia col-md-11">
                                            <h4>Details</h4>
                                            <p class="details_para">{!! $video->details !!}</p>
                                        </div>
                                        <div class="detailsItem detailsMedia col-md-2">
                                            <h4>Directors</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    @if($data['directors'] != null)
                                                    @foreach($data['directors'] as $director)
                                                    <a
                                                        href="{{ url('search/?director='.$director->name) }}">{{ $director->name.',' }}</a>
                                                    @endforeach
                                                    @else
                                                    N/A
                                                    @endif
                                                </li>
                                            </ul>
                                            <h4>Writers</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    @if($data['writers'] != null)
                                                    @foreach($data['writers'] as $writer)
                                                    <a href="{{ url('search/?writer='.$writer->name) }}">{{ $writer->name.',' }}
                                                    </a>
                                                    @endforeach
                                                    @else
                                                    N/A
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="detailsItem detailsMedia col-md-2">
                                            <h4>Actors</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    @if($data['actors'] != null)
                                                    @foreach($data['actors'] as $key => $actor)
                                                        @if ($key <= 15)
                                                        <a href="{{ url('search/?actor='.$actor->name) }}">{{ $actor->name.',' }}</a>
                                                        @endif
                                                    @endforeach
                                                    @else
                                                    N/A
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="detailsItem detailsMedia col-md-2">
                                            <h4>Category</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    <a
                                                        href="{{ url("search?sub-category=".$video->sub_category_name) }}">{{ $video->sub_category_name }}</a>
                                                </li>
                                            </ul>
                                            <div class="clear-fix"></div>
                                            <h4>Publish Date</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    <p class="content-txt bold seasons">
                                                        <span
                                                            class="green-clr hidden-sm">{{$video->release_date}}</span>
                                                    </p>
                                                </li>
                                            </ul>
                                            <h4>Duration</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    <p class="content-txt bold seasons">
                                                        <span class="duration_span">{{$video->duration}}</span>
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="detailsItem detailsMedia col-md-2">
                                            <h4>Ratings</h4>
                                            <div class="stars">
                                                <div class="rating">
                                                    @if($video->ratings >= 1)
                                                    <i class="fa fa-star"
                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                    @else
                                                    <i class="fa fa-star"
                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                    @endif
                                                    @if($video->ratings >= 2)
                                                    <i class="fa fa-star"
                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                    @else
                                                    <i class="fa fa-star"
                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                    @endif
                                                    @if($video->ratings >= 3)
                                                    <i class="fa fa-star"
                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                    @else
                                                    <i class="fa fa-star"
                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                    @endif
                                                    @if($video->ratings >= 4)
                                                    <i class="fa fa-star"
                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                    @else
                                                    <i class="fa fa-star"
                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                    @endif
                                                    @if($video->ratings >= 5)
                                                    <i class="fa fa-star"
                                                        style="color: #e1cb2b; font-weight: 900;font-size: 12px;"></i>
                                                    @else
                                                    <i class="fa fa-star"
                                                        style="color: #dddddd; font-weight: 100;font-size: 12px;"></i>
                                                    @endif
                                                </div>
                                            </div>
                                            <h4>Age</h4>
                                            <ul class="list-unstyled">
                                                <li class="listLabel">
                                                    <p class="content-txt bold seasons"> Recommended for ages 18+ and up
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Details -->
            </div>
            <div class="otmd_div">
                <!--  Tab OverView Trailers More Like And Details -->
                <div class="otmd">
                    <ul class="nav nav-tabs">
                        <li class="active">
                        <a id="overview_tab_{{ $video->video_id }}" class="overview_tab" data-toggle="tab" href="#overview_{{ $video->video_id }}"
                                style="border-bottom: 3px solid #E50914;">OVERVIEW</a>
                        </li>
                        @if($video->trailer_video)
                        <li>
                            <a id="trailersmore_tab_{{ $video->video_id }}" class="trailersmore_tab" data-toggle="tab" href="#trailersmore_{{ $video->video_id }}">TRAILERS & MORE</a>
                        </li>
                        @endif
                        <li>
                        <a id="morelike_tab_{{ $video->video_id }}" class="morelike_tab" data-toggle="tab" href="#morelike_{{ $video->video_id }}">MORE LIKE THIS</a>
                        </li>
                        <li>
                        <a id="details_tab_{{ $video->video_id }}" class="details_tab" data-toggle="tab" href="#details_{{ $video->video_id }}">DETAILS</a>
                        </li>
                    </ul>
                </div>
                <!-- Add Recently Added -->
                <div class="container container-main">
                    <div class="row">
                        <div class="col col-md-9 col-sm-6">
                            <h3 class="recent_title title">Recently Added</h3>
                        </div>
                        <div class="col col-md-3 col-sm-6 align-self-center text-right">
                            <a href="{{ url('recently-added') }}" class="btn btn-icon btn-main btn-effect">View All
                                <i class="ti-angle-double-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </div>

    <div class="latest-movies">
        <div class="container container-main">
            <div class="row">
                @foreach($videos_data as $data)
                <?php 
            //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
            $movie_description = App\Helpers\Helper::truncate($data['details'], 400, '...', true, true);
            $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
            ?>
                <div hover-id="{{ $data->unique_id }}" class="col-lg-2 col-md-3 col-sm-4 col-6 poster_div">
                    <a href="{{ url('movies/'.$data['unique_id'])  }}">
                        <div class="movie-box-1">
                            <div id="{{ $data->unique_id }}" class="poster">
                                <div class="movie_item">
                                    <img id="{{ $data->unique_id }}" class="poster_image"
                                        src="{{ $data['default_image'] }}"
                                        alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button"
                                        data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                        data-title="
                                <span class='popover-movie-title'>{{ $data['title'] }}</span>
                                <div class='movie-info-row'>
                                    <span class='movie-year'>{{ $data['release_date'] }}</span>
                                    <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                    <span class='movie-stars'>
                                    @if($data['ratings'] >= 1)
                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        @else
                                        <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                        @endif
                                        @if($data['ratings'] >= 2)
                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        @else
                                        <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                        @endif
                                        @if($data['ratings'] >= 3)
                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        @else
                                        <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                        @endif
                                        @if($data['ratings'] >= 4)
                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        @else
                                        <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                        @endif
                                        @if($data['ratings'] >= 5)
                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                        @else
                                        <i class='fa fa-star' style='color: #dddddd; font-weight: 100;'></i>
                                        @endif
                                    </span>
                                </div>" data-content="
                                <div class='movie-description'>
                                    <div>{!! $movie_description !!}</div>
                                </div>" data-container="body" data-html="true" data-original-title="" title="" />
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Recently Added -->
</section>



<ul id="video_categories">
    <li>{{ $sub_categories->id }}</li>
</ul>

<!-- <div class="ajax-loading">
   <span><i class="fa fa-circle clr_red"></i></span>
   <span><i class="fa fa-circle clr_white"></i></span>
   <span><i class="fa fa-circle clr_red"></i></span>
</div> -->
@endsection