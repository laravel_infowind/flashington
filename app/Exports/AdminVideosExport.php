<?php

namespace App\Exports;

use App\AdminVideo;
use Maatwebsite\Excel\Concerns\FromCollection;

class AdminVideosExport implements FromCollection
{
    public function collection()
    {
        return AdminVideo::all();
    }
}