@if(!empty($comments))
@foreach($comments as $value)
    <tr class="comment-{{ $value->id }}">
        <td class="comment-first">Admin</td>
        <td class="comment-subject">{{ $value->subject }}
        <div class="row-actions">
                <span class="status-{{ $value->id }}">
                    @if($value->status == 0)
                    <a class="menuitem comment-status" tabindex="-1" href="javascript:void(0)" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('ratings_approve_confirmation')}}&quot)">{{tr('approve')}} </a>
                    @else
                    <a class="menuitem comment-status" href="javascript:void(0)" tabindex="-1" url="{{route('admin.ratings.status',['id'=>$value->id])}}" onclick="return confirm(&quot;{{tr('decline_ratings')}}&quot)">{{tr('decline')}}</a>
                    @endif
                </span>
                |
                <span>
                    @if(Setting::get('admin_delete_control'))
                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                    @else
                    <a role="menuitem" tabindex="-1" href="{{route('admin.ratings.edit' , array('id' => $value->id))}}">{{tr('edit')}}</a>
                    @endif
                </span>
                |
                <span class="trash">
                    @if(Setting::get('admin_delete_control'))
                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                    @else
                    <a class="comment-del" role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('rating_delete_confirmation')}}&quot;);" href="javascript:void(0)" url="{{route('admin.ratings.delete' , array('id' => $value->id))}}">{{tr('delete')}}</a>
                    @endif
                </span>
            </div>
        </td>
        <td class="comment-rating">{{ $value->rating }}/10</td>
        <td class="comment-second">
        @php
            $review = (strlen($value->comment) > 50) ? substr($value->comment, 0, 50).'...' : $value->comment;
        @endphp
            {!! $review !!}
        </td>
    </tr>
@endforeach
@endif
    