@extends('layouts.admin')

@section('title', tr('add_rating'))

@section('content-header', tr('add_rating'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.ratings.index')}}"><i class="fa fa-users"></i> {{tr('ratings')}}</a></li>
    <li class="active">{{tr('add_rating')}}</li>
@endsection


@section('content')

@include('notification.notify')

@include('admin.ratings._form')

@endsection

@section('scripts')
    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

    </script>
@endsection
